Tarefas SAC 2 meses do RoadMap - Laercio Vacca - (22/01/2019)
 -------------------------------------------------------
 01 - Preparação do ambiente de desenvolvimento do front.
    - Preparação de estrutra de pastas.
    - Analizar o Impacto da troca da estrutura de pastas nos Paths existentes
    - SAC
 	  - Dasboard 
 		  - Components
 		    - MemoFile
	          - Html
	          - JS
 		    - Header
 		      - JS
 		      - Html
 		    - Footer
 		      - JS
 		      - HTML     
      index.html		
 	  - Resources
 	    - CSS
 	    - IMG	

http://localhost:3000/paginas/portal/dashboard/responsive/page.html
http://localhost:3000/dashboard/components/index.html
Antes   | Depois
----------------------
paginas | dashboard
portal  | components
        |

 ------------------------------------------------
 02 - Adequação do PSFSecurity para receber o SAC
 03 - Criação do menu responsavel por direcionar os usuarios do PSFSecurity, para o Dashboard do SAC.
 -------------------------------------------------
 04 - Limpeza dos arquivos e pastas que não estaão sendo utilizadas no novo SAC
 04 - Refatoração do page.js (Separação de funcionalidades, para melhor rendimento do desenvolvimento)
    A - Separação de Funcionalidades por arquivos.
    B - Separação de Funcionalidades de criação de blocos de HTML.(Menu, cards, etc....)
---------------------------------------------------
 05 - Algoritimo de Criação das Hash para Mapeamento
    A - Hash Atendimento/Processo
    B - Hash Chamada Servico
---------------------------------------------------
 06 - Identificação Positiva
 	A - Logica da entrada por Busca ou Vindo da URA
    B - Script das perguntas e respostas (Reload de outras perguntas quando clica em Reavaliar)
    C - Histórico - Consulta de chamados  
    D - Ação de avançar usando O mesmo Chamado ou um chamado Diferente - (Caso Chamado novo usar Servico)
    E - Chamada e Listagem do historico dos Chamados
    F - Redirecionamento da Identificação Positiva para o Dashboard
 ----------------------------------------------------------
 07 - Dashboard
 	A - Script de Consulta de Cartão
 	B - Script de Consulta de conta
 	C - Logica de Acesso Altitude
 	D - Atalho Procedimento
 ----------------------------------------------------------
 08 - Lançamentos Manuais Atendimento
    A - Criação do Script que Guarda os Lançamentos Manuais no Local Storage
    B - Script que junta todos os Lançamentos feitos e manda para finalização de Chamado
 -----------------------------------------------------------
 09 - Encerramento Atendimento
    A - Desenvolvimento dos envios de protocolo
    B - Desenvolvimento Observações Finais
    C - Listagem dos Lançamentos do Atendimento
    D - Encerramento
--------------------------------------------------------------------------------

Logica de inicio de chamadas e start do SAC
 - Start_app.js - Load inicial da Aplicação 
 - monta_estrutura.js - Responsavel por montar o esqueleto do dashboard do SAC
  