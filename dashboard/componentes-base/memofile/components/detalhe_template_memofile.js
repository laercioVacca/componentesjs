function DetalheTemplateMemofile() {
	this.templateHeader = function(){
		var _html = '<div id="header_faturas" class="grupo_faturas">';
		_html += '<div class="item_fatura" style="width: 20%">Data</div>';
		_html += '<div class="item_fatura" style="width: 70%; text-align: left; padding-left: 25px">Observação</div>';
		_html += '</div>';
		return _html;
	}
	this.itemMemofile = function(identificador){
		var retornoHtml;
		retornoHtml = '<div class="card">';
		retornoHtml += '<div class="card-header card_acordeon">';
		retornoHtml += '<a class="btn_drop_memo" href="#" data-target="#'+identificador+'" aria-expanded="true" aria-controls="'+identificador+'">';
		retornoHtml += '<div class="area_click" id="headingOne" style="width:100%; padding-left:10px ">';
		retornoHtml += '<div class="mb-0 titulo_fatura" style="width:20%;">28/04/2018 07:23:44</div>';
		retornoHtml += '<div class="mb-0 titulo_fatura" style="width:70%;text-align: left; padding-left: 25px">Enviado EMAIL com o Link da Fatura...</div>';
		retornoHtml += '</div>';
		retornoHtml += '</a>';
		retornoHtml += '</div>';
		retornoHtml += '<div id="'+identificador+'" class="collapse " aria-labelledby="headingOne2" data-parent="#accordion">';    
		retornoHtml += '<div class="card-body">';
		retornoHtml += '<div class="lista_detalhe_item">';
		//<!-- Detalhes Grupo 02 Dados Memo file -->
		retornoHtml += '<div class="list-blocos-group lista_grid_02 full_grid">'; 
		retornoHtml += '<div class="alt_din list-group-item list-group-item-primary titulo_divisor" >';
		retornoHtml += '<span class="item_lista_titulo">Dados Memo File</span>';
		retornoHtml += '<div class="lista_grupo_clear"></div>';
		retornoHtml += '</div>';
		retornoHtml += '</div>';
		retornoHtml += '<div class="list-blocos-group lista_grid_02 full_grid">'; 
		retornoHtml += '<div class="alt_din list-group-item list-group-item-primary" >';
		retornoHtml += '<span class="item_lista_titulo ">Data Ocorrência</span>';
		retornoHtml += '<span class="item_lista_valor ">03/12/2014</span>';
		retornoHtml += '<div class="lista_grupo_clear"></div>';
		retornoHtml += '</div>';
		retornoHtml += '</div>';
		retornoHtml += '<div class="list-blocos-group lista_grid_02 full_grid">'; 
		retornoHtml += '<div class="alt_din list-group-item list-group-item-primary" >';
		retornoHtml += '<span class="item_lista_titulo ">Hora</span>';
		retornoHtml += '<span class="item_lista_valor ">16:09:17</span>';
		retornoHtml += '<div class="lista_grupo_clear"></div>';
		retornoHtml += '</div>';
		retornoHtml += '</div>';
		retornoHtml += '<div class="list-blocos-group lista_grid_02 full_grid">'; 
		retornoHtml += '<div class="alt_din list-group-item list-group-item-primary" >';
		retornoHtml += '<span class="item_lista_titulo ">Módulo</span>';
		retornoHtml += '<span class="item_lista_valor ">Frente de Loja</span>';
		retornoHtml += '<div class="lista_grupo_clear"></div>';
		retornoHtml += '</div>';
		retornoHtml += '</div>';
		retornoHtml += '<div class="list-blocos-group lista_grid_02 full_grid"> ';
		retornoHtml += '<div class="alt_din list-group-item list-group-item-primary" >';
		retornoHtml += '<span class="item_lista_titulo ">Usuário</span>';
		retornoHtml += '<span class="item_lista_valor ">CCM - CCM Administrador</span>';
		retornoHtml += '<div class="lista_grupo_clear"></div>';
		retornoHtml += '</div>';
		retornoHtml += '</div>';
		retornoHtml += '<div class="list-blocos-group lista_grid_02 full_grid">'; 
		retornoHtml += '<div class="alt_din list-group-item list-group-item-primary">';
		retornoHtml += '<span class="item_lista_titulo ">Filial Usuário</span>';
		retornoHtml += '<span class="item_lista_valor ">900 - L900 SO/SP</span>';
		retornoHtml += '<div class="lista_grupo_clear"></div>';
		retornoHtml += '</div>';
		retornoHtml += '</div>';
		// <!-- Detalhes Grupo 03 Observação -->
		retornoHtml += '<div class="list-blocos-group lista_grid_02 full_grid">'; 
		retornoHtml += '<div class="alt_din list-group-item list-group-item-primary titulo_divisor" >';
		retornoHtml += '<span class="item_lista_titulo">Observação</span>';
		retornoHtml += '<div class="lista_grupo_clear"></div>';
		retornoHtml += '</div>';
		retornoHtml += '</div>';
		retornoHtml += '<div class="list-blocos-group lista_grid_02 full_grid">'; 
		retornoHtml += '<div class="alt_din list-group-item list-group-item-primary titulo_divisor" >';
		retornoHtml += '<span class="item_lista_titulo">';
		retornoHtml += '<textarea class="form-control" id="exampleTextarea" rows="3"></textarea>';
		retornoHtml += '</span>';
		retornoHtml += '<div class="lista_grupo_clear"></div>';
		retornoHtml += '</div>';
		retornoHtml += '</div>';
		retornoHtml += '</div>';
		retornoHtml += '</div>';
		retornoHtml += '</div>';
		retornoHtml += '</div>';
		return retornoHtml;
	};

}
var detalheTemplateMemofile = new DetalheTemplateMemofile();