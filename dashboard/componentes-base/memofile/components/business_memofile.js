require("./memofile/components/detalhe_template_memofile.js");

function BusinessMemofile() {
	this.load = function(_containerCard,_headerCard){
		this.containerCard = _containerCard;
		$(_headerCard).html(detalheTemplateMemofile.templateHeader());
		this.listaItem();
		
	};
	this.listaItem = function(){
		for (var i = 0; i < 6; i++) {
			var identificador = 'ident_'+i;
			$(this.containerCard).append(detalheTemplateMemofile.itemMemofile(identificador));
		}
		this.eventosMemofile();
	};
	this.eventosMemofile = function(){
		$(this.containerCard).unbind("click").on('click', '.btn_drop_memo', function(event) {
			event.preventDefault();
			var conteudoDrop = $(this).attr('data-target');
			$('.collapse').hide();
			$(conteudoDrop).show();
		});
	};
	
}

var businessMemofile = new BusinessMemofile();