
require("components/dadosCliente/components/template_dadosCliente.js");
function BusinessDadosCliente() {
	this.load = function(infosCurrentCli,elDadosCliente,panelNavegacaoRapida){
		this.currentCliente = infosCurrentCli;
		this.elDadosCliente = elDadosCliente;
		this.panelNavegacaoRapida = panelNavegacaoRapida;
		$(this.elDadosCliente).append(templateDadosCliente.infos(this.currentCliente));
		$(this.panelNavegacaoRapida).append(templateDadosCliente.navegacaoRapida());
		this.eventosComponent();		
	};
	this.eventosComponent = function(){
		var panelNavegacaoRapida = $('#panelNavegacaoRapida');
		$(this.elDadosCliente).on('click', '#ativaNavegacaoRapida', function(event) {
			event.preventDefault();
			if($(panelNavegacaoRapida).hasClass('navegacao_on')){
                $(panelNavegacaoRapida).animate({
                    height: 0,
                    opacity: 0
                },200);
                $('.uniao').animate({opacity: 0},200);
                $(panelNavegacaoRapida).removeClass('navegacao_on');
            }else{
                $('.uniao').animate({opacity: 1},200);
                $(panelNavegacaoRapida).animate({
                    height: 50,
                    opacity: 1
                },200);
                $(panelNavegacaoRapida).addClass('navegacao_on');
            }
		});
	    var alturaDinamica = {
	        start:function(){
	            this.aumentarAltura();
	        },
	        aumentarAltura:function(){
	            var larg = $(window).width();
	            $('#verMaisInfos').on('click', function(event) {
	                event.preventDefault();
	                  if($(this).hasClass('maisInfos')) {
	                    console.log('LARGURA TELA',larg)
	                    
	                    if(larg <=1024){
	                        $('.boxAlturaDinamicaDashboard').animate({height:230},200);
	                    }else{
	                        $('.boxAlturaDinamicaDashboard').animate({height:180},200);
	                    }
	                    $('.perguntasRespostas').fadeIn( "slow" );
	                    $('.showContent').fadeIn("slow");
	                    $(this).removeClass('maisInfos');
	                    $(this).addClass('menosInfos');
	                    $(this).html('<span class="fs1" aria-hidden="true" data-icon="&#xe101;"></span>');
	                  } else { 
	                    $('.boxAlturaDinamicaDashboard').animate({height:100},200);
	                    $('.perguntasRespostas').fadeOut( "fast" );
	                    $('.showContent').fadeOut( "fast" );
	                    $(this).addClass('maisInfos');
	                    $(this).removeClass('menosInfos');
	                    $(this).html('<span class="fs1" aria-hidden="true" data-icon="&#xe102;"></span>');
	                  }
	                
	            });
	        }
	    }
	    setTimeout(function(){
	        alturaDinamica.start();
	    },300)

	   // 
	   
	    $('#ListaIdentificacaoPositiva').on('click', '#btnAtivarMenuMobile', function(e) {
	    	e.preventDefault();
	    	$(this).toggleClass('showMenuMobile');
	    	// $(this).find('.fs1').attr('data-icon', '');
		    if($(this).hasClass('showMenuMobile')) {
		    	$(this).find('.fs1').attr('data-icon', '');
		     	$('#sidebar.mr_controleNavegacao').addClass('menuMobileAtivado')
		    }else {
		    	$(this).find('.fs1').attr('data-icon', '');
		    	$('#sidebar.mr_controleNavegacao').removeClass('menuMobileAtivado')	
		    }
		});
	    
	};
	
}

var businessDadosCliente = new BusinessDadosCliente();