function TemplateDadosCliente() {
	this.infos = function(currentCliente){
		this.currentCliente = currentCliente;
		var htmlTmp;
		htmlTmp = '<div id="dadosCliente" class="mr_dadosClienteDash page-title clearfix row">'; 
		htmlTmp += '<ul class="stats col-xs-12 col-sm-12 col-md-12 col-lg-12">';
		
		htmlTmp += this.infosCliente();
		htmlTmp += this.cartaoCliente();
		htmlTmp += this.ferramentas();

		htmlTmp += '</ul>'; 
		htmlTmp += '</div>';
		return htmlTmp ; 
	}
	// #########################################
	this.infosCliente = function(){
		var htmlTmp;
		htmlTmp  = '<li id="infosCliente" class="boxAlturaDinamicaDashboard marisa-pink-bg ">';
		htmlTmp += '<a class="pageCadastro" href="page_infos_cadastrais.html">';
		htmlTmp += '<span class="fs1" aria-hidden="true" data-icon="&#xe075;" ></span>'; 
		htmlTmp += '<div class="details">'; 
		htmlTmp += '<span class="big">'+this.currentCliente.nome+'</span>'; 
		htmlTmp += '<span id="cpfDadosCliente" class="small"><span class="labelCliente">CPF:</span>'+this.currentCliente.cpf+'</span> ';
		htmlTmp += '<span class="small"><span class="labelCliente">Conta:</span>'+this.currentCliente.situacaoConta+'</span>'; 
		htmlTmp += '<div class="showContent mr_showContentMobile">';
		htmlTmp += '<span class="small"><span class="labelCliente">Nome Mãe:</span>'+this.currentCliente.situacaoConta+'</span>';
		htmlTmp += '<span class="small"><span class="labelCliente">RG:</span>'+this.currentCliente.cliIdentidade+'</span>';
		htmlTmp += '<span class="small"><span class="labelCliente">e-mail:</span>'+this.currentCliente.email+'</span>';
		htmlTmp += '<span class="small"><span class="labelCliente">Data Nascimento / Idade:</span> '+this.currentCliente.dtNasc+' -  39 anos</span>';
		// htmlTmp += '<span class="small">&nbsp;</span>';
		htmlTmp += '</div>';
		htmlTmp += '</div>';
		htmlTmp += '</a>';
		htmlTmp += '</li>';
		return htmlTmp;
	}
	this.cartaoCliente = function(){
		var htmlTmp;
		htmlTmp  = '<li id="CartaoCliente" class="boxAlturaDinamicaDashboard marisa-blue-bg">'; 
		htmlTmp += '<span class="fs1" aria-hidden="true" data-icon="&#xe039;" ></span>'; 
		htmlTmp += '<div class="details"> ';
		htmlTmp += '<span class="big">'+this.currentCliente.numeroCartao+'</span> ';
		htmlTmp += '<span class="small"><span class="labelCliente">Cartão:</span>'+this.currentCliente.situacaoCartao+'</span> ';
		htmlTmp += '<div class="showContent mr_showContentMobile">';
		htmlTmp += '<span class="small"><span class="labelCliente">Data Cadastro:</span> '+this.currentCliente.situacaoCartao+' </span>';
		htmlTmp += '<span class="small"><span class="labelCliente">Data Última Alteração:</span> '+this.currentCliente.situacaoCartao+' </span>';
		htmlTmp += '<span class="small"><span class="labelCliente">Vencimento / Melhor Dia:</span> '+this.currentCliente.situacaoCartao+' </span>';
		htmlTmp += '<span class="small"><span class="labelCliente">Filial / Loja Origem:</span> '+this.currentCliente.situacaoCartao+'</span>';
		htmlTmp += '<span class="small"><span class="labelCliente">Filial / Loja Retirada:</span> '+this.currentCliente.situacaoCartao+'</span>';
		htmlTmp += '<span class="small">&nbsp;</span>';
		htmlTmp += '</div>';
		htmlTmp += '</div>';
		htmlTmp += '</li> ';
		return htmlTmp;
	}
	this.ferramentas = function(){
		var htmlTmp;
		htmlTmp = '<li id="ListaIdentificacaoPositiva" class="boxAlturaDinamicaDashboard sea-green-bg "> ';
		htmlTmp += '<span class="fs1" aria-hidden="true" data-icon="&#xe0f7;" ></span> ';
		htmlTmp += '<div class="details">';
		htmlTmp += '<div class="nav_top_ferramentas">';
		htmlTmp += '<span class="titulo big">';
		htmlTmp += 'Ferramentas';
		htmlTmp += '</span>';
		htmlTmp += '<a id="verMaisInfos" href="#" class="maisInfos btn_identificacao btn btn-warning">';
		htmlTmp += '<span class="fs1" aria-hidden="true" data-icon="&#xe102;"></span>';
		htmlTmp += '</a> ';
		htmlTmp += '</div> ';
		htmlTmp += '<div class="perguntasRespostas">';
		htmlTmp += '<span class="titulo_atendente">Atendente:</span>';
		htmlTmp += '<ul class="lista_perguntas_e_respostas">';
		htmlTmp += '<li class="item_perguntaResposta">';
		htmlTmp += '<span class="itens resposta">Nome:</span>';
		htmlTmp += '<span class="itens pergunta">Laercio Vacca Filho</span><br>';
		htmlTmp += '<span class="itens resposta">Matricula:</span>';
		htmlTmp += '<span class="itens pergunta">234.234</span><br>';
		htmlTmp += '<span class="itens resposta">Mensagens:</span>';
		htmlTmp += '<span class="itens pergunta">2</span>';
		htmlTmp += '</li>';
		htmlTmp += '</ul>';
		htmlTmp += '</div>';
		htmlTmp += '<div class="nav_acao_infos_cliente"> ';

		htmlTmp += '<ul class="sub_menu_ferramentas">';

		htmlTmp += '<li id="btnMenuMobile" class="item_menu">';
		htmlTmp += '<a id="btnAtivarMenuMobile" href="#" class="btn_identificacao btn btn-info">';
		htmlTmp += '<span class="fs1" aria-hidden="true" data-icon="&#xe102;">Menu</span>';
		htmlTmp += '</a>';
		htmlTmp += '</li>';

		htmlTmp += '<li class="item_menu">';
		htmlTmp += '<a id="ativaNavegacaoRapida" href="#" class="btn_identificacao btn btn-info">';
		htmlTmp += '<span class="fs1" aria-hidden="true" data-icon="&#xe08b;">Links Úteis</span>';
		htmlTmp += '</a>';
		htmlTmp += '</li>';

		htmlTmp += '<li id="btnProcedimentoMobile" class="item_menu">';
		htmlTmp += '<a id="linkProcedimentos" href="#" class="btn_identificacao btn btn-info">';
		htmlTmp += '<span class="fs1" aria-hidden="true" data-icon="&#xe0c3;">Procedimentos</span>';
		htmlTmp += '</a>';
		htmlTmp += '</li>';
		htmlTmp += '</ul>';

		htmlTmp += '</div>';
		htmlTmp += '<div class="uniao"></div>';
		htmlTmp += '</div> ';
		htmlTmp += '</li> ';
		return htmlTmp;
	}
	// #####
	this.navegacaoRapida = function(){
		var html;
		html  = '<div id="navegacao_rapida">';
		html += '<ul class="lista_menu">';
		html += '<li class="item">';
		html += '<a id="identificaReavaliar" href="http://128.1.40.192/CAisd/pdmweb.exe?OP=CREATE_NEW+FACTORY=in+KEEP.IsPopUp=1" target="_blank" class="btn_nav_rapida btn btn-info">';
		html += 'Chamado';
		html += '</a> ';
		html += '</li>';
		html += '<li class="item">';
		html += '<a id="identificaReavaliar" href="http://credi021/ReportServer/Pages/ReportViewer.aspx?%2fCredito%2fInformativo+de+Repasse+Icatu&amp;rs:Command=Render" class="btn_nav_rapida btn btn-info" target="_blank">';
		html += 'Icatu';
		html += '</a>';
		html += '</li>';
		html += '<li class="item">';
		html += '<a href="http://www.prodent.com.br/comper/pop_associados_contrato.aspx" class="btn_nav_rapida btn btn-info" target="_blank">';
		html += 'Marisa Odonto';
		html += '</a>';
		html += '</li>';
		html += '<li class="item">';
		html += '<a href="http://www.prodent.com.br" class="btn_nav_rapida btn btn-info" target="_blank">';
		html += 'Prodent';
		html += '</a>';
		html += '</li>';
		html += '<li class="item">';
		html += '<a href="http://www.receita.fazenda.gov.br/Aplicacoes/ATCTA/CPF/ConsultaPublica.asp" class="btn_nav_rapida btn btn-info" target="_blank">';
		html += 'Receita';
		html += '</a>';
		html += '</li>';
		html += '<li class="item">';
		html += '<a id="masterHideCard" class="btn_nav_rapida btn btn-info" href="#">';
		html += 'Hide Cards';
		html += '</a>';
		html += '</li>';
		html += '</ul>';
		html += '<div class="clear"></div>';
		html += '</div>';
		return html;
	}
	

}


var templateDadosCliente = new TemplateDadosCliente();