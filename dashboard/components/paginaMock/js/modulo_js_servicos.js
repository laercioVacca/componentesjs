
var controle_de_servicos_ativos = document.getElementById('controle_de_servicos_ativos');
var elemServicosAtivos          = document.getElementById('conteudo_dinamico_servicos_ativos');
var elemServicosCancelados      = document.getElementById('conteudo_dinamico_servicos_cancelados');

var classeAtivosCard = "go-green-bg";
var classeCanceladosCard = "marisa-red-bg";
var iconeAtivo = "&#xe0a9;";
var iconeCancelado = "&#xe093;";
var estiloElementoVazio = "";


var estiloCard;
var estiloModal;
var iconeEstatus;
var limiteCardCancelado = 100;
var tratarValor = new TratarValor();

var quant_cancelados = 0;
var quant_ativos = 0;



if(JSON.stringify(obj_servicos) != '[]')
{

	function varreduraObj(objGenerico,elementoGenerico){

		for (var i = 0; i < objGenerico.length; i++) {
			var valorFinal             = tratarValor.converter(objGenerico[i].valor);
			var _nomeProduto	       = objGenerico[i].nomeProduto;
			var _codProduto			   = objGenerico[i].codProduto;
			var _dtAdesao              = objGenerico[i].dtAdesao;
			var _parcelas              = objGenerico[i].qtdeParcelas;
			var _dtUltimaParcela       = objGenerico[i].dtUltParcela;
			var _statusServico         = objGenerico[i].status;
			var _valorServico    	   = tratarValor.converter(objGenerico[i].valor);
			var _dtCancelamentoServico = objGenerico[i].dtCancelamento;
			var _quantParcelasPostadas = objGenerico[i].nrParcela;
			var _identificadorUnico    = _codProduto+i+2;

			if(_statusServico == "ATIVO")
			{
				quant_ativos++; 
				elementoGenerico = elemServicosAtivos;
				estiloCard       = classeAtivosCard;
				estiloModal      = classeAtivosCard
				iconeEstatus     = iconeAtivo;
				
			}
			else
			{
				elementoGenerico = elemServicosCancelados;
				estiloCard       = classeCanceladosCard;
				estiloModal      = classeCanceladosCard;
				iconeEstatus     = iconeCancelado;
			}
			
			
			if(_statusServico == "CANCELADO" )
			{
				quant_cancelados++; 
				if(limiteCardCancelado > i)
				{
					criarCard(_nomeProduto,	_codProduto,_dtAdesao,_parcelas,_dtUltimaParcela,_statusServico,_valorServico,_dtCancelamentoServico,_quantParcelasPostadas,_identificadorUnico,elementoGenerico,estiloCard,estiloModal,iconeEstatus);
					clicarModal(elementoGenerico,estiloModal,iconeEstatus);
				}
			}
			else
			{
				criarCard(_nomeProduto,	_codProduto,_dtAdesao,_parcelas,_dtUltimaParcela,_statusServico,_valorServico,_dtCancelamentoServico,_quantParcelasPostadas,_identificadorUnico,elementoGenerico,estiloCard,estiloModal,iconeEstatus);
				clicarModal(elementoGenerico,estiloModal,iconeEstatus);
			}
			
		}
		logging('Opa temos ->',quant_ativos,controle_de_servicos_ativos);
		if(quant_ativos<=0){
			logging('Opa temos ->',quant_ativos,controle_de_servicos_ativos);
			$(controle_de_servicos_ativos).hide();
			$('#header_servicos_cancelados').removeClass('lista_grupo_clear_margin_top');
		}
		logging('quant_cancelados',quant_cancelados)
		if(quant_cancelados<+0){
			logging('quant_cancelados',quant_cancelados,controle_de_servicos_ativos)
		}
		
		
		
	}
 
	function criarCard(_nomeProduto,_codProduto,_dtAdesao,_parcelas,_dtUltimaParcela,_statusServico,_valorServico,_dtCancelamentoServico,_quantParcelasPostadas,_identificadorUnico,elementoGenerico,estiloCard,estiloModal,iconeEstatus){
	    var $containerLista = elementoGenerico;
		var $listaDinamica  = document.createElement('div');
		var $modalLvf = document.createElement('div');

		$modalLvf.innerHTML = templateModalServicos(_nomeProduto,_codProduto,_dtAdesao,_parcelas,_dtUltimaParcela,_statusServico,_valorServico,_dtCancelamentoServico,	_quantParcelasPostadas,	_identificadorUnico,estiloCard,	estiloModal,iconeEstatus);
	    $listaDinamica.innerHTML = templateCardServicos(_nomeProduto,_codProduto,_dtAdesao,_parcelas,_dtUltimaParcela,_statusServico,_valorServico,_dtCancelamentoServico,	_quantParcelasPostadas,	_identificadorUnico,estiloCard,	estiloModal,iconeEstatus);
	    
	    $($containerLista).prepend($modalLvf);
	    $($containerLista).append($listaDinamica);
	}

	function templateCardServicos(_nomeProduto,	_codProduto,_dtAdesao,_parcelas,_dtUltimaParcela,_statusServico,_valorServico,_dtCancelamentoServico,_quantParcelasPostadas,_identificadorUnico,estiloCard,estiloModal,iconeEstatus){
		var htmlCard;
			htmlCard = '<div class="card bg-light mb-3 custom_card">';
			htmlCard +=  '<div class="card-header '+estiloCard+'">';
			htmlCard +=  '<span class="fs1" aria-hidden="true" data-icon="'+iconeEstatus+'"></span>';
			htmlCard +=  '<span class="titulo_card_custom">'+_nomeProduto+'</span>';
			htmlCard +=  '</div>';
			htmlCard +=  '<div class="card-body">';
			htmlCard +=  '<div class="card-text card_custom_texto">';
			htmlCard +=  '<span class="titulo">Código</span>';
			htmlCard +=  '<span class="descricao"> '+_codProduto+'</span>';
			htmlCard +=  '</div>';
			htmlCard +=  '<div class="card-text card_custom_texto">';
			htmlCard +=  '<span class="titulo">Parcelas</span>';
			htmlCard +=  '<span class="descricao">  '+_parcelas+'</span>';
			htmlCard +=  '</div>';
			htmlCard +=  '<div class="card-text card_custom_texto border_controle">';
			htmlCard +=  '<span class="titulo">Adesão</span>';
			htmlCard +=  '<span class="descricao"> '+_dtAdesao+'</span>';
			htmlCard +=  '</div>';
			htmlCard +=  '</div>';
			htmlCard +=  '<div class="card-header card_custom_saiba_mais">';
			htmlCard +=  '<a href="#" data-identificador="'+_identificadorUnico+'" class="titulo_card_custom card_btn_saiba_mais isDown" data-target="#'+_identificadorUnico+'">Saiba Mais</a>';
			htmlCard +=  '<span class="fs1 saiba_mais_icon " aria-hidden="true" data-icon="&#xe102;"></span>';
			htmlCard +=  '</div>';
			htmlCard +=  '</div>';
			
		return htmlCard
	}


	function templateModalServicos(_nomeProduto,_codProduto,_dtAdesao,_parcelas,_dtUltimaParcela,_statusServico,_valorServico,_dtCancelamentoServico,	_quantParcelasPostadas,	_identificadorUnico,estiloCard,	estiloModal,iconeEstatus){

		var htmlModal =	 '<div class="LVF_custom_modal modal fade" id="'+_identificadorUnico+'" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">';
			htmlModal += '<div class="modal-dialog modal-dialog-centered" role="document">';
			htmlModal += '<div class="modal-content">';
			htmlModal += '<div class="modal-header '+estiloModal+'">';
			htmlModal += '<h5 class="modal-title" id="exampleModalLongTitle">';
			htmlModal += '<span class="fs1 icone_card_cancelado" aria-hidden="true" data-icon="'+iconeEstatus+'"></span>'; 
			htmlModal += _nomeProduto;
			htmlModal += '</h5>';
			htmlModal += '</div>';
			htmlModal += '<div class="modal-body">';
			htmlModal += '<span class="item_descritivo_modal padding_top_0" data-modal-conteudo="'+_nomeProduto+'">';
			htmlModal += '<span class="item_descritivo_modal_titulo">Código/descrição serviço</span>'; 
			htmlModal += '<span class="item_descritivo_modal_descricao"> '+_codProduto + ' ' + _nomeProduto +'</span>';
			htmlModal += '</span>';
			htmlModal += '<span class="item_descritivo_modal" data-valor-modal="'+_parcelas+'">';
			htmlModal += '<span class="item_descritivo_modal_titulo">Parcelas </span>'; 
			htmlModal += '<span class="item_descritivo_modal_descricao"> '+_parcelas+'</span>';
			htmlModal += '</span>';
			htmlModal += '<span class="item_descritivo_modal" data-valor-modal="'+_dtUltimaParcela+'" >';
			htmlModal += '<span class="item_descritivo_modal_titulo">Data última parcela </span> '; 
			htmlModal += '<span class="item_descritivo_modal_descricao"> '+_dtUltimaParcela+'</span>';
			htmlModal += '</span>';
			htmlModal += '<span class="item_descritivo_modal" data-valor-modal="'+_quantParcelasPostadas+'" >';
			htmlModal += '<span class="item_descritivo_modal_titulo">Qtde parcelas postadas </span>';  
			htmlModal += '<span class="item_descritivo_modal_descricao"> '+_quantParcelasPostadas+'</span>';
			htmlModal += '</span>';
			htmlModal += '<span class="item_descritivo_modal" data-valor-modal="'+_valorServico+'">';
			htmlModal += '<span class="item_descritivo_modal_titulo">Valor </span>';  
			htmlModal += '<span class="item_descritivo_modal_descricao">'+_valorServico+'</span>';
			htmlModal += '</span>';
			htmlModal += '<span class="item_descritivo_modal" data-valor-modal="'+_dtAdesao+'">';
			htmlModal += '<span class="item_descritivo_modal_titulo">Data Adesão</span>';   
			htmlModal += '<span class="item_descritivo_modal_descricao"> '+_dtAdesao+'</span>';
			htmlModal += '</span>';
			htmlModal += '<span class="item_descritivo_modal" data-valor-modal="'+_dtCancelamentoServico+'">';
			htmlModal += '<span class="item_descritivo_modal_titulo"> Data de cancelamento do serviço</span>'; 
			htmlModal += '<span class="item_descritivo_modal_descricao"> '+_dtCancelamentoServico+' </span>';
			htmlModal += '</span>';
			htmlModal += '<span class="item_descritivo_modal" data-valor-modal="'+_statusServico+'">';
			htmlModal += '<span class="item_descritivo_modal_titulo">Status do serviço</span>'; 
			htmlModal += '<span class="item_descritivo_modal_descricao"> '+_statusServico+'</span>';
			htmlModal += '</span>';
			htmlModal += '</div>';
			htmlModal += '<div class="modal-footer">';
			htmlModal += '<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>';
			htmlModal += '</div>';
			htmlModal += '</div>';
			htmlModal += '</div>';
			htmlModal += '</div>';
		return htmlModal;
	}
	function clicarModal(elBtn){
		$(elBtn).on('click', '.card_btn_saiba_mais', function(event) {
			event.preventDefault();
			var ATT = this.getAttribute("data-identificador");
			var ValorVazio = "";
			$('#'+ATT).modal('show');
			$('.item_descritivo_modal').each(function(index, el) {
				var ELDATA = el.getAttribute("data-valor-modal")
				if(ELDATA == " ")
				{
					$(this).addClass('elemento_vazio_modal')
				}
			});
		});
	}

	varreduraObj(obj_servicos,elemServicosAtivos);
	
 	
}
else
{
	$(elemServicosAtivos).append(mensagem.montaTemplate('Sem Serviços Contratados')) ;
 	$('#header_servicos_cancelados').hide();;
}


