function CardInformacoesFinanceiras() {
	
	this.load = function(){
		this.eventosCard()
	};
	
	this.getServico = function(){
		require("components/informacoesFinanceiras/js/servicos_js_tabs.js");
		var parameters,cpfCli,hashProcesso,getResult,usuCpf;
		cargaService();
		return obj_getInfoFinaceiras;
	};
	
	this.getTemplateLimites = function(flagReload){
		
		var objeto = this.getServico();
		
		this.codRetorno       = objeto.codigoRetorno;
		this.descricaoRetorno = objeto.descricaoRetorno;
		this.retornoServico   = objeto.objectsReturn ;
	
		if(this.codRetorno != undefined || this.codRetorno == 200 )
		{
			var card = this.templateCardLimites(flagReload);
		}else {
			return '';
		}
		return card;
		
	};
	
	this.getTemplate = function(flagReload){
		
		var objeto = this.getServico();
		
		this.codRetorno       = objeto.codigoRetorno;
		this.descricaoRetorno = objeto.descricaoRetorno;
		this.retornoServico   = objeto.objectsReturn ;
		
		if(this.codRetorno != undefined || this.codRetorno == 200 )
		{
			var card = this.templateCard(flagReload);
		}else {
			return '';
		}
		return card;
		
	};
	
	this.templateCardLimites = function(flagReload) {
		
		var html ;
		if (flagReload != undefined && flagReload.toUpperCase() == 'RELOAD') {
			html = '<div class="cardReload" >';
		}else {
			html = '<div id="cardLimites" class="cardServicos" >' ;
		}			
		//html  = '<div id="card_07_limites" class="cardServicos">';
		
		html += '<div class="card menu" id="menucard.id" style="margin: 0px;border: 5px solid #fbfaf6;max-width: 100%;">';
		html += '<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="false">';
		html += '<div class="panel panel-default">';
		html += '<div class="panel-heading" role="tab" id="heading card.id + "">';
		html += '<h4 class="panel-title">';
		html += '<span class="fs1" aria-hidden="true" data-icon="&#xe095;" style="left: 12px !important;position: absolute;top: 15px;z-index: 100;"></span>';
		html += '<a class="collapsed" id="menuCollapsecard.id" role="button" style="padding-right: 5px;color: #75104c;" data-toggle="" data-parent="#accordion" aria-expanded="true aria-controls="collapsecard">';
		html += '&nbsp;&nbsp;Limites';
		html += '</a>';
		html += '</h4>';
		html += '</div>';
		html += '<div id="collapsecard.id" class="panel-collapse collapse show" style="height: 200px;" role="tabpanel" aria-labelledby="headingcard.id">';
		html += '<div class="panel-body" style="visibility: visible;padding: 5px;height: 170px;overflow: auto;">';

		html += this.templateLimites();

		html += '</div>';
		html += '<div class="panel-footer col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding: 2px 5px 2px 2px;background-color: #dddddd;"> ';

		html += '<a href="informacoesFinanceiras" class="renderPage btn btn-info btn" style="margin: 3px 3px 3px 3px;height: 20px;float: right;"><span class="glyphicon glyphicon-th-list"> DETALHES</span> </a>';

		html += '</div>'; 
		html += '</div>';
		html += '</div>';
		html += '</div>';
		html += '</div>';
		html += '</div>';
		return html;
	};
	
	this.templateCard = function(flagReload) {
		
		var html ;
		if (flagReload != undefined && flagReload.toUpperCase() == 'RELOAD') {
			html = '<div class="cardReload" >';
		}else {
			html = '<div id="cardSaldos" class="cardServicos" >' ;
		}			
		//html  = '<div id="card_07" class="cardServicos">';
		html += '<div class="card menu" id="menucard.id" style="margin: 0px;border: 5px solid #fbfaf6;max-width: 100%;">';
		html += '<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="false">';
		html += '<div class="panel panel-default">';
		html += '<div class="panel-heading" role="tab" id="heading card.id + "">';
		html += '<h4 class="panel-title">';
		html += '<span class="fs1" aria-hidden="true" data-icon="&#xe020;" style="left: 12px !important;position: absolute;top: 15px;z-index: 100;"></span>';
		html += '<a class="collapsed" id="menuCollapsecard.id" role="button" style="padding-right: 5px;color: #75104c;" data-toggle="" data-parent="#accordion" aria-expanded="true aria-controls="collapsecard">';
		html += '&nbsp;&nbsp;Saldos';
		html += '</a>';
		html += '</h4>';
		html += '</div>';
		html += '<div id="collapsecard.id" class="panel-collapse collapse show" style="height: 200px;" role="tabpanel" aria-labelledby="headingcard.id">';
		html += '<div class="panel-body" style="visibility: visible;padding: 5px;height: 170px;overflow: auto;">';
		
		html += this.templateSaldos();
		
		html += '</div>';
		html += '<div class="panel-footer col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding: 2px 5px 2px 2px;background-color: #dddddd;"> ';
		
		html += '<a href="informacoesFinanceiras" class="renderPage btn btn-info btn" style="margin: 3px 3px 3px 3px;height: 20px;float: right;"><span class="glyphicon glyphicon-th-list"> DETALHES</span> </a>';
		
		html += '</div>'; 
		html += '</div>';
		html += '</div>';
		html += '</div>';
		html += '</div>';
		html += '</div>';
		return html;
	};
	
	this.templateSaldos = function(){
		var html = '';
		
		if(this.retornoServico.Header == undefined){
			return html;
		}		
		
		html  = '<ul class="list-group col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-bottom: 5px;padding-right: 0px;">';
		html  += '	<li class="list-group-item row" style="padding: 0px; margin: 0px;">';
		html  += '		<div class="col-xs-9 col-sm-9 col-md-9 col-lg-9" style="padding: 0px 5px 0px 5px;font-size: 13px;">Saldo devedor até a data atual</div>';
		html  += '		<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="padding: 0px;font-size: 13px;">'+ formatMoney( this.retornoServico.Header.informacoesFinanceirasSaldoVO.informacoesFinanceirasPosicaoSaldo.vrUtilizCompra, 2, ',', '.') +'</div>';
		html  += '	</li>';
		html  += '</ul>';
		html  += '<ul class="list-group col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-bottom: 5px;padding-right: 0px;">';
		html  += '	<li class="list-group-item row" style="padding: 0px; margin: 0px;">';
		html  += '		<div class="col-xs-9 col-sm-9 col-md-9 col-lg-9" style="padding: 0px 5px 0px 5px;font-size: 13px;">Saldo devedor Total</div>';
		html  += '		<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="padding: 0px;font-size: 13px;">'+ formatMoney( this.retornoServico.Header.informacoesFinanceirasSaldoVO.informacoesFinanceirasPosicaoSaldo.saldoDevTotal, 2, ',', '.') +'</div>';
		html  += '	</li>';
		html  += '</ul>';
		html  += '<ul class="list-group col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-bottom: 5px;padding-right: 0px;">';
		html  += '	<li class="list-group-item row" style="padding: 0px; margin: 0px;">';
		html  += '		<div class="col-xs-9 col-sm-9 col-md-9 col-lg-9" style="padding: 0px 5px 0px 5px;font-size: 13px;">Pgtos efetuado até data atual</div>';
		html  += '		<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="padding: 0px;font-size: 13px;">'+ formatMoney( this.retornoServico.Header.informacoesFinanceirasSaldoVO.informacoesFinanceirasPosicaoSaldo.vrPgtoAcum, 2, ',', '.') +'</div>';
		html  += '	</li>';
		html  += '</ul>';
		html  += '<ul class="list-group col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-bottom: 5px;padding-right: 0px;">';
		html  += '	<li class="list-group-item row" style="padding: 0px; margin: 0px;">';
		html  += '		<div class="col-xs-9 col-sm-9 col-md-9 col-lg-9" style="padding: 0px 5px 0px 5px;font-size: 13px;">Saldo devedor futuro</div>';
		html  += '		<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="padding: 0px;font-size: 13px;">'+ formatMoney( this.retornoServico.Header.informacoesFinanceirasSaldoVO.informacoesFinanceirasPosicaoSaldo.pgtoPend, 2, ',', '.') +'</div>';
		html  += '	</li>';
		html  += '</ul>';

		return html;
	};
	
	this.templateLimites = function(){
		var html = '';
		
		if(this.retornoServico.Header == undefined){
			return html;
		}
		
		html  = '<ul class="list-group col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-bottom: 5px;padding-right: 0px;">';
		html  += '	<li class="list-group-item row" style="padding: 0px; margin: 0px;">';
		html  += '		<div class="col-xs-9 col-sm-9 col-md-9 col-lg-9" style="padding: 0px 5px 0px 5px;font-size: 13px;">Limite de compras total</div>';
		html  += '		<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="padding: 0px;font-size: 13px;">'+ formatMoney( this.retornoServico.Header.informacoesFinanceirasContaVO.vrLimiteCompra, 2, ',', '.') +'</div>';
		html  += '	</li>';
		html  += '</ul>';
		html  += '<ul class="list-group col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-bottom: 5px;padding-right: 0px;">';
		html  += '	<li class="list-group-item row" style="padding: 0px; margin: 0px;">';
		html  += '		<div class="col-xs-9 col-sm-9 col-md-9 col-lg-9" style="padding: 0px 5px 0px 5px;font-size: 13px;">Limite de compras disponível</div>';
		html  += '		<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="padding: 0px;font-size: 13px;">'+ formatMoney( this.retornoServico.Header.informacoesFinanceirasContaVO.valorLimiteDisp, 2, ',', '.') +'</div>';
		html  += '	</li>';
		html  += '</ul>';
//		html  += '<ul class="list-group col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-bottom: 5px;padding-right: 0px;">';
//		html  += '	<li class="list-group-item row" style="padding: 0px; margin: 0px;">';
//		html  += '		<div class="col-xs-9 col-sm-9 col-md-9 col-lg-9" style="padding: 0px 5px 0px 5px;font-size: 13px;">Limite saque fácil total</div>';
//		html  += '		<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="padding: 0px;font-size: 13px;">'+ formatMoney( this.retornoServico.Header.informacoesFinanceirasContaVO.vrLimiteSaqueFacil, 2, ',', '.') +'</div>';
//		html  += '	</li>';
//		html  += '</ul>';
		html  += '<ul class="list-group col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-bottom: 5px;padding-right: 0px;">';
		html  += '	<li class="list-group-item row" style="padding: 0px; margin: 0px;">';
		html  += '		<div class="col-xs-9 col-sm-9 col-md-9 col-lg-9" style="padding: 0px 5px 0px 5px;font-size: 13px;">Limite saque fácil disponível</div>';
		html  += '		<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="padding: 0px;font-size: 13px;">'+ formatMoney( this.retornoServico.Header.informacoesFinanceirasContaVO.vrDisponivelSaqueFacil, 2, ',', '.') +'</div>';
		html  += '	</li>';
		html  += '</ul>';

		
		html  += '<ul class="list-group col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-bottom: 5px;padding-right: 0px;">';
		html  += '	<li class="list-group-item row" style="padding: 0px; margin: 0px;">';
		html  += '		<div class="col-xs-9 col-sm-9 col-md-9 col-lg-9" style="padding: 0px 5px 0px 5px;font-size: 13px;">Limite saque disponível</div>';
		html  += '		<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="padding: 0px;font-size: 13px;">'+ formatMoney( this.retornoServico.Header.informacoesFinanceirasContaVO.vrLimiteSaque, 2, ',', '.') +'</div>';
		html  += '	</li>';
		html  += '</ul>';
		html  += '<ul class="list-group col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-bottom: 5px;padding-right: 0px;">';
		html  += '	<li class="list-group-item row" style="padding: 0px; margin: 0px;">';
		html  += '		<div class="col-xs-9 col-sm-9 col-md-9 col-lg-9" style="padding: 0px 5px 0px 5px;font-size: 13px;">Limite celular disponível</div>';
		html  += '		<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="padding: 0px;font-size: 13px;">'+ formatMoney( this.retornoServico.Header.informacoesFinanceirasContaVO.limiteDisponivelCelular, 2, ',', '.') +'</div>';
		html  += '	</li>';
		html  += '</ul>';
		
		return html;
	};
	
	this.eventosCard = function(){
		// console.log('Load Eventos CardInformacoesFinanceiras')
	};


}
var cardInformacoesFinanceiras = new CardInformacoesFinanceiras();

