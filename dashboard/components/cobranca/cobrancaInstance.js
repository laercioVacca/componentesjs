





require("components/cobranca/components/templateCobranca.js");
require("components/cobranca/components/eventCobranca.js");
require("components/cobranca/components/businessCobranca.js");
require("components/cobranca/components/serviceCobranca.js");
require("components/cobranca/components/businessAcoesParcelas.js");
require("components/cobranca/components/manipuladorDomCobranca.js");
/* 
 * Classe utilizada para implementar as primeiras acoes do componente
 */
function CobrancaInstance() {
	
	if (!(this instanceof CobrancaInstance)){
        return new CobrancaInstance();
    }	
	
	// conteiner de variaveis
	this.vars = {};
	
	// Load inicial do componente
	this.load = function(navegacaoTabCobranca,conteudoTabCobranca,cpfCli){
		this.vars.navegacaoTabCobranca 	= navegacaoTabCobranca;
		this.vars.conteudoTabCobranca  	= conteudoTabCobranca;
		this.vars.cpfCli 				= cpfCli;
		
		
		businessCobranca.load(this.vars);
		eventCobranca.load();
	}
}

if(cobrancaInstance == undefined || cobrancaInstance == null){
	var cobrancaInstance = new CobrancaInstance();
}

