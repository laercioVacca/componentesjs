
/*
 * Classe utilizada para implementação de funcoes de negocio
 */
function BusinessCobranca() {
    
	if (!(this instanceof BusinessCobranca)){
        return new BusinessCobranca();
    }	

	this.load = function(varObj){
		this.varObj = varObj;
		this.infosClienteEmAtendimento = controllStorageSac.getinfosCliEmAtendimento();
		this.respConsultaCobranca = this.getConsultaClienteCobranca();
		this.controleMotagemTela();
	};
	this.montarNavegacaoTab = function(){
		var tabNavegacao = templateCobranca.tabNavegacao();
		this.varObj.navegacaoTabCobranca.html(tabNavegacao);
	};
	this.montarConteudoTab = function(nomeTab){
		var contentTab;
		if(nomeTab == "abaCliente" ){
			loadGif(this.varObj.conteudoTabCobranca); 
			this.varObj.conteudoTabCobranca.html(this.renderAbaCliente());
			eventCobranca.eventosComponent();
		}else if (nomeTab == "abaAcordo") {
			loadGif(this.varObj.conteudoTabCobranca);
			this.varObj.conteudoTabCobranca.html(this.renderAbaAcordo());
			this.dinamicaAbaAcordo();
		}else{
			loadGif(this.varObj.conteudoTabCobranca); 
			this.varObj.conteudoTabCobranca.html(this.renderAbaCliente());
		}
	}
	this.getDataAtual = function(){
		var dataAtual;
		var dataHoraAtual = retornaDataHoraAtual();
		var dataAtualArr = dataHoraAtual.split(" ");
		var dataAtual    = dataAtualArr[0];
		return dataAtual;
	}
	// get Servicos
	this.getConsultaClienteCobranca = function(){
		var respostaConsultaCli;
		var hashProcesso      =   controllStorageSac.criacaoHashUnicaProcesso("proc_");
        var propsConsulaCliCobranca = {
                "cliCpf":this.infosClienteEmAtendimento.cpf,
        }
		var respostaConsultaCli = serviceCobranca.consultaClienteCobranca(hashProcesso,propsConsulaCliCobranca);
		return respostaConsultaCli;
	}
	this.getConsultaOfertaAcordo = function(dataVencimento){
		var respostaConsultaCli;
		var hashProcesso      =   controllStorageSac.criacaoHashUnicaProcesso("proc_");
        var propsConsulaOfertaAcordo = {
                "cliCpf":this.infosClienteEmAtendimento.cpf,
                "dtVencimento":dataVencimento
        }
		var respostaConsultaCli = serviceCobranca.consultaOfertaAcordo(hashProcesso,propsConsulaOfertaAcordo);
		return respostaConsultaCli;
	}
	this.getParcelasAcordo = function(){
		var respostaConsultaCli;
		var hashProcesso      =   controllStorageSac.criacaoHashUnicaProcesso("proc_");
        var propsParcelaAcordo = {
                "cliCpf":this.infosClienteEmAtendimento.cpf,
                "dtVencimento":this.getDataAtual()
        }
		var respostaConsultaCli = serviceCobranca.consultaParcelasAcordo(hashProcesso,propsParcelaAcordo);
		var respParcelas = {
			"codigoRetorno":respostaConsultaCli.codigoRetorno,
			"descricaoRetorno":respostaConsultaCli.descricaoRetorno,
			"objectsReturn":respostaConsultaCli.objectsReturn,
			"parcelasObj":respostaConsultaCli.objectsReturn.EfetivaOfertaAcordoResponse
		}
		return respParcelas;
	}
	this.renderAbaCliente = function(){
		var conteudoAbaCliente  = templateCobranca.abaCliente(this.respConsultaCobranca);
		return conteudoAbaCliente;
	}
	this.renderAbaAcordo = function(dataAcordo){
		console.log('dataAcordo',dataAcordo)
		var primeiraParcelaDt
		if(dataAcordo == undefined || dataAcordo == null){
			primeiraParcelaDt = this.getDataAtual()
		}else {
			primeiraParcelaDt = dataAcordo;
		}
		var respostaOfertaAcordo = this.getConsultaOfertaAcordo(primeiraParcelaDt);
		var contentAbaAcordo;
		contentAbaAcordo = templateCobranca.abaAcordo(respostaOfertaAcordo,this.respConsultaCobranca);
		return contentAbaAcordo;
	}
	this.renderAbaHistorico = function(){
		var contentAbaHistorico;
		contentAbaHistorico = templateCobranca.abaHistorico();
		return contentAbaHistorico;
	}
	this.controleMotagemTela = function(){
		console.log('this.respConsultaCobranca.codigoRetorno',this.respConsultaCobranca)
		switch (this.respConsultaCobranca.codigoRetorno) {
			case 400:
				this.montarConteudoTab('abaCliente');
				break;
			case 405:
				this.montarConteudoTab('abaCliente');
				break;
			case 200:
				this.montarNavegacaoTab();
				this.montarConteudoTab('abaCliente');
				eventCobranca.getValoresObjeto(this.respConsultaCobranca)
				break;
			default:
				this.montarConteudoTab('abaCliente');
				break;
		}
	}
	this.dinamicaAbaAcordo = function(){
		var self = this;
		primeiraParcelaDt = self.getDataAtual()
		var respostaOfertaAcordo = self.getConsultaOfertaAcordo(primeiraParcelaDt);
		console.log('respostaOfertaAcordo',respostaOfertaAcordo)
		var $contentSelectOferta  = $('#contentSelectOferta');
		if(respostaOfertaAcordo.objectsReturn != undefined || respostaOfertaAcordo.objectsReturn != null ){
			$contentSelectOferta.html(templateCobranca.templateSelectOferta(respostaOfertaAcordo.objectsReturn.OfertaAcordoResponse));
			eventCobranca.eventoOfertaAcordo();
		}else {
			var codDescricao = this.apenasNumeros(respostaOfertaAcordo.descricaoRetorno);
			if(codDescricao == 5){
				var respParc = this.getParcelasAcordo();
				if(respParc.objectsReturn){
					console.log('parcelasAcordo----->>>',respParc.parcelasObj)
					$('#conteudoTabCobranca').html(templateCobranca.templateParcelas(respParc.parcelasObj));
					eventCobranca.eventoParecelasAcordo();
				}
				
			}else {
				$('#conteudoTabCobranca').html(templateCobranca.templateNaoEncontrado(respostaOfertaAcordo.descricaoRetorno));	
			}
			
		}
	}
	this.trataStatusParcela = function(codStatus,dtLimite,dataVencimento){
		var dataLimite    = this.separarData(dtLimite);
		var vencimento    = this.separarData(dataVencimento);
		var dtAtual       = this.getDataAtual();

		var limite        = dataLimite.split("/");
		var vencimento    = vencimento.split("/");
		var dataAtual     = dtAtual.split("/");

		var diaVencimento = vencimento[0];
		var mesVencimento = vencimento[1];
		var anoVencimento = vencimento[2];

		var diaLimite     = limite[0];
		var mesLimite     = limite[1];
		var anoLimite     = limite[2];

		var diaAtual     = dataAtual[0];
		var mesAtual     = dataAtual[1];
		var anoAtual     = dataAtual[2];

		function verificaDataLimite(){
			var retornoStatus;
			var flagAtraso = false;
			var DataAtual              = new Date(anoAtual,mesAtual,diaAtual);
			var DataVencimento         = new Date(anoVencimento,mesVencimento,diaVencimento);
            var DataLimite             = new Date(anoLimite,mesLimite,diaLimite);

            var valorDataAtual        = DataAtual.getTime();
            var valorDataVencimento   = DataVencimento.getTime();
            var valorDataLimite       = DataLimite.getTime();
            if(valorDataVencimento >= valorDataAtual ){
            	retornoStatus = "Em Aberto";
            }else if (valorDataVencimento <= valorDataLimite) {
            	retornoStatus = "Em atraso";
            }
			return retornoStatus;
		}
		
		var retornoStatusParcela;
		if(codStatus == "D"){
			retornoStatusParcela = verificaDataLimite();
						
		}else if (codStatus == "P") {
			retornoStatusParcela = "Paga";
		}else if (codStatus == "Q") {
			retornoStatusParcela = "Quebrado";
		}
		return retornoStatusParcela;
	}
	this.separarData = function(objData){
		var dataSeparada;
		if(objData != null || objData != undefined ){
			dataSeparada = formatDateTime(objData).split(" ");
		}else {
			dataSeparada = "----"
		}
		return dataSeparada[0];
	}
	this.apenasNumeros = function(string){
    	var numsStr = string.replace(/[^0-9]/g,'');
    	return parseInt(numsStr);
	}
}

if(businessCobranca == undefined || businessCobranca == null){
	var businessCobranca = new BusinessCobranca();
}
