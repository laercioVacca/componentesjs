function CardCobranca() {
	this.load = function(){
		this.eventosCard();
		this.getServico();
	}
	this.getServico = function(){
		require("components/cobranca/components/serviceCobranca.js");
		var respostaConsultaCli;
		var hashProcesso      =   controllStorageSac.criacaoHashUnicaProcesso("proc_");
        var propsParcelaAcordo = {
                "cliCpf":this.infosClienteEmAtendimento.cpf,
                "dtVencimento":this.getDataAtual()
        }
		var respostaConsultaCli = serviceCobranca.consultaParcelasAcordo(hashProcesso,propsParcelaAcordo);
		return respostaConsultaCli;
	
	}
	this.getDataAtual = function(){
		var dataAtual;
		var dataHoraAtual = retornaDataHoraAtual();
		var dataAtualArr = dataHoraAtual.split(" ");
		var dataAtual    = dataAtualArr[0];
		return dataAtual;
	}
	this.getTemplate = function(reload){
		this.flagReload = reload;
		this.infosClienteEmAtendimento = controllStorageSac.getinfosCliEmAtendimento();
		if(this.infosClienteEmAtendimento.situacaoContaCod == "21"){
			var objServico = this.getServico();
			var card = this.templateCard(objServico);
		}else {
			var card = this.templateCardCliSemCobranca();
		}
		
		return card;
		
	}
	this.templateCard = function (objServico) {
		
		var html ;
		if (this.flagReload != undefined) {
			html  =	'<div class="cardReload">';
		}else {
			html  =	'<div id="cardCobranca" class="cardServicos grd_sac">';
		}
		html +=	'<div class="card menu" id="menucard.id" style="margin: 0px;border: 5px solid #fbfaf6;max-width: 100%;">';
		html +=	'<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="false">';
		html +=	'<div class="panel panel-default">';
		html +=	'<div class="panel-heading" role="tab" id="heading card.id + "">';
		html +=	'<h4 class="panel-title">';
		html +=	'<a class="collapsed" id="menuCollapsecard.id" role="button" style="padding-right: 5px;color: #75104c;" data-toggle="" data-parent="#accordion" aria-expanded="true" aria-controls="collapsecard.id"> ';
		html +=	'<span class="glyphicon glyphicon-comment" style="left: -30px;"></span>';
		html +=	'Acordos';
		html +=	'</a>';
		html +=	'</h4>';
		html +=	'</div> ';
		html +=	'<div id="collapsecard.id" class="panel-collapse collapse show" style="height: 200px;" role="tabpanel" aria-labelledby="headingcard.id">';
		html +=	'<div class="panel-body" style="visibility: visible;padding: 5px;height: 170px">';
		var objParcelas = objServico.objectsReturn.EfetivaOfertaAcordoResponse;
		var parcelasAcordo = objServico.objectsReturn.EfetivaOfertaAcordoResponse.parcelas;
		var btnFooter;
		if(objServico.codigoRetorno == 200){
			if(parcelasAcordo.length > 0){
				html += this.listaAcordos(objParcelas);
				btnFooter = 'historico';
			}else{
				html += this.retornoMensagem('Nenhum Acordo encontrado');
				btnFooter = 'acordos';
			}
		}
		else{
			html += this.retornoMensagem(objServico.descricaoRetorno);
			
		}
		html +=	'</div> ';
		if(btnFooter == "historico"){
			html +=	'<div class="panel-footer col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding: 2px 5px 2px 2px;background-color: #dddddd;"> ';
			html +=	'<a href="cobranca" class="renderPage btn btn-info btn" style="margin: 3px 3px 3px 3px;height: 20px;float: right;">';
			html +=	'<span class="glyphicon glyphicon-th-list"> DETALHES</span> ';
			html +=	'</a>';
			html +=	'</div>';

		}else if (btnFooter == "acordos") {
			html +=	'<div class="panel-footer col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding: 2px 5px 2px 2px;background-color: #dddddd;"> ';
			html +=	'<a href="cobranca" class="renderPage btn btn-info btn" style="margin: 3px 3px 3px 3px;height: 20px;float: right;">';
			html +=	'<span class="glyphicon glyphicon-th-list"> ACORDOS</span> ';
			html +=	'</a>';
			html +=	'</div>';
		}
		html +=	'</div> ';
		html +=	'</div> ';
		html +=	'</div> ';
		html +=	'</div> ';
		html +=	'</div> ';
		return html;
	}
	this.listaAcordos = function(lista){
		var self =this;
		var html;
		console.log('lista',lista.qtdParcelas)
		console.log('lista',lista.parcelas)
		html  =	'<ul id="listaCardAcordo" class="list-group" style="margin-bottom: 5px;">';

		$.each(lista.parcelas, function(index, val) {
			var statusParcela = self.trataStatusParcela(val.statusParcela,val.datalLimite,val.dataVencimento);
			html +=	'<li class="item_lista_tooltipsac list-group-item row" style="padding: 0px;margin: 3px;">';
			html +=	'<div class="tamanho_texto_cards col-xs-4 col-sm-4 col-md-4 col-lg-4" style="padding: 0px 5px 0px 5px;">'+self.separarData(val.dataVencimento)+'</div>';
			html +=	'<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2" style="padding: 0px 5px 0px 5px; text-align: center">'+val.numeroParcela+ '/'+ lista.qtdParcelas +'</div>                           ';
			html +=	'<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="padding: 0px 5px 0px 5px;">'+formatMoney(val.vrParcela, 2, ',','.')+'</div>';
			html +=	'<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2" style="padding: 0px 5px 0px 5px;text-align: right;">';
			console.log('STATUS PARCELA CARD',statusParcela)
			switch (statusParcela) {
				case "emaberto":
					html +=	'<a href="#" class="btn_acordo btn_acordo_ok btn statusbtn btn-success btn-xs">';
					html +=	'<div class="tooltipsac">';
					html +=	'<div class="marcador"></div>';
					html +=	'Em aberto';
					html +=	'</div>';
					html +=	'<span class="glyphicon glyphicon-ok"></span> ';
					html +=	'</a>';
					break;
				case "ematraso":
					html +=	'<a href="#" class="btn_acordo btn_acordo_em_andamento btn statusbtn btn-info btn-xs">';
					html +=	'<div class="tooltipsac">';
					html +=	'<div class="marcador"></div>';
					html +=	'Em Atraso';
					html +=	'</div>';
					html +=	'<span class="glyphicon icon-info"></span> ';
					html +=	'</a>';
					break;
				case "quebrado":
					html +=	'<a href="#" class="btn_acordo btn_acordo_quebrado btn statusbtn btn-danger btn-xs" style="margin-top: 0px">';
					html +=	'<div class="tooltipsac">';
					html +=	'<div class="marcador"></div>';
					html +=	'Quebrado';
					html +=	'</div>';
					html +=	'<span class="glyphicon glyphicon-remove"></span> ';
					html +=	'</a>';
					break;
				case "paga":
					html +=	'<a href="#" class="btn_acordo btn_acordo_quebrado btn statusbtn btn-danger btn-xs" style="margin-top: 0px">';
					html +=	'<div class="tooltipsac">';
					html +=	'<div class="marcador"></div>';
					html +=	'Paga';
					html +=	'</div>';
					html +=	'<span class="glyphicon glyphicon-ok"></span> ';
					html +=	'</a>';
					break;
				default:
					html +=	'<a href="#" class="btn_acordo btn_acordo_ok btn statusbtn btn-success btn-xs">';
					html +=	'<div class="tooltipsac">';
					html +=	'<div class="marcador"></div>';
					html +=	'Em aberto';
					html +=	'</div>';
					html +=	'<span class="glyphicon glyphicon-ok"></span> ';
					html +=	'</a>';
					break;
			}
			html +=	'</div>';
			html +=	'</li>';
		});
		html +=	'</ul>';
		return html;
	}
	this.trataStatusParcela = function(codStatus,dtLimite,dataVencimento){
		var dataLimite    = this.separarData(dtLimite);
		var vencimento    = this.separarData(dataVencimento);
		var dtAtual       = this.getDataAtual();

		var limite        = dataLimite.split("/");
		var vencimento    = vencimento.split("/");
		var dataAtual     = dtAtual.split("/");

		var diaVencimento = vencimento[0];
		var mesVencimento = vencimento[1];
		var anoVencimento = vencimento[2];

		var diaLimite     = limite[0];
		var mesLimite     = limite[1];
		var anoLimite     = limite[2];

		var diaAtual     = dataAtual[0];
		var mesAtual     = dataAtual[1];
		var anoAtual     = dataAtual[2];

		function verificaDataLimite(){
			var retornoStatus;
			var flagAtraso = false;
			var DataAtual              = new Date(anoAtual,mesAtual,diaAtual);
			var DataVencimento         = new Date(anoVencimento,mesVencimento,diaVencimento);
            var DataLimite             = new Date(anoLimite,mesLimite,diaLimite);

            var valorDataAtual        = DataAtual.getTime();
            var valorDataVencimento   = DataVencimento.getTime();
            var valorDataLimite       = DataLimite.getTime();
            if(valorDataVencimento >= valorDataAtual ){
            	retornoStatus = "emaberto";
            }else if (valorDataVencimento <= valorDataLimite) {
            	retornoStatus = "ematraso";
            }
			return retornoStatus;
		}
		
		var retornoStatusParcela;
		if(codStatus == "D"){
			retornoStatusParcela = verificaDataLimite();
						
		}else if (codStatus == "P") {
			retornoStatusParcela = "paga";
		}else if (codStatus == "Q") {
			retornoStatusParcela = "quebrado";
		}
		return retornoStatusParcela;
	}
	this.retornoMensagem = function(txt){
		var html;
		html  = '<div class="Index_elemento_9 list-blocos-group lista_grid_02 margin_controle itemMensagemCobranca ">'; 
		html += '<div class="alt_din list-group-item list-group-item-primary" style="padding:5px;">';
		html += '<div class="alert_info_finaceira alert alert-warning alert-dismissible fade show" role="alert">';
		html += '<strong>'+txt+'</strong>';
		html += '</div>';
		html += '</div>';	
		html += '</div>';
		return html; 
	}
	this.templateCardCliSemCobranca = function () {
		var textoClienteSemCobranca = 'Cliente não encontrado na fila de cobrança. '
		var html ;
		html  =	'<div id="card_08" class="cardServicos grd_sac">';
			html +=	'<div class="card menu" id="menucard.id" style="margin: 0px;border: 5px solid #fbfaf6;max-width: 100%;">';
				html +=	'<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="false">';
					html +=	'<div class="panel panel-default">';
						html +=	'<div class="panel-heading" role="tab" id="heading card.id + "">';
							html +=	'<h4 class="panel-title">';
								html +=	'<a class="collapsed" id="menuCollapsecard.id" role="button" style="padding-right: 5px;color: #75104c;" data-toggle="" data-parent="#accordion" aria-expanded="true" aria-controls="collapsecard.id"> ';
									html +=	'<span class="glyphicon glyphicon-comment" style="left: -30px;"></span>';
									html +=	'Acordos';
								html +=	'</a>';
							html +=	'</h4>';
						html +=	'</div>';
						html +=	'<div id="collapsecard.id" class="panel-collapse collapse show" style="height: 200px;" role="tabpanel" aria-labelledby="headingcard.id">';
							html +=	'<div class="panel-body" style="visibility: visible;padding: 5px;height: 170px">';
								html += '<div class="Index_elemento_9 list-blocos-group lista_grid_02 margin_controle itemMensagemCobranca ">'; 
								
									html += '<div class="alt_din list-group-item list-group-item-primary" style="padding:5px;">';
										html += '<div class="alert_info_finaceira alert alert-warning alert-dismissible fade show" role="alert">';
											html += '<strong>'+textoClienteSemCobranca+'</strong>';
										html += '</div>';
									html += '</div>';
								html += '</div>';
							html +=	'</div>';
						html +=	'</div>';
					html +=	'</div>';
				html +=	'</div>';
			html +=	'</div>';
		html +=	'</div>';
		return html;
	}

	this.separarData = function(objData){
    	var dataSeparada;
		if(objData != null || objData != undefined ){
			dataSeparada = formatDateTime(objData).split(" ");
		}else {
			dataSeparada = "----"
		}
		return dataSeparada[0];
    }
	// 
	this.eventosCard = function(){
		$('.panel-body').on("mouseenter", '.item_lista_tooltipsac', function(event) {
            event.preventDefault();
            $(this).find('.tooltipsac').show().animate({opacity:1},function(){})
        });
        $('.panel-body').on("mouseleave", '.item_lista_tooltipsac', function(event) {
            event.preventDefault();
            $(this).find('.tooltipsac').hide().animate({opacity:0},function(){})
        });
	}

}
var cardCobranca = new CardCobranca();