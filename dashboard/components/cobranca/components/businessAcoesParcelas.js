
/*
 * Classe utilizada para implementar funcoes de validacoes
 * 
 */
function BusinessAcoesParcelas(){
	console.log('BusinessAcoesParcelas!!!');
    if (!(this instanceof BusinessAcoesParcelas)){
        return new BusinessAcoesParcelas();
    }
    // Ações para realizar o acordo
    this.infosClienteEmAtendimento = controllStorageSac.getinfosCliEmAtendimento();
    this.realizarAcordo = function(itens){
        var hashProcesso      =   controllStorageSac.criacaoHashUnicaProcesso("proc_");
        var propsEfeivaAcordo = {
                "cliCpf":this.infosClienteEmAtendimento.cpf,
                "dtVencimento":itens.dtVencimento,
                "descontoMaximo":itens.descontoMaximo,
                "idTarifa":itens.idTarifa,
                "codPolitica":itens.codPolitica,
                "qtdParcelas":itens.qtdParcelas,
                "vrParcela":itens.vrParcela,
                "txJuros":itens.txJuros,
                "vrDescontoConcedido":itens.vrDescontoConcedido
        }
        var respostaEfeivaAcordo = serviceCobranca.efetivaOfertaAcordo(hashProcesso,propsEfeivaAcordo);
        if(respostaEfeivaAcordo.codigoRetorno == 200){
        	var txtAuto = "Realizado Acordo em: "+retornaDataHoraAtual();
			lancamentosManuais.setLogAutomatico(txtAuto);
        }
        
        return respostaEfeivaAcordo
    }
	// 
    this.exibirBoleto = function(){
        var hashProcesso      =   controllStorageSac.criacaoHashUnicaProcesso("proc_");
        var propsExibirBoleto = {
                "cliCpf":this.infosClienteEmAtendimento.cpf,
                "flgPreAcordo":"0"
        }
        var respostaExibirBoleto = serviceCobranca.exibirBoletoAcordo(hashProcesso,propsExibirBoleto);
        return respostaExibirBoleto
    }
    this.enviarSms = function(){
        var celularCli;
        var hashProcesso      =   controllStorageSac.criacaoHashUnicaProcesso("proc_");
        var propsEnviarBoletoSms = {
                "cliCpf":this.infosClienteEmAtendimento.cpf,
                "foneCelular":this.infosClienteEmAtendimento.numeroCelular,
                "flgPreAcordo":"0"
        }
        var respostaEnviarSms = serviceCobranca.enviarBoletoAcordo(hashProcesso,propsEnviarBoletoSms);
        if(respostaEnviarSms.codigoRetorno == 200){
        	var txtAuto = "Enviado boleto por SMS: "+retornaDataHoraAtual();
			lancamentosManuais.setLogAutomatico(txtAuto);
        }
        return respostaEnviarSms
    }
    this.enviarEmail = function(){
        var hashProcesso      =   controllStorageSac.criacaoHashUnicaProcesso("proc_");
        var propsEnviarEmail = {
                "cliCpf":this.infosClienteEmAtendimento.cpf,
                "email":this.infosClienteEmAtendimento.email,
                "flgPreAcordo":"0"
        }
        var respostaEnviarEmail = serviceCobranca.enviarBoletoAcordo(hashProcesso,propsEnviarEmail);
        if(respostaEnviarEmail.codigoRetorno == 200){
        	var txtAuto = "Enviado boleto por Email: "+retornaDataHoraAtual();
			lancamentosManuais.setLogAutomatico(txtAuto);
        }
        return respostaEnviarEmail
    }
	this.renegociarAcordo = function(){
		this.infosClienteEmAtendimento = controllStorageSac.getinfosCliEmAtendimento();
		console.log('Dentro da função renegociarAcordo')
        var hashProcesso      =   controllStorageSac.criacaoHashUnicaProcesso("proc_");
        var propsRenegocia = {
                "cliCpf":this.infosClienteEmAtendimento.cpf,
                
        }
        var respostaRenegociaAcordo = serviceCobranca.renegociaAcordo(hashProcesso,propsRenegocia);
        if(respostaRenegociaAcordo.codigoRetorno == 200){
        	var txtAuto = "Realizado Renegociação do Acordo em: "+retornaDataHoraAtual();
			lancamentosManuais.setLogAutomatico(txtAuto);
        }
        return respostaRenegociaAcordo
	}
	this.retornoGenerico = function(){
    }
    this.retornoAtualizaDados = function(){
        reloadLocalStorageSac.load()
        // $('#tabCliente').trigger('click');
        // businessCobranca.montarConteudoTab('abaCliente');
        renderDashBoard.load('cardCobranca');
        // renderDashBoard.load('dadosCliente');
        
    }
    // Função para  atualizar Email Cliente
    this.cadastrarEnviarEmail = function(){
        var emailClienteEnvio = $('#campoaCadastroEmail').val();
        var atualizaEmailCli;
        var retorno;
        var hashProcesso        =   controllStorageSac.criacaoHashUnicaProcesso("proc_");
        var propsServicoAtualizarEmail = {
            "InformacaoCadastralCliente": {
                  "cliente": {
                     "cliCpf": this.infosClienteEmAtendimento.cpf,
                     "cliEmail": emailClienteEnvio
                  },
                  "paramHistorico": {
                     "tabelas":"JSON_TABLES_HISTORICO",
                     "colunas":"JSON_FIELDS_HISTORICO",
                     "processo":"CADASTRO_CLIENTE_SERVICE",
                     "parametro":"CLIENTE_SERVICE_EMAIL",
                     "status":"1"
                  },
                  "paramCadastro": {
                    "colunas":"JSON_FIELDS_CADASTRO",
                    "processo":"CADASTRO_CLIENTE_SERVICE",
                    "parametro":"CLIENTE_SERVICE_EMAIL",
                    "status":"1"
                  }
            }

        }

        var retorno = serviceCobranca.atualizarEmail(hashProcesso,propsServicoAtualizarEmail);
        return retorno;
    }
    this.cadastrarCelularCli = function(){
            var dddCeluCli = $("#campoaCadastroCeluDDD").val();
            var numCeluCli = $("#campoaCadastroCeluNumero").val();

            celularCliente = dddCeluCli + numCeluCli;
            
            if(numCeluCli.length < 9){
                return null;
            }
            var respAtualizaCelularCliente;
            var clienteParametros = {
                    "cliCpf" : controllStorageSac.getinfosCliEmAtendimento().cpf,
                    "cliDddCelular" : celularCliente.substring(0, 2),
                    "cliNumCelular" : celularCliente.substring(2, celularCliente.length)
            };
            var paramHistorico = {
                    "tabelas": "JSON_TABLES_HISTORICO",
                    "colunas": "JSON_FIELDS_HISTORICO",
                    "processo": "CADASTRO_CLIENTE_SERVICE",
                    "parametro": "CLIENTE_SERVICE_CELULAR",
                    "status": "1"
            };
            var paramCadastro = {
                    "colunas": "JSON_FIELDS_CADASTRO",
                    "processo": "CADASTRO_CLIENTE_SERVICE",
                    "parametro": "CLIENTE_SERVICE_CELULAR",
                    "status": "1"
            };
            var parametrosEmail = {
                    "cliente": clienteParametros,
                    "paramHistorico": paramHistorico,
                    "paramCadastro": paramCadastro
            };
            var informacaoCadastralCliente = {
                    "InformacaoCadastralCliente": parametrosEmail   
            };

            respAtualizaCelularCliente = serviceCobranca.atualizarCelularCli(hashProcesso, informacaoCadastralCliente)
            console.log('respAtualizaCelularCliente@@@@',respAtualizaCelularCliente)
            return respAtualizaCelularCliente;
    }
    this.verificaCadastroEmailCli = function(){
        var self = this;
        var funcBoletoEmail    = function(){return self.enviarEmail()};
        var callBackEmail = function(){return self.retornoGenerico()};

        var funcCadastroEmail = function(){return self.cadastrarEnviarEmail()}
        var callBackCadastroEmail = function(){reloadLocalStorageSac.load()}

        var flagPossuiEmail = false;
        if(this.infosClienteEmAtendimento.email != " " ){
            flagPossuiEmail = true;
            var propsModal
            propsModal = {
                "elementoRaiz":$('.referenciaModalcard'),
                "modalServico":true,
                "tipoModal":"acaodupla",
                "titulo":"Confirmação de Dados",
                "texto":"Deseja realmente Enviar O boleto por Email?",
                "callback":callBackCadastroEmail,
                "funcao":funcBoletoEmail
            }
            customModalSac.load(propsModal);
        }
        else {
            var propsModal
            propsModal = {
                "elementoRaiz":$('.referenciaModalcard'),
                "modalServico":true,
                "tipoModal":"cadastro",
                "titulo":"Cadastro de Dados",
                "texto":"Informe O email para Cadastro.",
                "callback":callBackEmail,
                "funcao":funcCadastroEmail
            }
            customModalSac.load(propsModal);
        }
    }
    // Função que verifica se cliente tem celular cadastrado
    this.verificaCadastroCelularCli = function(){
    	var self = this;

        var funcBoletoSMS     = function(){return self.enviarSms()};
        var callBackSMS 	  = function(){return self.retornoAtualizaDados()};

        var funcCadastroCelular 		= function(){return self.cadastrarCelularCli()}
        var callBackCadastroCelular 	= function(){reloadLocalStorageSac.load()}
        // celularCli = this.infosClienteEmAtendimento.numeroCelular;
        celularCli = " ";
        if(celularCli != " " ){
            var propsModal = {
                "elementoRaiz":$('.referenciaModalcard'),
                "modalServico":true,
                "tipoModal":"acaodupla",
                "titulo":"Confirmação de Dados",
                "texto":"Deseja realmente Enviar O boleto por SMS?",
                "callback":callBackSMS,
                "funcao":funcBoletoSMS
            }
            customModalSac.load(propsModal);
        }
        else {
            var propsModal = {
                "elementoRaiz":$('.referenciaModalcard'),
                "modalServico":true,
                "tipoModal":"cadastroCelular",
                "titulo":"Cadastro de Dados",
                "texto":"Informe O Numero do Celular para Cadastro.",
                "callback":callBackCadastroCelular,
                "funcao":funcCadastroCelular
            }
            customModalSac.load(propsModal);
        }
    }
    
}

if(businessAcoesParcelas == undefined || businessAcoesParcelas == null){
	var businessAcoesParcelas = new BusinessAcoesParcelas();
}

