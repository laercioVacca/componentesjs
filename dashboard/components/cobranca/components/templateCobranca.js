/*
 * Classe utilizada para criação dos templates HTML
 */
function TemplateCobranca() {
    
	if (!(this instanceof TemplateCobranca)){
        return new TemplateCobranca();
    }
    this.getOfertas = function(objOfertas){
    	this.objOfertas = objOfertas;
    }
    this.setOfertas = function(){
    	return this.objOfertas;
    }	
    this.tabNavegacao = function(){
    	var html;
		html  = '<ul class="nav nav-tabs lista_tabs" role="tablist">';
		html += '<li class="nav-item">';
		html += '<a class="navegacaoTabCobranca nav-link active show" id="tabCliente" data-toggle="tab" href="abaCliente" role="tab" aria-controls="tab_infos_pessoais" aria-selected="true">Cliente </a>';
		html += '</li>';
		html += '<li class="nav-item">';
		html += '<a class="navegacaoTabCobranca nav-link" id="tab_infos_referencias_pessoais" data-toggle="tab" href="abaAcordo" role="tab" aria-controls="tab_infos_referencias_pessoais" aria-selected="false">Acordo</a>';
		html += '</li>';
		html += '</ul>';
    	return html;
    }
    this.abaCliente = function(objConsultaCli){
    	var blocCorrespondencia;
		var abaClienteContainer;
		var flagSuspendeAcao;
		var tratamentoCampos;
    	var consultaCodigoRetorno    		= objConsultaCli.codigoRetorno;
    	var consultaDescricaoRetorno 		= objConsultaCli.descricaoRetorno;
    	var consultaObjeto           		= objConsultaCli.objectsReturn;
    	console.log('ABA CLI COBRA!!==========',consultaObjeto)

    	if(consultaObjeto != undefined || consultaObjeto != null){
    		var consultaResponse 		 		= consultaObjeto.ClienteCobrancaResponse;
			var acaoINT 						= consultaResponse.acaoINT;
			var valorCampoFraude 				= consultaResponse.flgFraude;

			var ValExibirOpcaoNulo 				= false;
			var ValExibirOpcaoSuspeita 			= false;
			var ValExibirOpcaoFraude			= false;
			var ValExibirOpcaoNao				= false;
			var ValExibirOpcaoReconheceDebito 	= false;

			if( valorCampoFraude != "S" && 
				valorCampoFraude != "N" && 
				valorCampoFraude != "F" && 
				valorCampoFraude != "R" && 
				valorCampoFraude == null){
				valorCampoFraude = "";
			}

			if( valorCampoFraude.length == 0 ||  
				consultaResponse.possuiPermissaoDesmarcarFraude == true ){
					ValExibirOpcaoNulo = true;
			}
			if( valorCampoFraude.length == 0 || 
				valorCampoFraude == "S" || 
				valorCampoFraude == "N" && 
				consultaResponse.possuiPermissaoDesmarcarFraude == true ){
					ValExibirOpcaoSuspeita = true;
			}
			if( valorCampoFraude.length == 0 || 
				valorCampoFraude == "F" || 
				valorCampoFraude == "N" && 
				consultaResponse.possuiPermissaoDesmarcarFraude == true ){
					ValExibirOpcaoFraude = true;
			}
			if( valorCampoFraude.length == 0 || 
				valorCampoFraude == "N" || 
				valorCampoFraude == "S" || 
				consultaResponse.possuiPermissaoDesmarcarFraude == true ){
					ValExibirOpcaoNao = true;
			}
			if( valorCampoFraude.length == 0 || 
				valorCampoFraude == "R" || 
				valorCampoFraude == "N" && 
				consultaResponse.possuiPermissaoDesmarcarFraude == true || 
				consultaResponse.possuiPermissaoMarcarFraude  ){
					ValExibirOpcaoReconheceDebito = true;
			}


			var valoresSelectFraude = {
				"possuiPermissaoDesmarcarFraude":consultaResponse.possuiPermissaoDesmarcarFraude,
				"possuiPermissaoMarcarFraude":consultaResponse.possuiPermissaoMarcarFraude,
				"valorSelect":valorCampoFraude,
				"exibirOpcaoFraude":ValExibirOpcaoFraude,
				"exibirOpcaoNao":ValExibirOpcaoNao,
				"exibirOpcaoNulo":ValExibirOpcaoNulo,
				"exibirOpcaoReconheceDebito":ValExibirOpcaoReconheceDebito,
				"exibirOpcaoSuspeita":ValExibirOpcaoSuspeita
			}
			var valoresRadioSuspAcao = {
				"permiteSuspenderAcao":consultaResponse.possuiPermissaoSuspenderAcao,
				"flagSuspendeAcao":consultaResponse.flgSuspendeAcao
			}
			var filialBloqueadaSax 				= consultaResponse.filialBloqueadaSax;
			var permiteAcessoPendencia		  	= consultaResponse.permiteAcessoPendencia;
			if(consultaResponse.bloqueioCorrespondencia == null){
				blocCorrespondencia = "Não";
			}else {
				blocCorrespondencia = "Sim";
			}
			function verificaFoneNull(num){
				var retornoDt;
				if(num != null || num != undefined){
					retornoDt = formatarCelular(num);
				}else {
					retornoDt = '---'
				}
				return retornoDt;
			}
	    	tratamentoCampos = {
	    		"Endereço Residencial":consultaResponse.cliEndCorrespondencia,
	    		"Endereço de Correspondência":consultaResponse.cliCorrespondencia,
	    		"Telefone Residencial":verificaFoneNull(consultaResponse.cliResFone), 
	    		"Telefone celular":verificaFoneNull(consultaResponse.cliNumCelular), 
	    		"Telefone Comercial":verificaFoneNull(consultaResponse.cliEmpFone), 
	    		"N. Fatura Vencida":consultaResponse.numeroFaturaVencida,
	    		"Agenda DD":this.separarData(consultaResponse.dataUltimoAgendamento),
	    		"Vencimento Ultima Fatura":this.separarData(consultaResponse.dataVencimentoUltimaFatura),
	    		"Saldo da Ult. Fatura ":formatMoney( consultaResponse.saldoTotalUltimaFatura, 2, ',','.'),
	    		"Saldo Min. Ult. Fatura ":formatMoney(consultaResponse.saldoMinimoUltimaFatura, 2, ',','.'),
	    		"Dia de Corte da Fatura ":consultaResponse.diaCorteFatura,
	    		"Dias em Atraso ":consultaResponse.diasAtraso,
	    		"Dívida Atual Compras ":formatMoney(consultaResponse.dividaAtualCompras, 2, ',','.'),
	    		"Dívida Total":formatMoney(consultaResponse.dividaTotal, 2, ',','.'),
	    		"Bloqueio de Correspondência ":blocCorrespondencia,
	    		"Limite de Crédito ":formatMoney(consultaResponse.limiteCredito, 2, ',','.'),
	    		"Limite Venda Direta":formatMoney(consultaResponse.limiteVendaDireta, 2, ',','.'),
	    		"Cadastro no SPC ":consultaResponse.flgSPC,
	    		"Saldo não compõe a cálculo do Acordo":consultaResponse.saldoNaoCompoeAcordo,
	    		"Cadastro no Serasa ":consultaResponse.flgSERASA,
	    		"Status":consultaResponse.status,
	    		"Cobradora":consultaResponse.nomeAssessoria, 
	    		"Origem":consultaResponse.origem
	    	}
			abaClienteContainer  = '<div id="abaCliente" data-obj="'+consultaResponse+'" class="left_box_grid showTab">';
			if(consultaCodigoRetorno != 200){
				abaClienteContainer += templateNaoEncontrado(consultaDescricaoRetorno);
			}
			else {
				abaClienteContainer += conteudoAbaCliente(tratamentoCampos);
			}
			abaClienteContainer += '</div>';
		}
		else {
			abaClienteContainer  = '<div id="abaCliente" data-obj="'+consultaResponse+'" class="left_box_grid showTab">';			
			abaClienteContainer += templateNaoEncontrado(consultaDescricaoRetorno);			
			abaClienteContainer += '</div>';
		}
    	function conteudoAbaCliente(tratamentoCampos){
    		var classeTextoFull;
    		var html;
    			html  = '<ul class="list-group">';
				html += '<li class="list-group-item list-group-item-info">Informações Pessoais</li>';
				html += '</ul>';
				html += '<div id="conteudo_dinamico_conta"  class="conteudo_dinamico_tabs">';
				var contador = 0;
				$.each(tratamentoCampos, function(index, val) {
					classeTextoFull = "fullTexto";
					tamanhoTitulo = 9;
					contador++;
					html += '<div class="Index_elemento_1 list-blocos-group lista_grid_02 '+classeTextoFull+' ">'; 
					html += '<div class="alt_din list-group-item list-group-item-primary teste_'+contador+'" >';
					if(tamanhoTitulo>=10){
						html += '<span class="item_lista_titulo tituloGG">'+index+'</span>';
						html += '<span class="item_lista_valor valorGG">'+val+'</span>';
					}else {
						html += '<span class="item_lista_titulo">'+index+'</span>';
						html += '<span class="item_lista_valor">'+val+'</span>';
					}
					html += '<div class="lista_grupo_clear"></div>';
					html += '</div>';
					html += '</div>';
				});
				html += templateEmitir2Via();
				html += templateSuspendeAcao(valoresRadioSuspAcao);
				html += templateFraude(valoresSelectFraude);
				html += templateDataAgendamento();
				html += templateObservacao();
				html += templateAgendar();
				html += '</div>';
    		return html;
    	}
    	function templateNaoEncontrado(txt){
    		var html;
			html  = '<div class="Index_elemento_9 list-blocos-group lista_grid_02 margin_controle itemMensagemCobranca ">'; 
			html += '<div class="alt_din list-group-item list-group-item-primary" style="padding:5px;">';
			html += '<div class="alert_info_finaceira alert alert-warning alert-dismissible fade show" role="alert">';
			html += '<strong>'+txt+'</strong>';
			html += '</div>';
			html += '</div>';
			html += '</div>';
    		return html;
    	}
    	function templateAgendar(){
			var html;
			html = '<div class="nav_parcelas nav_acao_cobranca col-xs-12 col-sm-12 col-md-12 col-lg-12" style="width: 98%">';
			html += '<span class="titulo_nav">Efetivação de Agendamento</span>';
			html += '<a id="agendarPagamento" href="#" class="btn_nav_parcelas btn btn-success">';
			html += '<span class="fs1" data-icon="&#xe03f;"> Agendar</span>';
			html += '</a>';
			html += '</div>';
			return html;
		}
		function templateEmitir2Via(){
			var html;
			html  = '<div class="Index_elemento_1 list-blocos-group lista_grid_02">'; 
			html += '<div class="alt_din list-group-item list-group-item-primary paddingCustom" >';
			html += '<span class="item_lista_titulo tituloG">Emitir 2ª Via de Fatura</span>';
			html += '<span class="item_lista_valor valorG">';
			html += '<button id="enviarEmail" type="button" data-modalsac-type="modalenvioemail" class="btnEnviarFatura buttonClick btn btn-primary mb-2">Email</button>';
			html += '<button id="enviarSms" type="button" data-modalsac-type="modalenviosms" class="btnEnviarFatura buttonClick btn btn-primary mb-2">SMS</button>';
			html += '</span>';
			html += '<div class="lista_grupo_clear"></div>';
			html += '</div>';
			html += '</div>';
			return html;
		}
		function templateSuspendeAcao(valoresRadioSuspAcao){
			var html;
			var permiteEditar = "";
			html  = '<div class="Index_elemento_1 list-blocos-group lista_grid_02">'; 
			html += '<div class="alt_din list-group-item list-group-item-primary paddingCustom" >';
			html += '<span class="item_lista_titulo tituloG">Suspende Ação</span>';
			html += '<span class="item_lista_valor valorG">';
			html += '<div class="radio">';
			if(valoresRadioSuspAcao.permiteSuspenderAcao != true){ permiteEditar = "disabled"}
			if(valoresRadioSuspAcao.flagSuspendeAcao == "N"){
				html += '<label class="boxRadio"><input id="radioSuspendeAcaoNao" type="radio" class="campoRadio" name="suspendeAcaoRadio" value="N" checked >Não</label>';
				html += '<label class="boxRadio"><input id="radioSuspendeAcaoSim" type="radio" class="campoRadio" name="suspendeAcaoRadio" value="S" '+permiteEditar+'>Sim</label>';
			}
			if(valoresRadioSuspAcao.flagSuspendeAcao == "S"){
				html += '<label class="boxRadio"><input id="radioSuspendeAcaoNao" type="radio" class="campoRadio" name="suspendeAcaoRadio" value="N" '+permiteEditar+'>Não</label>';
				html += '<label class="boxRadio"><input id="radioSuspendeAcaoSim" type="radio" class="campoRadio" name="suspendeAcaoRadio" value="S" checked>Sim</label>';

			}
			html += '</div>';
			html += '</span>';
			html += '<div class="lista_grupo_clear"></div>';
			html += '</div>';
			html += '</div>';
			return html;
		}
		function templateFraude(valoresSelectFraude){
			var pemiteEditar;
			var itemSelecionado = "";
			var abilitaEditar = "";
			var html;
			// Tratar valor
			if(valoresSelectFraude.possuiPermissaoDesmarcarFraude == true || valoresSelectFraude.possuiPermissaoMarcarFraude == true){
				pemiteEditar = true;
			}else {
				pemiteEditar = false;
			}

			html  = '<div class="Index_elemento_1 list-blocos-group lista_grid_02">'; 
			html += '<div class="alt_din list-group-item list-group-item-primary paddingCustom" >';
			
			html += '<span class="item_lista_titulo tituloG customItemTitulo01">Fraude</span>';
			html += '<span class="item_lista_valor valorG customItemValor01">';
			if(pemiteEditar != true){abilitaEditar = "disabled"}
			html += '<select class="form-control" '+abilitaEditar+' data-editar="'+pemiteEditar+'" id="cliFraude" style="width: 60%; float: left;">';
			
			if (valoresSelectFraude.exibirOpcaoNulo) {
				if(valoresSelectFraude.valorSelect == ""){itemSelecionado = 'selected'}
				html += '<option value="" '+ itemSelecionado +'>Selecione...</option>';
			}
			
			if (valoresSelectFraude.exibirOpcaoSuspeita) {
				if(valoresSelectFraude.valorSelect == "S"){itemSelecionado = 'selected'}
				html += '<option value="S" '+ itemSelecionado +'>Suspeita de Fraude</option>';
			}
			
			if (valoresSelectFraude.exibirOpcaoFraude) {
				if(valoresSelectFraude.valorSelect == "F"){itemSelecionado = 'selected'}
				html += '<option value="F" '+ itemSelecionado +'>Fraude Confirmada</option>';
			}
			
			if (valoresSelectFraude.exibirOpcaoNao) {
				if(valoresSelectFraude.valorSelect == "N"){itemSelecionado = 'selected'}
				html += '<option value="N" '+ itemSelecionado +'>Não é Fraude</option>';
			}
			
			if(valoresSelectFraude.exibirOpcaoReconheceDebito){
				if(valoresSelectFraude.valorSelect == "R"){itemSelecionado = 'selected'}
				html += '<option value="R" '+ itemSelecionado +'>Reconhecer Débito</option>';
			}

			html += '</select>';
			html += '</span>';
			html += '<div class="lista_grupo_clear"></div>';
			html += '</div>';
			html += '</div>';
			return html;
		}
		function templateObservacao(){
			var html;
			html  = '<div class="Index_elemento_1 list-blocos-group lista_grid_02">'; 
			html += '<div class="alt_din list-group-item list-group-item-primary paddingCustom" >';
			html += '<span class="item_lista_titulo tituloG" style="float:left;padding-top:12px">Observação</span>';
			html += '<span class="item_lista_valor valorG" style="float:right;padding-left: 0;">';
			html += '<textarea class="textolancamentoEditar form-control" rows="2" id="campoTextoObservacao"></textarea>';
			html += '</span>';
			html += '<div class="lista_grupo_clear"></div>';
			html += '</div>';
			html += '</div>';
			return html;
		}
		function templateDataAgendamento(){
			var html;
			html  = '<div class="Index_elemento_1 list-blocos-group lista_grid_02">'; 
			html += '<div class="alt_din list-group-item list-group-item-primary templateAgendamento paddingCustom" style="padding-top: 10px;padding-bottom: 9px;" >';
			html += '<span class="item_lista_titulo tituloG" style="float:left;padding-top:7px">Data Agendamento</span>';
			html += '<span class="item_lista_valor valorG" style="float:right;padding-left: 0;">';
			html += '<input type="text" id="agendamentoData" class="inputAgendamento dataAgendamento form-control" placeholder="">';
			html += '<input type="text" id="agendamentoDiasAtraso" class="inputAgendamento diasAtraso form-control" placeholder="Dias de Atraso">';
			html += '</span>';
			html += '<div class="lista_grupo_clear"></div>';
			html += '</div>';
			html += '</div>';
			return html;
		}
    	return abaClienteContainer;
    }
    this.separarData = function(objData){
		var dataSeparada;
		if(objData != null || objData != undefined ){
			dataSeparada = formatDateTime(objData).split(" ");
		}else {
			dataSeparada = "----"
		}
		
		return dataSeparada[0];
	}
    // ###
    this.abaAcordo = function(objConsultaOfertaAcordo,infoCli){
    	var html;
    	this.codigoRetornoOfertaAcordo 	   = objConsultaOfertaAcordo.codigoRetorno;
    	this.descricaoRetornoOfertaAcordo  = objConsultaOfertaAcordo.descricaoRetorno;
    	this.objectsReturnOfertaAcordo     = objConsultaOfertaAcordo.objectsReturn;
    	if(this.objectsReturnOfertaAcordo != undefined || this.objectsReturnOfertaAcordo != null){
    		this.conteudoRespostaServico       = objConsultaOfertaAcordo.objectsReturn.OfertaAcordoResponse;
	    	// Monta primeira parte das informações da aba acordo
	    	var infosAbaCliente = infoCli.objectsReturn.ClienteCobrancaResponse;

	    	var infosAcordo = {
	    		"Quantidade de Ofertas":this.conteudoRespostaServico.qtdOfertas,
	    		"Taxa de Correção":this.conteudoRespostaServico.txCorrecao,
	    		"Dívida Atual Compras":formatMoney(this.conteudoRespostaServico.vrDividaAtualCompras, 2, ',','.'),
	    		"Dívida Total":formatMoney(this.conteudoRespostaServico.vrDividaTotal, 2, ',','.'),
	    		"Bloqueio de Correspondência":infosAbaCliente.bloqueioCorrespondencia,
	    		"Qtde. de Acordos Realizados":"----",
	    		"Situação Acordo Anterior":"----",
	    		"Saldo não compõe a cálculo do Acordo":formatMoney(infosAbaCliente.saldoNaoCompoeAcordo, 2, ',','.'),
	    		"Dias em Atraso":infosAbaCliente.diasAtraso,
				"Renda Mensal":"----",
				"Dia de Corte da Fatura":infosAbaCliente.diaCorteFatura,
				"Dia Vencimento da Fatura":this.separarData(infosAbaCliente.dataVencimentoUltimaFatura)

	    	}
	    	
			html  = '<div id="abaAcordo" class="left_box_grid">';
			html += '<ul class="list-group">';
			html += '<li class="list-group-item list-group-item-info">Informações - Acordo</li>';
			html += '</ul>';
			html += '<div id="conteudo_dinamico_conta"  class="conteudo_dinamico_tabs">';
			var contador = 0;
			html += '<div id="infosGeraisAcordo">';
			$.each(infosAcordo, function(index, val) {
				html += '<div class="Index_elemento_1 list-blocos-group lista_grid_02">'; 
				html += '<div class="alt_din list-group-item list-group-item-primary">';
				html += '<span class="item_lista_titulo tituloGG">'+index+'</span>';
				html += '<span class="item_lista_valor valorGG">'+val+'</span>';
				html += '<div class="lista_grupo_clear"></div>';
				html += '</div>';
				html += '</div>';
			});
			html += '</div>';
			html += this.templateEscolhaData();
			html += '<div id="contentPossiveisOfertas">';
			html += '<div id="contentSelectOferta"></div>';
			// html += this.templateSelectOferta(this.conteudoRespostaServico);
			html += '</div>';
			html += '</div>';
			html += '</div>';
	    	return html;

    	}
    }
   	// ###
    this.templateEscolhaData = function(){
    	var html;
		html  = '<div class="Index_elemento_1 list-blocos-group lista_grid_02 fullItem ">'; 
		html += '<div class="alt_din list-group-item list-group-item-primary templateAgendamento paddingCustom" >';
		html += '<span class="item_lista_titulo tituloG" style="float:left;padding-top:7px">Data Primeira Parcela</span>';
		html += '<span class="item_lista_valor valorG" style="float:right;padding-left: 0;">';
		html += '<input type="text" id="dataPrimeiraParcela" data-ultimoItem-select="" class="inputAgendamento dataAgendamento form-control" placeholder="">';
		html += '</span>';
		html += '<div class="lista_grupo_clear"></div>';
		html += '</div>';
		html += '</div>';
		return html;
    }
    this.templateSelectOferta = function(conteudoRespostaServico){
    	console.log('conteudoRespostaServico=>>',conteudoRespostaServico);
    	var html;
		html  = '<div class="Index_elemento_1 list-blocos-group lista_grid_02 fullItem ">'; 
		html += '<div class="alt_din list-group-item list-group-item-primary templateAgendamento paddingCustom" >';
		html += '<span class="item_lista_titulo tituloG" style="float:left;padding-top:7px">Política de Acordo</span>';
		html += '<span class="item_lista_valor valorG" style="float:right;padding-left: 0;">';
		html += '<div class="form-group">';
		html += '<select class="form-control" id="selecioneOfertaAcordo">';
		html += '<option value="">Selecione...</option</option>';
		$.each(conteudoRespostaServico.acordos, function(index, val) {
			 html += '<option value="'+val.descontoOferta+'">'+val.descontoOferta+'</option>'
		});
		html += '</select>';
		html += '</div>';
		html += '</span>';
		html += '<div class="lista_grupo_clear"></div>';
		html += '</div>';
		html += '</div>';
		html += '<div id="dadosAcordoEscolhido"></div>';
		templateCobranca.getOfertas(conteudoRespostaServico)
		return html;
    }
    this.templateCardOferta = function(item){
    	var html;
    	var self = this;
    	html = '<div class="cardOfertaSelecionada">';
		html += '<div class="card-header">';
		html += '<div class="tituloCardOferta">';
        html += '<div class="fs1 iconeCardOferta" aria-hidden="true" data-icon="&#xe0d4;"></div>';
		html += '<strong>Código Política</strong> '+item.codPolitica;
		html += '</div>';
		html += '</div>';
		html += '<div class="card-body">';
 
		html += '<div class="cardOfertaSelecionada_item">';
		html += '<div class="fs1 iconeCardOferta" aria-hidden="true" data-icon=""></div>';
		html += '<span class="cardOfertaSelecionada_item_titulo">Desconto Oferta</span>';
		html += '<span class="cardOfertaSelecionada_item_valor">'+item.descontoOferta+'</span>';
		html += '<div class="lista_grupo_clear"></div>';
		html += '</div>';
		// 
		html += '<div class="cardOfertaSelecionada_item">';
		html += '<div class="fs1 iconeCardOferta" aria-hidden="true" data-icon=""></div>';
		html += '<span class="cardOfertaSelecionada_item_titulo">Quantidade de Parcelas</span>';
		html += '<span class="cardOfertaSelecionada_item_valor">'+item.qtdParcelas+'</span>';
		html += '<div class="lista_grupo_clear"></div>';
		html += '</div>';
		// 
		html += '<div class="cardOfertaSelecionada_item">';
		html += '<div class="fs1 iconeCardOferta" aria-hidden="true" data-icon=""></div>';
		html += '<span class="cardOfertaSelecionada_item_titulo">Taxa de Juros</span>';
		html += '<span class="cardOfertaSelecionada_item_valor">'+item.txJuros+'%</span>';
		html += '<div class="lista_grupo_clear"></div>';
		html += '</div>';
		// formatMoney( , 2, ',','.')
		html += '<div class="cardOfertaSelecionada_item">';
		html += '<div class="fs1 iconeCardOferta" aria-hidden="true" data-icon=""></div>';
		html += '<span class="cardOfertaSelecionada_item_titulo">Valor Desconto Concedido</span>';
		html += '<span class="cardOfertaSelecionada_item_valor">'+formatMoney(item.vrDescontoConcedido , 2, ',','.')+'</span>';
		html += '<div class="lista_grupo_clear"></div>';
		html += '</div>';
		// 
		html += '<div class="cardOfertaSelecionada_item">';
		html += '<div class="fs1 iconeCardOferta" aria-hidden="true" data-icon=""></div>';
		html += '<span class="cardOfertaSelecionada_item_titulo">Valor Desconto Maximo Permitido</span>';
		html += '<span class="cardOfertaSelecionada_item_valor">'+formatMoney(item.vrDescontoMaxPermitido, 2, ',','.')+'</span>';
		html += '<div class="lista_grupo_clear"></div>';
		html += '</div>';
		// 
		html += '<div class="cardOfertaSelecionada_item">';
		html += '<div class="fs1 iconeCardOferta" aria-hidden="true" data-icon=""></div>';
		html += '<span class="cardOfertaSelecionada_item_titulo">Valor Parcela</span>';
		html += '<span class="cardOfertaSelecionada_item_valor">'+formatMoney(item.vrParcela, 2, ',','.')+'</span>';
		html += '<div class="lista_grupo_clear"></div>';
		html += '</div>';
		// 
		html += '<div class="cardOfertaSelecionada_item">';
		html += '<div class="fs1 iconeCardOferta" aria-hidden="true" data-icon=""></div>';
		html += '<span class="cardOfertaSelecionada_item_titulo">Valor Total Acordo</span>';
		html += '<span class="cardOfertaSelecionada_item_valor">'+formatMoney(item.vrTotalAcorod, 2, ',','.')+'</span>';
		html += '<div class="lista_grupo_clear"></div>';
		html += '</div>';
		html += '</div>';
		html += '<div class="card-footer text-muted">';
		html += '<div class="navegacaoCardAcordo">';
		html += '<a id="btnRealizarAcordo" href="#" class="btnCardAcordoFooter sidebar_link btn btn-success btn">';
		html += '<div class="fs1 iconeCardOferta" aria-hidden="true" data-icon=""></div>';
		html += 'Realizar Acordo';
		html += '</a>';
		html += '<div class="lista_grupo_clear"></div>';
		html += '</div>';
		html += '</div>';
		html += '</div>';
		return html;
    }
    // ###
    this.templateNaoEncontrado = function(txt){
    		var html;
			html  = '<div class="Index_elemento_9 list-blocos-group lista_grid_02 margin_controle itemMensagemCobranca ">'; 
			html += '<div class="alt_din list-group-item list-group-item-primary" style="padding:5px;">';
			html += '<div class="alert_info_finaceira alert alert-warning alert-dismissible fade show" role="alert">';
			html += '<strong>'+txt+'</strong>';
			html += '</div>';
			html += '</div>';
			html += '</div>';
    		return html;
    }
    // ####
    this.templateParcelas = function(parcelasObj){
    	console.log('parcelasObj templateParcelas',parcelasObj.parcelas)
    	var quantidadeParcelas = parcelasObj.qtdParcelas;
    	var html;
    	var self = this;
		html  = '<div id="abaAcordo" class="left_box_grid">';
		html += '<ul class="list-group">';
		html += '<li class="list-group-item list-group-item-info">Informações Parcelas</li>';
		html += '</ul>';
		html += '<div id="conteudoParcelas">';
		html += '<div class="box_lista_parcelas">';
		html += '<ul class="lista_parcela listaHeader">';
		html += '<li class="itemParcela itemHeader" style="width:65px">Parcela</li>';
		html += '<li class="itemParcela itemHeader">Vencimento</li>';
		html += '<li class="itemParcela mobileHide itemHeader">Data Limite</li>';
		html += '<li class="itemParcela itemHeader">Valor</li>';
		html += '<li class="itemParcela mobileHide itemHeader">Valor Pago</li>';
		html += '<li class="itemParcela mobileHide itemHeader">Data Pagamento</li>';
		html += '<li class="itemParcela itemHeader">Status</li>';
		html += '<li class="itemParcela itemHeader">Codigo de Barras</li>';
		html += '</ul>';
		$.each(parcelasObj.parcelas, function(index, val) {
			html += '<ul class="lista_parcela">';
			html += '<li class="itemParcela"  style="width:65px">'+val.numeroParcela+ '/'+ quantidadeParcelas +'</li>';
			html += '<li class="itemParcela">'+self.separarData(val.dataVencimento)+'</li>';
			html += '<li class="itemParcela mobileHide">'+self.separarData(val.datalLimite)+'</li>';
			html += '<li class="itemParcela">'+formatMoney(val.vrParcela, 2, ',','.')+'</li>';
			html += '<li class="itemParcela mobileHide">'+formatMoney(val.vrPagamento, 2, ',','.')+'</li>';
			html += '<li class="itemParcela mobileHide">'+self.separarData(val.dataPagamento)+'</li>';
			html += '<li class="itemParcela">'+businessCobranca.trataStatusParcela(val.statusParcela,val.datalLimite,val.dataVencimento)+'</li>';
			html += '<li class="itemParcela"><a class="modalCodigoBarra" data-codigoBarra="'+val.linhaDigitavel+'" href="#">'+self.reduzirString(val.linhaDigitavel,10)+'...</a></li>';
			html += '</ul>';
		});
	
		html += '<div class="clear" style="clear: both"></div>';

		html += '</div>';
		html += '<div class="acoesParcelas col-xs-12 col-sm-12 col-md-12 col-lg-12">';
		html += '<span class="tituloAcoes">Enviar Boleto</span>';
		html += '<a id="btnRenegociar" href="#" class="btnAcaoParcela icone_visualizar btn btn-danger">';
		html += '<span class="fs1" data-icon="&#xe0fd;">Renegociar Acordo</span>';
		html += '</a>';
		html += '<a id="btnEnviEmail" href="#" class="btnAcaoParcela btn_envio_email btn btn-success">';
		html += '<span class="fs1" data-icon="&#xe040;"> E-mail</span>';
		html += '</a>';
		html += '<a id="btnEnvSms"href="#" class="btnAcaoParcela btn_envio_sms btn btn-success">';
		html += '<span class="fs1" data-icon="&#xe054;">SMS</span>';
		html += '</a>';
		html += '<a id="btnExibeBoleto" href="#" class="btnAcaoParcela icone_visualizar btn btn-success">';
		html += '<span class="fs1" data-icon="&#xe04f;">Exibir Boleto</span>';
		html += '</a>';
		
		html += '</div>';
		html += '</div>';
		html += '</div> ';

    	return html;
    }
    this.reduzirString = function(txt,tamanho){
    	var retornoTxt;
    	var tamanhoStrTxt = txt.length;
    	if(tamanhoStrTxt > tamanho){
    		retornoTxt = txt.slice(0, tamanho)
    	}
    	return retornoTxt;
    }	
	
}

if(templateCobranca == undefined || templateCobranca == null){
	var templateCobranca = new TemplateCobranca();
}