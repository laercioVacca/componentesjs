
/*
 * CLasse utilizada para implementacao dos eventos de tela e forms
 */
function EventCobranca() {
    if (!(this instanceof EventCobranca)){
        return new EventCobranca();
    }
    this.getValoresObjeto = function(obj){
        this.valoresObj = obj;
    }
	this.load = function(){
		this.infosClienteEmAtendimento = controllStorageSac.getinfosCliEmAtendimento();
        this.DataCurrentAtendimento    = controllStorageSac.getDataFlagAtendimento()
        this.numeroProtocolo           =  this.DataCurrentAtendimento.atendimentoInfos.idProtocolo;
        
        if(this.valoresObj != undefined){
            this.valoresRetornadosCli      = this.valoresObj.objectsReturn.ClienteCobrancaResponse;
            this.podeSuspenderAcao         = this.valoresRetornadosCli.possuiPermissaoSuspenderAcao
            this.oldValFlagFraude          = this.valoresRetornadosCli.flgFraude;
            this.oldValFlagSuspendeAcao    = this.valoresRetornadosCli.flgSuspendeAcao;
        }
        
        this.oldValObservacao          = $('#campoTextoObservacao').val();
        this.oldValDataAgendamento     = $('#agendamentoData').val();
	};
    this.realizarAgendamento = function(){
        var editarSelectFraude    = $('#cliFraude').attr('data-editar');
        var campoFlagFraude       = $('#cliFraude').val();
        var campoObservacao       = $('#campoTextoObservacao').val();
        var campoFlagSuspendeAcao = $('input:radio[name=suspendeAcaoRadio]:checked').val();
        var campoDataAgendamento  = $('#agendamentoData').val();
        var respostaConsultaCli;
        var novoValorFlagFraude;
        var novoValorObservacao;
        var novoValorFlagSuspendeAcao;
        var novoValorDataAgendamento;
        if(this.oldValFlagFraude == campoFlagFraude ){
            novoValorFlagFraude = this.oldValFlagFraude;
        }
        else {
            if(editarSelectFraude == "false"){
                novoValorFlagFraude = this.oldValFlagFraude;
            }else {
                novoValorFlagFraude = campoFlagFraude;
            }
        }
        if(this.oldValObservacao != campoObservacao){
            novoValorObservacao = campoObservacao;
        }else {
            novoValorObservacao = this.oldValObservacao;
        }
        if(this.oldValFlagSuspendeAcao != campoFlagSuspendeAcao){
            if(this.podeSuspenderAcao){
                novoValorFlagSuspendeAcao = campoFlagSuspendeAcao;
            }else {
                novoValorFlagSuspendeAcao = this.oldValFlagSuspendeAcao
            }
        }else {
            novoValorFlagSuspendeAcao = this.oldValFlagSuspendeAcao;
        }
        if(this.oldValDataAgendamento != campoDataAgendamento){
            novoValorDataAgendamento = campoDataAgendamento;
        }else {
            novoValorDataAgendamento;
        }
        var hashProcesso =   controllStorageSac.criacaoHashUnicaProcesso("proc_");
        var propsAgendar = {
                "cliCpf":this.infosClienteEmAtendimento.cpf,
                "cpfUsuario":this.infosClienteEmAtendimento.cpfUser,
                "numeroProtocolo":this.numeroProtocolo,
                "flgFraude":novoValorFlagFraude,
                "observacao":novoValorObservacao,
                "flgSuspensaoAcao":novoValorFlagSuspendeAcao,
                "dtAgendamento":novoValorDataAgendamento
        }
        var respostaConsultaCli = serviceCobranca.salvarAgendamentoCliente(hashProcesso,propsAgendar);
        if(respostaConsultaCli.codigoRetorno == 200){
            var txtAuto = "Realizado Agendamento Em: "+retornaDataHoraAtual();
            lancamentosManuais.setLogAutomatico(txtAuto);
        }
        return respostaConsultaCli;
    }
    this.retornoAgendamento = function(){
        console.log('retornoAgendamento')
        businessCobranca.montarConteudoTab('abaCliente')
    }
    this.eventoDataPicker = function(){
        $( "#agendamentoData" ).datepicker({ 
            changeMonth: true, 
            changeYear: true, 
            stepMonths: 12, 
            showOtherMonths: true,
            selectOtherMonths: true, 
            dateFormat: "dd/mm/yy",
            firstDay: 0,
            hideIfNoPrevNext: true,
            showAnim: 'slideDown',
            //showOn: "both",  
            showStatus: true,
            showButtonPanel: true
        });
        $("#agendamentoData").on('focus', function (e) {
            e.preventDefault();
            $(this).datepicker('show');
            $(this).datepicker('widget').css('z-index', 1053);
        });
        // 
        $( "#dataPrimeiraParcela" ).datepicker({ 
            changeMonth: true, 
            changeYear: true, 
            stepMonths: 12, 
            showOtherMonths: true,
            selectOtherMonths: true, 
            dateFormat: "dd/mm/yy",
            firstDay: 0,
            hideIfNoPrevNext: true,
            showAnim: 'slideDown',
            //showOn: "both",  
            showStatus: true,
            showButtonPanel: true
        });
        $("#dataPrimeiraParcela").on('focus', function (e) {
            e.preventDefault();
            $(this).datepicker('show');
            $(this).datepicker('widget').css('z-index', 1053);
        });
    }
    this.eventoOfertaAcordo = function(infosAcordoEscolhido){
        var $contentSelectOferta          = $('#contentSelectOferta');
        var $dadosAcordoEscolhido         = $('#dadosAcordoEscolhido');
        var $dataPrimeiraParcela          = $("#dataPrimeiraParcela" );
        var $btnRealizarAcordo            = $('#btnRealizarAcordo');
        var $campoContentPossiveisOfertas = $('#contentPossiveisOfertas');
        var $selecioneOfertaAcordo        = $("#selecioneOfertaAcordo");
        var self = this;
        $dataPrimeiraParcela.mask("99/99/9999");
        $dataPrimeiraParcela.val(businessCobranca.getDataAtual())
        $dataPrimeiraParcela.on('change', function(event) {
            event.preventDefault();
            var novaDataParcela = $(this).val()
            var respostaNovaChamadaOfertaAcordo = businessCobranca.getConsultaOfertaAcordo(novaDataParcela);
            if(respostaNovaChamadaOfertaAcordo.objectsReturn != undefined || respostaNovaChamadaOfertaAcordo.objectsReturn != null){
                var novoObjetoRetorno = respostaNovaChamadaOfertaAcordo.objectsReturn;
                var responseNovaChamada = novoObjetoRetorno.OfertaAcordoResponse;
                $contentSelectOferta.html(templateCobranca.templateSelectOferta(responseNovaChamada));
                var ultimaOfertaSelecionada = $(this).attr('data-ultimoItem-select');
                if(ultimaOfertaSelecionada != undefined || ultimaOfertaSelecionada != ""){
                   $("#selecioneOfertaAcordo").prop('selectedIndex', ultimaOfertaSelecionada);
                   $("#selecioneOfertaAcordo").trigger("change") 
                }
                
            }else {
                $contentSelectOferta.html(templateCobranca.templateNaoEncontrado(respostaNovaChamadaOfertaAcordo.descricaoRetorno))
            }
        });
        $contentSelectOferta.on('change',$selecioneOfertaAcordo, function(event) {
            var frontIdOfertaEscolhida = $("#selecioneOfertaAcordo option:selected").val();
            var ofertasDisponiveis = templateCobranca.setOfertas();
            $('#dataPrimeiraParcela').attr('data-ultimoItem-select',$("#selecioneOfertaAcordo").prop('selectedIndex'))
            var ofertaEscolhida = $.grep(ofertasDisponiveis.acordos,function(n,i){
                return(n.descontoOferta == frontIdOfertaEscolhida )
            })
            if(JSON.stringify(ofertaEscolhida) != '[]'){
                var infosAcordoEscolhido = {
                    "dtVencimento":$('#dataPrimeiraParcela').val(),
                    "descontoMaximo":ofertaEscolhida[0].vrDescontoMaxPermitido,
                    "idTarifa":ofertasDisponiveis.idTarifa,
                    "codPolitica":ofertaEscolhida[0].codPolitica,
                    "qtdParcelas":ofertaEscolhida[0].qtdParcelas,
                    "vrParcela":ofertaEscolhida[0].vrParcela,
                    "txJuros":ofertaEscolhida[0].txJuros,
                    "vrDescontoConcedido":ofertaEscolhida[0].vrDescontoConcedido
                }
                $('#dadosAcordoEscolhido').html(templateCobranca.templateCardOferta(ofertaEscolhida[0]));
                acaoRealizarAcordo(infosAcordoEscolhido);
            }
        });
        function acaoRealizarAcordo(infosAcordoEscolhido){
           $('#btnRealizarAcordo').on('click', function(event) {
                event.preventDefault();
                var funcRealizarAcordo     = function(){return businessAcoesParcelas.realizarAcordo(infosAcordoEscolhido)};
                var callBackRealizarAcordo = function(){return businessAcoesParcelas.retornoAtualizaDados()}
                
                var elementoRaiz = $('.referenciaModalcard');
                propsModal = {
                    "elementoRaiz":elementoRaiz,
                    "modalServico":true,
                    "tipoModal":"acaodupla",
                    "titulo":"Confirmação de Dados",
                    "texto":"Deseja realmente Realizar este Acordo?",
                    "callback":callBackRealizarAcordo,
                    "funcao":funcRealizarAcordo
                }
                customModalSac.load(propsModal);
            });
        }
    }
    this.eventoParecelasAcordo = function(){
        var self = this;
        var $elementoRaiz = $('.referenciaModalcard');
        var $conteudoParcelas    = $('#conteudoParcelas');
        var $exibirBoletoParcela = $('#btnExibeBoleto');
        var $enviarSmsParcela    = $('#btnEnvSms');
        var $enviarEmailParcela  = $('#btnEnviEmail');
        var $renegociaAcordo     = $('#btnRenegociar');

        var renegociaFunc            = function(){return businessAcoesParcelas.renegociarAcordo()};
        var callBackRenegocia        = function(){return businessAcoesParcelas.retornoAtualizaDados()}

        $exibirBoletoParcela.on('click', function(event) {
            event.preventDefault();
            var objExibirBoleto       = businessAcoesParcelas.exibirBoleto();
            var respostaParcelaCodigo = objExibirBoleto.codigoRetorno;
            var descricaoRetorno      = objExibirBoleto.descricaoRetorno;
            var boletoParcelaBase64   = objExibirBoleto.objectsReturn.BoletoRequest;

            var propsModal
            propsModal = {
                "elementoRaiz":$elementoRaiz,
                "titulo":"Boleto Acordo",
                "objServico":objExibirBoleto,
            }
            modalPdfSac.load(propsModal);
        });
        $enviarSmsParcela.on('click', function(event) {
            event.preventDefault();
            businessAcoesParcelas.verificaCadastroCelularCli();
        });
        $enviarEmailParcela.on('click', function(event) {
            event.preventDefault();
            businessAcoesParcelas.verificaCadastroEmailCli();
        });
        $renegociaAcordo.on('click', function(event) {
            event.preventDefault();
            var propsModal
            propsModal = {
                "elementoRaiz":$elementoRaiz,
                "modalServico":true,
                "tipoModal":"acaodupla",
                "titulo":"Confirmação de Dados",
                "texto":"Deseja realmente Renegociar O Acordo?",
                "callback":callBackRenegocia,
                "funcao":renegociaFunc
            }
            customModalSac.load(propsModal);
        });
        $('.modalCodigoBarra').on('click', function(event) {
            event.preventDefault();
            var codigoBarraParcela = $(this).attr('data-codigoBarra');
            marisaMessage.showDialog(marisaMessageType_INFO, "Detalhe Codigo de Barras",codigoBarraParcela);
        });
    }
	this.eventosComponent = function(){
        this.eventoDataPicker();
        $("#agendamentoData" ).mask("99/99/9999");
        var elementoRaiz = $('.referenciaModalcard');
        var self = this;
        var enviarBoleto;
        ativacaoAutomatica();
        $("#agendamentoData").on('change', function(event) {
            event.preventDefault();
            var dataHoraAtual = retornaDataHoraAtual();
            var dataAtualArr = dataHoraAtual.split(" ")
            var dataCampo = $(this).val().split('/');
            var dataAtual = dataAtualArr[0].split('/');
            dia_Atual = dataAtual[0];
            mes_Atual = dataAtual[1];
            ano_Atual = dataAtual[2];
            dia_Campo = dataCampo[0];
            mes_Campo = dataCampo[1];
            ano_Campo = dataCampo[2];
            var calculoDia = dia_Campo - dia_Atual;
            var calculoMes = mes_Campo - mes_Atual;
            var calculoAno = ano_Campo - ano_Atual;
            if(calculoDia < "0" || calculoMes < "0" || calculoAno < "0"){
                marisaMessage.showDialog(marisaMessageType_WARNING, "Alerta","A data  não pode ser anterior a data de Hoje!");
                $("#agendamentoData").val("");
                $("#agendamentoData").focus();
            } 
            var DataEntrada         = new Date(ano_Campo,mes_Campo,dia_Campo);
            var DataAtual           = new Date(ano_Atual,mes_Atual,dia_Atual);
            var DiasEmMilisegundos  = 2505600000;
            var valorDataAgendada   = DataEntrada.getTime();
            var valorLimite30Dias   = DataAtual.getTime()+DiasEmMilisegundos;
            if( valorDataAgendada > valorLimite30Dias ){
                marisaMessage.showDialog(marisaMessageType_WARNING, "Alerta","A data  não pode ser maior que 30 dias!");
                $("#agendamentoData").val("");
                $("#agendamentoData").focus();
            }
        });
        $('.left_box_grid.showTab').show();
        $('.tabs_infos').on('click', '.nav-link', function(event) {
            event.preventDefault();
            var tabAtiva =  $(this).attr('href');
            hideTab();
            $('#'+tabAtiva).show();
            $("#agendamentoData" ).mask("99/99/9999");
            self.eventoDataPicker();
        });
        $('#btnEditarInfos').on('click', function(event) {
            event.preventDefault();
            var targetPage = $(this).attr('href');
            ClearApp.clear('#panelDashboard')
            renderPages.render(targetPage); 
        });
        $('.btn_popup_acao').on('click', function(event) {
            event.preventDefault();
            elemento = $(this);
            getAtt = $(this).attr("cp-popup-historico");
            PopUp.render(elemento,getAtt);
        });

        $('#enviarEmail').on('click', function(event) {
            event.preventDefault();
            businessAcoesParcelas.verificaCadastroEmailCli();
        });
       
        $('#enviarSms').on('click', function(event) {
            event.preventDefault();
            businessAcoesParcelas.verificaCadastroCelularCli();
        });
        /*Realizar Agendamento*/
        var agendarFunc         = function(){return self.realizarAgendamento()};
        var callBackAgendamento = function(){return self.retornoAgendamento()}

        $('#agendarPagamento').on('click', function(event) {
            event.preventDefault();
            var propsModal
            propsModal = {
                "elementoRaiz":elementoRaiz,
                "modalServico":true,
                "tipoModal":"acaodupla",
                "titulo":"Confirmação de Dados",
                "texto":"Deseja realmente Realizar o Agendamento?",
                "callback":callBackAgendamento,
                "funcao":agendarFunc
            }
            customModalSac.load(propsModal);
        });
        $('.icone_visualizar').on('click', function(event) {
            event.preventDefault();
            var elementoRaiz = $('.referenciaModalcard');
            var propsModal = {
                "elementoRaiz":elementoRaiz,
                "tipoModal":"modalPdf",
                "titulo":"Fatura",
                "texto":"Deseja realmente Salvar a Pendência ?"
            }
            modalEnvioEmailSms.load(propsModal)
        });
        $('#navegacaoTabCobranca').on('click', '.navegacaoTabCobranca', function(event) {
        	event.preventDefault();
        	var nomeTab = $(this).attr('href');
        	businessCobranca.montarConteudoTab(nomeTab)
        })
	    function hideTab(){
	        $('.left_box_grid').hide()
	        $('.left_box_grid').removeClass('showTab');
	    }
	    function ativacaoAutomatica(){
	        if($('#box_cobranca').hasClass('ATIVAR_TAB')){
	            hideTab();
	            $(".nav-link").removeClass('active');
	            $("#box_tab_acordo_parcelas").addClass('active');
	            $("#conteudo_tab_parcelas").show();
	        }
	    }
	}; 
}

if(eventCobranca == undefined || eventCobranca == null){
	var eventCobranca = new EventCobranca();
}

