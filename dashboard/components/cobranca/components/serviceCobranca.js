
/*
 * Classe utilizada para implementacao das funcoes de chamadas a servicos
 */
function ServiceCobranca() {
    if (!(this instanceof ServiceCobranca)){
        return new ServiceCobranca();
    }
    this.consultaOfertaAcordo = function(idProcesso,parameters){
    	console.log('consultaOfertaAcordo parameters',parameters)
    	this.servicoOfertaAcordo  = new GenericServiceV2('psfcobranca');
		this.getOfertaAcordo      = this.servicoOfertaAcordo.callPostService("acordo", "ofertaAcordo",idProcesso, parameters);
		return this.getOfertaAcordo;
    }

	this.consultaClienteCobranca = function(idProcesso,parameters){
		this.servicoConsultaCliCobranca  = new GenericServiceV2('psfcobranca');
		this.getConsultaCliCobranca      = this.servicoConsultaCliCobranca.callPostService("clienteCobranca", "consultaClienteCobranca",idProcesso, parameters);
		return this.getConsultaCliCobranca;
	};
	this.salvarAgendamentoCliente = function(idProcesso,parameters){
		this.servicoAgendamentoCliCobranca  = new GenericServiceV2('psfcobranca');
		this.getAgendamentoCliCobranca      = this.servicoAgendamentoCliCobranca.callPostService("clienteCobranca", "salvarClienteCobranca",idProcesso, parameters);
		return this.getAgendamentoCliCobranca;
	}

	this.efetivaOfertaAcordo = function(idProcesso,parameters){
		console.log('consultaOfertaAcordo parameters',parameters)
    	this.servicoEfetivaOfertaAcordo  = new GenericServiceV2('psfcobranca');
		this.getEfetivaOfertaAcordo      = this.servicoEfetivaOfertaAcordo.callPostService("acordo", "efetivaOfertaAcordo",idProcesso, parameters);
		return this.getEfetivaOfertaAcordo;
	}
	this.consultaParcelasAcordo = function(idProcesso,parameters){
		console.log('consultaParcelasAcordo parameters',parameters)
    	this.servicoConsultaParcelaAcordo  = new GenericServiceV2('psfcobranca');
		this.getParcelaAcordo      = this.servicoConsultaParcelaAcordo.callPostService("acordo", "efetivaOfertaAcordo",idProcesso, parameters);
		return this.getParcelaAcordo;
	}

	this.renegociaAcordo = function(idProcesso,parameters){
		console.log('renegociaAcordo parameters',parameters)
    	this.servicoRenegociaAcordo  = new GenericServiceV2('psfcobranca');
		this.getRenegociaAcordo      = this.servicoRenegociaAcordo.callPostService("acordo", "renegociaAcordo",idProcesso, parameters);
		return this.getRenegociaAcordo;
	}

	this.exibirBoletoAcordo = function(idProcesso,parameters){
		console.log('exibirBoletoAcordo parameters',parameters)
    	this.servicoExibirBoleto  = new GenericServiceV2('psfcobranca');
		this.getExibirBoleto      = this.servicoExibirBoleto.callPostService("acordo", "v2/exibirBoleto",idProcesso, parameters);
		return this.getExibirBoleto;
	}
	this.enviarBoletoAcordo = function(idProcesso,parameters){
		console.log('enviarBoletoAcordo parameters >>',parameters)
    	this.servicoEnviarBoleto  = new GenericServiceV2('psfcobranca');
		this.getEnviarBoleto      = this.servicoEnviarBoleto.callPostService("acordo", "v2/enviarBoleto",idProcesso, parameters);
		return this.getEnviarBoleto;
	}
	this.atualizarEmail = function(idProcesso,parameters){
		this.servicoAtualizaEmail  = new GenericService('psfclientservices');
		this.getAtualizaEmailCli      = this.servicoAtualizaEmail.callPostService("dadosCliente", "atualizarDadosCliente",idProcesso, parameters);
		return this.getAtualizaEmailCli;
	};

	this.atualizarCelularCli = function(idProcesso,parameters){
		this.servicoAtualizaCeluCli  = new GenericService('psfclientservices');
		this.getAtualizaCeluCli      = this.servicoAtualizaCeluCli.callPostService("dadosCliente", "atualizarDadosCliente",idProcesso, parameters);
		return this.getAtualizaCeluCli;
	};
	
	
	

	
	
	
	
}


if(serviceCobranca == undefined || serviceCobranca == null){
	var serviceCobranca = new ServiceCobranca();
}



