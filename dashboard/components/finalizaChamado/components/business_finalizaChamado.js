
require("components/finalizaChamado/components/template_finalizaChamado.js");

function BusinessFinalizaChamado() {
	this.load = function(elemPai,tituloClienteAtendido,tiposDeEnvioProc,listaLancamentosAtendimento,navAcoesFinalizaChamado){
		
		this.DataCurrentAtendimento 	 = controllStorageSac.getDataFlagAtendimento();
		this.flagPageFinalizar           = true;
		this.boxComponent 			     = elemPai;
		this.tituloClienteAtendido 		 = tituloClienteAtendido;
		this.tiposDeEnvioProc            = tiposDeEnvioProc;
		this.listaLancamentosAtendimento = listaLancamentosAtendimento;
		this.navAcoesFinalizaChamado     = navAcoesFinalizaChamado;

		// console.log('this.DataCurrentAtendimento',this.boxComponent,this.tituloClienteAtendido)
		// console.log('this.DataCurrentAtendimento',this.DataCurrentAtendimento.atendimentoInfos.idProtocolo)
		
		this.contrutorComponente();
	};
	this.contrutorComponente = function(){
		$(this.tituloClienteAtendido).html(templateFinalizaChamado.tituloClienteAtendido('2232322'));
		$(this.tiposDeEnvioProc).html(templateFinalizaChamado.tiposEnvioCliente());
		
		initAtendimento.load(this.listaLancamentosAtendimento);
		this.eventosComponent();
	}
	this.abilitarEnvio = function(conteudoParaEnviar){
		console.log('conteudoParaEnviar>>>abilitar',conteudoParaEnviar)
		var btnEnviarPorEmail = $('#btnEnviarPorEmail');
		var btnEnviarPorSMS   = $('#btnEnviarPorSMS');
		if(conteudoParaEnviar != 0 && conteudoParaEnviar!== "" ){
			abilitarBtn(btnEnviarPorSMS,btnEnviarPorEmail);
			this.getCamposParaEnvio();
			console.log('conteudoParaEnviar>>>abilitar2',conteudoParaEnviar)
		}else {
			desabilitarBtn(btnEnviarPorSMS,btnEnviarPorEmail);
			console.log('conteudoParaEnviar>>>abilitar3',conteudoParaEnviar)
		}
		function abilitarBtn(btnEnviarPorSMS,btnEnviarPorEmail){
			$(btnEnviarPorSMS).removeClass('disabled').attr('aria-disabled', 'false');
			$(btnEnviarPorEmail).removeClass('disabled').attr('aria-disabled', 'false');
		}
		function desabilitarBtn(btnEnviarPorSMS,btnEnviarPorEmail){
			$(btnEnviarPorSMS).addClass('disabled').attr('aria-disabled', 'true');
			$(btnEnviarPorEmail).addClass('disabled').attr('aria-disabled', 'true');
		}
	}

	this.getCamposParaEnvio = function(){
		this.telefoneClienteEnvio        	= $('#telefoneClienteEnvio');
		this.emailClienteEnvio				= $('#emailClienteEnvio');
	}
	this.validaEmailClienteParaEnvio = function(){
		var flagEmailOK = false;
		if(this.emailClienteEnvio.val() != ''){
			flagEmailOK = true;
			// if(IsEmail(this.emailClienteEnvio.val())){
			// 	flagEmailOK = true;	
			// }else {
			// 	flagEmailOK = false;
			// }
		}else{
			flagEmailOK = false;
		}
		return flagEmailOK;
	}
	this.validaFoneClienteParaEnvio = function(){
		var flagFoneOK = false;
		if(this.telefoneClienteEnvio.val() != ''){
			flagFoneOK = true;
		}else{
			flagFoneOK = false;
		}
		return flagDadosOK;
	}
	this.enviarProtocolosPorEmail = function(){
		if(this.emailClienteEnvio.val() != ''){
			console.log('enviarProtocolosPorEmail==============')
		}
		else{
			// marisaMessage.showDialog(marisaMessageType_WARNING, "Alerta","Preencha corretamente o campo de email");
		}
		
	}
	this.enviarProtocolosPorSMS = function(){
		if(this.validaFoneClienteParaEnvio()){
			console.log('enviarProtocolosPorSMS')
		}else{
			// marisaMessage.showDialog(marisaMessageType_WARNING, "Alerta","Preencha corretamente o campo de Telefone");
		}
	}

	this.eventosComponent = function(){
		var self = this;
		var checkAtualizarEmailCliente    = $('#checkAtualizarEmailCliente');
		var checkAtualizarTelefoneCliente = $('#checkAtualizarTelefoneCliente');
		$(this.boxComponent).on('click', '#btnInseirLog', function(event) {
			event.preventDefault();
		});
		$(this.boxComponent).on('change', '#selectTiposEnvio', function(event) {
			event.preventDefault();
			var EnviarVal = $(this).val();
			console.log('EnviarVal>>',EnviarVal)
			self.abilitarEnvio(EnviarVal)
		});
		$(this.boxComponent).on('click', '#btnEnviarPorEmail', function(event) {
			event.preventDefault();
			console.log('self.validaEmailClienteParaEnvio()',self.validaEmailClienteParaEnvio())
			var elementoRaiz = $('.referenciaModalcard');
			var enviarDados = function(){
               return self.enviarProtocolosPorEmail();
            }
            var naoenviarDados = function(){
               return false;
            }
            var propsModal = {
                "elementoRaiz":elementoRaiz,
                "tipoModal":"acaodupla",
                "titulo":"Confirmação de Dados",
                "texto":"Deseja realmente Salvar Estes Dados?",
                "funcao":enviarDados
            }
            
    		var msg = "Deseja realmente cancelar a operação?";
    		marisaMessage.questionDialog("Cadastro de Celular",msg, "Sim", "Não", enviarDados, naoenviarDados, marisaMessageType_WARNING);
    		
            marisaMessage.showDialog(marisaMessageType_WARNING, "Alerta","Preencha corretamente o campo de email")
            // customModalSac.load(propsModal)
			
		});
		$(this.boxComponent).on('click', '#btnEnviarPorSMS', function(event) {
			event.preventDefault();
			var elementoRaiz = $('.referenciaModalcard');
			var enviarDados = function(){
               return self.enviarProtocolosPorSMS();
            }
            var propsModal = {
                "elementoRaiz":elementoRaiz,
                "tipoModal":"acaodupla",
                "titulo":"Confirmação de Dados",
                "texto":"Deseja realmente Salvar Estes Dados?",
                "funcao":enviarDados
            }
            // customModalSac.load(propsModal)
		});
		$(this.boxComponent).on('change', checkAtualizarEmailCliente, function(event) {
			event.preventDefault();
			if(checkAtualizarEmailCliente.checked != false){
				var EmailCliente = $(this).val();
				console.log('EmailCliente>>',EmailCliente)
			}
			
		});
		$(this.boxComponent).on('change', checkAtualizarTelefoneCliente, function(event) {
			event.preventDefault();
			if(checkAtualizarTelefoneCliente.checked != false){
				var TelefoneCliente = $(this).val();
				console.log('TelefoneCliente>>',TelefoneCliente)
			}
			
			
		});

	};
	
}

var businessFinalizaChamado = new BusinessFinalizaChamado();