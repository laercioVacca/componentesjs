function TemplateFinalizaChamado() {
	this.tituloClienteAtendido = function(idProtocolo){
		var tmp;
		tmp  = '<ul class="list-group">';
        tmp += '<li class="list-group-item list-group-item-info">';
        tmp += 'Cliente Atendido - Protocolo n°<span class="destaque_protocolo">'+idProtocolo+'</span>';
        tmp += '</li>';
        tmp += '</ul>'
		return tmp;
	};
	this.tiposEnvioCliente = function(){
		var tmp;
		tmp  = '<div id="conteudo_" class="listaFullGrupo conteudo_dinamico_tabs left_box_grid">';
		tmp += this.estruturaEnvio();
		tmp += '</div>';
		return tmp;
	}
	this.listaLancamentosAtendimento = function(){
		var tmp;
		tmp  = '<div id="listaLancamentos" class="content_finalizacao left_box_grid">';
		tmp += '<ul class="list-group">';
		tmp += '<li class="list-group-item list-group-item-info">Observações</li>';
		tmp += '</ul>';
		tmp += '<div class="panel-collapse collapse show" role="tabpanel" aria-labelledby="headingcard.id">';
		tmp += '<div class="panel-body" style="visibility: visible;padding: 3px 0 3px 0;overflow: auto;">';
		tmp += '<form>';
		tmp += '<div class="form-group">';
		tmp += '<textarea class="textolancamentoEditar form-control" rows="5" id="campoTextoLogManual"></textarea>';       
		tmp += '</div>';
		tmp += '</form>';
		tmp += '</div>';
		tmp += '</div>';
		tmp += '</div>';
		tmp += '<div id="listaLancamentos" class="content_finalizacao left_box_grid">';
		tmp += '<ul class="list-group">';
		tmp += '<li class="list-group-item list-group-item-info">Lançamentos Atendimento</li>';
		tmp += '</ul>';
		tmp += '<div class="panel-collapse collapse show" role="tabpanel" aria-labelledby="headingcard.id">';
		tmp += '<div class="panel-body" style="visibility: visible;padding: 3px 0 3px 0;overflow: auto;">';

		tmp += '<table class="table table-striped">';
		tmp += '<thead>';
		tmp += '<tr>';
		tmp += '<th class="infos_header" scope="col">Data / Hora</th>';
		tmp += '<th class="infos_header" scope="col">Filial</th>';
		tmp += '<th class="infos_header" scope="col">Usuário</th>';
		tmp += '<th class="infos_header" scope="col">Observação</th>';
		tmp += '</tr>';
		tmp += '</thead>';
		tmp += '<tbody>';
		tmp += '<tr>';
		tmp += '<td class="infostxt"><a href="#"> 17/08/2018 15:41:57</a></td>';
		tmp += '<td class="infostxt">900 </td>';
		tmp += '<td class="infostxt">SANDRA MARIA BOMBARDI </td>';
		tmp += '<td class="infostxt">SEGURO MARISA ODONTO PLUS VENDIDO NESTA DATA </td>';
		tmp += '</tr>';
		tmp += '<tr>';
		tmp += '<td class="infostxt"><a href="#">17/08/2018 15:41:57</a></td>';
		tmp += '<td class="infostxt">900 </td>';
		tmp += '<td class="infostxt">CCM - ADMINISTRADOR</td>';
		tmp += '<td class="infostxt">PAGTO DE FATURA VIA BOLETO NOSSO N: 2000282470302 VLR R$ 172,09 CED...</td>';
		tmp += '</tr>';
		tmp += '<tr>';
		tmp += '<td class="infostxt"><a href="#">17/08/2018 15:41:57</a></td>';
		tmp += '<td class="infostxt">900 </td>';
		tmp += '<td class="infostxt">CCM - ADMINISTRADOR</td>';
		tmp += '<td class="infostxt">PROTOCOLO #107334024 - dados ok// cli sol inf de parc de fat ciente...</td>';
		tmp += '</tr>';
		tmp += '</tbody>';
		tmp += '</table>';
		tmp += '</div>';
		tmp += '</div>';
		tmp += '</div>';
		return tmp;
	}
	this.acaoFinalizaChamado = function(){
		var tmp;
			tmp = '<div class="nav_page_infos_cadastrais col-xs-12 col-sm-12 col-md-12 col-lg-12">';
			tmp += '<a id="btnFinalizar" href="#" data-toggle="modal" class="btn_page_infos btn btn-danger">';
			tmp += '<span class="fs1" data-icon="&#xe0fd;"> FINALIZAR</span>';
			tmp += '</a>';
			tmp += '<a id="btnProtocoloAberto" href="#" data-toggle="modal" class="btn_page_infos  btn btn-success">';
			tmp += '<span class="fs1" data-icon="&#xe0ff;"> MANTER PROTOCOLO EM ABERTO</span>';
			tmp += '</a>';
			tmp += '</div>';
		return tmp;
	}


	this.estruturaEnvio = function(){
		var retornoHtml;

		

		retornoHtml  = templateSelectEnvioProtocolo();
		// retornoHtml += templateSelectNotificacaoEncerramento();
		// retornoHtml += templateSelectEnvioHistorico();
		// retornoHtml += templateSelectEnvioCancelamento();

		

		retornoHtml += templateInputEmail();
		retornoHtml += templateInputTelefone();
		

		// Template Select envio de protocolo
		function templateSelectEnvioProtocolo(){
			var tmpSelect01;
				tmpSelect01  = '<div class="list-blocos-group lista_grid_02 full_grid">'; 
				tmpSelect01 += '<div class="alt_din list-group-item list-group-item-primary" >';
				tmpSelect01 += '<span class="item_lista_titulo contentFormDropTitulo">Meio de envio</span>';
				tmpSelect01 += '<span class="item_lista_valor contentFormDrop">';
				tmpSelect01 += '<div class="form-group">';
				tmpSelect01 += '<select id="selectTiposEnvio" class="form-control select_drop">';
				tmpSelect01 += '<option value="0" selected>Escolha</option>';
				tmpSelect01 += '<option value="protocolo">Protocolo</option>';
				tmpSelect01 += '<option value="notificacaoEncerramento">Notificaçao do Encerramento</option>';
				tmpSelect01 += '<option value="historico">Histórico</option>';
				tmpSelect01 += '<option value="comprovantesCancelamento">Comprovantes de Cancelamento</option>';
				tmpSelect01 += '</select>';
				// tmpSelect01 += '<button type="submit" class="btn btn-primary">Enviar</button>';
				tmpSelect01 += '</div>';
				tmpSelect01 += '</span>';
				tmpSelect01 += '<div class="lista_grupo_clear"></div>';
				tmpSelect01 += '</div>';
				tmpSelect01 += '</div>';
			return tmpSelect01;
		}
		// Template Select Notificação encerramento
		function templateSelectNotificacaoEncerramento(){
			var tmpSelect02;
				tmpSelect02  = '<div class="list-blocos-group lista_grid_02 full_grid">'; 
				tmpSelect02 += '<div class="alt_din list-group-item list-group-item-primary">';
				tmpSelect02 += '<span class="item_lista_titulo contentFormDropTitulo">Meio de notificaçao do encerramento</span>';
				tmpSelect02 += '<span class="item_lista_valor contentFormDrop">';
				tmpSelect02 += '<div class="form-group">';
				tmpSelect02 += '<select id="inputState" class="form-control select_drop">';
				tmpSelect02 += '<option selected>Escolha</option>';
				tmpSelect02 += '<option value="telefone">Telefone</option>';
				tmpSelect02 += '<option value="email">E-mail</option>';
				tmpSelect02 += '<option value="correio">Correio</option>';
				tmpSelect02 += '<option value="fax">Fax</option>';
				tmpSelect02 += '<option value="sms">SMS</option>';
				tmpSelect02 += '</select>';
				tmpSelect02 += '<button type="submit" class="btn btn-primary">Enviar</button>';
				tmpSelect02 += '</div>';
				tmpSelect02 += '</span>';
				tmpSelect02 += '<div class="lista_grupo_clear"></div>';
				tmpSelect02 += '</div>';
				tmpSelect02 += '</div>';
			return tmpSelect02;
		}
		// Template Select meio Envio Historico
		function templateSelectEnvioHistorico(){
			var tmpSelect02;
				tmpSelect02  = '<div class="list-blocos-group lista_grid_02 full_grid">'; 
				tmpSelect02 += '<div class="alt_din list-group-item list-group-item-primary">';
				tmpSelect02 += '<span class="item_lista_titulo contentFormDropTitulo">Meio de envio do histórico</span>';
				tmpSelect02 += '<span class="item_lista_valor contentFormDrop">';
				tmpSelect02 += '<div class="form-group">';
				tmpSelect02 += '<select id="inputState" class="form-control select_drop">';
				tmpSelect02 += '<option selected>Escolha</option>';
				tmpSelect02 += '<option value="email">E-mail</option>';
				tmpSelect02 += '<option value="correio">Correio</option>';
				tmpSelect02 += '<option value="fax">Fax</option>';
				tmpSelect02 += '</select>';
				tmpSelect02 += '<button type="submit" class="btn btn-primary">Enviar</button>';
				tmpSelect02 += '</div>';
				tmpSelect02 += '</span>';
				tmpSelect02 += '<div class="lista_grupo_clear"></div>';
				tmpSelect02 += '</div>';
				tmpSelect02 += '</div>';
			return tmpSelect02;
		}
		// Template Select Meio de envio de comprovantes de cancelamento
		function templateSelectEnvioCancelamento(){
			var tmpSelect02;
				tmpSelect02  = '<div class="list-blocos-group lista_grid_02 full_grid">'; 
				tmpSelect02 += '<div class="alt_din list-group-item list-group-item-primary">';
				tmpSelect02 += '<span class="item_lista_titulo contentFormDropTitulo">Meio de envio de comprovantes de cancelamento</span>';
				tmpSelect02 += '<span class="item_lista_valor contentFormDrop">';
				tmpSelect02 += '<div class="form-group">';
				tmpSelect02 += '<select id="inputState" class="form-control select_drop">';
				tmpSelect02 += '<option selected>Escolha</option>';
				tmpSelect02 += '<option value="email">E-mail</option>';
				tmpSelect02 += '<option value="correio">Correio</option>';
				tmpSelect02 += '<option value="fax">Fax</option>';
				tmpSelect02 += '</select>';
				tmpSelect02 += '<button type="submit" class="btn btn-primary">Enviar</button>';
				tmpSelect02 += '</div>';
				tmpSelect02 += '</span>';
				tmpSelect02 += '<div class="lista_grupo_clear"></div>';
				tmpSelect02 += '</div>';
				tmpSelect02 += '</div>';
			return tmpSelect02;
		}
		// Template Select Meio de envio de comprovantes de cancelamento
		function templateInputEmail(){
			var tmpSelect02;
				tmpSelect02  = '<div class="list-blocos-group lista_grid_02 full_grid">'; 
				tmpSelect02 += '<div class="alt_din list-group-item itemEmail list-group-item-primary">';
				tmpSelect02 += '<span class="item_lista_titulo contentFormDropTitulo">Email</span>';
				tmpSelect02 += '<span class="item_lista_valor contentFormDrop">';
				tmpSelect02 += '<div class="form-group">';
				tmpSelect02 += '<input type="email" class="form-control input_drop" id="emailClienteParaEnvio" placeholder="Email">';
				tmpSelect02 += '<div class="checkbox"><label><input class="checkEmail" id="checkAtualizarEmailCliente" type="checkbox" value="">Atualizar Cadastro</label></div>';
				tmpSelect02 += '<a id="btnEnviarPorEmail" href="#" class="sidebar_link btn btn-success disabled btn" role="button" aria-disabled="true"><span class="glyphicon glyphicon-ok"> ENVIAR</span></a>';				
				tmpSelect02 += '</div>';
				tmpSelect02 += '</span>';
				tmpSelect02 += '<div class="lista_grupo_clear"></div>';
				tmpSelect02 += '</div>';
				tmpSelect02 += '</div>';
			return tmpSelect02;
		}
		// Template Input Telefone
		function templateInputTelefone(){
			var tmpSelect02;
				tmpSelect02  = '<div class="list-blocos-group lista_grid_02 full_grid">'; 
				tmpSelect02 += '<div class="alt_din list-group-item itemEmail list-group-item-primary">';
				tmpSelect02 += '<span class="item_lista_titulo contentFormDropTitulo">Telefone</span>';
				tmpSelect02 += '<span class="item_lista_valor contentFormDrop">';
				tmpSelect02 += '<div class="form-group">';
				tmpSelect02 += '<input type="text" class="form-control input_drop" id="telefoneClienteParaEnvio" placeholder="Telefone">';
				tmpSelect02 += '<div class="checkbox"><label><input class="checkEmail" id="checkAtualizarTelefoneCliente" type="checkbox" value="">Atualizar Telefone</label></div>';

				tmpSelect02 += ' <a id="btnEnviarPorSMS" href="#" class="sidebar_link btn btn-success disabled btn" role="button" aria-disabled="true"><span class="glyphicon glyphicon-ok"> ENVIAR</span></a>';
				// tmpSelect02 += '<a id="btnEnviarPorSMS" href="#" class="btn btn-secondary btn-lg disabled" role="button" aria-disabled="true"><span class="glyphicon glyphicon-ok"> ENVIAR</span></a>';



				tmpSelect02 += '</div>';
				tmpSelect02 += '</span>';
				tmpSelect02 += '<div class="lista_grupo_clear"></div>';
				tmpSelect02 += '</div>';
				tmpSelect02 += '</div>';
			return tmpSelect02;
		}

		// Template para Listagem de assuntos por chamados
		function templateListaAssuntosChamados(identificador){
			var retornoAssuntos;
			// box assuntos final
			retornoAssuntos = '<div id="listaAssuntosDetalheChamado">';
			retornoAssuntos += '<div class="list-blocos-group lista_grid_02 full_grid">';
				retornoAssuntos += '<a class="btn_drop_assuntos" href="#" data-target="#assuntos_'+identificador+'" aria-expanded="true" aria-controls="assuntos_'+identificador+'">'; 
					retornoAssuntos += '<div class="alt_din list-group-item list-group-item-primary titulo_divisor">';
						retornoAssuntos += '<span class="item_lista_titulo">1º Assunto</span>';
						retornoAssuntos += '<div class="lista_grupo_clear"></div>';
					retornoAssuntos += '</div>';
				retornoAssuntos += '</a>';
			retornoAssuntos += '</div>';
			retornoAssuntos += '<div id="assuntos_'+identificador+'" class="collapse " aria-labelledby="headingOne2" data-parent="#accordion">';
				// 01
				retornoAssuntos += '<div class="list-blocos-group lista_grid_02 full_grid"> ';
					retornoAssuntos += '<div class="alt_din list-group-item list-group-item-primary" >';
						retornoAssuntos += '<span class="item_lista_titulo ">Tipo</span>';
						retornoAssuntos += '<span class="item_lista_valor ">RENEGOCIACAO DE PROMESSA OU ACORDO</span>';
						retornoAssuntos += '<div class="lista_grupo_clear"></div>';
					retornoAssuntos += '</div>';
				retornoAssuntos += '</div>';
				// 02
				retornoAssuntos += '<div class="list-blocos-group lista_grid_02 full_grid"> ';
					retornoAssuntos += '<div class="alt_din list-group-item list-group-item-primary" >';
						retornoAssuntos += '<span class="item_lista_titulo ">Status</span>';
						retornoAssuntos += '<span class="item_lista_valor ">RESOLVIDO</span>';
						retornoAssuntos += '<div class="lista_grupo_clear"></div>';
					retornoAssuntos += '</div>';
				retornoAssuntos += '</div>';
				// 03
				retornoAssuntos += '<div class="list-blocos-group lista_grid_02 full_grid"> ';
					retornoAssuntos += '<div class="alt_din list-group-item list-group-item-primary" >';
						retornoAssuntos += '<span class="item_lista_titulo ">Prazo de solução</span>';
						retornoAssuntos += '<span class="item_lista_valor ">Imediato</span>';
						retornoAssuntos += '<div class="lista_grupo_clear"></div>';
					retornoAssuntos += '</div>';
				retornoAssuntos += '</div>';
				// 04
				retornoAssuntos += '<div class="list-blocos-group lista_grid_02 full_grid">'; 
					retornoAssuntos += '<div class="alt_din list-group-item list-group-item-primary titulo_divisor titulo_divisor_textarea" >';
						retornoAssuntos += '<span class="item_lista_titulo">Observação</span>';
						retornoAssuntos += '<div class="lista_grupo_clear"></div>';
					retornoAssuntos += '</div>';
				retornoAssuntos += '</div>';
				// 05
				retornoAssuntos += '<div class="list-blocos-group lista_grid_02 full_grid">'; 
					retornoAssuntos += '<div class="alt_din list-group-item list-group-item-primary titulo_divisor" >';
						retornoAssuntos += '<span class="item_lista_titulo">';
							retornoAssuntos += '<textarea class="form-control" id="exampleTextarea" rows="3"></textarea>';
						retornoAssuntos += '</span>';
						retornoAssuntos += '<div class="lista_grupo_clear"></div>';
					retornoAssuntos += '</div>';
				retornoAssuntos += '</div>';
			retornoAssuntos += '</div>';
			retornoAssuntos += '</div>';
			// box assuntos final
			// retornoAssuntos += '</div>';
			// retornoAssuntos += '</div>';
			// retornoAssuntos += '</div>';
			// retornoAssuntos += '</div>';
			return retornoAssuntos;
		}


		return retornoHtml;
	};
	this.semInfos = function(){
		var tmp;
		tmp  = '<div style="text-align: center;" class="alert alert-danger" role="alert">';
  		tmp += 'Sem Chamados Anteriores!';
		tmp += '</div>';
		return tmp;
	}

	

	

}
var templateFinalizaChamado = new TemplateFinalizaChamado();