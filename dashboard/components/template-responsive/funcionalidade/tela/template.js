require("../../../../resources/js/marisaServices.js");
require("../../../../resources/js/marisaMessages.js");

var registro;

function require(script) {
	$.ajax({
		url : script,
		dataType : "script",
		async : false, // <-- This is the key
		success : function() {
			// all good...
		},
		error : function() {
			throw new Error("Could not load script " + script);
		}
	});
}

var exclusaoConfirmada = function() {
	//console.log("Callback exclusaoConfirmada");
	marisaMessage.showDialog(marisaMessageType_SUCCESS, "Sucesso",
			"O registro \'" + registro + "\' foi excluido com sucesso!");
}

var exclusaoCancelada = function() {
	//console.log("Callback exclusaoCancelada");
	// marisaMessage.showDialog(marisaMessageType_INFO,"Teste", "O registro
	// \'"+registro+"\' foi não foi excluido!");
}

function confirmarExclusao(reg) {
	registro = reg;
	var resp = marisaMessage.questionDialog("Excluir",
			"Deseja realmente excluir o registro \'" + reg + "\'?", "Sim",
			"Não", exclusaoConfirmada, exclusaoCancelada);
}

$(document).ready(function() {    
	
	var val = {
			  01: {
			    id: 01,
			    text: 'Isis'
			  },
			  02: {
			    id: 02,
			    text: 'Sophia'
			  },
			  03: {
			    id: 03,
			    text: 'Alice'
			  },
			  04: {
			    id: 04,
			    text: 'Isabella'
			  },
			  05: {
			    id: 05,
			    text: 'Manuela'
			  }
			};	

	var pick = $("#pickList").pickList({
	  data: val
	});

	var pick2 = $("#pickList2").pickList({
	  data: val
	});

	$("#getSelected").click(function() {
	  //console.log(pick.getValues());
	});
	
	$("#getSelected2").click(function() {
	  //console.log(pick2.getValues());
	});
});

(function($) {

	  $.fn.pickList = function(options) {
	    var opts = $.extend({}, $.fn.pickList.defaults, options);
	    this.fill = function() {
	      var option = '';
	      $.each(opts.data, function(key, val) {
	        option += '<option data-id=' + val.id + '>' + val.text + '</option>';
	      });
	      this.find('.pickData').append(option);
	    };
	    this.controll = function() {
	      var pickThis = this;
	      pickThis.find(".pAdd").on('click', function() {
	        var p = pickThis.find(".pickData option:selected");
	        p.clone().appendTo(pickThis.find(".pickListResult"));
	        p.remove();
	      });
	      pickThis.find(".pAddAll").on('click', function() {
	        var p = pickThis.find(".pickData option");
	        p.clone().appendTo(pickThis.find(".pickListResult"));
	        p.remove();
	      });
	      pickThis.find(".pRemove").on('click', function() {
	        var p = pickThis.find(".pickListResult option:selected");
	        p.clone().appendTo(pickThis.find(".pickData"));
	        p.remove();
	      });
	      pickThis.find(".pRemoveAll").on('click', function() {
	        var p = pickThis.find(".pickListResult option");
	        p.clone().appendTo(pickThis.find(".pickData"));
	        p.remove();
	      });
	    };
	    this.getValues = function() {
	      var objResult = [];
	      this.find(".pickListResult option").each(function() {
	        objResult.push({
	          id: $(this).data('id'),
	          text: this.text
	        });
	      });
	      return objResult;
	    };
	    this.init = function() {
	      var pickListHtml = "";
	      this.append(pickListHtml);
	      this.fill();
	      this.controll();
	    };
	    this.init();
	    return this;
	  };

	  $.fn.pickList.defaults = {
// add: 'Add',
// addAll: 'Add All',
// remove: 'Remove',
// removeAll: 'Remove All'
	  };


	}(jQuery));


	

