function GravarDadosClienteDash(){
	this.storageClienteDash = function(obj){
		this.dadosCliDash = obj
		this.currentCpfCliente = this.dadosCliDash.cpf;
		localStorage.setItem('dadosDash',JSON.stringify(this.montaObjCurrentCliente(this.dadosCliDash)));
	};
	this.montaObjCurrentCliente = function(obj){
		var self = this;
		var dadosCliCurrent = {
			"nome":obj.nome,
			"cpf":obj.cpf,
			"situacaoConta":obj.situacaoConta ,
			"situacaoContaCod":obj.situacaoContaCod ,
			"numeroCartao":obj.numeroCartao ,
			"situacaoCartao":obj.situacaoCartao,
			"situacaoCartaoCod":obj.situacaoCartaoCod,
			"situacaoEmbossamentoCod":obj.situacaoEmbossamentoCod,
			"buscaPorAdicional":obj.buscaPorAdicional,
			"endereco":obj.endereco,
			"cidadeUf":obj.cidadeUf,
			"celular":obj.celular,
			"email":obj.email,
			"dtNasc":obj.dtNasc,
			"sexo":obj.sexo,
			"cliResDdd":obj.cliResDdd,
			"cliResFone":obj.cliResFone,
			"cliIdentidade":obj.cliIdentidade,
			"cliIdeOrgaoEmi":obj.cliIdeOrgaoEmi,
			"cliIdeUfEmi":obj.cliIdeUfEmi,
			"cliEmpSalario":obj.cliEmpSalario,
			"inativo":obj.inativo,
			"diasCarenciaReanalise":obj.diasCarenciaReanalise,
			"ultimoProtocolo":this.setUltimoAtendimento(),
		}
		
		return dadosCliCurrent;
	}
	this.dadosDash = function(){
		this.dadosStorage = JSON.parse(localStorage.getItem('dadosDash'));
		return this.dadosStorage
	}
	this.getUltimoAtendimento = function(ultimoProtocolo){
		console.log('this.ultimoProtocolo ->',this.ultimoProtocolo)
		this.ultimoProtocolo = ultimoProtocolo;
	}
	this.setUltimoAtendimento = function(){
		
		return this.ultimoProtocolo;
	}
}
var gravarDadosClienteDash = new GravarDadosClienteDash();