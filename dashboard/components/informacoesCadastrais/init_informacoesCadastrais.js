require("components/informacoesCadastrais/components/business_informacoesCadastrais.js");
/*
	this.load() - chama o business de infos Cadastrais
	this.getCamposForm() - Pega todos os campos dos form e manda como paramentro para a business .
*/

function InitInformacoesCadastrais() {
	this.load = function(boxComponente){
		businessInformacoesCadastrais.load(boxComponente,this.getCamposForm());
	}
	this.getCamposForm = function(){
		var formGrupo = {
		        "formGrupo01":{
		            "GRU01_nome":$('#GRU01_nome'),
		            "GRU01_cpf":$('#GRU01_cpf'),
		            "GRU01_tipoConta":$('#GRU01_tipoConta'),
		            "GRU01_numeroCartao":$('#GRU01_numeroCartao'),
		            "GRU01_rg":$('#GRU01_rg'),
		            "GRU01_emissor":$('#GRU01_emissor'),   
		            "GRU01_uf":$('#GRU01_uf'),
		            "GRU01_dataNascimento":$('#GRU01_dataNascimento'),
		            "GRU01_radioSexo":$('#GRU01_radioSexo'),
		            "GRU01_selectEstadoCivil":$('#GRU01_selectEstadoCivil'),
		            "GRU01_nomeMae":$('#GRU01_nomeMae'),
		            "GRU01_nomePai":$('#GRU01_nomePai'),
		            "GRU01_dddCelular01":$('#GRU01_dddCelular01'),
		            "GRU01_numeroCelular01":$('#GRU01_numeroCelular01'),
		            "GRU01_dddCelular02":$('#GRU01_dddCelular02'),
		            "GRU01_numeroCelular02":$('#GRU01_numeroCelular02'),
		            "GRU01_dependentes":$('#GRU01_dependentes'),
		            "GRU01_email":$('#GRU01_email'),
		        },
		        "formGrupo02":{
		            "GRU02_nomeReferencia":$('#GRU02_nomeReferencia'),
		            "GRU02_apelidoRef01":$('#GRU02_apelidoRef01'),
		            "GRU02_dddTipoFoneRef01":$('#GRU02_dddTipoFoneRef01'),
		            "GRU02_numeroTipoFoneRef01":$('#GRU02_numeroTipoFoneRef01'),
		            "GRU02_selectParentescoRef01":$('#GRU02_selectParentescoRef01'),
		            "GRU02_nomeReferencia02":$('#GRU02_nomeReferencia02'),
		            "GRU02_apelidoRef02":$('#GRU02_apelidoRef02'),
		            "GRU02_dddTipoFoneRef02":$('#GRU02_dddTipoFoneRef02'),
		            "GRU02_numeroTipoFoneRef02":$('#GRU02_numeroTipoFoneRef02'),
		            "GRU02_selectParentescoRef02":$('#GRU02_selectParentescoRef02'),
		        },
		        "formGrupo03":{
		            "GRU03_cepInfoResidencia":$('#GRU03_cepInfoResidencia'),
		            "GRU03_estado":$('#GRU03_estado'),
		            "GRU03_numero":$('#GRU03_numero'),
		            "GRU03_complemento":$('#GRU03_complemento'),
		            "GRU03_bairro":$('#GRU03_bairro'),
		            "GRU03_cidade":$('#GRU03_cidade'),
		            "GRU03_uf":$('#GRU03_uf'),
		            "GRU03_mesResidencia":$('#GRU03_mesResidencia'),
		            "GRU03_anosResidencia":$('#GRU03_anosResidencia'),
		            "GRU03_selectTiporesidencia":$('#GRU03_selectTiporesidencia'),
		            "GRU03_radioTipoFone":$("input[name='GRU03_radioTipoFone']:checked"),
		            "GRU03_dddTipoFone":$('#GRU03_dddTipoFone'),
		            "GRU03_numeroTipoFone":$('#GRU03_numeroTipoFone'),
		            "GRU03_selectTipoFone02":$('#GRU03_selectTipoFone02'),
		            "GRU03_dddCelular01":$('#GRU03_dddCelular01'),
		            "GRU03_celular01":$("#GRU03_celular01"),
		            "GRU03_dddCelular02":$("#GRU03_dddCelular02"),
		            "GRU03_celular02":$("#GRU03_celular02"),
		            "GRU03_dddFoneRecado":$("#GRU03_dddFoneRecado"),
		            "GRU03_recadoFalarCom":$("#GRU03_recadoFalarCom"),
		        },
		        "formGrupo04":{
		            "GRU04_empresa":$('#GRU04_empresa'),
		            "GRU04_departamento":$('#GRU04_departamento'),
		            "GRU04_atividade":$('#GRU04_atividade'),
		            "GRU04_selectProfissional":$('#GRU04_selectProfissional'),
		            "GRU04_rua" :$('#GRU04_rua'),
		            "GRU04_cep":$('#GRU04_cep'),
		            "GRU04_estado":$('#GRU04_estado'),
		            "GRU04_bairro":$('#GRU04_bairro'),
		            "GRU04_cidade":$('#GRU04_cidade'),
		            "GRU04_radioTipoFone":$('#GRU04_radioTipoFone'),
		            "GRU04_dddTipoFone":$('#GRU04_dddTipoFone'),
		            "GRU04_numeroTipoFone":$('#GRU04_numeroTipoFone'),
		            "GRU04_rendaMensal":$('#GRU04_rendaMensal'),
		            "GRU04_outrasRendas":$('#GRU04_outrasRendas'),
		            "GRU04_origemRenda":$('#GRU04_origemRenda'),
		            "GRU04_complemento":$('#GRU04_complemento'),
		            "GRU04_numero":$('#GRU04_numero'),
		            "GRU04_cgcCnpjEmpresa":$('#GRU04_cgcCnpjEmpresa'),
		            "GRU04_selectDataAberturaEmpresaMes":$('#GRU04_selectDataAberturaEmpresaMes'),
		            "GRU04_selectDataAberturaEmpresaAno":$('#GRU04_selectDataAberturaEmpresaAno'),
		            "GRU04_selectTempoEmpresaAnos":$('#GRU04_selectTempoEmpresaAnos'),
		            "GRU04_selectTempoEmpresaMeses":$('#GRU04_selectTempoEmpresaMeses'),
		        },
		        "formGrupo05":{
		            
		            
		            "GRU05_selectBanco" :$('#GRU05_selectBanco'),
		            "GRU05_possuiCrediario" :$('#GRU05_possuiCrediario'),
		            "GRU05_qualLoja" :$('#GRU05_qualLoja'),
		            "GRU05_possuiOutrosCartoes" :$('#GRU05_possuiOutrosCartoes'),
		            
		            
		        }
    		}
    	return formGrupo;
	}
}
var initInformacoesCadastrais = new InitInformacoesCadastrais();


