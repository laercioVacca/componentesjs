function InitManipuladorDom(){
    this.load = function(formGrupo){
        // popula selects
        this.residenciaAnos(formGrupo.formGrupo03.GRU03_anosResidencia);
        this.residenciaMeses(formGrupo.formGrupo03.GRU03_mesResidencia);
        this.tempoEmpresaAnos(formGrupo.formGrupo04.GRU04_selectTempoEmpresaAnos);
        this.tempoEmpresaMeses(formGrupo.formGrupo04.GRU04_selectTempoEmpresaMeses);
        this.aberturaEmpresaAnos(formGrupo.formGrupo04.GRU04_selectDataAberturaEmpresaAno);
        this.aberturaEmpresaMeses(formGrupo.formGrupo04.GRU04_selectDataAberturaEmpresaMes);
        this.profissional(formGrupo.formGrupo04.GRU04_selectProfissional);
    }
    this.residenciaAnos = function(elmSelect){
        for (var i = 0; i < 100; i++) {
            $(elmSelect).append('<option value="'+i+'">'+i+' anos</option>');
        }
    };
    this.residenciaMeses = function(elmSelect){
        for (var i = 0; i < 13; i++) {
            $(elmSelect).append('<option value="'+i+'">'+i+' Meses</option>');
        }
    };
    this.profissional = function(elmSelect){
        for (var i = 0; i < 13; i++) {
            $(elmSelect).append('<option value="'+i+'">'+i+' Meses</option>');
        }
    }
    this.atividade = function(elmSelect){
        for (var i = 0; i < 13; i++) {
            $(elmSelect).append('<option value="'+i+'">'+i+' Meses</option>');
        }
    };
    this.tempoEmpresaAnos = function(elmSelect){
        for (var i = 0; i < 100; i++) {
            $(elmSelect).append('<option value="'+i+'">'+i+' anos</option>');
        }
    };
    this.tempoEmpresaMeses = function(elmSelect){
        for (var i = 0; i < 13; i++) {
            $(elmSelect).append('<option value="'+i+'">'+i+' Meses</option>');
        }
    };
    this.aberturaEmpresaAnos = function(elmSelect){
        for (var i = 0; i < 100; i++) {
            $(elmSelect).append('<option value="'+i+'">'+i+' anos</option>');
        }
    };
    this.aberturaEmpresaMeses = function(elmSelect){
        for (var i = 0; i < 13; i++) {
            $(elmSelect).append('<option value="'+i+'">'+i+' Meses</option>');
        }
    };
   
}

var initManipuladorDom     = new InitManipuladorDom();