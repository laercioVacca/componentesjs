function HelperValidacao(){
    this.nomes = function (grup,valor,currentInput) {
        var flagNome = false;
        if (valor != ''){
            var arrayValor = valor.split(" ");
            if (arrayValor.length >= 2){
                flagNome = true;
            } 
            else {
                flagNome = false;
            }
        }
        this.controleTabsErro(flagNome,grup,currentInput);
        return flagNome;
    };
    this.rg = function(grup,valor,currentInput){
        var flagRg = false;
        if (valor != ''){
            flagRg = true;
        }
        this.controleTabsErro(flagRg,grup,currentInput);
        return true;
    }
    this.dataNascimento = function(grup,valorDataNasc,currentInput){
        var flagDataNasc = false;
        if (valorDataNasc != '') {
            flagDataNasc = this.tratarIdade(valorDataNasc);
        }
        this.controleTabsErro(flagDataNasc,grup,currentInput);
        return flagDataNasc;
    };
    this.cep = function(grup,valorCampo,currentInput){
        var flagCep;
        console.log('valorCampo&*****',valorCampo,grup,currentInput)
        if(valorCampo != ''){
            flagCep = true;
        }else{
            flagCep = false;
        }
        this.controleTabsErro(flagCep,grup,currentInput);
        return flagCep;
    }
    this.generica = function(grup,valorCampo,currentInput){
        var flagGenerica = false;
        if(valorCampo != ''){
            flagGenerica = true;
        }
        this.controleTabsErro(flagGenerica,grup,currentInput);
        return flagGenerica;
    }
    this.validaSelect = function(grup,valorCampo,currentInput){
        console.log('currentInput valorCampo>>> ',currentInput,valorCampo)
        var flagValidaSelect = false;
            $(currentInput).change(function(event) {
                console.log('OOOOOOOPPPPPPPAAAAA',$(this).val())
                if($(this).val() != ''){
                    flagValidaSelect = true;
                }
            });
            
        
        
        this.controleTabsErro(flagValidaSelect,grup,currentInput);
        return flagValidaSelect;
    }
    this.controleTabsErro = function(flagErro,currentTab,currentInput){
        var erroGrupo = "grup_"+currentTab;
        if(!flagErro){
           $(currentInput[0]).addClass(erroGrupo);
        }else{
            $(currentInput[0]).removeClass(erroGrupo);
        }
    };
    this.showModalAviso = function(texto){
        var msg = marisaMessage.questionDialog("Confirmação de Cadastro",texto, "OPA", "Não", funcOk, abortaPerguntaOnly);
        var funcOk = this.confirmaEdicaoCadastro();
        return msg
    };
    this.tempoResidenciaAnos = function(anos){
        console.log('tempoResidencia####()',anos)
    };
    this.tempoResidenciaMeses = function(meses){
        console.log('tempoResidencia###()',meses)
    }
    this.confirmaEdicaoCadastro = function(){
        console.log('Grava!!!!')
    };
    this.tratarIdade = function(data){
        var idade = this.calcularIdade(data);
        if (idade >= 16 && idade <= 80 ) {
            return true;
        } 
        if (idade <= 15 || idade > 80) {           
            return false;
        }
    };
    this.calcularIdade = function(data){
        hoje = new Date();
        var i = 0;
        var array_data = data.split("/");
        if (array_data.length != 3) {
            return false
        }
        var ano
        ano = parseInt(array_data[2]);
        if (isNaN(ano)){
            return false;
        }
        var mes
        mes = parseFloat(array_data[1]);
        if (isNaN(mes)){
            return false
        }
        var dia
        dia = parseFloat(array_data[0]);
        if (isNaN(dia)){
            return false
        }
        if (ano <= 99) {
            ano += 1900
        }
        //subtraio os anos das duas datas
        idade = hoje.getYear() + 1900 - ano - 1; //-1 porque ainda nao fez anos durante este ano

        //se subtraio os meses e for menor que 0 entao nao cumpriu anos. Se for maior sim ja cumpriu
        if (hoje.getMonth() + 1 - mes < 0) { //+ 1 porque os meses comecam em 0
            return idade;
        }
        if (hoje.getMonth() + 1 - mes >= 0) {
            if (hoje.getMonth() + 1 - mes > 0) {
                return idade + 1;
            } else if (hoje.getUTCDate() >= dia) {
                return idade + 1;
            }
        }
        //entao eh porque sao iguais. Vejo os dias
        //se subtraio os dias e der menor que 0 entao nao cumpriu anos. Se der maior ou igual sim que jï¿½ cumpriu
        return idade;
    }

    
}


var helperValidacao            = new HelperValidacao();