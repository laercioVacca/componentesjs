require("components/informacoesCadastrais/components/helperValidacao.js");
require("components/informacoesCadastrais/components/manipuladorDom.js");
require("components/informacoesCadastrais/components/service_informacoesCadastrais.js");

function BusinessInformacoesCadastrais() {
	this.load = function(boxComponent,formGrupo){
		this.boxComponent = boxComponent;
		this.formGrupo 	  = formGrupo; //Objeto contendo todos os campos do formulario objeto completo
		this.eventosComponent();
		initManipuladorDom.load(this.formGrupo);
	};
	this.validarAnosResidencia = function(anosMorando){
		console.log('this.anosResidencia',anosMorando)
	};
	this.validarMesesResidencia = function(mesesMorando){
		console.log('this.MesesResidencia', mesesMorando  )
	};
	this.opcoesValidadorForm = function(){
		var validatorOptions = {
	        delay: 301,
	        html: false,
	        disable: true,
	        focus: true,
	        custom: {
	            // Bloco 01 Informações Pessoais
	            'GRU01_nome':function($el){
	                var check = helperValidacao.nomes("formGrupo01",$el.val().trim(),$el);
	                return !check;
	            },
	            'GRU01_cpf':function($el){
	                
	            },
	            'GRU01_tipoConta':function($el){},
	            'GRU01_numeroCartao':function($el){},
	            
	            'GRU01_rg':function($el){
	                var check = helperValidacao.rg("formGrupo01",$el.val().trim(),$el);
	                return !check;
	            },
	            'GRU01_emissor':function($el){},
	            'GRU01_uf':function($el){},
	            'GRU01_dataNascimento':function($el){
	                var check = helperValidacao.dataNascimento("formGrupo01",$el.val().trim(),$el);
	                return !check;
	            },
	            'GRU01_radioSexo':function($el){
	                console.log('$el',$el)
	            },
	            'GRU01_selectEstadoCivil':function($el){},     
	            'GRU01_nomeMae':function($el){
	                var check = helperValidacao.nomes("formGrupo01",$el.val().trim(),$el);
	                return !check;
	            },
	            'GRU01_nomePai':function($el){},
	            'GRU01_dddCelular01':function($el){},
	            'GRU01_numeroCelular01':function($el){},
	            'GRU01_dddCelular02':function($el){},
	            'GRU01_numeroCelular02':function($el){},
	            'GRU01_dependentes':function($el){},
	            'GRU01_email':function($el){},
	            // Bloco 02 Referênciais Pessoais
	            'GRU02_nomeReferencia':function($el){
	                var check = helperValidacao.nomes("formGrupo02",$el.val().trim(),$el);
	                return !check;
	            },
	            'GRU02_apelidoRef01':function($el){},
	            'GRU02_radioTipoFoneRef01':function($el){},
	            'GRU02_dddTipoFoneRef01':function($el){},
	            'GRU02_numeroTipoFoneRef01':function($el){},
	            'GRU02_selectParentescoRef01':function($el){},
	            'GRU02_nomeReferencia02':function($el){},
	            'GRU02_apelidoRef02':function($el){},
	            'GRU02_radioTipoFoneRef02':function($el){},
	            'GRU02_dddTipoFoneRef02':function($el){},
	            'GRU02_numeroTipoFoneRef02':function($el){},
	            'GRU02_selectParentescoRef02':function($el){},
	            // Bloco 03 residenciais
	            'GRU03_cepInfoResidencia':function($el){

	                var check = helperValidacao.cep("formGrupo03",$el.val().trim(),$el)
	                return !check;
	            },
	            'GRU03_estado':function($el){},
	            'GRU03_numero':function($el){},
	            'GRU03_complemento':function($el){
	                var check = helperValidacao.generica("formGrupo03",$el.val().trim(),$el)
	                return !check;
	            },
	            'GRU03_mesResidencia':function($el){
	                // helperValidacao.tempoResidenciaMeses($el.val());
	            },
	            'GRU03_anosResidencia':function($el){
	                // helperValidacao.tempoResidenciaAnos($el.val());
	            },
	            'GRU03_bairro':function($el){},
	            'GRU03_cidade':function($el){},
	            'GRU03_uf':function($el){},
	            'GRU03_resideDesde':function($el){},
	            'GRU03_selectTiporesidencia':function($el){},
	            'GRU03_radioTipoFone':function($el){},
	            'GRU03_dddTipoFone':function($el){},
	            'GRU03_numeroTipoFone':function($el){},
	            'GRU03_selectTipoFone02':function($el){},
	            'GRU03_dddCelular01':function($el){},
	            'GRU03_celular01':function($el){
	                var check = helperValidacao.generica("formGrupo03",$el.val().trim(),$el)
	                return !check;
	            },
	            'GRU03_dddCelular02':function($el){},
	            'GRU03_celular02':function($el){},
	            'GRU03_dddFoneRecado':function($el){},
	            'GRU03_recadoFalarCom':function($el){},
	            // Bloco 04 Comerciais
	            'GRU04_empresa':function($el){},
	            'GRU04_departamento':function($el){},
	            'GRU04_atividade':function($el){},
	            'GRU04_selectProfissional':function($el){},
	            'GRU04_rua':function($el){helperValidacao.generica("formGrupo04",$el.val().trim(),$el)}, 
	            'GRU04_numero':function($el){helperValidacao.generica("formGrupo04",$el.val().trim(),$el);},            
	            'GRU04_complemento':function($el){helperValidacao.generica("formGrupo04",$el.val().trim(),$el);},
	            'GRU04_cep':function($el){},
	            'GRU04_estado':function($el){},
	            'GRU04_bairro':function($el){},
	            'GRU04_cidade':function($el){},
	            'GRU04_radioTipoFone':function($el){},
	            'GRU04_dddTipoFone':function($el){},
	            'GRU04_numeroTipoFone':function($el){},
	            'GRU04_rendaMensal':function($el){},
	            'GRU04_outrasRendas':function($el){},
	            'GRU04_origemRenda':function($el){},
	            // Bloco 05 Outras Informações

	            
	            'GRU05_selectBanco':function($el){
	            	helperValidacao.validaSelect("formGrupo05",$el.val().trim(),$el);
	            	
	            },
	           	'GRU05_possuiCrediario':function($el){
	           		helperValidacao.validaSelect("formGrupo05",$el.val().trim(),$el);
	           	},
	           	'GRU05_qualLoja':function($el){
	           		helperValidacao.validaSelect("formGrupo05",$el.val().trim(),$el);
	           	},
	           	'GRU05_possuiOutrosCartoes':function($el){
	           		helperValidacao.validaSelect("formGrupo05",$el.val().trim(),$el);
	           	},

	            	
	            
	            feedback: {
	             success: 'glyphicon-ok',
	             error: 'glyphicon-remove'
	            }, 
	            errors: {
	                cpf: "CPF inválido!",
	                cpfVendedor: "CPF inválido!",
	                dataNascimento: "Data de Nascimento inválida",
	                nome: "Informe um nome e sobrenome!",
	                telefone: "Informe um telefone válido!",
	            },
	        }
	    };
	    return validatorOptions;
	};
	this.ativarColorTab = function(campos){
        var conjuntoErros = [];
        var grupoCampos01 = [];
        var grupoCampos02 = [];
        var grupoCampos03 = [];
        var grupoCampos04 = [];
        var grupoCampos05 = [];
        conjuntoErros.push(campos)
        console.log('conjuntoErros>@@@@',conjuntoErros)
        $.each(campos, function(index, val) {
            var el = val.className.split(" ");
            if(el.length >= 2){
                if(el[1] == "grup_formGrupo01" ){
                    grupoCampos01.push(val);
                }else if (el[1] == "grup_formGrupo02") {
                    grupoCampos02.push(val);
                }else if (el[1] == "grup_formGrupo03") {
                    grupoCampos03.push(val);
                }else if (el[1] == "grup_formGrupo04") {
                    grupoCampos04.push(val);
                }else if (el[1] == "grup_formGrupo05") {
                    grupoCampos05.push(val);
                }
            }
        });
        if(grupoCampos01.length > 0){
            $('.formGrupo01').addClass('TAB_ERRO');
            $('.titulo_formGrupo01').addClass('ITEM_ERRO');
            
        }else{
            $('.formGrupo01').removeClass('TAB_ERRO');
            $('.titulo_formGrupo01').removeClass('ITEM_ERRO');
        }
        // Grupo 02
        if(grupoCampos02.length > 0){
            $('.formGrupo02').addClass('TAB_ERRO');
            $('.titulo_formGrupo02').addClass('ITEM_ERRO');
        }else{
            $('.formGrupo02').removeClass('TAB_ERRO');
            $('.titulo_formGrupo02').removeClass('ITEM_ERRO');
        }
        // Grupo 03
        if(grupoCampos03.length > 0){
            $('.formGrupo03').addClass('TAB_ERRO');
            $('.titulo_formGrupo03').addClass('ITEM_ERRO');
        }else{
            $('.formGrupo03').removeClass('TAB_ERRO');
            $('.titulo_formGrupo03').removeClass('ITEM_ERRO');
        }
        // Grupo 04
        if(grupoCampos04.length > 0){
            $('.formGrupo04').addClass('TAB_ERRO');
            $('.titulo_formGrupo04').addClass('ITEM_ERRO');
        }else{
            $('.formGrupo04').removeClass('TAB_ERRO');
            $('.titulo_formGrupo04').removeClass('ITEM_ERRO');
        }
        // Grupo 05
        if(grupoCampos05.length > 0){
            $('.formGrupo05').addClass('TAB_ERRO');
            $('.titulo_formGrupo05').addClass('ITEM_ERRO');
        }else{
            $('.formGrupo05').removeClass('TAB_ERRO');
            $('.titulo_formGrupo05').removeClass('ITEM_ERRO');
        }
    };
    this.manipulacaoTabErro = function(){
        var arrTabsTodas = $('.nav-link');
        var arrTabsComErro = $('.TAB_ERRO');
        if(arrTabsComErro.length > 0){
            for (var i = 0; i < arrTabsTodas.length; i++) {
                hideTab($(arrTabsTodas[i]))
            }
            showTab($(arrTabsComErro[0]));
        }else{
            showTab($(arrTabsTodas[0]));
            hideTab($(arrTabsTodas[1]))
            hideTab($(arrTabsTodas[2]))
            hideTab($(arrTabsTodas[3]))
            hideTab($(arrTabsTodas[4]))
        }
	    function showTab(elemento){
	        var elemento = elemento;
	        var _href = $(elemento[0]).attr('href');
	        $('#'+_href).css('display', 'block');
	        $(elemento[0]).tab('show');
	        
	    };
	     
	    function hideTab(elemento){
	        var elemento = elemento;
	        var _href = $(elemento[0]).attr('href');
	        $('#'+_href).css('display', 'none');
	    }
	};
	this.validarReferenciasPessoais = function(){
        var nomeRef01       = this.boxComponent.GRU02_nomeReferencia[0];
        var apelidoRef01    = this.boxComponent.GRU02_apelidoRef01[0];
        var telefoneRef01   = this.boxComponent.GRU02_numeroTipoFoneRef01[0];
        var parentescoRef01 = this.boxComponent.GRU02_selectParentescoRef01[0];
        var nomeRef02       = this.boxComponent.GRU02_nomeReferencia02[0];
        var apelidoRef02    = this.boxComponent.GRU02_apelidoRef02[0];
        var telefoneRef02   = this.boxComponent.GRU02_numeroTipoFoneRef02[0];
        var parentescoRef02 = this.boxComponent.GRU02_selectParentescoRef02[0];

        arrayCamposRef01 = [nomeRef01,apelidoRef01,telefoneRef01,parentescoRef01];
        arrayCamposRef02 = [nomeRef02,apelidoRef02,telefoneRef02,parentescoRef02];

        if(validaRef01(arrayCamposRef01) == false && validaRef02(arrayCamposRef02) == false ){
            helperValidacao.showModalAviso('E obrigatorio pelo menos 1 referencia ser cadastrada')
        }

	        
	    var validaRef01 = function(cpRef01){
	        var flagRef01 = false;
	        $.each(cpRef01, function(index, val) {
	             if($(val).val() != ""){
	                flagRef01 = true;
	             }
	        });
	        return flagRef01;
	    }
	    var validaRef02 = function(cpRef02){
	        var flagRef02 = false;
	        var self = this;
	        $.each(cpRef02, function(index, val) {
	            // self.manipuladorErroForm(val)
	            if($(val).val() != ""){
	                flagRef02 = true;
	            }
	        });
	        return flagRef02;
	    }
	    // this.manipuladorErroForm = function(elm){
	    //     var personalErro = '<ul class="list-unstyled"><li>Preencha este campo.</li></ul>';
	    //     elm.setAttribute('required','required')
	    // }
	};
	this.getValoresSubmit = function(){
		varreGru01(this.formGrupo.formGrupo01);
		varreGru02(this.formGrupo.formGrupo02);
		varreGru03(this.formGrupo.formGrupo03);
		varreGru04(this.formGrupo.formGrupo04);
		varreGru05(this.formGrupo.formGrupo05);

		function varreGru01(gru01){
			$.each(gru01, function(index, val) {
				// console.log('varreGru01', $(val[0]).val())
			});
		}
		function varreGru02(gru02){
			$.each(gru02, function(index, val) {
				// console.log('varreGru02', val[0])
			});
		}
		function varreGru03(gru03){
			$.each(gru03, function(index, val) {
				console.log('varreGru03',val[0])
			});
		}
		function varreGru04(gru04){
			$.each(gru04, function(index, val) {
				console.log('varreGru04',val[0])
			});
		}
		function varreGru05(gru05){
			$.each(gru05, function(index, val) {
				console.log('varreGru05',val[0])
			});
		}
	}
	this.valorRadio = function(cpRadio){
        var _radioValor;
        $("*[name="+cpRadio+"]").change(function(){
            _radioValor = $(this).attr('value');
            console.log('Clicou no radio@',_radioValor)
        });
        return _radioValor;
    }
    this.valorSelect = function(cpSelect){
        var _selectValor;
        $('#'+cpSelect).change(function(){
            _selectValor = $(this).val();
            console.log('Clicou no _selectValor@',_selectValor)
        });
        return _selectValor;
    }


	this.eventosComponent = function(){
		window.addEventListener('keydown', function(e){
	        if((e.key=='Escape'||e.key=='Esc'||e.keyCode==27)){
	            e.preventDefault();
	            return false;
	        }
    	}, true);
		// mascaras
    	$(this.formGrupo.formGrupo01.GRU01_rg).mask("999.999.999-99");
	    $(this.formGrupo.formGrupo01.GRU01_dataNascimento).mask("99/99/9999");
	    $(this.formGrupo.formGrupo01.GRU01_dddCelular01).mask("99");
	    $(this.formGrupo.formGrupo01.GRU01_numeroCelular01).mask("99999-9999");
	    $(this.formGrupo.formGrupo01.GRU01_dddCelular02).mask("99");
	    $(this.formGrupo.formGrupo01.GRU01_numeroCelular02).mask("99999-9999");
        
		$('#campoRecadoFalarCom').hide();
		$('.left_box_grid.showTab').show();

		var idSelect       = this.formGrupo.formGrupo03.GRU03_selectTipoFone02[0].id;
		var idSelectMes    = this.formGrupo.formGrupo03.GRU03_mesResidencia[0].id;
        var idSelectAno    = this.formGrupo.formGrupo03.GRU03_anosResidencia[0].id;
        var arrCamposErros = $('.form-control');
		var self = this;

		$(this.boxComponent).on('click', '.nav-link', function(event) {
			event.preventDefault();
			var tabAtiva =  $(this).attr('href');
			var flagValidaForm = $(this).attr('valida-form');
			$('.left_box_grid').hide();
			$('#'+tabAtiva).show();
		});
		$(this.boxComponent).on('click','#btnCancelar', function(event) {
			event.preventDefault();
			var targetPage = $(this).attr('href');
			ClearApp.clear('#panelDashboard')
			renderPages.render(targetPage); 
		});
        $('#'+idSelect).change(function(){
            var _selectValor = $(this).val();
            if(_selectValor == "recado" ){
                $('#campoRecadoFalarCom').show();
            }else {
               $('#campoRecadoFalarCom').hide(); 
            }
        });
        $('#'+idSelectMes).change(function(){
            var _selectValorMes = $(this).val();
            self.validarMesesResidencia(_selectValorAno)
        });
        $('#'+idSelectAno).change(function(){
            var _selectValorAno = $(this).val();
            self.validarAnosResidencia(_selectValorMes)
        });

        // 

	    $('#formInfosCadastrais').validator(this.opcoesValidadorForm()).on('submit', function (e) {
	        // funçôes chamadas no submit do form
        	self.ativarColorTab(arrCamposErros)
	        self.manipulacaoTabErro();

        	if (e.isDefaultPrevented()) {
	           
	        } 
	        else {
	           self.getValoresSubmit();
	           helperValidacao.showModalAviso();
	        }
	        return false;// Bloqueando o submit do form
	    });
	};
	
}

var businessInformacoesCadastrais = new BusinessInformacoesCadastrais();