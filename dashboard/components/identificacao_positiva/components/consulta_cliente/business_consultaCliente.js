function BusinessConsultaCliente() {
	this.getCampos = function(){
		this.campoCpfSearch = $('#cpfSearch');
		this.tipoSearch     = $('#tipoSearch');
		console.log('OPP')
		// this.eventosComponente();
	}
	this.acaoBuscar = function(){
		var self = this;
		if(this.campoCpfSearch.val() != undefined && this.campoCpfSearch.val().length > 0){
			if($('#tipoSearch').val() == 'CPF' && !validarCPF(this.campoCpfSearch.val())){
				// logging("Warning");
				// marisaMessage.showDialog(marisaMessageType_WARNING, "Alerta","CPF inválido!");
				// this.campoCpfSearch.focus();
				
				// $('#panelCliente').empty();
				// $('#panelStats').empty();
				// $('#panelCards').empty();

				// cpf = '';
				// showPainel($('#panelDashboard'));
				// clickMenu('menuDashboard');

			}else{
				waitingDialog.show('Processando...', {dialogSize : 'sm',progressType : 'info'});
				setTimeout(function () {
					// corrigi a url
					var windowURL = window.location.href;
					//logging(windowURL);
					if(windowURL.indexOf("cpf=") > -1){
						windowURL = windowURL.replace("cpf=" + getUrlVars()['cpf'], "cpf=" + addZeroEsquerda($("#cpfSearch").val().replace(/[^\d]+/g,""), 11) );
					}else{
						windowURL = windowURL + "&" + "cpf=" + addZeroEsquerda($("#cpfSearch").val().replace(/[^\d]+/g,""), 11);
					}
					//logging(windowURL);
					window.history.pushState({},"", windowURL);
					
					$('#panelCliente').empty();
					$('#panelStats').empty();
					$('#panelCards').empty();

					self.consultaServico();
					
				
					setTimeout(function () {
						logging("waitingDialog.hide()");
						waitingDialog.hide();
					}, 500);
		
				}, 300);
			}
		
		}else{
			cpf = '';
			marisaMessage.showDialog(marisaMessageType_WARNING, "Alerta","Preencha corretamente o campo de pesquisa!<br>Utilize um CPF ou número de Cartão.");
			$('#cpfSearch').focus();
		}
	};
	// função que manipula as infos recebidas pelo service
	this.consultaServico = function(){
		var search = {};
		search.tipo = this.tipoSearch[0].value; 
		search.value = this.campoCpfSearch[0].value; 
		var parameters = {
			"Search" : search
		};
		var result = this.serviceSearch(parameters);
		if (result["codigoRetorno"] == 200) {
			this.getInfosCliente(result);
		} 
		else if (result["codigoRetorno"] == 204) {
	        marisaMessage.showDialog(marisaMessageType_WARNING, "Alerta","Cliente não encontrado!");
	        
		} else {
			marisaMessage.showDialog(marisaMessageType_ERROR, "Erro","Falha ao buscar cliente!");
		}
	};
	this.serviceSearch = function(parameters){
		var result = marisaService.callPostService("/dashboard", "searchClient", parameters);
		return result;
	}
	this.getInfosCliente = function(objResult){
		var objInfosCliente = objResult["objectsReturn"]["ClientDashboard"];
		this.objInfosCliente = objInfosCliente;
		console.log('this.objInfosCliente',this.objInfosCliente)


		/*
		var clientDashboard = objResult["objectsReturn"]["ClientDashboard"];
		_INFO_CLIENTE = { 
				'inativoCliente':clientDashboard.inativo,
				'diasCarenciaReanaliseCliente':clientDashboard.diasCarenciaReanalise,
				'emailCliente':clientDashboard.email,
				'nomeCliente':clientDashboard.nome,
				'cpfCliente':clientDashboard.cpf,
				'numeroRgCliente':clientDashboard.cliIdentidade,
				'orgaoEmissorCliente':clientDashboard.cliIdeOrgaoEmi,
				'estadoEmissorCliente':clientDashboard.cliIdeUfEmi,
				'sexoCliente':clientDashboard.sexo,
				'dtNascCliente':clientDashboard.dtNasc,
				'statCodCreditoCliente':clientDashboard.situacaoContaCod,
				'statCodCartaoCliente':clientDashboard.situacaoCartaoCod,
				'statCodEmbossamentoCliente':clientDashboard.situacaoEmbossamentoCod,
				'foneResDddCliente':clientDashboard.cliResDdd,
				'foneResCliente':clientDashboard.cliResFone,
				'salarioCliente':clientDashboard.cliEmpSalario,
				'rgDtExpedCliente':clientDashboard.cliDtExpDoc

		}

		cpf = clientDashboard.cpf;
		statCodCredito = clientDashboard.situacaoContaCod;
		dtNasc = clientDashboard.dtNasc;
		sexo = clientDashboard.sexo;
		nomeCliente = clientDashboard.nome.toUpperCase();
		inativo = clientDashboard.inativo;
		diasCarenciaReanalise = clientDashboard.diasCarenciaReanalise;
			
		$('#scoreCliente').val(clientDashboard.score);

		if(clientDashboard.buscaPorAdicional == undefined || clientDashboard.buscaPorAdicional == null || clientDashboard.buscaPorAdicional == 'N'){
			if(getUrlVars()['adicional'] == 'true'){
				clientDashboard.buscaPorAdicional = 'S';
				removeParam('adicional');
			}
		}
			
		dadosCadastroCliente = 'getDadosCadastroCliente(clientDashboard)';
		
		$('#tipoSearch').val('CPF'); 
		$('#cpfSearch').val(addZeroEsquerda(cpf,11)); 					
		$('#cpfSearch').unmask();
		$('#cpfSearch').mask("999.999.999-99");
		
		var raizCliente = $('#panelCliente');

	    var htmlDadosCliente = 'getHtmlDadosCliente(clientDashboard)';
	    $('<div>').html(htmlDadosCliente).appendTo(raizCliente);
	    
		if(target == undefined || target == null){ // no target
		    var raizStats = $('#panelStats');
		    var clienteStats = 'getStats(clientDashboard.cpf)';
		    var htmlStats =' getHtmlStats(clienteStats)';
		    $('<div>').html(htmlStats).appendTo(raizStats);
		    
		    var raizCards = $('#panelCards');
		    var htmlPopups = "" ;
			htmlPopups += "		" + getHtmlCardPopupClienteCessaoCarteira() ;
			htmlPopups += "		" + getHtmlCardPopupClienteTransferidoItau() ;
			htmlPopups += "		" + getHtmlCardPopupClienteInativo() ;
		    htmlCards += "		" + getHtmlCardInformacoesCadastrais(clientDashboard) ;
		    htmlCards += "		" + getHtmlCardCartoes(getPlasticos(clientDashboard.cpf, "S", clientDashboard.situacaoContaCod), 2, true) ;
		    htmlCards += "		" + getHtmlCardSolicitacoes(getSolicitacoes(), 2, true) ;
			htmlCards += "		" + cardMemofile.templateCard() ; 
		    htmlCards += "";
		    
		    $('<div>').html(htmlCards).appendTo(raizCards);				
			
		    loadScriptsDashboard();

			logging("...");
			
			showPainel($('#panelDashboard'));
			clickMenu('menuDashboard');
			
		}else{
			eval(target)();
		}
		*/
		return objInfosCliente;
	};
	this.controleExibicao = function(){
		var flagExibir = false;
		if(this.objInfosCliente){
			flagExibir = true;
		}else {
			flagExibir = false
		}
		return flagExibir;
	}
	this.retornoInfosTratadas = function(){
		var objInfosCliente = this.objInfosCliente;
		return objInfosCliente;
	}
	
}
var businessConsultaCliente = new BusinessConsultaCliente();