function DetalheTemplateIdentificaPositiva() {
	this.formConsulta = function(){
		var _html ;
		_html = '<div class="referenciaModalcard"></div>';
		_html += '<div class="mr_naveBusca page-title clearfix row" style="margin: -5px -17px -10px -17px;padding: 0px 0px 0px 0px;">'; 
		_html += '<ul class="stats col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding: 0px 0px 0px 0px;list-style-type: none;"> ';
		_html += '<li id="menuPesquisa" class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="border-radius: 5px !important;background-color: #b3c9e0ab;"> ';
		_html += '<div class="details">';                        
		_html += '<div class="input-group" style="float: right;"> ';
		_html += '<div class="input-group-btn search-panel"> ';
		_html += '<select class="select_tipo_busca btn btn-default" id="tipoSearch" > ';
		_html += '<option value="">Buscar por:</option>'; 									 
		_html += '<option selected="" value="CPF">CPF</option>'; 									 
		_html += '<option value="CARTAO">CARTÃO</option> ';							 
		_html += '</select>';							 
		_html += '</div>';                            
		_html += '<input type="text" inputmode="tel" class="form-control" placeholder="Entre com o CPF" id="cpfSearch" style="height: 40px;font-size: 20px;border: 1px solid #f9cbe0;" onkeypress="return isNumberKey(event);">';                            
		_html += '<span id="clearSearch" class="glyphicon glyphicon-remove-circle"></span>'; 
		_html += '<input type="hidden" id="scoreCliente" >';
		_html += '<div class="input-group-btn" style="">';                                 
		_html += '<button id="btnSearch" class="btn btn-default" type="submit">'; 
		_html += '<i class="glyphicon glyphicon-search"></i>'; 
		_html += '</button>';                             
		_html += '</div>';                         
		_html += '</div>';                
		_html += '</div>';             
		_html += '</li>';                       
		_html += '</ul>';         
		_html += '</div> ';   
		return _html;
	}
	this.templateHeader = function(){
		var _html = '<div id="header_faturas" class="grupo_faturas">';
		_html += '<div class="item_fatura" style="width: 20%">Data</div>';
		_html += '<div class="item_fatura" style="width: 70%; text-align: left; padding-left: 25px">Número do Chamado </div>';
		_html += '<div class="item_fatura" style="width: 70%; text-align: left; padding-left: 25px">Data prevista para solução</div>';
		_html += '<div class="item_fatura" style="width: 70%; text-align: left; padding-left: 25px">Status</div>';
		_html += '</div>'; 
		return _html;
	}

	this.itemCardsInfos = function(infosCliente,questionarioCurrent){
		var html;
			html = '<div id="dadosCliente" class="page-title clearfix row"> ';
			html += '<ul class="stats col-xs-12 col-sm-12 col-md-12 col-lg-12"> ';

			html += this.infoCliente(infosCliente);
			html += this.infoCartaoCliente(infosCliente);

			html += this.perguntasCliente(questionarioCurrent);

			html += '</ul>';
			html += '</div>';
			html += '<div class="nav_acao_infos_cliente"> ';
			html += '<a id="identificaReavaliar" href="#" class="btn_identificacao btn btn-info">';
			html += '<span class="fs1" aria-hidden="true" data-icon="">Reavaliar</span>';
			html += '</a> ';
			html += '<a id="identificaAvancar" href="#" data-toggle="modal" class="btn_identificacao btn btn-success">';
			html += '<span class="fs1" aria-hidden="true" data-icon="&#xe0d4;">Avançar</span>';
			html += '</a>';
			html += '</div>';
			html += '</div> ';
			html += '</li> ';
			html += '</ul> ';
			html += '</div> ';
		return html;
	};
	// templates dos blocos de informacoes
	this.infoCliente = function(infosCliente){
		
		var htmlInfoCliente; 
			htmlInfoCliente = '<li id="infosCliente" class="boxAlturaDinamica marisa-pink-bg col-xs-12 col-sm-12 col-md-12 col-lg-4">';
			htmlInfoCliente += '<span class="fs1" aria-hidden="true" data-icon="&#xe075;" ></span> ';
			htmlInfoCliente += '<div class="details"> ';
			htmlInfoCliente += '<span class="big">'+infosCliente.nome+'</span> ';
			htmlInfoCliente += '<span id="cpfDadosCliente" class="small"><span class="labelCliente">CPF:</span>'+infosCliente.cpf+' </span>'; 
			htmlInfoCliente += '<span class="small"><span class="labelCliente">Situação Conta:</span>'+infosCliente.situacaoConta+'</span>'; 
			htmlInfoCliente += '<div class="_showContent">';
			htmlInfoCliente += '<span class="small"><span class="labelCliente">Nome Mãe:</span> TEREZINHA DOS SANTOS ANDRADE</span>';
			htmlInfoCliente += '<span class="small"><span class="labelCliente">RG:</span>'+infosCliente.rg+'</span>';
			htmlInfoCliente += '<span class="small"><span class="labelCliente">e-mail:</span>'+infosCliente.email+'</span>';
			htmlInfoCliente += '<span class="small"><span class="labelCliente">Data Nascimento / Idade:</span> '+infosCliente.dataNascimento+' - '+infosCliente.idade+' </span>';
			htmlInfoCliente += '<span class="small">&nbsp;</span>';
			htmlInfoCliente += '</div>';
			htmlInfoCliente += '</div>';
			htmlInfoCliente += '</li> ';
		return htmlInfoCliente;
	}
	this.infoCartaoCliente = function(infosCliente){
		var htmlInfoCartaoCliente;
			htmlInfoCartaoCliente = '<li id="CartaoCliente" class="boxAlturaDinamica marisa-blue-bg col-xs-12 col-sm-12 col-md-12 col-lg-4"> ';
			htmlInfoCartaoCliente += '<span class="fs1" aria-hidden="true" data-icon="&#xe039;" ></span> ';
			htmlInfoCartaoCliente += '<div class="details"> ';
			htmlInfoCartaoCliente += '<span class="big">'+infosCliente.numeroCartao+'</span> ';
			htmlInfoCartaoCliente += '<span id="cpfDadosCliente" class="small"><span class="labelCliente">Data Cadastro</span>'+infosCliente.dataCadastro+' </span> ';
			htmlInfoCartaoCliente += '<span class="small"><span class="labelCliente">Situação Cartão:</span>'+infosCliente.situacaoCartao+'</span> ';
			htmlInfoCartaoCliente += '<div class="_showContent">';
			htmlInfoCartaoCliente += '<span class="small"><span class="labelCliente">Data Última Alteração:</span>'+infosCliente.dataUltimaAlteracao+'</span>';
			htmlInfoCartaoCliente += '<span class="small"><span class="labelCliente">Vencimento / Melhor Dia:</span> '+infosCliente.vencimento+'</span>';
			htmlInfoCartaoCliente += '<span class="small"><span class="labelCliente">Filial / Loja Origem:</span>'+infosCliente.filialLojaOrigem+'</span>';
			htmlInfoCartaoCliente += '<span class="small"><span class="labelCliente">Filial / Loja Retirada:</span>'+infosCliente.filialLojaRetirada+'</span>';
			htmlInfoCartaoCliente += '<span class="small"><span class="labelCliente"></span></span>';
			htmlInfoCartaoCliente += '<span class="small">&nbsp;</span>';
			htmlInfoCartaoCliente += '</div>';
			htmlInfoCartaoCliente += '</div>';
			htmlInfoCartaoCliente += '</li> ';
		return htmlInfoCartaoCliente;
	}
	this.perguntasCliente = function (questionarioCurrent){
		console.log(questionarioCurrent)

		var htmlPerguntasCliente
			htmlPerguntasCliente = '<li id="ListaIdentificacaoPositiva" class="boxAlturaDinamica sea-green-bg col-xs-12 col-sm-12 col-md-12 col-lg-4"> ';
			htmlPerguntasCliente += '<span class="fs1" aria-hidden="true" data-icon="&#xe0f7;" ></span> ';
			htmlPerguntasCliente += '<div class="details"> ';
			htmlPerguntasCliente += '<span class="big">Identificação Positiva</span>';
			htmlPerguntasCliente += '<div class="perguntasRespostas">';
			htmlPerguntasCliente += '<ul id="pergGrupo01" class="lista_perguntas_e_respostas">';

			var perguObri = questionarioCurrent.listaObrigatorio;
			var perguOpci = questionarioCurrent.listaOpcional;
			var codPerg;
			var perg01 = [];
			var perg02 = [];
			perguOpci.sort(function(a,b){
				if (a.pergCod > b.pergCod) {
						return 1;
				}
				if (a.pergCod < b.pergCod) {
					return -1;
				}
				// a must be equal to b
				return 0;
			})
			var perguntasIniciais  = perguOpci.slice(0, 5)
			var perguntasRestantes = perguOpci.slice(5, perguOpci.length);

			$.each( perguntasIniciais, function(index,val) {
				htmlPerguntasCliente += '<li class="perg01 item_perguntaResposta">';
				htmlPerguntasCliente += '<span class="itens pergunta">'+ val.pergunta+'</span>';
				htmlPerguntasCliente += '<span class="itens resposta">'+val.resposta+'</span>';
				htmlPerguntasCliente += '</li>';
			});
			htmlPerguntasCliente += '</ul>';
			htmlPerguntasCliente += '<ul id="pergGrupo02" class="lista_perguntas_e_respostas off">';
			$.each(perguntasRestantes, function(index,val) {
				htmlPerguntasCliente += '<li class="perg02 item_perguntaResposta">';
				htmlPerguntasCliente += '<span class="itens pergunta">'+ val.pergunta+'</span>';
				htmlPerguntasCliente += '<span class="itens resposta">'+val.resposta+'</span>';
				htmlPerguntasCliente += '</li>';
			});

			htmlPerguntasCliente += '</li>';
		return htmlPerguntasCliente;
	}
	this.listaItems = function(identificador){
		var retornoHtml;
		retornoHtml = '<div class="card">';
		retornoHtml += '<div class="card-header card_acordeon">';
		retornoHtml += '<a class="btn_drop_memo" href="#" data-target="#'+identificador+'" aria-expanded="true" aria-controls="'+identificador+'">';
		retornoHtml += '<div class="area_click" id="headingOne" style="width:100%; padding-left:10px ">';
		retornoHtml += '<div class="MRF_float mb-0 titulo_fatura" style="width:17%;">28/04/2018</div>';
		retornoHtml += '<div class="MRF_float mb-0 titulo_fatura mobileNone" style="width:25%; "> 22.529.678 </div>';
		retornoHtml += '<div class="MRF_float mb-0 titulo_fatura mobileNone" style="width:25%;"> 05/12/2018  </div>';
		retornoHtml += '<div class="MRF_float mb-0 titulo_fatura" style="width:25%;"> EM ATENDIMENTO  </div>';	

		retornoHtml += '</div>';
		retornoHtml += '</a>';
		retornoHtml += '</div>';
		retornoHtml += '<div id="'+identificador+'" class="collapse " aria-labelledby="headingOne2" data-parent="#accordion">';    
		retornoHtml += '<div class="">';
		retornoHtml += '<div class="lista_detalhe_item">';
		//<!-- Detalhes Grupo 02 Dados Memo file -->
		retornoHtml += '<div class="list-blocos-group lista_grid_02 full_grid">'; 
		retornoHtml += '<div class="alt_din list-group-item list-group-item-primary titulo_divisor" >';
		retornoHtml += '<span class="item_lista_titulo">Cliente Atendido</span>';
		retornoHtml += '<div class="lista_grupo_clear"></div>';
		retornoHtml += '</div>';
		retornoHtml += '</div>';
		retornoHtml += '<div class="list-blocos-group lista_grid_02 full_grid">'; 
		retornoHtml += '<div class="alt_din list-group-item list-group-item-primary" >';
		retornoHtml += '<span class="item_lista_titulo ">Nº do Chamado</span>';
		retornoHtml += '<span class="item_lista_valor "> 22.529.678  </span>';
		retornoHtml += '<div class="lista_grupo_clear"></div>';
		retornoHtml += '</div>';
		retornoHtml += '</div>';
		retornoHtml += '<div class="list-blocos-group lista_grid_02 full_grid">'; 
		retornoHtml += '<div class="alt_din list-group-item list-group-item-primary" >';
		retornoHtml += '<span class="item_lista_titulo ">Data e Hora de abertura</span>';
		retornoHtml += '<span class="item_lista_valor ">05/12/2018 10:44</span>';
		retornoHtml += '<div class="lista_grupo_clear"></div>';
		retornoHtml += '</div>';
		retornoHtml += '</div>';

		retornoHtml += templateSelectEnvioProtocolo();
		retornoHtml += templateSelectNotificacaoEncerramento();
		retornoHtml += templateSelectEnvioHistorico();
		retornoHtml += templateSelectEnvioCancelamento();

		retornoHtml += '<div class="list-blocos-group lista_grid_02 full_grid"> ';
		retornoHtml += '<div class="alt_din list-group-item list-group-item-primary" >';
		retornoHtml += '<span class="item_lista_titulo ">Quantidade de rechamadas</span>';
		retornoHtml += '<span class="item_lista_valor ">0</span>';
		retornoHtml += '<div class="lista_grupo_clear"></div>';
		retornoHtml += '</div>';
		retornoHtml += '</div>';

		retornoHtml += templateInputEmail();

		retornoHtml += '<div class="list-blocos-group lista_grid_02 full_grid"> ';
		retornoHtml += '<div class="alt_din list-group-item list-group-item-primary content_btn_salvar_drop">';
		retornoHtml += '<a id="btnSalvarDadosEnvio" href="#" data-toggle="modal" class="btnAcaoSalvar btn_page_infos btn btn-success">';
		retornoHtml += '<span class="fs1" data-icon="">SALVAR DADOS DE ENVIO</span>';
		retornoHtml += '</a>';
		retornoHtml += '<div class="lista_grupo_clear"></div>';
		retornoHtml += '</div>';
		retornoHtml += '</div>';

		// listagem de assuntos

		retornoHtml += templateListaAssuntosChamados(identificador);

		// Template Select envio de protocolo
		function templateSelectEnvioProtocolo(){
			var tmpSelect01;
				tmpSelect01  = '<div class="list-blocos-group lista_grid_02 full_grid">'; 
				tmpSelect01 += '<div class="alt_din list-group-item list-group-item-primary" style="padding: 7px 0 5px 0;" >';
				tmpSelect01 += '<span class="item_lista_titulo contentFormDropTitulo">Meio de envio do protocolo</span>';
				tmpSelect01 += '<span class="item_lista_valor contentFormDrop">';
				tmpSelect01 += '<div class="form-group">';
				tmpSelect01 += '<select id="inputState" class="form-control select_drop">';
				tmpSelect01 += '<option selected>Escolha</option>';
				tmpSelect01 += '<option value="telefone">Telefone</option>';
				tmpSelect01 += '<option value="email">E-mail</option>';
				tmpSelect01 += '<option value="correio">Correio</option>';
				tmpSelect01 += '<option value="fax">Fax</option>';
				tmpSelect01 += '<option value="sms">SMS</option>';
				tmpSelect01 += '</select>';
				tmpSelect01 += '<button type="submit" class="btn btn-primary">Enviar</button>';
				tmpSelect01 += '</div>';
				tmpSelect01 += '</span>';
				tmpSelect01 += '<div class="lista_grupo_clear"></div>';
				tmpSelect01 += '</div>';
				tmpSelect01 += '</div>';
			return tmpSelect01;
		}
		// Template Select Notificação encerramento
		function templateSelectNotificacaoEncerramento(){
			var tmpSelect02;
				tmpSelect02  = '<div class="list-blocos-group lista_grid_02 full_grid">'; 
				tmpSelect02 += '<div class="alt_din list-group-item list-group-item-primary" style="padding: 7px 0 5px 0;" >';
				tmpSelect02 += '<span class="item_lista_titulo contentFormDropTitulo">Meio de notificaçao do encerramento</span>';
				tmpSelect02 += '<span class="item_lista_valor contentFormDrop">';
				tmpSelect02 += '<div class="form-group">';
				tmpSelect02 += '<select id="inputState" class="form-control select_drop">';
				tmpSelect02 += '<option selected>Escolha</option>';
				tmpSelect02 += '<option value="telefone">Telefone</option>';
				tmpSelect02 += '<option value="email">E-mail</option>';
				tmpSelect02 += '<option value="correio">Correio</option>';
				tmpSelect02 += '<option value="fax">Fax</option>';
				tmpSelect02 += '<option value="sms">SMS</option>';
				tmpSelect02 += '</select>';
				tmpSelect02 += '<button type="submit" class="btn btn-primary">Enviar</button>';
				tmpSelect02 += '</div>';
				tmpSelect02 += '</span>';
				tmpSelect02 += '<div class="lista_grupo_clear"></div>';
				tmpSelect02 += '</div>';
				tmpSelect02 += '</div>';
			return tmpSelect02;
		}
		// Template Select meio Envio Historico
		function templateSelectEnvioHistorico(){
			var tmpSelect02;
				tmpSelect02  = '<div class="list-blocos-group lista_grid_02 full_grid">'; 
				tmpSelect02 += '<div class="alt_din list-group-item list-group-item-primary" style="padding: 7px 0 5px 0;" >';
				tmpSelect02 += '<span class="item_lista_titulo contentFormDropTitulo">Meio de envio do histórico</span>';
				tmpSelect02 += '<span class="item_lista_valor contentFormDrop">';
				tmpSelect02 += '<div class="form-group">';
				tmpSelect02 += '<select id="inputState" class="form-control select_drop">';
				tmpSelect02 += '<option selected>Escolha</option>';
				tmpSelect02 += '<option value="email">E-mail</option>';
				tmpSelect02 += '<option value="correio">Correio</option>';
				tmpSelect02 += '<option value="fax">Fax</option>';
				tmpSelect02 += '</select>';
				tmpSelect02 += '<button type="submit" class="btn btn-primary">Enviar</button>';
				tmpSelect02 += '</div>';
				tmpSelect02 += '</span>';
				tmpSelect02 += '<div class="lista_grupo_clear"></div>';
				tmpSelect02 += '</div>';
				tmpSelect02 += '</div>';
			return tmpSelect02;
		}
		// Template Select Meio de envio de comprovantes de cancelamento
		function templateSelectEnvioCancelamento(){
			var tmpSelect02;
				tmpSelect02  = '<div class="list-blocos-group lista_grid_02 full_grid">'; 
				tmpSelect02 += '<div class="alt_din list-group-item list-group-item-primary" style="padding: 7px 0 5px 0;" >';
				tmpSelect02 += '<span class="item_lista_titulo contentFormDropTitulo">Meio de envio de comprovantes de cancelamento</span>';
				tmpSelect02 += '<span class="item_lista_valor contentFormDrop">';
				tmpSelect02 += '<div class="form-group">';
				tmpSelect02 += '<select id="inputState" class="form-control select_drop">';
				tmpSelect02 += '<option selected>Escolha</option>';
				tmpSelect02 += '<option value="email">E-mail</option>';
				tmpSelect02 += '<option value="correio">Correio</option>';
				tmpSelect02 += '<option value="fax">Fax</option>';
				tmpSelect02 += '</select>';
				tmpSelect02 += '<button type="submit" class="btn btn-primary">Enviar</button>';
				tmpSelect02 += '</div>';
				tmpSelect02 += '</span>';
				tmpSelect02 += '<div class="lista_grupo_clear"></div>';
				tmpSelect02 += '</div>';
				tmpSelect02 += '</div>';
			return tmpSelect02;
		}
		// Template Select Meio de envio de comprovantes de cancelamento
		function templateInputEmail(){
			var tmpSelect02;
				tmpSelect02  = '<div class="list-blocos-group lista_grid_02 full_grid">'; 
				tmpSelect02 += '<div class="alt_din list-group-item list-group-item-primary" style="padding: 7px 0 5px 0;" >';
				tmpSelect02 += '<span class="item_lista_titulo contentFormDropTitulo">Email</span>';
				tmpSelect02 += '<span class="item_lista_valor contentFormDrop">';
				tmpSelect02 += '<div class="form-group">';
				tmpSelect02 += '<input type="email" class="form-control input_drop" id="emailIdentificaPositivo" placeholder="Email">';
				tmpSelect02 += '</div>';
				tmpSelect02 += '</span>';
				tmpSelect02 += '<div class="lista_grupo_clear"></div>';
				tmpSelect02 += '</div>';
				tmpSelect02 += '</div>';
			return tmpSelect02;
		}
		// Template para Listagem de assuntos por chamados
		function templateListaAssuntosChamados(identificador){
			var retornoAssuntos;
			retornoAssuntos = '<div class="list-blocos-group lista_grid_02 full_grid">';
			retornoAssuntos += '<a class="btn_drop_assuntos" href="#" data-target="#assuntos_'+identificador+'" aria-expanded="true" aria-controls="assuntos_'+identificador+'">'; 
			retornoAssuntos += '<div class="alt_din list-group-item list-group-item-primary titulo_divisor">';
			retornoAssuntos += '<span class="item_lista_titulo">1º Assunto</span>';
			retornoAssuntos += '<div class="lista_grupo_clear"></div>';
			retornoAssuntos += '</div>';
			retornoAssuntos += '</a>';
			retornoAssuntos += '</div>';

			retornoAssuntos += '<div id="assuntos_'+identificador+'" class="collapse " aria-labelledby="headingOne2" data-parent="#accordion">';
			retornoAssuntos += '<div class="list-blocos-group lista_grid_02 full_grid"> ';
			retornoAssuntos += '<div class="alt_din list-group-item list-group-item-primary" >';
			retornoAssuntos += '<span class="item_lista_titulo ">Tipo</span>';
			retornoAssuntos += '<span class="item_lista_valor ">RENEGOCIACAO DE PROMESSA OU ACORDO</span>';
			retornoAssuntos += '<div class="lista_grupo_clear"></div>';
			retornoAssuntos += '</div>';
			retornoAssuntos += '</div>';
			retornoAssuntos += '<div class="list-blocos-group lista_grid_02 full_grid"> ';
			retornoAssuntos += '<div class="alt_din list-group-item list-group-item-primary" >';
			retornoAssuntos += '<span class="item_lista_titulo ">Status</span>';
			retornoAssuntos += '<span class="item_lista_valor ">RESOLVIDO</span>';
			retornoAssuntos += '<div class="lista_grupo_clear"></div>';
			retornoAssuntos += '</div>';
			retornoAssuntos += '</div>';
			retornoAssuntos += '<div class="list-blocos-group lista_grid_02 full_grid"> ';
			retornoAssuntos += '<div class="alt_din list-group-item list-group-item-primary" >';
			retornoAssuntos += '<span class="item_lista_titulo ">Prazo de solução</span>';
			retornoAssuntos += '<span class="item_lista_valor ">Imediato</span>';
			retornoAssuntos += '<div class="lista_grupo_clear"></div>';
			retornoAssuntos += '</div>';
			retornoAssuntos += '</div>';
			retornoAssuntos += '<div class="list-blocos-group lista_grid_02 full_grid">'; 
			retornoAssuntos += '<div class="alt_din list-group-item list-group-item-primary titulo_divisor titulo_divisor_textarea" >';
			retornoAssuntos += '<span class="item_lista_titulo">Observação</span>';
			retornoAssuntos += '<div class="lista_grupo_clear"></div>';
			retornoAssuntos += '</div>';
			retornoAssuntos += '</div>';
			retornoAssuntos += '<div class="list-blocos-group lista_grid_02 full_grid">'; 
			retornoAssuntos += '<div class="alt_din list-group-item list-group-item-primary titulo_divisor" >';
			retornoAssuntos += '<span class="item_lista_titulo">';
			retornoAssuntos += '<textarea class="form-control" id="exampleTextarea" rows="3"></textarea>';
			retornoAssuntos += '</span>';
			retornoAssuntos += '<div class="lista_grupo_clear"></div>';
			retornoAssuntos += '</div>';
			retornoAssuntos += '</div>';
			retornoAssuntos += '</div>';
			retornoAssuntos += '</div>';
			retornoAssuntos += '</div>';
			retornoAssuntos += '</div>';

			retornoAssuntos += '</div>';

			return retornoAssuntos;
		}


		return retornoHtml;
	};
	

}

var detalheTemplateIdentificaPositiva = new DetalheTemplateIdentificaPositiva();