function ModalProtocolo(){
	this.load = function(_contentInfos,objInfosCliente,propsModalProtocolo){
		console.log('propsModal objInfosCliente lalalalalalala==============================>>',objInfosCliente)
		this.objInfosCliente = objInfosCliente;
		if(propsModalProtocolo != 0){
			var tmp = this.template(propsModalProtocolo);
			$(_contentInfos).append(tmp)
		}else {
			var tmp = this.template(propsModalProtocolo);
			$(_contentInfos).append(tmp)
		}
		
		$('#ModalIdentificacaoAvancar').modal('show');
		this.acaoNovoChamado();
		this.acaoReutilizar();
		return;
	};
	this.template = function(propsModalProtocolo){
		var html;
			html  = '<div id="ModalIdentificacaoAvancar" class="modal fade modalSAC" tabindex="-1" role="dialog">';
			html += '<div class="modal-dialog modal-dialog-centered" role="document">';
			html += '<div id="modalContent" class="modal-content">';
			html += '<div class="modal-header" id="messageHeader" style="display: block; background-color: rgba(33, 129, 212, 0.45);">';
			html += '<button type="button" class="close" data-dismiss="modal" aria-label="Close" style="padding-top: 0px;"><span aria-hidden="true">×</span></button>';
			html += '<h4 id="messageTitle" class="modal-title">Atenção</h4>';
			html += '</div>';
			html += '<div class="modal-body" style="display: inline-flex;font-size: 14px;">';
			html += '<h1 style="margin-top: 4px;"><i id="dialog-icon" style="color: rgba(33, 129, 212, 0.95);" class="fa fa-question-circle col-lg-1"></i></h1>';
			html += '<div>';
			html += '<label id="dialog-message" style="padding-top: 7px;">';
			html += '<div class="form-inline has-feedback">';
			html += '<span>Você deseja utilizar o último chamado em atendimento<br> N° <span class="destaque_chamado"> '+propsModalProtocolo+' </span>  ou criar um novo ?</span>';
			html += '</div>';					
			html += '</label>';
			html += '</div>';
			html += '</div>';
			html += '<div class="modal-footer" style="background-color: rgb(243, 243, 243);padding: 10px;">';
			html += '<button type="button" id="btnNovo" class="btn btn-primary" style="font-size: 14px;">Novo</button>';
			html += '<button type="button" id="btnReutilizar" style="font-size: 14px;" onclick="" class="btn btn-default">Reutilizar</button>';
			html += '</div>';
			html += '</div>';
			html += '</div>';
			html += '</div>';
			return html;
	};
	this.getNovoProtocolo = function(){
		serviceIdentificacaoPositiva.novoProtocolo(parameters)
	}
	this.acaoNovoChamado = function(){
		var self = this;
		var disparaAcaoNovoChamado = $('#btnNovo').on('click', function(event) {
			event.preventDefault();
			console.log('Confirmado Novo Chamado');
			$('#ModalIdentificacaoAvancar').modal('hide');
			$('#panelIdentificacaoPositiva').hide();
			setTimeout(function(){
				loadDashBoard.load(self.objInfosCliente);
			},200)
			
			
				
		});
		return disparaAcaoNovoChamado;
	};
	this.acaoReutilizar = function(){
		var self = this;
		var disparaAcaoReutilizarChamado = $('#btnReutilizar').on('click', function(event) {
			event.preventDefault();
			console.log('reutiliza')
			controllStorageSac.setFlagIdentificacaoPositiva();
			$('#ModalIdentificacaoAvancar').modal('hide');
			$('#panelIdentificacaoPositiva').hide();
			setTimeout(function(){
				loadDashBoard.load(self.objInfosCliente);
			},200)
			
			
		});
		return disparaAcaoReutilizarChamado;
	}
}
var modalProtocolo = new ModalProtocolo();