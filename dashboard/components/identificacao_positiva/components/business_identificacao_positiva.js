

require("components/identificacao_positiva/components/detalhe_template_identificacao_positiva.js");
require("components/identificacao_positiva/components/modalProtocolo.js");
require("components/identificacao_positiva/components/service_identificaPositiva.js");
require("components/consultaChamados/init_consultaChamados.js");
require("components/popups/init_popup.js");


var controleEstadoComponent = {};

function BusinessIdentificaPositiva() {
	this.load = function(popUpCliente,masterContent,contentBusca,contentInfos,contentChamados){
		this.popUpCliente    = popUpCliente;
		this.masterContent   = masterContent;
		this.contentBusca    = contentBusca;
		this.contentInfos 	 = contentInfos;
		this.contentChamados = contentChamados;
		this.initRender();
		this.getCampos();
		this.buscaClienteDireto();
		// this.masterContent.hide();
	};
	this.getUrlVars = function() {
	    var vars = {};
	    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi,    
	    function(m,key,value) {
	      vars[key] = value;
	    });
    	return vars;
	}
	// Funções que tratão a pesquisa do cliente####
    this.getCampos = function(){
		this.campoCpfSearch = $('#cpfSearch');
		this.tipoSearch     = $('#tipoSearch');
	}
	this.buscaClienteDireto = function(){
		this.flagBuscaDireta = this.getUrlVars().acessoAltitude;
		this.cpfAcessoAltitude = this.getUrlVars().cpfAlt;
		if(this.flagBuscaDireta == 'ok'){
			this.consultaServico();
		}
	}
	this.acaoBuscar = function(){
		var self = this;
		if(this.campoCpfSearch.val() != undefined && this.campoCpfSearch.val().length > 0){
			if($('#tipoSearch').val() == 'CPF' && !validarCPF(this.campoCpfSearch.val())){
				 marisaMessage.showDialog(marisaMessageType_WARNING, "Alerta","CPF inválido!");
				this.campoCpfSearch.focus();
				$(this.masterContent).empty();
			}else{
				waitingDialog.show('Processando...', {dialogSize : 'sm',progressType : 'info'});
				setTimeout(function () {
					var windowURL = window.location.href;
					if(windowURL.indexOf("cpf=") > -1){
						windowURL = windowURL.replace("cpf=" + getUrlVars()['cpf'], "cpf=" + addZeroEsquerda($("#cpfSearch").val().replace(/[^\d]+/g,""), 11) );
					}else{
						windowURL = windowURL + "&" + "cpf=" + addZeroEsquerda($("#cpfSearch").val().replace(/[^\d]+/g,""), 11);
					}
					window.history.pushState({},"", windowURL);
					$(this.masterContent).empty();
					$('#panelCliente').empty();
					$('#panelStats').empty();
					$('#panelCards').empty();
					self.consultaServico();
					setTimeout(function () {
						logging("waitingDialog.hide()");
						waitingDialog.hide();
					}, 500);
		
				}, 300);
			}
		
		}else{
			cpf = '';
			marisaMessage.showDialog(marisaMessageType_WARNING, "Alerta","Preencha corretamente o campo de pesquisa!<br>Utilize um CPF ou número de Cartão.");
			$('#cpfSearch').focus();
		}
	};
	this.consultaServico = function(){
		var search = {};
		if(this.flagBuscaDireta == 'ok'){
			search.tipo = 'CPF'; 
			search.value = this.cpfAcessoAltitude;

		}else {
			search.tipo = this.tipoSearch[0].value; 
			search.value = this.campoCpfSearch[0].value;
		}
		var parameters = {
			"Search" : search
		};
		// var result = serviceIdentificacaoPositiva.buscaCliente(parameters);
		result = {
			    	"codigoRetorno": 200,
			    	"descricaoRetorno": "OK",
			    	"objectsReturn": {
			        "ClientDashboard": {
			            "nome": "MARIA ALCILEIDE DOS SANTOS",
			            "cpf": "29723456818",
			            "situacaoConta": "CONTA ATIVA NORMAL",
			            "situacaoContaCod": "1",
			            "numeroCartao": "6034750704899623",
			            "situacaoCartao": "CARTAO ATIVO",
			            "situacaoCartaoCod": "1",
			            "situacaoEmbossamentoCod": "7",
			            "buscaPorAdicional": "N",
			            "endereco": "R FRANCISCO PY, 58",
			            "cidadeUf": "SAO PAULO/SP",
			            "celular": "11970417125",
			            "email": "ALCILEIDE2012VIDA@HOTMAIL.COM",
			            "dtNasc": "10/11/1972",
			            "sexo": "F",
			            "filCod": 563,
			            "filCodRetirada": 563,
			            "dtCadastro": "05/10/2006",
			            "dtUltAlteracao": "06/11/2018",
			            "diaVencimento": "7",
			            "nmMae": "FRANCISCA ADELIA DOS SANTOS",
			            "cliResDdd": "11",
			            "cliResFone": "23667923",
			            "cliIdentidade": "55619028",
			            "cliIdeOrgaoEmi": "SSPSP",
			            "cliIdeUfEmi": "SP",
			            "cliEmpSalario": 3000,
			            "inativo": "N",
			            "diasCarenciaReanalise": "-1"
			        }
			    	}
				}



		// console.log('result',result)
		if (result["codigoRetorno"] == 200) {
			
			this.getInfosCliente(result);
		} 
		else if (result["codigoRetorno"] == 204) {
	        marisaMessage.showDialog(marisaMessageType_WARNING, "Alerta","Cliente não encontrado!");
		} else {
			marisaMessage.showDialog(marisaMessageType_ERROR, "Erro","Falha ao buscar cliente!");
		}
	};
	this.getInfosCliente = function(objResult){
		this.objInfosCliente   = objResult["objectsReturn"]["ClientDashboard"];

		var temosPerguntas = this.identificarCliente(this.objInfosCliente.cpf);
		var questionarioCurrent;
		if(temosPerguntas){
			questionarioCurrent = temosPerguntas.objectsReturn.questionario;
		}
		var infoCliente = {
			"nome":this.objInfosCliente.nome,
			"cpf":this.objInfosCliente.cpf,
			"situacaoConta":this.objInfosCliente.situacaoConta,
			"nomeMae":this.objInfosCliente.nome,
			"rg":this.objInfosCliente.cliIdentidade,
			"email":this.objInfosCliente.email,
			"dataNascimento":this.objInfosCliente.dtNasc,
			"idade":this.calcularIdade(this.objInfosCliente.dtNasc),
			"numeroCartao":this.objInfosCliente.numeroCartao,
			"dataCadastro":this.objInfosCliente.cliEmpSalario,
			"situacaoCartao":this.objInfosCliente.situacaoCartao,
			"dataUltimaAlteracao":this.objInfosCliente.situacaoEmbossamentoCod,
			"vencimento":this.objInfosCliente.dtNasc,
			"filialLojaOrigem":this.objInfosCliente.dtNasc,
			"filialLojaRetirada":this.objInfosCliente.dtNasc,
		}
		gravarDadosClienteDash.storageClienteDash(this.objInfosCliente);
		initPopUp.load(this.popUpCliente,this.objInfosCliente);

		initConsultaChamados.load(this.contentChamados,this.objInfosCliente.cpf);

		$(this.contentInfos).html(detalheTemplateIdentificaPositiva.itemCardsInfos(infoCliente,questionarioCurrent));
	};
	
	this.initRender = function(){
		$(this.contentBusca).append(detalheTemplateIdentificaPositiva.formConsulta());
		this.eventosComponent()
	}
	this.eventosComponent = function(){
		var self = this;
		$(this.contentInfos).on('click', '#identificaAvancar', function(event) {
			event.preventDefault();
			console.log('identifica avancarrrrr')
			modalProtocolo.load(self.contentInfos,self.objInfosCliente,initConsultaChamados.callBack())
		});
		$(this.contentInfos).on('click', '#identificaReavaliar', function(event) {
			event.preventDefault();
			if($('.lista_perguntas_e_respostas').hasClass('off')){
				$('.lista_perguntas_e_respostas').removeClass('off');
				$('#pergGrupo02').show();
				$('#pergGrupo01').hide();
			}else {
				$('.lista_perguntas_e_respostas').addClass('off');
				$('#pergGrupo02').hide();
				$('#pergGrupo01').show();
			}
		});
		$(this.contentBusca).on('click', '#clearSearch', function(event) {
			event.preventDefault();
			$('#cpfSearch').val("");
		});
		$(this.contentBusca).on('click', '#btnSearch', function(event) {
			event.preventDefault();
			
			self.acaoBuscar();
		});
	};
	this.identificarCliente = function(cpfCli){
		var perguntasPositivas;
		parameters = {
				"cliCpf":cpfCli
		}
		var retorno = serviceIdentificacaoPositiva.perguntasPositivas(parameters);
		console.log('Debug',retorno);
		if (retorno["codigoRetorno"] == 200) {
			perguntasPositivas = retorno;
		} 
		else if (retorno["codigoRetorno"] == 204) {
			perguntasPositivas = false;
	        marisaMessage.showDialog(marisaMessageType_WARNING, "Alerta","Cliente não encontrado!");
		} else {
			perguntasPositivas = false;
			marisaMessage.showDialog(marisaMessageType_ERROR, "Erro","Falha ao buscar cliente!");
		}
		return perguntasPositivas;
	};
	this.calcularIdade = function(dataNasc){
		var d, anoNasc, mesNasc, diaNasc, anoAtual,mesAtual,diaAtual;
		console.log(dataNasc = dataNasc.split('/'))
    	d = new Date();
        anoAtual = d.getFullYear();
        mesAtual = d.getMonth() + 1;
        diaAtual = d.getDate();
        diaNasc = dataNasc[0];
        mesNasc = dataNasc[1];
        anoNasc = dataNasc[2];
    	idadeCli = anoAtual - anoNasc;
	    if (mesAtual < mesNasc || mesAtual == mesNasc && diaAtual < diaNasc) {
	        idadeCli--;
	    }
		return idadeCli < 0 ? 0 : idadeCli;
	}
}

var businessIdentificaPositiva = new BusinessIdentificaPositiva();