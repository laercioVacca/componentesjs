
	controleExibicao = true ;
function IdentificacaoPositiva()
	var identificacaoPositiva = {
		render:function () {
			var conteudo = document.querySelector('#identificacaoPositiva');
			$(conteudo).append(this.templateInfosCliente());
			$(conteudo).append(this.templateConsultaChamados());
			this.acaoAvancar();
			this.acaoReavaliar();
			return;
		},
		templateInfosCliente:function(){
			var html;
			html = '<div id="dadosCliente" class="page-title clearfix row"> ';
			html += '<ul class="stats col-xs-12 col-sm-12 col-md-12 col-lg-12"> ';
			html += '<li id="infosCliente" class="boxAlturaDinamica marisa-pink-bg col-xs-12 col-sm-12 col-md-12 col-lg-4">';
			html += '<span class="fs1" aria-hidden="true" data-icon="&#xe075;" ></span> ';
			html += '<div class="details"> ';
			html += '<span class="big">MARIA INES DA SILVA</span> ';
			html += '<span id="cpfDadosCliente" class="small"><span class="labelCliente">CPF:</span> </span>'; 
			html += '<span class="small"><span class="labelCliente">Situação Conta:</span> CONTA ATIVA</span>'; 
			html += '<div class="_showContent">';
			html += '<span class="small"><span class="labelCliente">Nome Mãe:</span> TEREZINHA DOS SANTOS ANDRADE</span>';
			html += '<span class="small"><span class="labelCliente">RG:</span> 34772143</span>';
			html += '<span class="small"><span class="labelCliente">Situação Conta:</span> CONTA ATIVA NORMAL </span>';
			html += '<span class="small"><span class="labelCliente">e-mail:</span> LIVIASANDRADE14@GMAIL.COM</span>';
			html += '<span class="small"><span class="labelCliente">Data Nascimento / Idade:</span> 14/01/1979 -  39 anos</span>';
			html += '<span class="small">&nbsp;</span>';
			html += '</div>';
			html += '</div>';
			html += '</li> ';
			html += '<li id="CartaoCliente" class="boxAlturaDinamica marisa-blue-bg col-xs-12 col-sm-12 col-md-12 col-lg-4"> ';
			html += '<span class="fs1" aria-hidden="true" data-icon="&#xe039;" ></span> ';
			html += '<div class="details"> ';
			html += '<span class="big">6034.7520.1361.9038 </span> ';
			html += '<span id="cpfDadosCliente" class="small"><span class="labelCliente">Data Cadastro</span> 01/12/2014 </span> ';
			html += '<span class="small"><span class="labelCliente">Situação Cartão:</span> CARTÃO ATIVO</span> ';
			html += '<div class="_showContent">';
			html += '<span class="small"><span class="labelCliente">Data Última Alteração:</span> 11/09/2017 </span>';
			html += '<span class="small"><span class="labelCliente">Vencimento / Melhor Dia:</span> 16</span>';
			html += '<span class="small"><span class="labelCliente">Filial / Loja Origem:</span> L 900 SP / SP  </span>';
			html += '<span class="small"><span class="labelCliente">Filial / Loja Retirada:</span> L 900 SP / SP</span>';
			html += '<span class="small"><span class="labelCliente"></span></span>';
			html += '<span class="small">&nbsp;</span>';
			html += '</div>';
			html += '</div>';
			html += '</li> ';
			html += '<li id="ListaIdentificacaoPositiva" class="boxAlturaDinamica sea-green-bg col-xs-12 col-sm-12 col-md-12 col-lg-4"> ';
			html += '<span class="fs1" aria-hidden="true" data-icon="&#xe0f7;" ></span> ';
			html += '<div class="details"> ';
			html += '<span class="big">Identificação Positiva</span>';
			html += '<div class="perguntasRespostas">';
			html += '<ul class="lista_perguntas_e_respostas">';
			html += '<li class="item_perguntaResposta">';
			html += '<span class="itens pergunta"> Qual e o dia de vencimento de sua fatura?</span>';
			html += '<span class="itens resposta">17</span>';
			html += '</li>';
			html += '<li class="item_perguntaResposta">';
			html += '<span class="itens pergunta">Qual a sua atividade profissional?</span>';
			html += '<span class="itens resposta">AUXILIARES DE ESCRITORIO E SIMILARES</span>';
			html += '</li>';
			html += '<li class="item_perguntaResposta">';
			html += '<span class="itens pergunta">QUAIS SAO OS QUATRO ULTIMOS DIGITOS DE SEU TELEFONE COMERICAL?</span>';
			html += '<span class="itens resposta">34993529</span>';
			html += '</li> ';
			html += '<li class="item_perguntaResposta">';
			html += '<span class="itens pergunta">Qual e a sua data de admissao no trabalho?</span>';
			html += '<span class="itens resposta">30/12/1999</span>';
			html += '</li>';
			html += '<li class="item_perguntaResposta">';
			html += '<span class="itens pergunta">Ha quanto tempo reside em seu endereço:</span>';
			html += '<span class="itens resposta">19</span>';
			html += '</li>';
			html += '</ul>';
			html += '</div>';
			html += '<div class="nav_acao_infos_cliente"> ';
			html += '<a id="identificaReavaliar" href="#" class="btn_identificacao btn btn-info">';
			html += '<span class="fs1" aria-hidden="true" data-icon="">Reavaliar</span>';
			html += '</a> ';
			html += '<a id="identificaAvancar" href="#" data-toggle="modal" class="btn_identificacao btn btn-success">';
			html += '<span class="fs1" aria-hidden="true" data-icon="&#xe0d4;">Avançar</span>';
			html += '</a>';
			html += '</div>';
			html += '</div> ';
			html += '</li> ';
			html += '</ul> ';
			html += '</div> ';
			return html;
		},
		templateConsultaChamados:function(){
			var htmlConsChamados;
				htmlConsChamados  = '<div id="card_01" class="card_identificacao_positiva col-xs-12 col-sm-12 col-md-12 col-lg-12">';
				htmlConsChamados += '<div class="card menu" id="menucard.id" style="margin: 0px;border: 5px solid #fbfaf6;max-width: 100%;">';
				htmlConsChamados += '<div class="panel-group" id="accordion" role="tablist aria-multiselectable="false">';
				htmlConsChamados += '<div class="panel panel-default">';
				htmlConsChamados += '<div class="panel-heading" role="tab" id="heading card.id + "">';
				htmlConsChamados += '<h4 class="panel-title">';
				htmlConsChamados += '<a class="collapsed" id="menuCollapsecard.id" role="button" style="padding-right: 5px;color: #75104c;" data-toggle="" data-parent="#accordion" aria-expanded="true aria-controls="collapsecard.id"> ';
				htmlConsChamados += '<span class="glyphicon glyphicon-thumbs-up" style="left: -30px;"></span>';
				htmlConsChamados += 'Histórico - Consulta de Chamados';
				htmlConsChamados += '</a>';
				htmlConsChamados += '</h4>';
				htmlConsChamados += '</div>';
				htmlConsChamados += '<div id="collapsecard.id" class="panel-collapse collapse show" role="tabpanel" aria-labelledby="headingcard.id">';
				htmlConsChamados += '<div class="panel-body" style="visibility: visible;padding: 5px;overflow: auto;">';
				htmlConsChamados += '<table class="table table-striped">';
				htmlConsChamados += '<thead>';
				htmlConsChamados += '<tr>';
				htmlConsChamados += '<th scope="col">#</th>';
				htmlConsChamados += '<th scope="col">Abertura</th>';
				htmlConsChamados += '<th scope="col">Chamado </th>';
				htmlConsChamados += '<th scope="col">Status </th>';
				htmlConsChamados += '</tr>';
				htmlConsChamados += '</thead>';
				htmlConsChamados += '<tbody>';
				htmlConsChamados += '<tr>';
				htmlConsChamados += '<th scope="row">1</th>';
				htmlConsChamados += '<td>20/05/2015 </td>';
				htmlConsChamados += '<td>66.211.172 </td>';
				htmlConsChamados += '<td>INTERROMPIDO </td>';
				htmlConsChamados += '</tr>';
				htmlConsChamados += '<tr>';
				htmlConsChamados += '<th scope="row">2</th>';
				htmlConsChamados += '<td>29/09/2014 </td>';
				htmlConsChamados += '<td>64.179.343</td>';
				htmlConsChamados += '<td>INTERROMPIDO</td>';
				htmlConsChamados += '</tr>';
				htmlConsChamados += '<tr>';
				htmlConsChamados += '<th scope="row">3</th>';
				htmlConsChamados += '<td>29/09/2014</td>';
				htmlConsChamados += '<td>64.179.343</td>';
				htmlConsChamados += '<td>INTERROMPIDO</td>';
				htmlConsChamados += '</tr>';
				htmlConsChamados += '</tbody>';
				htmlConsChamados += '</table>';				
				htmlConsChamados += '</div>';
				htmlConsChamados += '</div>';
				htmlConsChamados += '</div>';
				htmlConsChamados += '</div>';
				htmlConsChamados += '</div>';
				htmlConsChamados += '</div>';
			return htmlConsChamados;
		},
		acaoAvancar:function(){
			var btnAvancar = $('#identificaAvancar');
			$(btnAvancar).on('click', function(event) {
				event.preventDefault();
				ModalPortalSAC.render('#identificacaoPositiva')
			});
			return;
		},
		acaoReavaliar:function(){
			var btnReavaliar = $('#identificaReavaliar');
			$(btnReavaliar).on('click', function(event) {
				event.preventDefault();
				console.log('Vamos reavaliar!!!')
			});
		}
	}
	// 
	var ModalPortalSAC = {
		render:function(local){
			$(local).append(this.template())
			$('#ModalIdentificacaoAvancar').modal('show');
			this.acaoNovoChamado();
			this.acaoReutilizar();
			return;
		},
		template:function(){
			var html;
				html  = '<div id="ModalIdentificacaoAvancar" class="modal fade modalSAC" tabindex="-1" role="dialog">';
				html += '<div class="modal-dialog modal-dialog-centered" role="document">';
				html += '<div id="modalContent" class="modal-content">';
				html += '<div class="modal-header" id="messageHeader" style="display: block; background-color: rgba(33, 129, 212, 0.45);">';
				html += '<button type="button" class="close" data-dismiss="modal" aria-label="Close" style="padding-top: 0px;"><span aria-hidden="true">×</span></button>';
				html += '<h4 id="messageTitle" class="modal-title">Atenção</h4>';
				html += '</div>';
				html += '<div class="modal-body" style="display: inline-flex;font-size: 14px;">';
				html += '<h1 style="margin-top: 4px;"><i id="dialog-icon" style="color: rgba(33, 129, 212, 0.95);" class="fa fa-question-circle col-lg-1"></i></h1>';
				html += '<div>';
				html += '<label id="dialog-message" style="padding-top: 7px;">';
				html += '<div class="form-inline has-feedback">';
				html += '<span>Você deseja utilizar o último chamado em atendimento<br> N° <span class="destaque_chamado"> 22.534.789 </span>  ou criar um novo ?</span>';
				html += '</div>';					
				html += '</label>';
				html += '</div>';
				html += '</div>';
				html += '<div class="modal-footer" style="background-color: rgb(243, 243, 243);padding: 10px;">';
				html += '<button type="button" id="btnNovo" class="btn btn-primary" style="font-size: 14px;">Novo</button>';
				html += '<button type="button" id="btnReutilizar" style="font-size: 14px;" onclick="" class="btn btn-default">Reutilizar</button>';
				html += '</div>';
				html += '</div>';
				html += '</div>';
				html += '</div>';
			return html;
		},
		acaoNovoChamado:function(){
			var disparaAcaoNovoChamado = $('#btnNovo').on('click', function(event) {
				event.preventDefault();
				console.log('Confirmado Novo Chamado');
				$('#ModalIdentificacaoAvancar').modal('hide');
				var __URL = window.location.href + "?flagRender=ok";
				window.location = __URL;
				components.render();
				$('#panelIdentificacaoPositiva').hide();
				
			});
			return disparaAcaoNovoChamado;
		},
		acaoReutilizar:function(){
			var disparaAcaoReutilizarChamado = $('#btnReutilizar').on('click', function(event) {
				event.preventDefault();
				console.log('Confirmado Reutilizar');
				$('#ModalIdentificacaoAvancar').modal('hide');
				$('#panelIdentificacaoPositiva').hide();
				controllStorageSac.setFlagIdentificacaoPositiva();
				// var __URL = window.location.href + "?flagRender=ok";
				// window.location = __URL;
				components.render();
			});
			return disparaAcaoReutilizarChamado;
		}
	}
	// 
	identificacaoPositiva.render();
}

