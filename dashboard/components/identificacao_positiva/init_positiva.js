require("components/identificacao_positiva/components/business_identificacao_positiva.js");

function InitIdentiPositiva() {
	this.load = function(){
	    
		var popUpCliente    = $('#masterPopUpCliente');
		var masterContent   = $('#masterIdentificacaoPositiva');
		var contentBusca    = $('#consultaCliente');
		var contentInfos    = $('#masterContentInfosCliente');
		var contentChamados = $('#contentComponentChamados');

		businessIdentificaPositiva.load(popUpCliente,masterContent,contentBusca,contentInfos,contentChamados);
	}
	
}
var initIdentiPositiva = new InitIdentiPositiva();


