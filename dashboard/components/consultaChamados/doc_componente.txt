
Documentação Components.

 Consulta Chamado
 	- Components
 		- card_consultaChamados.js
 		- template_consultaChamados.js
 		- business_consultaChamados.js
        - service_consultaChamados.js
 	- consultaChamados.html
 	- init_consultaChamados.js
-------------------------------------------	
Componente Chamado na identificação Positiva importado o init_consultaChamados.js
no bussines_identificacao_positiva, e instanciado no metodo renderComponent do mesmo
Instanciação do Component consulta Chamados

1 Parametro -> this.contentChamados (div para renderizar o component);
2 Parametro -> this.objInfosCliente.cpf (CPF do Cliente Corrent usado pra chamar o servico dos chamados)

initConsultaChamados.load(this.contentChamados,this.objInfosCliente.cpf)

Parametros servico protocolo

{
    "cpfUsuario":"11111111111",
    "loginUsuario":"CCM",
    "ipUsuario":"0:0:0:0:0:0:0:1",
    "modulo":5,
    "rotina":14,
    "idProcesso": "5555555555",
    "idServico": "777777777777777",    
    "service":"protocolo",
    "functionality":"gerarProtocolo",
    "filial":"900",
    "requestObjects":{
		"Protocolo":{
			"cliCpf": "11815286822",
			"reutilizar": "true"
                 }
         }
}
