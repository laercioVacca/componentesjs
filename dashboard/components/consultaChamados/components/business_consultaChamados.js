
require("components/consultaChamados/components/template_consultaChamados.js");
require("components/consultaChamados/components/service_consultaChamados.js");
function BusinessConsultaChamados() {
	this.load = function(boxComponent,cpfCurrentCliente,hashProcesso){
		this.ultimoAtendimento = 0;
		this.hashProcesso      = hashProcesso;
		this.cpfCurrentCliente  = cpfCurrentCliente;
		this.boxComponent 		= boxComponent;
		this.consultaServico(cpfCurrentCliente,hashProcesso);
		this.contrutorComponente();
	};
	this.contrutorComponente = function(){
		if(this.codigoRetorno == 200){
			if(this.objectsReturn.chamados.length != 0){
				$(this.boxComponent).html(templateConsultaChamados.estrutura());
				this.listaItem();
			}else {
				$(this.boxComponent).html(templateConsultaChamados.semInfos())
			}
		}else {
			$(this.boxComponent).html(templateConsultaChamados.semInfos())
		}
		this.eventosComponent();
	}
	this.consultaServico = function(){
		
		parameters = {
				"cliCpf":this.cpfCurrentCliente
		}
		this.historicoChamado 	= serviceConsultaChamados.historico(this.hashProcesso,parameters);

		this.codigoRetorno 		= this.historicoChamado.codigoRetorno;
		this.descricaoRetorno 	= this.historicoChamado.descricaoRetorno;
		this.objectsReturn    	= this.historicoChamado.objectsReturn;

		console.log('serio????? getHistoricoChamados ',this.historicoChamado)
	}
	this.listaItem = function(){
		var self = this;
		var chamados = false;
		this.tipoTmpl = "identPositiva";
		$.each(this.objectsReturn.chamados, function(index, val) {
			var identificador =  'ident_' + index;
			var propsListaChamados = {
				'idUnico':identificador,
				'dataAbertura':val.dtAbertura,
				'numeroProtocolo':val.numeroProtocolo,
				'status':val.status,
			};
			if(index <= 4){
				if(index == 0){
					self.ultimoAtendimento = self.getUltimoAtendimento(index,val.status,val.numeroProtocolo)
				}
				self.controleTemplateLista(self.tipoTmpl,propsListaChamados)
			}
			else {
				return false;
			}
		});
		
	};
	this.getUltimoAtendimento = function(index,status,protocolo){
		var ultimoProtocolo = 0;
		if(status == "EM ATENDIMENTO"  && index == 0){
			ultimoProtocolo = protocolo;
		}
		return ultimoProtocolo;
	}
	this.setUltimoAtendimento = function(){
		return this.ultimoAtendimento;
	}
	this.controleTemplateLista = function(render,propsListaChamados){
		if(this.tipoTmpl == 'identPositiva'){
			$('#masterContentListaChamados').append(templateConsultaChamados.itensIdentificacaoPositiva(propsListaChamados));
		}else {
			$('#masterContentListaChamados').append(templateConsultaChamados.listaItens(propsListaChamados));
		}
	}
 	this.salvarDadosDeEnvio = function(){
		return console.log('SALVANDO o Retorno dos dados!!!!');
	};
	this.eventosComponent = function(){
		$(this.boxComponent).on('click', '.btn_drop_memo', function(event) {
			event.preventDefault();
		});
		$(this.boxComponent).on('click', '.btn_drop_memo', function(event) {
			event.preventDefault();
			var conteudoDrop = $(this).attr('data-target');
			$('.collapse').hide();
			$(conteudoDrop).show();
		});
		$(this.boxComponent).on('click', '.btn_drop_assuntos', function(event) {
			event.preventDefault();
			var conteudoDrop = $(this).attr('data-target');
			// $('.collapse').hide();
			$(conteudoDrop).show();
		});
		$(this.boxComponent).on('click','.btnAcaoSalvar', function(event) {
            event.preventDefault();
            var elementoRaiz = $('.referenciaModalcard');
			var gravarDadosEnvio = function(){
               return self.salvarDadosDeEnvio();
            }
            var propsModal = {
                "elementoRaiz":elementoRaiz,
                "tipoModal":"acaodupla",
                "titulo":"Confirmação de Dados",
                "texto":"Deseja realmente Salvar Estes Dados?",
                "funcao":gravarDadosEnvio
            }
            customModalSac.load(propsModal)
        });
	};
	
}

var businessConsultaChamados = new BusinessConsultaChamados();