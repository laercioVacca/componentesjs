function TemplateConsultaChamados() {
	
	this.estrutura = function(){
		var retornoHtml;
			retornoHtml  = '<div class="card menu" id="menucard.id" style="margin: 0px;border: 5px solid #fbfaf6;max-width: 100%;">';
			retornoHtml += '<div class="panel-group" id="accordion" role="tablist aria-multiselectable="false">';
			retornoHtml += '<div class="panel panel-default">';
			retornoHtml += '<div class="panel-heading" role="tab" id="heading card.id + "">';
			retornoHtml += '<h4 class="panel-title">';
			retornoHtml += '<a class="collapsed" id="menuCollapsecard.id" role="button" style="padding-right: 5px;color: #75104c;" data-toggle="" data-parent="#accordion" aria-expanded="true aria-controls="collapsecard.id">'; 
			retornoHtml += '<span class="glyphicon glyphicon-thumbs-up" style="left: -30px;"></span>';
			retornoHtml += 'Histórico - Consulta de Chamados';
			retornoHtml += '</a>';
			retornoHtml += '</h4>';
			retornoHtml += '</div>';
			retornoHtml += '<div id="collapsecard.id" class="panel-collapse collapse show" role="tabpanel" aria-labelledby="headingcard.id">';
			retornoHtml += '<div class="panel-body" style="visibility: visible;padding: 0px;overflow: auto;">';
			retornoHtml += '<div class="MSF_drop_type01 page-title">';
			retornoHtml += '<div class="dropPadrao left_box_grid showTab">';
			retornoHtml += '<div class="conteudo_dinamico_tabs">';
			retornoHtml += '<div class="grupo_faturas lista_chamados col-sm-12 col-md-12 col-lg-12 col-xl-12">';
			retornoHtml += '<div class="item_fatura titulo_header itemData">Data Abertura</div>';
			retornoHtml += '<div class="item_fatura titulo_header itemChamado">Numero do Chamado </div>';
			// retornoHtml += '<div class="item_fatura titulo_header itemSolucao">Data prevista para solução </div>';
			retornoHtml += '<div class="item_fatura titulo_header itemStatus">Status Chamado </div>';
			retornoHtml += '<div class="clear"></div>';
			retornoHtml += '</div>';
			retornoHtml += '<div id="masterContentListaChamados" class="MRF_listadrop">';
			retornoHtml += '</div>';
			retornoHtml += '</div>';
			retornoHtml += '</div>';
			retornoHtml += '</div>';
			retornoHtml += '</div>';
			retornoHtml += '</div>';
			retornoHtml += '</div>';
			retornoHtml += '</div>';
			retornoHtml += '</div>';		
		return retornoHtml;
	};
	this.itensIdentificacaoPositiva = function(objProps){
		var retornoHtml;
		retornoHtml = '<div class="card">';
			retornoHtml += '<div class="card-header card_acordeon">';
					retornoHtml += '<div class="area_click" id="headingOne" style="width:100%; padding-left:10px ">';
						retornoHtml += '<div class="titulo_fatura itemData">'+objProps.dataAbertura+'</div>';
						retornoHtml += '<div class="titulo_fatura itemChamado">'+objProps.numeroProtocolo+'</div>';
						// retornoHtml += '<div class="titulo_fatura itemSolucao"> 05/12/2018</div>';
						retornoHtml += '<div class="titulo_fatura itemStatus">'+objProps.status+'</div>';	
					retornoHtml += '</div>';
			retornoHtml += '</div>';
		retornoHtml += '</div>';
		return retornoHtml;
	}
	this.listaItens = function(objProps){
		var retornoHtml;
		retornoHtml = '<div class="card">';
		retornoHtml += '<div class="card-header card_acordeon">';
		retornoHtml += '<a class="btn_drop_memo" href="#" data-target="#'+objProps.idUnico+'" aria-expanded="true" aria-controls="'+objProps.idUnico+'">';
		retornoHtml += '<div class="area_click" id="headingOne" style="width:100%; padding-left:10px ">';
		retornoHtml += '<div class="titulo_fatura itemData">'+objProps.dataAbertura+'</div>';
		retornoHtml += '<div class="titulo_fatura itemChamado">'+objProps.numeroProtocolo+'</div>';
		retornoHtml += '<div class="titulo_fatura itemSolucao"> 05/12/2018</div>';
		retornoHtml += '<div class="titulo_fatura itemStatus">'+objProps.status+'</div>';	
		retornoHtml += '</div>';
		retornoHtml += '</a>';
		retornoHtml += '</div>';

		retornoHtml += '<div id="'+objProps.idUnico+'" class="collapse " aria-labelledby="headingOne2" data-parent="#accordion">';    
		retornoHtml += '<div class="">';
		retornoHtml += '<div class="lista_detalhe_item">';
		retornoHtml += '<div class="list-blocos-group lista_grid_02 full_grid">'; 
		retornoHtml += '<div class="alt_din list-group-item list-group-item-primary titulo_divisor" >';
		retornoHtml += '<span class="item_lista_titulo">Cliente Atendido</span>';
		retornoHtml += '<div class="lista_grupo_clear"></div>';
		retornoHtml += '</div>';
		retornoHtml += '</div>';
		retornoHtml += '<div class="list-blocos-group lista_grid_02 full_grid">'; 
		retornoHtml += '<div class="alt_din list-group-item list-group-item-primary" >';
		retornoHtml += '<span class="item_lista_titulo ">Nº do Chamado</span>';
		retornoHtml += '<span class="item_lista_valor "> 22.529.678  </span>';
		retornoHtml += '<div class="lista_grupo_clear"></div>';
		retornoHtml += '</div>';
		retornoHtml += '</div>';
		retornoHtml += '<div class="list-blocos-group lista_grid_02 full_grid">'; 
		retornoHtml += '<div class="alt_din list-group-item list-group-item-primary" >';
		retornoHtml += '<span class="item_lista_titulo ">Data e Hora de abertura</span>';
		retornoHtml += '<span class="item_lista_valor ">05/12/2018 10:44</span>';
		retornoHtml += '<div class="lista_grupo_clear"></div>';
		retornoHtml += '</div>';
		retornoHtml += '</div>';

		retornoHtml += templateSelectEnvioProtocolo();
		retornoHtml += templateSelectNotificacaoEncerramento();
		retornoHtml += templateSelectEnvioHistorico();
		retornoHtml += templateSelectEnvioCancelamento();

		retornoHtml += '<div class="list-blocos-group lista_grid_02 full_grid"> ';
		retornoHtml += '<div class="alt_din list-group-item list-group-item-primary" >';
		retornoHtml += '<span class="item_lista_titulo ">Quantidade de rechamadas</span>';
		retornoHtml += '<span class="item_lista_valor ">0</span>';
		retornoHtml += '<div class="lista_grupo_clear"></div>';
		retornoHtml += '</div>';
		retornoHtml += '</div>';

		retornoHtml += templateInputEmail();

		retornoHtml += '<div class="list-blocos-group lista_grid_02 full_grid"> ';
		retornoHtml += '<div class="alt_din list-group-item list-group-item-primary content_btn_salvar_drop">';
		retornoHtml += '<a id="btnSalvarDadosEnvio" href="#" data-toggle="modal" class="btnAcaoSalvar btn_page_infos btn btn-success">';
		retornoHtml += '<span class="fs1" data-icon="">SALVAR DADOS DE ENVIO</span>';
		retornoHtml += '</a>';
		retornoHtml += '<div class="lista_grupo_clear"></div>';
		retornoHtml += '</div>';
		retornoHtml += '</div>';

		// listagem de assuntos

		retornoHtml += templateListaAssuntosChamados(objProps.idUnico);


		//box de ações 

		retornoHtml += '<div id="acoesNavegacao" class="list-blocos-group lista_grid_02 full_grid"> ';
		retornoHtml += '<div class="alt_din list-group-item list-group-item-primary lista_btn_detalhe_chamado">';
		
		retornoHtml += '<a id="btnComunicaClienteObs" href="#" data-toggle="modal" class="btnAcao btn_page_infos btn btn-success">';
		retornoHtml += '<span class="fs1" data-icon="">Comunicação ao Cliente</span>';
		retornoHtml += '</a>';
		
		retornoHtml += '<a id="btnRechamadaObs" href="#" data-toggle="modal" class="btnAcao btn_page_infos btn btn-success">';
		retornoHtml += '<span class="fs1" data-icon="">Rechamada</span>';
		retornoHtml += '</a>';
		
		retornoHtml += '<a id="btnHistoricoObs" href="#" data-toggle="modal" class="btnAcao btn_page_infos btn btn-success">';
		retornoHtml += '<span class="fs1" data-icon="">Historico</span>';
		retornoHtml += '</a>';
		retornoHtml += '<div class="lista_grupo_clear"></div>';
		retornoHtml += '</div>';
		retornoHtml += '<div id="boxInfoAcao">INFOS</div>';
		retornoHtml += '</div>';

		

		retornoHtml += '</div>';
		retornoHtml += '</div>';
		retornoHtml += '</div>';
		retornoHtml += '</div>';

		// Template Select envio de protocolo
		function templateSelectEnvioProtocolo(){
			var tmpSelect01;
				tmpSelect01  = '<div class="list-blocos-group lista_grid_02 full_grid">'; 
				tmpSelect01 += '<div class="alt_din list-group-item list-group-item-primary" style="padding: 7px 0 5px 0;" >';
				tmpSelect01 += '<span class="item_lista_titulo contentFormDropTitulo">Meio de envio do protocolo</span>';
				tmpSelect01 += '<span class="item_lista_valor contentFormDrop">';
				tmpSelect01 += '<div class="form-group">';
				tmpSelect01 += '<select id="inputState" class="form-control select_drop">';
				tmpSelect01 += '<option selected>Escolha</option>';
				tmpSelect01 += '<option value="telefone">Telefone</option>';
				tmpSelect01 += '<option value="email">E-mail</option>';
				tmpSelect01 += '<option value="correio">Correio</option>';
				tmpSelect01 += '<option value="fax">Fax</option>';
				tmpSelect01 += '<option value="sms">SMS</option>';
				tmpSelect01 += '</select>';
				tmpSelect01 += '<button type="submit" class="btn btn-primary">Enviar</button>';
				tmpSelect01 += '</div>';
				tmpSelect01 += '</span>';
				tmpSelect01 += '<div class="lista_grupo_clear"></div>';
				tmpSelect01 += '</div>';
				tmpSelect01 += '</div>';
			return tmpSelect01;
		}
		// Template Select Notificação encerramento
		function templateSelectNotificacaoEncerramento(){
			var tmpSelect02;
				tmpSelect02  = '<div class="list-blocos-group lista_grid_02 full_grid">'; 
				tmpSelect02 += '<div class="alt_din list-group-item list-group-item-primary" style="padding: 7px 0 5px 0;" >';
				tmpSelect02 += '<span class="item_lista_titulo contentFormDropTitulo">Meio de notificaçao do encerramento</span>';
				tmpSelect02 += '<span class="item_lista_valor contentFormDrop">';
				tmpSelect02 += '<div class="form-group">';
				tmpSelect02 += '<select id="inputState" class="form-control select_drop">';
				tmpSelect02 += '<option selected>Escolha</option>';
				tmpSelect02 += '<option value="telefone">Telefone</option>';
				tmpSelect02 += '<option value="email">E-mail</option>';
				tmpSelect02 += '<option value="correio">Correio</option>';
				tmpSelect02 += '<option value="fax">Fax</option>';
				tmpSelect02 += '<option value="sms">SMS</option>';
				tmpSelect02 += '</select>';
				tmpSelect02 += '<button type="submit" class="btn btn-primary">Enviar</button>';
				tmpSelect02 += '</div>';
				tmpSelect02 += '</span>';
				tmpSelect02 += '<div class="lista_grupo_clear"></div>';
				tmpSelect02 += '</div>';
				tmpSelect02 += '</div>';
			return tmpSelect02;
		}
		// Template Select meio Envio Historico
		function templateSelectEnvioHistorico(){
			var tmpSelect02;
				tmpSelect02  = '<div class="list-blocos-group lista_grid_02 full_grid">'; 
				tmpSelect02 += '<div class="alt_din list-group-item list-group-item-primary" style="padding: 7px 0 5px 0;" >';
				tmpSelect02 += '<span class="item_lista_titulo contentFormDropTitulo">Meio de envio do histórico</span>';
				tmpSelect02 += '<span class="item_lista_valor contentFormDrop">';
				tmpSelect02 += '<div class="form-group">';
				tmpSelect02 += '<select id="inputState" class="form-control select_drop">';
				tmpSelect02 += '<option selected>Escolha</option>';
				tmpSelect02 += '<option value="email">E-mail</option>';
				tmpSelect02 += '<option value="correio">Correio</option>';
				tmpSelect02 += '<option value="fax">Fax</option>';
				tmpSelect02 += '</select>';
				tmpSelect02 += '<button type="submit" class="btn btn-primary">Enviar</button>';
				tmpSelect02 += '</div>';
				tmpSelect02 += '</span>';
				tmpSelect02 += '<div class="lista_grupo_clear"></div>';
				tmpSelect02 += '</div>';
				tmpSelect02 += '</div>';
			return tmpSelect02;
		}
		// Template Select Meio de envio de comprovantes de cancelamento
		function templateSelectEnvioCancelamento(){
			var tmpSelect02;
				tmpSelect02  = '<div class="list-blocos-group lista_grid_02 full_grid">'; 
				tmpSelect02 += '<div class="alt_din list-group-item list-group-item-primary" style="padding: 7px 0 5px 0;" >';
				tmpSelect02 += '<span class="item_lista_titulo contentFormDropTitulo">Meio de envio de comprovantes de cancelamento</span>';
				tmpSelect02 += '<span class="item_lista_valor contentFormDrop">';
				tmpSelect02 += '<div class="form-group">';
				tmpSelect02 += '<select id="inputState" class="form-control select_drop">';
				tmpSelect02 += '<option selected>Escolha</option>';
				tmpSelect02 += '<option value="email">E-mail</option>';
				tmpSelect02 += '<option value="correio">Correio</option>';
				tmpSelect02 += '<option value="fax">Fax</option>';
				tmpSelect02 += '</select>';
				tmpSelect02 += '<button type="submit" class="btn btn-primary">Enviar</button>';
				tmpSelect02 += '</div>';
				tmpSelect02 += '</span>';
				tmpSelect02 += '<div class="lista_grupo_clear"></div>';
				tmpSelect02 += '</div>';
				tmpSelect02 += '</div>';
			return tmpSelect02;
		}
		// Template Select Meio de envio de comprovantes de cancelamento
		function templateInputEmail(){
			var tmpSelect02;
				tmpSelect02  = '<div class="list-blocos-group lista_grid_02 full_grid">'; 
				tmpSelect02 += '<div class="alt_din list-group-item list-group-item-primary" style="padding: 7px 0 5px 0;" >';
				tmpSelect02 += '<span class="item_lista_titulo contentFormDropTitulo">Email</span>';
				tmpSelect02 += '<span class="item_lista_valor contentFormDrop">';
				tmpSelect02 += '<div class="form-group">';
				tmpSelect02 += '<input type="email" class="form-control input_drop" id="emailIdentificaPositivo" placeholder="Email">';
				tmpSelect02 += '</div>';
				tmpSelect02 += '</span>';
				tmpSelect02 += '<div class="lista_grupo_clear"></div>';
				tmpSelect02 += '</div>';
				tmpSelect02 += '</div>';
			return tmpSelect02;
		}
		// Template para Listagem de assuntos por chamados
		function templateListaAssuntosChamados(identificador){
			var retornoAssuntos;
			// box assuntos final
			retornoAssuntos = '<div id="listaAssuntosDetalheChamado">';
			retornoAssuntos += '<div class="list-blocos-group lista_grid_02 full_grid">';
				retornoAssuntos += '<a class="btn_drop_assuntos" href="#" data-target="#assuntos_'+identificador+'" aria-expanded="true" aria-controls="assuntos_'+identificador+'">'; 
					retornoAssuntos += '<div class="alt_din list-group-item list-group-item-primary titulo_divisor">';
						retornoAssuntos += '<span class="item_lista_titulo">1º Assunto</span>';
						retornoAssuntos += '<div class="lista_grupo_clear"></div>';
					retornoAssuntos += '</div>';
				retornoAssuntos += '</a>';
			retornoAssuntos += '</div>';
			retornoAssuntos += '<div id="assuntos_'+identificador+'" class="collapse " aria-labelledby="headingOne2" data-parent="#accordion">';
				// 01
				retornoAssuntos += '<div class="list-blocos-group lista_grid_02 full_grid"> ';
					retornoAssuntos += '<div class="alt_din list-group-item list-group-item-primary" >';
						retornoAssuntos += '<span class="item_lista_titulo ">Tipo</span>';
						retornoAssuntos += '<span class="item_lista_valor ">RENEGOCIACAO DE PROMESSA OU ACORDO</span>';
						retornoAssuntos += '<div class="lista_grupo_clear"></div>';
					retornoAssuntos += '</div>';
				retornoAssuntos += '</div>';
				// 02
				retornoAssuntos += '<div class="list-blocos-group lista_grid_02 full_grid"> ';
					retornoAssuntos += '<div class="alt_din list-group-item list-group-item-primary" >';
						retornoAssuntos += '<span class="item_lista_titulo ">Status</span>';
						retornoAssuntos += '<span class="item_lista_valor ">RESOLVIDO</span>';
						retornoAssuntos += '<div class="lista_grupo_clear"></div>';
					retornoAssuntos += '</div>';
				retornoAssuntos += '</div>';
				// 03
				retornoAssuntos += '<div class="list-blocos-group lista_grid_02 full_grid"> ';
					retornoAssuntos += '<div class="alt_din list-group-item list-group-item-primary" >';
						retornoAssuntos += '<span class="item_lista_titulo ">Prazo de solução</span>';
						retornoAssuntos += '<span class="item_lista_valor ">Imediato</span>';
						retornoAssuntos += '<div class="lista_grupo_clear"></div>';
					retornoAssuntos += '</div>';
				retornoAssuntos += '</div>';
				// 04
				retornoAssuntos += '<div class="list-blocos-group lista_grid_02 full_grid">'; 
					retornoAssuntos += '<div class="alt_din list-group-item list-group-item-primary titulo_divisor titulo_divisor_textarea" >';
						retornoAssuntos += '<span class="item_lista_titulo">Observação</span>';
						retornoAssuntos += '<div class="lista_grupo_clear"></div>';
					retornoAssuntos += '</div>';
				retornoAssuntos += '</div>';
				// 05
				retornoAssuntos += '<div class="list-blocos-group lista_grid_02 full_grid">'; 
					retornoAssuntos += '<div class="alt_din list-group-item list-group-item-primary titulo_divisor" >';
						retornoAssuntos += '<span class="item_lista_titulo">';
							retornoAssuntos += '<textarea class="form-control" id="exampleTextarea" rows="3"></textarea>';
						retornoAssuntos += '</span>';
						retornoAssuntos += '<div class="lista_grupo_clear"></div>';
					retornoAssuntos += '</div>';
				retornoAssuntos += '</div>';
			retornoAssuntos += '</div>';
			retornoAssuntos += '</div>';
			// box assuntos final
			// retornoAssuntos += '</div>';
			// retornoAssuntos += '</div>';
			// retornoAssuntos += '</div>';
			// retornoAssuntos += '</div>';
			return retornoAssuntos;
		}


		return retornoHtml;
	};
	this.semInfos = function(){
		var tmp;
		tmp  = '<div style="text-align: center;" class="alert alert-danger" role="alert">';
  		tmp += 'Sem Chamados Anteriores!';
		tmp += '</div>';
		return tmp;
	}

}
var templateConsultaChamados = new TemplateConsultaChamados();