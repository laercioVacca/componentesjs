function ServiceConsultaChamados() {

	this.historico = function(idProcesso,parameters){
		this.servicoHistoricoChamados  = new GenericService('psfclientservices');
		this.getHistoricoChamados      = this.servicoHistoricoChamados.callPostService("historicoChamado", "obterHistoricoChamado",idProcesso, parameters);
		return this.getHistoricoChamados;
	};
	
}
var serviceConsultaChamados = new ServiceConsultaChamados()