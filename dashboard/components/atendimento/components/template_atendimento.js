function TemplateAtendimento() {
	
	this.atendimentoBox = function(flagPage){
		console.log('flagPage TemplateAtendimento>>>>>>>>',flagPage)
		var retornoHtml;
			retornoHtml  = '<div id="cardProcedimento" class="card_atendimento" nome-cp="teste-CP02">';
			retornoHtml += '<div class="card menu" id="menucard.id" style="margin: 0px;border: 5px solid #fbfaf6;max-width: 100%;">';
			retornoHtml += '<div class="panel-group" id="accordion" role="tablist aria-multiselectable="false">';
			retornoHtml += '<div class="panel panel-default">';
			retornoHtml += '<div class="panel-heading" role="tab" id="heading card.id + "">';
			retornoHtml += '<h4 class="panel-title">';
			retornoHtml += '<a class="collapsed" id="btnShowProcedimentos" role="button" style="padding-right: 5px;color: #75104c;" data-toggle="" data-parent="#accordion" aria-expanded="true aria-controls="collapsecard.id">'; 
			retornoHtml += '<span class="glyphicon glyphicon-thumbs-up" style="left: -30px;"></span>';
			retornoHtml += 'Atendimento';
			retornoHtml += '</a>';
			retornoHtml += '</h4>';
			retornoHtml += '</div>';

			retornoHtml += '<div id="lancamentosAtendimento" class="panel-collapse collapse show" role="tabpanel" aria-labelledby="headingcard.id">';
				retornoHtml += '<div class="panel-body">';
				// box log
					retornoHtml += '<div class="box_opcao_envio">';
						retornoHtml += '<h3 class="titulo_lancamentos">Lançamento Manual</h3>';
						retornoHtml += '<ul class="lista_opcao_envio">';
							retornoHtml += '<li class="item_lista espaco">';
								retornoHtml += '<form>';
									retornoHtml += '<div class="form-group">';
										retornoHtml += '<textarea class="textolancamentoEditar form-control" id="campoTextoLogManual"></textarea>';						    	
									retornoHtml += '</div>';
									retornoHtml += '<div class="box_btn_inserir form-group">';
										retornoHtml += '<a href="#" id="btnInseirLog" class="btn btn-primary mb-2">Inserir</a>';
									retornoHtml += '</div>';
								retornoHtml += '</form>';
							retornoHtml += '</li>';
						retornoHtml += '</ul>';
						retornoHtml += '<div class="clear"></div>';
					retornoHtml += '</div>';
				// box log	
					retornoHtml += '<h3 class="titulo_lancamentos">Lançamentos</h3>';
					retornoHtml += '<div id="box_lancamentos_dinamicos">';
					retornoHtml += '</div>';
				// box log
				retornoHtml += '</div>';
				// box log
				retornoHtml += '<div class="panel-footer col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding: 2px 5px 2px 2px;background-color: #dddddd;">'; 
					retornoHtml += '<a id="btnFinalizacaoChamado" href="finalizaChamado.html" class="sidebar_link btn btn-success btn" style="margin: 3px 3px 3px 3px;height: 20px;float: right;">';
						retornoHtml += '<span class="glyphicon glyphicon-ok"> FINALIZAR</span>'; 
					retornoHtml += '</a>';
				retornoHtml += '</div>';
				// box log
			retornoHtml += '</div>';

			retornoHtml += '</div>';
			retornoHtml += '</div>';
			retornoHtml += '</div>';
			retornoHtml += '</div>';
		return retornoHtml;
	};
	this.lancamentoManualTmp = function(){
		retornoHtml += '<div class="box_opcao_envio">';
		retornoHtml += '<h3 class="titulo_lancamentos">Lançamento Manual</h3>';
		retornoHtml += '<ul class="lista_opcao_envio">';
		retornoHtml += '<li class="item_lista espaco">';
		retornoHtml += '<form>';
		retornoHtml += '<div class="form-group">';
		retornoHtml += '<textarea class="textolancamentoEditar form-control" id="campoTextoLogManual"></textarea>';						    	
		retornoHtml += '</div>';
		retornoHtml += '<div class="box_btn_inserir form-group">';
		retornoHtml += '<button id="btnInseirLog" type="submit" class="btn btn-primary mb-2">Inserir</button>';
		retornoHtml += '</div>';
		retornoHtml += '</form>';
		retornoHtml += '</li>';
		retornoHtml += '</ul>';
		retornoHtml += '<div class="clear"></div>';
		retornoHtml += '</div>';
	};

	this.listaLogAtend = function(idLog,identificadorCardLog,tituloLog,textoLog){
		var html;
		html =  '<div id="carlogID_'+idLog+'" class="card">';
		html += '<div class="card-header" id="headingOne2" style="padding-top:0px;padding-bottom: 0px">';
		html += '<a class="att_btn_editar" href="#" data-toggle="collapse" data-target="#'+identificadorCardLog+'" aria-expanded="true" aria-controls="collapseOne2">';
		html += '<span class="nomeLancamento">'+tituloLog+'</span>';				    		
		html += '</a>';
		html += '<a href="#" log-id="'+idLog+'" class="excluir_log btn_editar_log_atendimento btn btn-danger btn-xs" style="margin-top: 3px">';
		html += '<span class="glyphicon glyphicon-remove"></span>'; 
		html += '</a> ';
		html += '</div>';
		html += '<div id="'+identificadorCardLog+'" class="collapse" aria-labelledby="headingOne2" data-parent="#accordion">';
		html += '<div class="card-body">';
		html += '<form>';
		html += '<div class="form-group">';
		html += '<textarea class="textolancamentoEditar form-control" id="campoTexto" rows="5">'+textoLog+'</textarea>';
		html += '</div>';
		html += '<div class="btn_editar_texto form-group">';
		html += '<button id="btnEditartextoLancamento" type="submit" class="btn btn-primary mb-2">Aplicar</button>';
		html += '</div>';
		html += '</form>';
		html += '</div>';
		html += '</div>';
		html += '</div>';
		return html;
	}

}
var templateAtendimento = new TemplateAtendimento();