function LancamentosManuais(){
	this.init = function(){
		this.hasAtendimento();
		console.log('Comecando Storage',this.getValorStorage())
		this.listaLancamentosExistentes();
	}
	// se tiver algum Lançamento no storage  Lista
	this.listaLancamentosExistentes = function(){
		var logStorage = this.getValorStorage();
		var boxListaLog = $('#box_lancamentos_dinamicos');
		var self = this;
		console.table(logStorage)
		$(boxListaLog).empty();
		$.each(logStorage, function(index, val) {
			console.log('GRAVO AS HORAS',val)
		    idLog  				 = val.id;
		    data 				 = val.data;	
			tituloLog            = val.titulo
			textoLog             = val.textoLog;
			identificadorCardLog = val.idUnicoCard;
			ordemObj             = index;
			$(boxListaLog).append(self.templateListaLog(idLog,data,identificadorCardLog,tituloLog,textoLog,ordemObj));
		});
		this.deletarLog();
		this.editarLog();
	};
	
	// Pega o Numero corrent do protocolo de atendimento e monta o log.
	this.hasAtendimento = function(){
		var procAttCurrent = 0;
		var currentProcAtendimento = JSON.parse(localStorage.getItem('hashAtendimento_'+webLink.cpfUsuario));
		if(currentProcAtendimento != undefined ){
			procAttCurrent = currentProcAtendimento.numeroProtocolo
		}
		return procAttCurrent;
	}
	// Pega os lançamentos manuais inseridos e guarda no localstorage
	this.setLogManual = function(textLogManual,listaLogManual){
		this.logLancamentos = JSON.parse(localStorage.getItem("logLancamentos_"+this.hasAtendimento()));
		if(this.logLancamentos == undefined){
			this.logLancamentos = [];
		}else{
			console.log('Tem conteudo no storage',this.logLancamentos)
		}
		this.boxlistaLogManual = listaLogManual;
		this.getValCampoTexto(textLogManual);
		this.deletarLog();
		this.editarLog();
	};
	// Pega os lançamentos Automatios e guarda no localstorage
	this.setLogAutomatico = function(textLogAuto){
		this.logLancamentos = JSON.parse(localStorage.getItem("logLancamentos_"+this.hasAtendimento()));
		if(this.logLancamentos == undefined){
			this.logLancamentos = [];
		}else{
			console.log('Tem conteudo no storage',this.logLancamentos)
		}
		this.getValCampoTexto(textLogAuto);
		this.deletarLog();
		this.editarLog();
	};
	
	this.getValCampoTexto = function(textLogManual,listaLogManual){
		this.txt = textLogManual;
		this.setInfoStorage(this.txt)
		return this.txt;
	}
	// Listagem dinamica dos lançamentos manuais
	this.setInfoStorage = function(textologManual){
		var idLog = new Date().getTime();
		var idUnicoCard = 'UnicID'+idLog;
		var retornaDataHoraAtual = '10/03/2019 as 19:45';//Deletar no ambiente java
		var tituloTratadoLog = textologManual.substring(0,15);
		console.log('tituloTratadoLog',tituloTratadoLog)
		var logRecebido = {
			id:idLog,
			data:retornaDataHoraAtual,
			titulo:tituloTratadoLog,
			textoLog:textologManual,
			idUnicoCard:idUnicoCard
		};
		this.logLancamentos.push(logRecebido);
		localStorage.setItem("logLancamentos_"+this.hasAtendimento(),JSON.stringify(this.logLancamentos));
		this.listaLogIncluidos(this.logLancamentos);
	};
	this.listaLogIncluidos = function(objLogStorage){
		this.objLogStorage = objLogStorage;
		var boxListaLog = $('#box_lancamentos_dinamicos');
		var obj_log = this.objLogStorage;
		var textoLog;
		var identificadorCardLog;
		var ordemObj;
		for (var i = 0; i < obj_log.length; i++) {
			idLog  				 = obj_log[i].id;
			data                 = obj_log[i].data
			tituloLog            = obj_log[i].titulo
			textoLog             = obj_log[i].textoLog;
			identificadorCardLog = obj_log[i].idUnicoCard;
			ordemObj             = i;
		}

		$(boxListaLog).append(this.templateListaLog(idLog,data,identificadorCardLog,tituloLog,textoLog,ordemObj));
	};
	this.deletarLog = function(){
		var id_Log;
		var self = this;
		$('.card').on('click','.excluir_log',function(e) {
			event.preventDefault();
			event.stopPropagation();
			event.stopImmediatePropagation();
			id_Log = $(this).attr('log-id');
			self.getCurrentLog(id_Log);
			$('#carlogID_'+id_Log).remove();
			
		});

	};
	this.editarLog = function(){
		var ordemLog,editaTxt,logStorage,idEditado,textoEditado;
		var self = this
		$('#box_lancamentos_dinamicos').on('click', '#btnEditartextoLancamento', function(event) {
			event.preventDefault();
			event.stopPropagation();
			event.stopImmediatePropagation();
			ordemLog = $(this).attr('ordem-log');
			editaTxt = $('#editaTxt_'+ordemLog).val();
			logStorage = self.getValorStorage();
			idEditado = logStorage[ordemLog].id;
			idEditCard = logStorage[ordemLog].idUnicoCard;
			self.setValorEditStorage(ordemLog,idEditado,idEditCard,editaTxt);
		});

	};
	this.getValorStorage = function(){
		var getLog = JSON.parse(localStorage.getItem("logLancamentos_"+this.hasAtendimento()));
		return getLog;
	}
	this.setValorEditStorage = function(ordemLog,idEditado,idEditCard,textologManual){
		var tituloTratadoLog = textologManual.substring(0,25);
		var retornaDataHoraAtual = '10/03/2019 as 19:45';//Deletar no ambiente java
		var tabelaLog = this.getValorStorage();
		tabelaLog[ordemLog] = {
            id:idEditado,
			data:retornaDataHoraAtual,
			titulo:tituloTratadoLog,
			textoLog:textologManual,
			idUnicoCard:idEditCard
        };
		localStorage.setItem("logLancamentos_"+this.hasAtendimento(),JSON.stringify(tabelaLog));
		this.listaLancamentosExistentes();
	}

	this.getCurrentLog = function(ID){
		console.log('dentro delet',this.getValorStorage())
		idLogCurrent = ID;
		var currentLog = [];
		var sobraObj   = [];
		refThis = this;
		$.each(this.getValorStorage(), function(index, val) {
			if(val.id == idLogCurrent){
				currentLog.push(val);
				console.log('INDEX >>>',index)
				// refThis.logLancamentos.splice(index);
			}else{
				sobraObj.push(val);
			}
		});
		var RESTO = this.logLancamentos;
		console.log('OBJ APOS DELET',sobraObj)
		localStorage.setItem("logLancamentos_"+this.hasAtendimento(),JSON.stringify(sobraObj));
		// console.log('SOBRA DO OBJ APOS DELET',sobraObj)
		// localStorage.setItem("logLancamentos",JSON.stringify(sobraObj));
		// console.log('OBJ APOS DELET',this.logLancamentos)
		return currentLog
	};
	
	this.templateListaLog = function(idLog,data,identificadorCardLog,tituloLog,textoLog,ordemObj){
		var html;
		html =  '<div id="carlogID_'+idLog+'" class="card">';
		html += '<div class="card-header" id="headingOne2" style="padding-top:0px;padding-bottom: 0px">';
		html += '<a class="att_btn_editar" href="#" data-toggle="collapse" data-target="#'+identificadorCardLog+'" aria-expanded="true" aria-controls="collapseOne2">';
		html += '<span class="nomeLancamento">'+ data + ' | ' + tituloLog+'</span>';				    		
		html += '</a>';
		html += '<a href="#" log-id="'+idLog+'" class="excluir_log btn_editar_log_atendimento btn btn-danger btn-xs" style="margin-top: 3px">';
		html += '<span class="glyphicon glyphicon-remove"></span>'; 
		html += '</a> ';
		html += '</div>';
		html += '<div id="'+identificadorCardLog+'" class="collapse" aria-labelledby="headingOne2" data-parent="#accordion">';
		html += '<div class="card-body">';
		html += '<form>';
		html += '<div class="form-group">';
		html += '<textarea class="textolancamentoEditar form-control"  id="editaTxt_'+ordemObj+'" rows="5">'+textoLog+'</textarea>';
		html += '</div>';
		html += '<div class="btn_editar_texto form-group">';
		html += '<a href="#" ordem-log="'+ordemObj+'" id="btnEditartextoLancamento" class="btn btn-primary mb-2">Aplicar</a>';
		html += '</div>';
		html += '</form>';
		html += '</div>';
		html += '</div>';
		html += '</div>';
		return html;
	}
	

}
var lancamentosManuais = new LancamentosManuais();

	
