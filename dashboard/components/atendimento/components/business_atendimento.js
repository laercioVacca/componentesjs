
require("components/atendimento/components/template_atendimento.js");
require("components/atendimento/components/lancamentos_automaticos.js");

function BusinessAtendimento() {
	this.load = function(boxComponent){
		
		this.boxComponent 		= boxComponent;
		console.log('get Flag Component BusinessAtendimento =================>LAERCIO',this.boxComponent)
		this.contrutorComponente();
	};
	this.contrutorComponente = function(){
		$(this.boxComponent).html(templateAtendimento.atendimentoBox('flagPage'));
		this.eventosComponent();
		lancamentosManuais.init();
	}
	this.getValoresLancamentos = function(textoLancamento){
		var conjuntolancamentos = [];
		conjuntolancamentos.push({"texto":textoLancamento});
		return conjuntolancamentos
	}
	this.eventosComponent = function(){
		var self = this;
		$(this.boxComponent).on('click', '#btnInseirLog', function(event) {
			event.preventDefault();
			var textLogManual  = $('#campoTextoLogManual').val();
			var listaLogManual = $('#box_lancamentos_dinamicos');
			if(textLogManual != ''){
				lancamentosManuais.setLogManual(textLogManual,listaLogManual);
				$('#campoTextoLogManual').val('');
			}
		});
		var arrServicos = ["servico01","servico02","servico03"]
		var props = {
			"nome":"Serviços Disponiveis",
			"objetos":arrServicos
		}
		$('#btnFinalizacaoChamado').on('click', function(event) {
            event.preventDefault();
            var pageName = $(this).attr('href');
            var componenteNome = 'finalizaChamado';
            ClearApp.clear('#panelDashboard');
            renderPageComponente.render(componenteNome,pageName);

        });
		$('#btnInseirLog').on('click',function(event) {
			event.preventDefault();
			
		});
	};
	
}

var businessAtendimento = new BusinessAtendimento();