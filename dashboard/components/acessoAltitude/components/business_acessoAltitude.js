
require("components/acessoAltitude/components/service_acessoAltitude.js");
function BusinessAcessoAltitude() {
	this.load = function(param){
		this.currentLink = param;
		console.log('this.currentLink@@@!!!!',this.currentLink)
		this.preparaWebLink();
		this.resultServico 	   = this.getService();
		this.objRetornoServico = this.resultServico.objectsReturn;
		this.realizarLogin();

	};
	this.getParametrosUrl = function(){
		var _url  = 'http://10.150.251.5/servlet/servicos/ServicosServlet';
	    _url += '?idServico=SAC&chaveUsuario=SAC_ADRIANASS&botaoRetornar=SAC&idCliente=05760464698&'; 
		_url += 'idPositiva=S&filial=900&autentica=N&footerIdPos=N&showBarraFooter=N&CPF_OPERADOR=27381720833';
	}
	this.parametrosServico = function(){
		var login = {};
		login.t = '0';
		login.filial = this.pad(this.currentLink.filial, 4, '0');
		login.carbin = '603475';
		login.login = this.usuario(this.currentLink.chaveUsuario).toUpperCase();
		login.senha = '';
		var parameters = {
			"Login" : login
		};

		return parameters;
	}
	this.getService = function(){
		console.log('this.parametrosServico()',this.parametrosServico())
		var result;
		var retornoServico = {};
		result = serviceAcessoAltitude.getAcessoMobileAuto(this.parametrosServico());
		retornoServico = {
			"codigoRetorno":result["codigoRetorno"],
		}
		return result;
	}
	this.preparaWebLink = function(){
		webLink.setVar("filial", this.pad(this.currentLink.filial, 4, '0'));
		webLink.setVar("sessao", '');
		webLink.setVar("weblinkageweb", '');
		webLink.setVar("webapplip", '') ;
		webLink.setVar("ipUsuario", '' );
		webLink.setVar("webfilcod", this.pad(this.currentLink.filial, 4, '0'));
		webLink.setVar("token", '' );
		webLink.setVar("modulo", 18 );
		webLink.setVar("rotina", 101 );
		webLink.setVar("cpfUsuario", this.cpf(this.currentLink.CPF_OPERADOR) );
		webLink.setVar("carBin", '603475' );
		webLink.setVar("loginUsuario", this.usuario(this.currentLink.chaveUsuario).toUpperCase() );
	}
	this.gravaWebLink = function(){
		webLink.filial = this.pad(this.currentLink.filial, 4, '0');
		webLink.sessao = this.resultServico.Sessao;
		webLink.weblinkageweb = this.objRetornoServico.WebLinkAgeWeb;
		webLink.webapplip = this.objRetornoServico.WebLink.applIp ;
		webLink.ipUsuario = this.objRetornoServico.Login.ip ;
		webLink.webfilcod = this.objRetornoServico.Login.filial ;
		webLink.token = this.objRetornoServico.Login.token ;
		webLink.modulo = 18 ;
		webLink.rotina = 101 ;
		webLink.cpfUsuario = this.objRetornoServico.WebLinkAgeWeb.substring(0, 11) ;
		webLink.carBin = this.objRetornoServico.WebLinkAgeWeb.substring(50).trim() ;
		webLink.loginUsuario = this.objRetornoServico.WebLinkAgeWeb.substring(11, 26).trim() ;
	}
	this.gravaStorage = function(){
		console.log('Gravas Storage Altitude')
		localStorage.removeItem('login_'+webLink.getVar("loginUsuario"));
		localStorage.setItem('login_'+webLink.getVar("loginUsuario"), filial);
		localStorage.setItem('webLink_'+webLink.getVar("token"), JSON.stringify(webLink));
	}
	this.realizarLogin = function(){
		if(this.resultServico.codigoRetorno != undefined){
			if (this.resultServico.codigoRetorno == 200) {
				var objLogin = this.resultServico.codigoRetorno.Login;
				console.log("Acesso: ");
				console.log(objLogin);
				var objReturn = this.resultServico;
				console.log("objReturn: ");
				console.log(objReturn);
				this.gravaWebLink();

				var filial = webLink.getVar("filial") + "";
				this.gravaStorage();
				

				var self = this;
			    window.setTimeout(function(){
			    	document.location.href = "http://" + window.location.host + "/psfsac/portalsac/dashboard/index.html?token="+webLink.getVar("token")+"&acessoAltitude=ok&cpfAlt="+self.currentLink.idCliente;
			    }, 1000);
				return false;
			}
			else if (result["codigoRetorno"] != 500) {
				alert(result["codigoRetorno"] + " - " +  result["descricaoRetorno"] );
				return false;
			} 
			else {			
				alert(result["codigoRetorno"] + " - " +  result["descricaoRetorno"] );
				return false;
			}
		}else{
			alert("Erro ao efetuar Login!");
			return false;
		}
	}
	this.pad = function(texto, tamanho, caracter) {
	  caracter = caracter || '0';
	  texto = texto + '';
	  return texto.length >= tamanho ? texto : new Array(tamanho - texto.length + 1).join(caracter) + texto;
	}
	this.cpf = function(str) {
		return(("00000000000"+str).slice(-11));
	}
	this.usuario = function(str) {
		return((str + " ").substring(0,25));
	}
	// ###
	this.eventosComponent = function(){
		$(this.boxComponent).on('click', '.elm', function(event) {
			event.preventDefault();
		});
		
	};
	
}

var businessAcessoAltitude = new BusinessAcessoAltitude();