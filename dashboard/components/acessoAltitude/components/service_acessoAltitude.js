function ServiceAcessoAltitude() {

	this.getAcessoMobileAuto = function(parameters){
		var getAcessoAltitude;
		getAcessoAltitude =  this.callServiceAcessoAltitude("login", "getAcessoMobileAuto", parameters);
		return getAcessoAltitude;
	};
	this.callServiceAcessoAltitude =  function(service, functionality, parameters) {
		console.log('callServiceAcessoAltitude$$',webLink)
		var result;
		var requestService = {};
		requestService.cpfUsuario		= webLink.getVar("cpfUsuario");
		requestService.loginUsuario		= webLink.getVar("loginUsuario");
		requestService.ipUsuario		= webLink.getVar("ipUsuario");
		requestService.modulo			= webLink.getVar("modulo");
		requestService.rotina			= webLink.getVar("rotina");
		requestService.service			= service;
		requestService.functionality	= functionality;
		requestService.filial			= webLink.getVar("filial");
		requestService.requestObjects	= parameters;
		service = service.indexOf("/") == 0 ? service.substr(1, service.length) : service;
		
		//Call GetObject Rest Service
	    $.ajax({
	        url: "/psfsecurity"+"/rest/" + service + "/" + functionality,
	    	type: "POST",
	    	async: false,
	    	data: {requestService: JSON.stringify(requestService)},	    	
	    	success: function(data) {    		
	    		result = JSON.parse(data);
	    	},
			error: function (msg, url, line) {
				try{
					result = JSON.parse(msg.responseText);
				}catch (e) {
					result = "ERROR: msg = " + msg + ", url = " + url + ", line = " + line;
				}	        
			}
	    });
	    
	    return result;
	    
	};
	
}
var serviceAcessoAltitude = new ServiceAcessoAltitude()