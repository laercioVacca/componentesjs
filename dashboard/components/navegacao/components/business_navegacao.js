
require("components/navegacao/components/template_navegacao.js");


function BusinessNavegacao() {
	this.load = function(contentNavegacao){
		this.contentNavegacao = contentNavegacao;
		this.controleRenderComponent();
		this.eventosComponent();		
	};
	this.controleRenderComponent = function(){
		$(this.contentNavegacao).html(templateNavegacao.menu())
	}
	this.renderPage = function(page){
		var contentPrincipal = $('#panelDashboard');
		this.pagePath = 'components/informacoesCadastrais/'+page;
		$(contentPrincipal).empty()
		$(contentPrincipal).load(this.pagePath)
	}
	this.marcarRotas = function(){
		
	}
	this.eventosComponent = function(){
		var self = this;
		var panelNavegacaoRapida = $('#panelNavegacaoRapida');
		$(this.contentNavegacao).on('click', '.openMenuMobile', function(event) {
			event.preventDefault();
		});
		$(this.contentNavegacao).on('click', '.renderPageOld', function(event) {
			event.preventDefault();
			var pathePage = $(this).attr('href');
			self.renderPage(pathePage);
		});
		
        $(this.contentNavegacao).on('click','.renderPage',  function(event) {
			$('.sidebar_link').removeClass('ativarMenuDesk');
			console.log('click menu')
            event.preventDefault();
            event.stopPropagation();
            var pageName = $(this).attr('href');
            var componenteNome = $(this).attr('href');
            $(this).addClass('ativarMenuDesk');
            $('#menuDashboard').find('.iconeMenuAtivoDesk').hide();

            renderPageComponente.render(componenteNome,pageName);
            
        });


	};



	
	
}




var businessNavegacao = new BusinessNavegacao();