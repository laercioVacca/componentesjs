
require("components/procedimento/components/template_procedimento.js");

function BusinessProcedimento() {
	this.load = function(boxComponent){
		this.boxComponent = boxComponent;
		this.contrutorComponente();
	};
	this.contrutorComponente = function(){
		$(this.boxComponent).html(templateProcedimento.procedimentoBox());
		this.eventosComponent();
	}
	
	this.eventosComponent = function(){
		$(this.boxComponent).on('click', '.btn_drop_memo', function(event) {
			event.preventDefault();
		});
	};
	
}

var businessProcedimento = new BusinessProcedimento();