function TemplateProcedimento() {
	
	this.procedimentoBox = function(){
		var retornoHtml;
		retornoHtml  = '<div id="cardProcedimento" class="card_procedimento" nome-cp="teste-CP02" style="margin-bottom: 15px;">';
		retornoHtml += '<div class="card menu" id="menucard.id" style="margin: 0px;border: 5px solid #fbfaf6;max-width: 100%;">';
		retornoHtml += '<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="false">';
		retornoHtml += '<div class="panel panel-default">';
		retornoHtml += '<div class="panel-heading" role="tab" id="">';
		retornoHtml += '<h4 class="panel-title">';
		retornoHtml += '<a class="collapsed" id="btnShowProcedimentos" role="button" style="padding-right: 5px;color: #75104c;" data-toggle="" data-parent="#accordion" aria-expanded="true aria-controls="collapsecard.id"> ';
		retornoHtml += '<span class="glyphicon glyphicon-thumbs-up" style="left: -30px;"></span>';
		retornoHtml += 'Procedimentos';
		retornoHtml += '</a>';
		retornoHtml += '</h4>';
		retornoHtml += '</div>';
		retornoHtml += '<div id="collapsecard.id" class="panel-collapse collapse show" role="tabpanel" aria-labelledby="headingcard.id">';
		retornoHtml += '<div class="panel-body">';
		retornoHtml += '<a href="#" class="btnRedirectProcedimentos btn btn-info"><span class="fs1" aria-hidden="true" data-icon="&#xe0c3;">Navegar</span></a>';
		retornoHtml += '</div>';
		retornoHtml += '</div>';
		retornoHtml += '</div>';
		retornoHtml += '</div>';
		retornoHtml += '</div>';
		retornoHtml += '</div>';
		return retornoHtml;
	};

}
var templateProcedimento = new TemplateProcedimento();