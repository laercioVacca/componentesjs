function ServiceProtocolo() {

	this.novoProtocolo = function(parameters){
		this.servicoNovoProtocolo  = new GenericService('psfclientservices');
		this.getNovoProtocolo      = this.servicoNovoProtocolo.callPostService("historicoChamado", "obterHistoricoChamado", parameters);
		return this.getNovoProtocolo;
	};
	
}
var serviceProtocolo = new ServiceProtocolo()