require("components/servicos/components/editarServico.js");
require("components/servicos/components/contratarServico.js");
require("components/servicos/components/cancelarServico.js");

require("components/servicos/components/eventServicos.js");
require("components/servicos/components/businessServicos.js");
require("components/servicos/components/serviceServicos.js");
require("components/servicos/components/templateServicos.js");
require("components/servicos/components/modalServicos.js");

/* 
 * Classe utilizada para implementar as primeiras acoes do componente
 */
function ServicosInstance() {
	
	if (!(this instanceof ServicosInstance)){
        return new ServicosInstance();
    }	
	
	// conteiner de variaveis
	this.vars = {};
	
	// Load inicial do componente
	this.load = function(){
		var dadosCurrent    = controllStorageSac.getinfosCliEmAtendimento();
    	// var cpfCli = dadosCurrent.cpf;
    	var cpfCli = '29723456818';
		this.vars.cpf           = cpfCli;
		this.vars.currentFilial = webLink.filial;
	
		businessServicos.load(this.vars);
		eventServicos.load();
	}
}

if(servicosInstance == undefined || servicosInstance == null){
	var servicosInstance = new ServicosInstance();
}

