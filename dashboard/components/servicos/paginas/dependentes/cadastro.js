
require("../../../../resources/libs/jquery-maskedinput/jquery.mask.js");
require("../../../../resources/libs/bootstrap-validator/validator.min.js");
require("../../../../resources/libs/underscore/lodash.min.js");
require("../../../../resources/libs/jquery-ui-1.12.1.custom/jquery-ui.min.js");
require("../../../../resources/libs/jquery-ui-1.12.1.custom/i18n/datepicker-pt-BR.js");

require("../../../../resources/libs/datatable/js/jquery.dataTables.min.js");
require("../../../../resources/libs/datatable/js/dataTables.bootstrap.js");

require("../../../../resources/libs/datatable/js/dataTables.buttons.min.js");
require("../../../../resources/libs/datatable/js/buttons.flash.min.js");

require("../../../../resources/libs/datatable/js/dataTables.fixedHeader.min.js");
require("../../../../resources/libs/datatable/js/dataTables.responsive.min.js");
require("../../../../resources/libs/datatable/js/responsive.bootstrap.min.js");
require("../../../../resources/libs/datatable/js/dataTables.select.min.js");
require("../../../../resources/libs/datatable/js/dataTables.fixedColumns.min.js");



//seta o valor do atrubuto dependendo do valor da variavel passada do servicos
if(globalDataAcaoPagina === 'editar'){
	document.getElementById('card.cadastro_dependente').setAttribute("data-acao", 'editar');
}else{
	document.getElementById('card.cadastro_dependente').setAttribute("data-acao", 'contratar');
}
//Pega qual ação a pagina vai realizar e armazena nesta Variavel dataAcaoPagina
var dataAcaoPagina =  document.getElementById('card.cadastro_dependente').getAttribute("data-acao");

//logging('Debug dentro do cadastro.js globalDataAcaoPagina',globalDataAcaoPagina);

var formCadastroMode = 1; // 1 = incluir,  2 = editar
var rowIndexEdit = 0;
var rowIndexDelete = 0;
var registroId = 0;
var registroDescr = "0";
var objDependentesExtraida = [];
var editarSemDependente = true;

function require(script) {
	$.ajax({
		url : script,
		dataType : "script",
		async : false, // <-- This is the key
		success : function() {
			// all good...
		},
		error : function() {
			throw new Error("Could not load script " + script);
		}
	});
}

 //logging('data Acao Pagina',dataAcaoPagina)

function jsonReplaceAll(data, node, find, replace) {
	data.forEach(function(record) {
		var line = record;
		for ( var key in line) {
			if (key === node && line[key] === find) {
				line[key] = replace;
			}
		}
	});
	return data;
}

function removeFromArray(array, prop, value) {
	logging("procurando "+value);
	var i = 0;
	array.forEach(function(record) {
		var line = record;
		for ( var key in line) {
			if (key === prop && line[key] === value) {
				logging("removendo "+value);
				array.splice(i, 1);
			}
		}
		i++;
	});	
    return array;
}

var exclusaoConfirmada = function() {
	logging("Callback exclusaoConfirmada");
	excluirDependente(registroId);
}

var exclusaoCancelada = function() {
	logging("Callback exclusaoCancelada");
}

function confirmarExclusao(idRegistro, descrRegistro) {
	registroId = idRegistro;
	registroDescr = descrRegistro;
	var resp = marisaMessage.questionDialog("Excluir", "Deseja realmente excluir o registro \'" + idRegistro + " - "
					+ descrRegistro + "\'?", "Sim", "Não", exclusaoConfirmada, exclusaoCancelada);
}

$(document).ready(function() {
	
//	$("#cpfCad").keydown(function(event) {
//        if (event.ctrlKey==true && (event.which == '118' || event.which == '86')) {
//            $("#cpfCad").val(formatCpf(window.clipboardData.getData("Text"),11));
//            return true;
//         }
//    });
		
	$("#cpfCad").focusout(function(event) {
		logging(this.value);
		if(this.value != ""){
			$("#cpfCad").val(formatCpf(this.value,11));
		}
        return true;
	});
	
	//$( "#dataNascCad" ).datepicker( $.datepicker.regional[ "pt-BR" ] );
	//$( "#dataNascCad" ).datepicker( "option", "dateFormat", "dd/mm/yy" );
	$( "#dataNascCad" ).datepicker({ 
									 changeMonth: true, 
									 //changeYear: true, 
									 stepMonths: 12, 
									 showOtherMonths: true,
								     selectOtherMonths: true, 
								     dateFormat: "dd/mm/yy",
								     firstDay: 0,
								     hideIfNoPrevNext: true,
								     showAnim: 'slideDown',
								     //showOn: "both",  
								     showStatus: true,
								     showButtonPanel: true,
//								     beforeShow: function(text, inst){        
//								         var next_day = new Date(
//								             inst.selectedYear,
//								             inst.selectedMonth,
//								             inst.selectedDay
//								         );        
//								         next_day.setDate(next_day.getDate()+1);
//								         logging(inst.selectedMonth);
//								         logging(next_day.getMonth());
//								         if(inst.selectedMonth != next_day.getMonth())
//								             return {numberOfMonths: 2};
//								         else
//								             return {numberOfMonths: 1};
//								     }
									});
	$("#dataNascCad" ).mask("99/99/9999");
	$('#cpfCad').mask("000.000.000-00", {reverse: true});
	
	$("#dataNascCad").on('focus', function (e) {
	    e.preventDefault();
	    $(this).datepicker('show');
	    $(this).datepicker('widget').css('z-index', 1051);
	});	

	$("#dataNascCad").on('change', function (e) {
		e.preventDefault();
		if($("#dataNascCad").val().length > 0){
			tratarIdadeDependente($("#dataNascCad").val());
		}
	});	
	
	initTableDependentes();
	
//    $('#formCadastroDep #btnIncluir').on( 'click', '#btnIncluir', function (e) {
    $('#btnIncluir').on( 'click', function (e) {
    	logging("btnIncluir clicked");
    	formCadastroMode = 1;
    	e.preventDefault();
    	$('#formCadastroDep').trigger("reset");
    	$('#modalCadastroDep #formModalTitle').prop('innerText', 'Incluir Dependente');
    	//$('#modalCadastroDep #codMotivo_I').val('Auto');
    	$('#modalCadastroDep #cpfCad').val('');
    	$('#modalCadastroDep #nomeCad').val('');
    	$('#modalCadastroDep #nomeMaeCad').val('');
    	$('#modalCadastroDep #dataNascCad').val('');
    	$('#modalCadastroDep #sexoCad').val('');
    	$('#modalCadastroDep #estadoCivilCad').val('');
    	$('#modalCadastroDep #grauParentescoCad').val('');
    	
    	$('#modalCadastroDep #rgCad').val('');
    	$('#modalCadastroDep #orgaoEmissorCad').val('');
    	$('#modalCadastroDep #ufEmissorCad').val('');
    	
    	$('#modalCadastroDep').find('[type="radio"]').prop('checked', false);
    	

		//$("#modalCadastroDep #cpf").val(formatCpf(objLista.cliCpf));
		
		$('#modalCadastroDep').modal('show');
		
		$('#cpfCad').focus();
		
    });
    
    $('#cpfCad').parent().tooltip({
    	   title : $('#cpfCad').data('title')
    	  });    
    
    var validatorOptions = {
            delay: 300,
            html: true,
            disable: true,
            focus: true,
            custom: {
                "cpf": function ($el) {
                	logging($el);
                    var checkCpf = $el.val().trim();
                    var check = validarCPF(checkCpf);
                    //logging(check);
                    return !check;
                },
			    "cpfDuplicado": function ($el) {
			    	logging($el);
			    	var checkCpf = $el.val().trim();
			    	var check = verificaCPFDuplicado(checkCpf);
			    	//logging(check);
			    	return !check;
			    }
            },
            errors: {
                cpf: "CPF inválido!",
                cpfDuplicado: "CPF já cadastrado!"
            },
            feedback: {
            	    success: 'glyphicon-ok',
            	    error: 'glyphicon-remove'
            	  }

    }
    $('#formCadastroDep').validator(validatorOptions);    
    
	$('#formCadastroDep').validator().on('submit', function (e) {
		logging("formCadastroMode: "+formCadastroMode);
		if (e.isDefaultPrevented()) {
			// handle the invalid form...
		} else {
			
			if(formCadastroMode == 1){ // incluir
				incluirDependente();
			}else{ // editar
				editarDependente();
			}
			return false;//Bloqueando o submit do form
		}
	});
    
    $('#table-dependentes').on( 'click', '#btnExcluir', function (e) {
    	e.preventDefault();
    	var table = $('#table-dependentes').dataTable();
        var tr = $(this).closest('tr');
        rowIndexDelete = table.fnGetPosition( tr[0] );;
        //rowIndexDelete = tr[0].rowIndex;
        var row = $("#table-dependentes").DataTable().row(tr);    	
        var data = row.data();
        // TODO: CORRIGIR NOME COLUNAS
        confirmarExclusao(rowIndexDelete, data['nome']);
    } );

    $('#table-dependentes').on( 'click', '#btnEditar', function (e) {
    	logging("btnEditar clicked");
    	formCadastroMode = 2;    	
    	e.preventDefault();
    	
    	var table = $('#table-dependentes').dataTable();
        var tr = $(this).closest('tr');
        rowIndexEdit = table.fnGetPosition( tr[0] );
        //rowIndexEdit = tr[0].rowIndex;
        var row = $("#table-dependentes").DataTable().row(tr);    	
        var data = row.data();
        
    	var objListaParentesco = $("#modalCadastroDep #grauParentescoCad").find('option');
    	var parentesco;
    	$.each(objListaParentesco, function(index, item) {
    		if (data['grauParentesco'] == item.text) {
    			parentesco = item.value;
    		}										        		
    	});

    	$('#formCadastroDep').trigger("reset");
    	$('#modalCadastroDep #formModalTitle').prop('innerText', 'Editar Dependente');
    	//$('#modalCadastroDep #codMotivo_I').val('Auto');
    	$('#modalCadastroDep #cpfCad').val(data['cpf']);
    	$('#modalCadastroDep #nomeCad').val(data['nome']);
    	$('#modalCadastroDep #nomeMaeCad').val(data['nomeMae']);
    	//$('#modalCadastroDep #dataNascCad').datepicker("setValue", data['dataNasc']);
    	$('#modalCadastroDep #dataNascCad').val(data['dataNasc']);
    	//$('#modalCadastroDep #dataNascCad').val(desformatDate(data['dataNasc']));
    	$('#modalCadastroDep #sexoCad').val(data['sexo']);
    	$('#modalCadastroDep #estadoCivilCad').val(data['estadoCivil']);
    	
    	$('#modalCadastroDep #rgCad').val(data['rg']);
    	$('#modalCadastroDep #orgaoEmissorCad').val(data['orgaoEmissor']);
    	$('#modalCadastroDep #ufEmissorCad').val(data['ufEmissor']);
    	
    	$('#modalCadastroDep #grauParentescoCad').val(parentesco);
    	$('#modalCadastroDep').find('[type="radio"]').prop('checked', false);
    	
		$('#modalCadastroDep').modal('show');

		$('#cpfCad').focus();
        
    } );


	if(dataAcaoPagina != 'editar'){
		popularDadosSeguroDep(codServicoContratando);
	}
	
	
});

function verificaCPFDuplicado(value){
	var resp = true;
	var count = 0;
	var getObj = $("#table-dependentes").DataTable().rows().data();
	if(getObj.length > 0){
		for (var i = 0; i < getObj.length; i++) {
			if(getObj[i].cpf.replace(/[^\d]+/g,"") == value.replace(/[^\d]+/g,"")){
				count = count + 1;
			}
		}
	}
	if(formCadastroMode == 2 && count > 1){
		resp = false;
	}else if(formCadastroMode == 1 && count > 0){
		resp = false;
	}
	return resp;
}

function validarCPF(value) {
	// retira mascara e nao numeros
	value = value + "";
	var cpf = value.replace(/[^\d]+/g,"");
	if (cpf == '')
		return true; // pode ser nulo

	if (cpf.length != 11)
		return false;

	// testa se os 11 digitos são iguais, que não pode.
	var valido = 0;
	for (i = 1; i < 11; i++) {
		if (cpf.charAt(0) != cpf.charAt(i))
			valido = 1;
	}
	if (valido == 0)
		return false;

	// calculo primeira parte
	aux = 0;
	for (i = 0; i < 9; i++)
		aux += parseInt(cpf.charAt(i)) * (10 - i);
	check = 11 - (aux % 11);
	if (check == 10 || check == 11)
		check = 0;
	if (check != parseInt(cpf.charAt(9)))
		return false;

	// calculo segunda parte
	aux = 0;
	for (i = 0; i < 10; i++)
		aux += parseInt(cpf.charAt(i)) * (11 - i);
	check = 11 - (aux % 11);
	if (check == 10 || check == 11)
		check = 0;
	if (check != parseInt(cpf.charAt(10)))
		return false;
	return true;

}

//logging('LOG codServicoEditando <> ', codServicoEditando);

function listarProdutosContratados(codServicoEditando){
	// logging('dentro da função que lista contratados',codServicoEditando)
	var infosProdutoContratado = [];
	$.each(arrayProdutosContratados, function(index, val) {
		//logging('opaaaaa',val)
		if(val.cod == codServicoEditando ){
			infosProdutoContratado.push(val.objeto);
		}
		
	});
	logging('infosProdutoContratado',infosProdutoContratado)
	return infosProdutoContratado;
}


function initTableDependentes(){
	var s = waitingDialog.show('Processando...', {dialogSize : 'sm',progressType : 'info'});
	setTimeout(function () {
	
		var objDataTable = $("#table-dependentes").DataTable();
	    objDataTable.destroy();	
	    var objLista;
	    
	    var numeroRgCliente 	 = _INFO_CLIENTE.numeroRgCliente;
	    var orgaoEmissorCliente  = _INFO_CLIENTE.orgaoEmissorCliente;
	    var estadoEmissorCliente = _INFO_CLIENTE.estadoEmissorCliente;
	  
	    $('#numeroRgCliente').val(numeroRgCliente);
		$('#orgaoEmissorCliente').val(orgaoEmissorCliente);
		$('#estadoEmissorCliente').val(estadoEmissorCliente);
		
	    // TODO: BUSCAR DEPENDENTES CADASTRADOS NO BANCO
	    if(dataAcaoPagina == 'editar'){
			var getInfosContratados = listarProdutosContratados(codServicoEditando);
			var dataProdDispDep = getInfosContratados[0].dependentes//[0];
			
			$('#depVlrMensalSeguro').val('R$ ' + formatMoney(getInfosContratados[0].valorPremio,2,',','.'));
			$('#coberturaDescr').val(getInfosContratados[0].servico);
			$('#coberturaValor').val('R$ ' + formatMoney(getInfosContratados[0].valorCobertura,2,',','.'));
			
	    	logging('getInfosContratados',dataProdDispDep);	
	    }
		
		if(dataProdDispDep != undefined ){
			objLista = [];
			
			logging('pssDptNome: ' + dataProdDispDep[0].pssDptNome);
			
			$('#depVlrMensalSeguro').val('R$ ' + formatMoney(getInfosContratados[0].valorPremio,2,',','.'));
			//$('#depVlrMensalSeguro').val('R$ ' + formatMoney(getInfosContratados[0].valorPremio + (getInfosContratados[0].valorPremio * getInfosContratados[0].dependentes.length),2,',','.'));
			$('#coberturaDescr').val(getInfosContratados[0].servico);
			$('#coberturaValor').val('R$ ' + formatMoney(getInfosContratados[0].valorCobertura,2,',','.'));
			
			dataProdDispDep.forEach(function(entry) {
				
				var objListaGrauParentesco = $("#grauParentescoCad").find('option');
				
			    logging('dependentes:');
			    logging(entry);
			    
			    logging('pssDptCpf: ' + entry.pssDptCpf);
			    
			    var dependente = {};
			    if(entry.pssDptCpf != undefined){
			    	dependente.cpf = formatCpf(entry.pssDptCpf);
			    }else{
			    	dependente.cpf = "";
			    }
			    if(entry.pssDptNome != undefined){
			    	dependente.nome = entry.pssDptNome.toUpperCase();
			    }else{
			    	dependente.nome = "";
			    }
			    if(entry.pssDptNomeMae != undefined){
			    	dependente.nomeMae = entry.pssDptNomeMae.toUpperCase();
			    }else{
			    	dependente.nomeMae = "";
			    }
			    if(entry.pssDptDtNascimento != undefined){
			    	dependente.dataNasc = formatDate(entry.pssDptDtNascimento);
			    	//dependente.dataNasc = entry.pssDptDtNascimento;
			    }else{
			    	dependente.dataNasc = "";
			    }
			    if(entry.pssDptSexoDomi != undefined){
			    	dependente.sexo = entry.pssDptSexoDomi;
			    }else{
			    	dependente.sexo = "";
			    }
			    if(entry.pssDptEcDomi != undefined){
			    	dependente.estadoCivil = entry.pssDptEcDomi;
			    }else{
			    	dependente.estadoCivil = "";
			    }
			    
			    if(entry.pssDptIdentidade != undefined){
			    	dependente.rg = entry.pssDptIdentidade;
			    }else{
			    	dependente.rg = "";
			    }
			    if(entry.pssDptOrgaoEmi != undefined){
			    	dependente.orgaoEmissor = entry.pssDptOrgaoEmi;
			    }else{
			    	dependente.orgaoEmissor = "";
			    }
			    if(entry.pssDptUfEmi != undefined){
			    	dependente.ufEmissor = entry.pssDptUfEmi;
			    }else{
			    	dependente.ufEmissor = "";
			    }
			    
			    $.each(objListaGrauParentesco, function(index, item) {
	        		// Verificando se o item do combo é igual ao item da lista
	        		if (entry.pssDptGpDomi == item.value) {
	        			dependente.grauParentesco = item.text;
	        		}										        		
	        	});
			    //dependente.grauParentesco = entry.pssDptGpDomi;
			    
			    dependente.situacao = 'Importado [Cad. ' + formatDate(entry.pssDptDtAdesao) + ']';
			    dependente.acao = "";
			    
			    objLista.push(dependente);
			
			})
			
		    
		}else{
			objLista = {};
			
		    objLista.cpf = "";
		    objLista.nome = "";
		    objLista.nomeMae = "";
		    objLista.dataNasc = "";
		    objLista.sexo = "";
		    objLista.estadoCivil = "";
		    objLista.grauParentesco = "";
		    objLista.rg = "";
		    objLista.orgaoEmissor = "";
		    objLista.ufEmissor = "";
		    objLista.situacao = "";		
		    objLista.acao = "";		
		}
	    
		logging(objLista);
		
	    
		if (!$.fn.DataTable.isDataTable( "#table-dependentes" )) {
			logging("Init #table-dependentes");
			var table = $('#table-dependentes').dataTable( {
		        language: language,	 
		        responsive: true,
				bFilter : false,               
				bLengthChange: false,
				columns: [
					{title : "&nbsp;", 
						//data : 'codigo', 
						width: "15%",
					    render: function( data, type, full, meta ) {
					        return '';
					    }
					},
					{ title: "Cpf" , data: "cpf"},
		            { title: "Nome" , data: "nome"},
		            { title: "NomeMae" , data: "nomeMae"},
		            { title: "Data Nasc", data: "dataNasc" },
		            { title: "Sexo", data: "sexo" },
		            { title: "Estado Civil", data: "estadoCivil" },
		            { title: "Parentesco", data: "grauParentesco" },
		            { title: "RG", data: "rg" },
		            { title: "Orgão Emissor", data: "orgaoEmissor" },
		            { title: "UF Emissor", data: "ufEmissor" },
		            { title: "Situação", data: "situacao" },
		            { title: "Ações" }
		        ],			
				columnDefs: [ {
			            targets: 12,
			            orderable: false,
			            defaultContent: "" +
			            		"<a class='btn btn-warning btn-xs' id='btnEditar' style='margin: 0px 0px 0px 5px;height: 20px;color: white;'><span class='glyphicon glyphicon-pencil'></span></a>" 
						        +"<a class='btn btn-danger btn-xs' id='btnExcluir' style='margin: 0px 0px 0px 5px;height: 20px;color: white;'><span class='glyphicon glyphicon-remove'></span></a>" 
			        },
			        {
			            orderable: false,
			            className: 'select-checkbox',
			            targets:   0
			        },
		            {
		                targets: [ 1 ],
		                visible: false,
		                searchable: false
		            },
		            {
		                targets: [ 3 ],
		                visible: false,
		                searchable: false
		            },
		            {
		            	targets: [ 5 ],
		            	visible: false,
		            	searchable: false
		            },
		            {
		            	targets: [ 6 ],
		            	visible: false,
		            	searchable: false
		            },
		            {
		            	targets: [ 8 ],
		            	visible: false,
		            	searchable: false
		            },
		            {
		            	targets: [ 9 ],
		            	visible: false,
		            	searchable: false
		            },
		            {
		            	targets: [ 10 ],
		            	visible: false,
		            	searchable: false
		            }			
				],
				data: objLista,
		        select: {
		            //style:    'os',
		            //selector: 'td:first-child',
		        	style: 'single'
		        },
		        fnCreatedRow: function( nRow, aData, iDataIndex ) {
		        	$(nRow).children("td").css("overflow", "hidden");
		        	$(nRow).children("td").css("white-space", "nowrap");
		        	$(nRow).children("td").css("text-overflow", "ellipsis");
		        	$(nRow).addClass('RowControleDependentes');
		        }
			
		    } );
		  
		}	    
		
		objDataTable = $("#table-dependentes").DataTable();
		new $.fn.dataTable.FixedHeader( objDataTable );	
		//logging('Disparar!!')
		//$(tabelaDep, window.parent.document).val(objDataTable);
		
		$('.select-item').html('');
	
		waitingDialog.hide();
		
	  }, 500);
	
}


function incluirDependente(){
	var table = $('#table-dependentes').DataTable();
	 
	table.row.add( {
	        "":					"",
	        "cpf":				$('#cpfCad').val(),//.replace(/[^\d]+/g,''),
	        "nome":				$('#nomeCad').val().toUpperCase(),
	        "nomeMae":			$('#nomeMaeCad').val(),
	        "dataNasc":			$('#dataNascCad').val(),
	        //"dataNasc":			formatDate($('#dataNascCad').val()+ " 00:00:00"),
	        "sexo":				$('#sexoCad').val(),
	        "estadoCivil":		$('#estadoCivilCad').val(),
	        "grauParentesco":	$('#grauParentescoCad option:selected').text(),
	        "rg":				$('#rgCad').val(),
	        "orgaoEmissor":		$('#orgaoEmissorCad').val(),
	        "ufEmissor":		$('#ufEmissorCad option:selected').text(),
	        "situacao":     	"Novo",
	    } ).draw();
	
	//$(tabelaDep, window.parent.document).val(table);
	
	$('.select-item').html('');
	
	$('#modalCadastroDep').modal('hide');
	
}

function excluirDependente(){
	var table = $('#table-dependentes').dataTable();
	 
	table.fnDeleteRow(rowIndexDelete);
	
	//$(tabelaDep, window.parent.document).val(table);
	
	$('.select-item').html('');
	
}

function editarDependente(){
	var table = $('#table-dependentes').dataTable();
	 
	table.fnUpdate(  {
        "cpf":				$('#cpfCad').val(),//.replace(/[^\d]+/g,''),
        "nome":				$('#nomeCad').val().toUpperCase(),
        "nomeMae":			$('#nomeMaeCad').val(),
        "dataNasc":			$('#dataNascCad').val(),
        //"dataNasc":			formatDate($('#dataNascCad').val()+ " 00:00:00"),
        "sexo":				$('#sexoCad').val(),
        "estadoCivil":		$('#estadoCivilCad').val(),
        "grauParentesco":	$('#grauParentescoCad option:selected').text(),
        "rg":				$('#rgCad').val(),
        "orgaoEmissor":		$('#orgaoEmissorCad').val(),
        "ufEmissor":		$('#ufEmissorCad option:selected').text(),
        "situacao":     	"Alterado",
    }, rowIndexEdit);
	
	//$(tabelaDep, window.parent.document).val(table);
	
	$('.select-item').html('');
	
	$('#modalCadastroDep').modal('hide');
	
}

// function popularDadosClienteDep(){
// 	//$('#cadDependentes').contents().find('#depPlasNumCartao').val();
	
// 	$('#depPlasNumCartao').val( $('#plasNumCartao', window.parent.document).val() );
// 	$('#depCliNome').val( $('#cliNome', window.parent.document).val() );
// 	$('#depStaCredito').val( $('#staCredito', window.parent.document).val() );
// 	$('#depStaPlastico').val( $('#staPlastico', window.parent.document).val() );
	
// }

function popularDadosSeguroDep(codServicoContratando){
	var infosProduto = [];
	$.each(arrayProdutosDisponiveis, function(index, val) {
	
		if(val.cod === codServicoContratando ){
			infosProduto.push(val.objeto);
		}
		
	});
	var descricaoCoberturaValor = infosProduto[0].descricaoCobertura;
	var valorSeguro             = infosProduto[0].valorMinimoPremio;
	var valorCober			    = infosProduto[0].valorCobertura;

	$('#depVlrMensalSeguro').val('R$ ' + formatMoney(valorSeguro,2,',','.'));
	$('#coberturaDescr').val(descricaoCoberturaValor);
	$('#coberturaValor').val('R$ ' + formatMoney(valorCober,2,',','.'));
	
	var numeroRgCliente 	 = _INFO_CLIENTE.numeroRgCliente;
	var orgaoEmissorCliente  = _INFO_CLIENTE.orgaoEmissorCliente;
	var estadoEmissorCliente = _INFO_CLIENTE.estadoEmissorCliente;
	  
    $('#numeroRgCliente').val(numeroRgCliente);
	$('#orgaoEmissorCliente').val(orgaoEmissorCliente);
	$('#estadoEmissorCliente').val(estadoEmissorCliente);
	
}

function getDeps(){
	var TrasformObjDep = [];
	var getObj = $("#table-dependentes").DataTable().rows().data();
	objDependentesExtraida = [];
	//logging('Pegando o getDeps',getObj)
	for (var i = 0; i < getObj.length; i++) {
		//logging('For ----',getObj[i])
		objDependentesExtraida.push(getObj[i]);
	}

	
	
	$.each(objDependentesExtraida, function(index, val) {
		//logging('index',index,val.cpf)
		var objListaParentesco = $("#modalCadastroDep #grauParentescoCad").find('option');
		var parentesco;
		$.each(objListaParentesco, function(index, item) {
			if (val.grauParentesco == item.text) {
				parentesco = item.value;
			}										        		
		});
		TrasformObjDep.push({
		 	'pssDptCpf':val.cpf.replace(/[^\d]+/g,""),
		 	'pssDptDtNascimento':getDate(val.dataNasc),
		 	'pssDptEcDomi':val.estadoCivil,
		 	'pssDptGpDomi':parentesco,
		 	'pssDptNome':val.nome.toUpperCase(),
		 	'pssDptNomeMae':val.nomeMae,
		 	'pssDptOrgaoEmi':val.orgaoEmissor,
		 	'pssDptIdentidade':val.rg + "",
		 	'pssDptSexoAtrib':2,
		 	'pssDptSexoDomi':val.sexo,
		 	'pssDptUfEmi':val.ufEmissor,
		 	'pssDptEcAtrib':4,
		 	'pssDptGpAtrib': 14
		 })
	});

     
  
	//logging('TrasformObjDep',TrasformObjDep);
	return TrasformObjDep;
}

function getCodParentesco(txt){
	var objListaParentesco = $("#modalCadastroDep #grauParentescoCad").find('option');
	var parentesco;
	$.each(objListaParentesco, function(index, item) {
		if (txt == item.text) {
			parentesco = item.value;
		}										        		
	});
	return parseInt(parentesco);
}

function tratarIdadeDependente(nascimento) {
	logging("nascimento: "+nascimento);
	var idade = calcularIdade(nascimento);
    if (idade >= 18 && idade <= 100 ) {
            //alert('Para maiores de 18 anos, é obrigatório informar o \nRG, Orgão Emissor e UF Emissor');
            marisaMessage.showDialog(marisaMessageType_INFO, "Idade", "Para maiores de 18 anos, é obrigatório informar o <br>RG, Orgão Emissor e UF Emissor");
            $("#rgCad").attr("required", "true");
            $("#orgaoEmissorCad").attr("required", "true");
            $("#ufEmissorCad").attr("required", "true");
            //document.getElementById("rgCad").focus();
    } else{
    	$("#rgCad").removeAttr("required");
    	$("#orgaoEmissorCad").removeAttr("required");
    	$("#ufEmissorCad").removeAttr("required");
    }
    if (idade < 0 || idade > 100) {
    		marisaMessage.showDialog(marisaMessageType_WARNING, "Idade", "Data de Nascimento Inválida!");
            //alert('Data de Nascimento Inválida');
            document.getElementById("dataNascCad").value = "";
            document.getElementById("dataNascCad").focus();
            return false;
    }
}

function getInformacaoAdicionalCliente() {
	
	informacoesAdicionaisCliente = [];
	informacoesAdicionaisCliente.push($("#numeroRgCliente").val());
	informacoesAdicionaisCliente.push($("#orgaoEmissorCliente").val().toUpperCase());
	informacoesAdicionaisCliente.push($("#estadoEmissorCliente").val().toUpperCase());
		
}

//calcular a idade de uma pessoa
//recebe a data como um string em formato portugues
//devolve um inteiro com a idade. Devolve false em caso de que a data seja incorreta ou maior que o dia atual
function calcularIdade(data) {

	//calculo a data de hoje
	hoje = new Date();
	var i = 0;

	//calculo a data que recebo
	//descomponho a data em um array
	var array_data = data.split("/");
	//se o array nao tem tres partes, a data eh incorreta
	if (array_data.length != 3) {
		return false
	}

	//comprovo que o ano, mes, dia sï¿½o corretos
	var ano
	ano = parseInt(array_data[2]);
	if (isNaN(ano))
		return false

	var mes
	mes = parseFloat(array_data[1]);
	if (isNaN(mes))
		return false

	var dia
	dia = parseFloat(array_data[0]);

	if (isNaN(dia))
		return false

		//se o ano da data que recebo so tem 2 cifras temos que muda-lo a 4
	if (ano <= 99) {
		ano += 1900
	}

	//subtraio os anos das duas datas
	idade = hoje.getYear() + 1900 - ano - 1; //-1 porque ainda nao fez anos durante este ano

	//se subtraio os meses e for menor que 0 entao nao cumpriu anos. Se for maior sim ja cumpriu
	if (hoje.getMonth() + 1 - mes < 0) { //+ 1 porque os meses comecam em 0
		return idade;
	}

	if (hoje.getMonth() + 1 - mes >= 0) {
		if (hoje.getMonth() + 1 - mes > 0) {
			return idade + 1;
		} else if (hoje.getUTCDate() >= dia) {
			return idade + 1;
		}
	}

	//entao eh porque sao iguais. Vejo os dias
	//se subtraio os dias e der menor que 0 entao nao cumpriu anos. Se der maior ou igual sim que jï¿½ cumpriu

	return idade;
}



function confirmaCancelamentoCadastroDependentes(){
	if(dataAcaoPagina == 'editar'){

		setTimeout(function(){
			var msg = "Deseja realmente cancelar a edição de Dependentes?";
			var resp = marisaMessage.questionDialog("Cadastro de Dependentes",
					msg, "Sim", "Não", cancelamentoCadastroDependentesConfirmada, abortaPerguntaOnly);
		}, 100);

	}
	else if (tipoEdicaoAdicional == 1) {
		
		setTimeout(function(){
			var msg = "Deseja realmente cancelar o Cadastro de Dependentes?";
			var resp = marisaMessage.questionDialog("Cadastro de Dependentes",
					msg, "Sim", "Não", cancelamentoCadastroDependentesConfirmada, abortaPerguntaOnly);
		}, 100);
		
	}else{
		
		setTimeout(function(){
			var msg = "Deseja realmente cancelar o Cadastro de Dependentes?";
			var resp = marisaMessage.questionDialog("Cadastro de Dependentes",
					msg, "Sim", "Não", cancelamentoCadastroDependentesConfirmada, abortaPerguntaOnly, marisaMessageType_WARNING);
		}, 100);
		
	}
	return false;		
}

var cancelamentoCadastroDependentesConfirmada = function(e) {
	// logging("Callback cancelamentoEdicaoAdicionalConfirmada");
	
	showPainel($('#panelDashboard')); 
	clickMenu('menuDashboard');
}



$('#formDependente').validator().on('submit', function (e) {
	if (e.isDefaultPrevented()) {
		
	}else{
		objDependentesExtraida = getDeps();

		getInformacaoAdicionalCliente();

		// $('#submita_dependentes').prop("disabled", false);
		
		if(dataAcaoPagina === 'editar'){
			
			confirmarEdicaoServico();
		}
		else{
			confirmaContratacaoServico();
		}
		return false;//Bloqueando o submit do form
	}
		
		 
	
});

