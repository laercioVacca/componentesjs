
require("../../../../resources/libs/jquery-maskedinput/jquery.mask.js");
require("../../../../resources/libs/bootstrap-validator/validator.min.js");
require("../../../../resources/libs/underscore/lodash.min.js");
require("../../../../resources/libs/jquery-ui-1.12.1.custom/jquery-ui.min.js");
require("../../../../resources/libs/jquery-ui-1.12.1.custom/i18n/datepicker-pt-BR.js");

require("../../../../resources/libs/datatable/js/jquery.dataTables.min.js");
require("../../../../resources/libs/datatable/js/dataTables.bootstrap.js");

require("../../../../resources/libs/datatable/js/dataTables.buttons.min.js");
require("../../../../resources/libs/datatable/js/buttons.flash.min.js");

require("../../../../resources/libs/datatable/js/dataTables.fixedHeader.min.js");
require("../../../../resources/libs/datatable/js/dataTables.responsive.min.js");
require("../../../../resources/libs/datatable/js/responsive.bootstrap.min.js");
require("../../../../resources/libs/datatable/js/dataTables.select.min.js");
require("../../../../resources/libs/datatable/js/dataTables.fixedColumns.min.js");



//seta o valor do atrubuto dependendo do valor da variavel passada do servicos
if(globalDataAcaoPagina === 'editar'){
	document.getElementById('cadBeneficiarios').setAttribute("data-acao", 'editar');
}else{
	document.getElementById('cadBeneficiarios').setAttribute("data-acao", 'contratar');
}

//Pega qual ação a pagina vai realizar e armazena nesta Variavel dataAcaoPagina
var dataAcaoPagina =  document.getElementById('cadBeneficiarios').getAttribute("data-acao");
console.log('data Acao Pagina',dataAcaoPagina);
console.log('Debug dentro do cadastro.js globalDataAcaoPagina',globalDataAcaoPagina);


var formCadastroMode = 1; // 1 = incluir,  2 = editar
var rowIndexEdit = 0;
var rowIndexDelete = 0;
var registroId = 0;
var registroDescr = "";
var objBeneficiariosExtraida = [];
var objCadastroCliente = null;
var objInfoAdicionais = null;
//var editarSemDependente = true;

function require(script) {
	$.ajax({
		url : script,
		dataType : "script",
		async : false, // <-- This is the key
		success : function() {
			// all good...
		},
		error : function() {
			throw new Error("Could not load script " + script);
		}
	});
}

function jsonReplaceAll(data, node, find, replace) {
	data.forEach(function(record) {
		var line = record;
		for ( var key in line) {
			if (key === node && line[key] === find) {
				line[key] = replace;
			}
		}
	});
	return data;
}

function removeFromArray(array, prop, value) {
	console.log("procurando "+value);
	var i = 0;
	array.forEach(function(record) {
		var line = record;
		for ( var key in line) {
			if (key === prop && line[key] === value) {
				console.log("removendo "+value);
				array.splice(i, 1);
			}
		}
		i++;
	});	
    return array;
}

var exclusaoConfirmada = function() {
	console.log("Callback exclusaoConfirmada");
	excluirBeneficiario(registroId);
}

var exclusaoCancelada = function() {
	console.log("Callback exclusaoCancelada");
}

function confirmarExclusao(idRegistro, descrRegistro) {
	registroId = idRegistro;
	registroDescr = descrRegistro;
	var resp = marisaMessage.questionDialog("Excluir", "Deseja realmente excluir o registro \'" + idRegistro + " - "
					+ descrRegistro + "\'?", "Sim", "Não", exclusaoConfirmada, exclusaoCancelada);
}

$(document).ready(function() {
	
	$( "#dataNascCad" ).datepicker({ 
		 changeMonth: true, 
		 changeYear: true, 
		 stepMonths: 12, 
		 showOtherMonths: true,
	     selectOtherMonths: true, 
	     dateFormat: "dd/mm/yy",
	     firstDay: 0,
	     hideIfNoPrevNext: true,
	     showAnim: 'slideDown',
	     //showOn: "both",  
	     showStatus: true,
	     showButtonPanel: true
	});
	$("#dataNascCad" ).mask("99/99/9999");
	
	$( "#cadCliDtExped" ).datepicker({ 
		changeMonth: true, 
		changeYear: true, 
		stepMonths: 12, 
		showOtherMonths: true,
		selectOtherMonths: true, 
		dateFormat: "dd/mm/yy",
		firstDay: 0,
		hideIfNoPrevNext: true,
		showAnim: 'slideDown',
		//showOn: "both",  
		showStatus: true,
		showButtonPanel: true
	});
	$("#cadCliDtExped" ).mask("99/99/9999");
	
	$("#cadCliSalario" ).mask("###.##0,00", {reverse: true});
	
	$("#dataNascCad").on('focus', function (e) {
	    e.preventDefault();
	    $(this).datepicker('show');
	    $(this).datepicker('widget').css('z-index', 1053);
	});	

	$("#dataNascCad").on('change', function (e) {
		e.preventDefault();
		if($("#dataNascCad").val().length > 0){
			tratarDataNascimento($("#dataNascCad").val()); 
		}
	});	
	
	$("#cadCliDtExped").on('focus', function (e) {
		e.preventDefault();
		$(this).datepicker('show');
		$(this).datepicker('widget').css('z-index', 1052);
	});	
	
	$("#cadCliDtExped").on('change', function (e) {
		e.preventDefault();
		if($("#cadCliDtExped").val().length > 0){
			tratarDataExpedicao($("#cadCliDtExped").val()); 
		}
	});	
	
	$("#cadCliIdadeSaida").on('change', function (e) {
		e.preventDefault();
		if($("#cadCliIdadeSaida").val().length > 0){
			tratarIdade(_INFO_CLIENTE.dtNascCliente, $("#cadCliIdadeSaida").val()); 
		}
	});	
	
    $('#btnIncluir').on( 'click', function (e) {
    	console.log("btnIncluir clicked");
    	formCadastroMode = 1;
    	e.preventDefault();
    	$('#formCadastroBen').trigger("reset");
    	$('#modalCadastroBen #formModalTitle').prop('innerText', 'Incluir Beneficiário');
    	$('#modalCadastroBen #nomeCad').val('');
    	$('#modalCadastroBen #dataNascCad').val('');
    	$('#modalCadastroBen #grauParentescoCad').val('');
    	$('#modalCadastroBen #percentCad').val('');
    	
//    	$('#modalCadastroBen #rgCad').val('');
//    	$('#modalCadastroBen #orgaoEmissorCad').val('');
//    	$('#modalCadastroBen #ufEmissorCad').val('');
    	
//    	$('#modalCadastroBen').find('[type="radio"]').prop('checked', false);
    	
		$('#modalCadastroBen').modal('show');
		
		$('#nomeCad').focus();
		
    } );
    
    $('#nomeCad').parent().tooltip({
    	   title : $('#nomeCad').data('title')
    	  });    
    
    var validatorOptionsBeneficiario = {
            delay: 300,
            html: true,
            disable: true,
            focus: true,
            custom: {
                "percent": function ($el) {
                	console.log($el);
                    var percent = $el.val().trim();
                    var check = validarPercentual(percent); 
                    console.log(check);
                    return !check;
                }
            },
            errors: {
            	percent: "Percentual total não deve ser maior que 100!"
            },
            feedback: {
            	    success: 'glyphicon-ok',
            	    error: 'glyphicon-remove'
            	  }

    }
    $('#formCadastroBen').validator(validatorOptionsBeneficiario);
    
	$('#formCadastroBen').validator().on('submit', function (e) {
		console.log("formCadastroMode: "+formCadastroMode);
		if (e.isDefaultPrevented()) {
			// handle the invalid form...
		} else {
			if(formCadastroMode == 1){ // incluir
				incluirBeneficiario();
			}else{ // editar
				editarBeneficiario();
			}
			return false;//Bloqueando o submit do form
		}
	});    
    
    var validatorOptions = {
    		delay: 300,
    		html: true,
    		disable: true,
    		focus: true,
    		custom: {
    			"email": function ($el) {
    				console.log($el);
    				var email = $el.val().trim();
    				var check = validarEmail(email);
    				console.log(check);
    				return !check;
    			},
    			'telefone': function ($el) {
    				console.log($el);
    				var checkFone = $el.val().trim();
    				var check = true;
    				if (checkFone != '') {
    					check = validarFone(checkFone);
    				}
    				console.log(check);
    				return !check;
    			}
    		},
    		errors: {
    			telefone: "Telefone inválido!"
    		},
    		feedback: {
    			success: 'glyphicon-ok',
    			error: 'glyphicon-remove'
    		}
    		
    }
    $('#formBeneficiario').validator(validatorOptions);    
    
	$('#formBeneficiario').validator().on('submit', function (e) {
		console.log("formCadastroMode: "+formCadastroMode);
		if (e.isDefaultPrevented()) {
			console.log("formBeneficiario: isDefaultPrevented");
		} else {
				
			console.log("formBeneficiario: isNOTDefaultPrevented");
			
			objCadastroCliente = {
				"cliResDdd" 		: $('#cadCliResDdd').val(),
				"cliResFone"		: $('#cadCliResFone').val(),
				"cliEmail" 			: $('#cadCliEmail').val(),
				"cliIdentidade"	    : $('#cadCliIdentidade').val(),
				"cliIdeOrgaoEmi" 	: $('#cadCliOrgEmissor').val(),
				"cliIdeDtExped" 	: $('#cadCliDtExped').val(),
				"cliEmpSalario" 	: $('#cadCliSalario').val()
			}
			
			objInfoAdicionais = {
					"profissao"			: $('#cadCliProfissao').val() + "",
					"idadeSaida"		: $('#cadCliIdadeSaida').val() + "",
					"endOutroPais"		: $('#cadCliEndOutroPais').val() + "",
					"visitaPais"		: $('#cadCliVisitaPais').prop( "checked" ) + "",
					"politicoExposto"	: $('#cadCliPolExposto').prop( "checked" ) + "",
			}
			
			objBeneficiariosExtraida = getBeneficiarios();
			
			if(!validarPercentualTotal()){
				
				marisaMessage.showDialog(marisaMessageType_WARNING, "Atenção", "Se existirem beneficiários o percentual de participação total deve ser igual a 100!");
				
			}else{
				
				// $('#submitBeneficiarios').prop("disabled", false);
				console.log("Vamos Gravar tudo!! ",objBeneficiariosExtraida);
				if(dataAcaoPagina === 'editar'){
					console.log('Vamos Salvar a edição!!')
					confirmarEdicaoServico();
				}
				else{
					confirmaContratacaoServico();
				}
				
			}

			return false;//Bloqueando o submit do form
		}
	});
    
    $('#table-beneficiarios').on( 'click', '#btnExcluir', function (e) {
    	e.preventDefault();
    	var table = $('#table-beneficiarios').dataTable();
        var tr = $(this).closest('tr');
        rowIndexDelete = table.fnGetPosition( tr[0] );;
        var row = $("#table-beneficiarios").DataTable().row(tr);    	
        var data = row.data();
        confirmarExclusao(rowIndexDelete, data['nomeCad']);
    } );

    $('#table-beneficiarios').on( 'click', '#btnEditar', function (e) {
    	console.log("btnEditar clicked");
    	formCadastroMode = 2;    	
    	e.preventDefault();
    	
    	var table = $('#table-beneficiarios').dataTable();
        var tr = $(this).closest('tr');
        rowIndexEdit = table.fnGetPosition( tr[0] );
        //rowIndexEdit = tr[0].rowIndex;
        var row = $("#table-beneficiarios").DataTable().row(tr);    	
        var data = row.data();
        
    	$('#formCadastroBen').trigger("reset");
    	$('#modalCadastroBen #formModalTitle').prop('innerText', 'Editar Beneficiário');
    	
    	$('#modalCadastroBen #nomeCad').val(data['nomeBeneficiario']);
    	$('#modalCadastroBen #dataNascCad').val(data['dataNascimento']);
    	$('#modalCadastroBen #grauParentescoCad').val(data['codParentesco']);
    	$('#modalCadastroBen #percentCad').val(data['percentualParticipacao']);
    	
    	//$('#modalCadastroBen').find('[type="radio"]').prop('checked', false);
    	
		$('#modalCadastroBen').modal('show');

		$('#nomeCad').focus();
        
    } );


	//var s = waitingDialog.show('Processando...', {dialogSize : 'sm',progressType : 'info'});
    
	popularDadosInforPessoal();
	popularDadosSeguroBen();
	popularDadosInforAdicionaisPrev();
	
	initTableBeneficiarios();
	
	//waitingDialog.hide();

	console.log('Fim do load...');
	
});




//function listarProdutosContratados(codServicoEditando){
//	// console.log('dentro da função que lista contratados',codServicoEditando)
//	var infosProdutoContratado = [];
//	$.each(arrayProdutosContratados, function(index, val) {
//		if(val.cod == codServicoEditando ){
//			infosProdutoContratado.push(val.objeto);
//		}
//	});
//	console.log('infosProdutoContratado',infosProdutoContratado)
//	return infosProdutoContratado;
//}


function initTableBeneficiarios(){
	setTimeout(function () {
	
		var objDataTable = $("#table-beneficiarios").DataTable();
	    objDataTable.destroy();	
	    var objLista;
	
	    console.log('dataAcaoPagina',dataAcaoPagina);
	    if(dataAcaoPagina == 'editar'){
			//var getInfosContratados = listarProdutosContratados(codServicoEditando);
			//var getInfosContratados = getObjectArrayProdutosContratados(codServicoEditando);
			//var dataProdDispBen = getInfosContratados.beneficiarios;
			var dataProdDispBen = getObjectArrayProdutosContratados(codServicoEditando).beneficiarios;
			
//			$('#cadVlrMensalSeguro').val('R$ ' + formatMoney(getInfosContratados[0].valorPremio,2,',','.'));
//			$('#cadCoberturaDescr').val(getInfosContratados[0].servico);
//			$('#cadCoberturaValor').val('R$ ' + formatMoney(getInfosContratados[0].valorCobertura,2,',','.'));
//			
//	    	console.log('getInfosContratados',dataProdDispBen);	
//	    }
//	    console.log('_INFO_CLIENTE',_INFO_CLIENTE);
//		if(dataProdDispBen != undefined ){
			objLista = [];
			
			console.log('nomeBeneficiario: ' + dataProdDispBen[0].nomeBeneficiario);
			
//			$('#cadVlrMensalSeguro').val('R$ ' + formatMoney(getInfosContratados[0].valorPremio,2,',','.'));
//			$('#cadCoberturaDescr').val(getInfosContratados[0].servico);
//			$('#cadCoberturaValor').val('R$ ' + formatMoney(getInfosContratados[0].valorCobertura,2,',','.'));
		    
			dataProdDispBen.forEach(function(entry) {
				
				//var objListaGrauParentesco = $("#grauParentescoCad").find('option');
				
			    console.log('beneficiarios:');
			    console.log(entry);
			    
			    console.log('pssDptCpf: ' + entry.pssDptCpf);
			    
			    var beneficiario = {};
			    if(entry.codBeneficiario != undefined){
			    	beneficiario.codBeneficiario = entry.codBeneficiario;
			    }else{
			    	beneficiario.codBeneficiario = "";
			    }
			    if(entry.codContrato != undefined){
			    	beneficiario.codContrato = entry.codContrato;
			    }else{
			    	beneficiario.codContrato = "";
			    }
			    if(entry.nomeBeneficiario != undefined){
			    	beneficiario.nomeBeneficiario = entry.nomeBeneficiario.toUpperCase();
			    }else{
			    	beneficiario.nomeBeneficiario = "";
			    }
			    if(entry.dataNascimento != undefined){
			    	beneficiario.dataNascimento = formatDate(entry.dataNascimento);
			    }else{
			    	beneficiario.dataNascimento = "";
			    }
			    if(entry.codParentesco != undefined){
			    	beneficiario.codParentesco = entry.codParentesco;
			    }else{
			    	beneficiario.codParentesco = "";
			    }
			    if(entry.grauParentesco != undefined){
			    	beneficiario.grauParentesco = entry.grauParentesco.toUpperCase();
			    }else{
			    	beneficiario.grauParentesco = "";
			    }
			    
			    if(entry.percentualParticipacao != undefined){
			    	beneficiario.percentualParticipacao = entry.percentualParticipacao;
			    }else{
			    	beneficiario.percentualParticipacao = "";
			    }
			    beneficiario.situacao = 'Importado [Cad. ' + formatDate(entry.dataAlteracao) + ']';
			    beneficiario.acao = "";
			    
			    objLista.push(beneficiario);
			
			})
			
		    
		}else{
			objLista = {};
			
		    objLista.cpf = "";
		    objLista.nome = "";
		    objLista.nomeMae = "";
		    objLista.dataNasc = "";
		    objLista.sexo = "";
		    objLista.estadoCivil = "";
		    objLista.grauParentesco = "";
		    objLista.rg = "";
		    objLista.orgaoEmissor = "";
		    objLista.ufEmissor = "";
		    objLista.situacao = "";		
		    objLista.acao = "";		
		}
	    
		console.log(objLista);
		
	    
		if (!$.fn.DataTable.isDataTable( "#table-beneficiarios" )) {
			console.log("Init #table-beneficiarios");
			var table = $('#table-beneficiarios').dataTable( {
		        language: language,	 
		        responsive: true,
				bFilter : false,               
				bLengthChange: false,
				columns: [
					{title : "&nbsp;", 
						//data : 'codigo', 
						width: "15%",
					    render: function( data, type, full, meta ) {
					        return '';
					    }
					},
					{ title: "Código" , data: "codBeneficiario"},
					{ title: "Contrato" , data: "codContrato"},
		            { title: "Nome" , data: "nomeBeneficiario"},
		            { title: "Data Nasc" , data: "dataNascimento"},
		            { title: "Cod. Parentesco", data: "codParentesco" },
		            { title: "Parentesco", data: "grauParentesco" },
		            { title: "Participação", data: "percentualParticipacao" },
		            { title: "Situação", data: "situacao" },
		            { title: "Ações" }
		        ],			
				columnDefs: [ {
			            targets: 9,
			            orderable: false,
			            defaultContent: "" +
			            		"<a class='btn btn-warning btn-xs' id='btnEditar' style='margin: 0px 0px 0px 5px;height: 20px;color: white;'><span class='glyphicon glyphicon-pencil'></span></a>" 
						        +"<a class='btn btn-danger btn-xs' id='btnExcluir' style='margin: 0px 0px 0px 5px;height: 20px;color: white;'><span class='glyphicon glyphicon-remove'></span></a>" 
			        },
			        {
			            orderable: false,
			            className: 'select-checkbox',
			            targets:   0
			        },
		            {
		                targets: [ 1 ],
		                visible: false,
		                searchable: false
		            },
		            {
		            	targets: [ 2 ],
		            	visible: false,
		            	searchable: false
		            },
		            {
		            	targets: [ 5 ],
		            	visible: false,
		            	searchable: false
		            }
		            
				],
				data: objLista,
		        select: {
		            //style:    'os',
		            //selector: 'td:first-child',
		        	style: 'single'
		        },
		        fnCreatedRow: function( nRow, aData, iDataIndex ) {
		        	$(nRow).children("td").css("overflow", "hidden");
		        	$(nRow).children("td").css("white-space", "nowrap");
		        	$(nRow).children("td").css("text-overflow", "ellipsis");
		        }
			
		    } );
		  
		}	    
		
		objDataTable = $("#table-beneficiarios").DataTable();
		new $.fn.dataTable.FixedHeader( objDataTable );	
		
		//$(tabelaDep, window.parent.document).val(objDataTable);
		
		$('.select-item').html('');
	
	  }, 500);
	
}


function incluirBeneficiario(){
	var table = $('#table-beneficiarios').DataTable();
	 
	table.row.add( {
	        "":							"",
	        "codBeneficiario":			0,
	        "codContrato":				0,
	        "nomeBeneficiario":			$('#nomeCad').val(),
	        "dataNascimento":			$('#dataNascCad').val(),
	        //"dataNascimento":			formatDate($('#dataNascCad').val()+ " 00:00:00"),
	        "codParentesco":			$('#grauParentescoCad option:selected').val(),
	        "grauParentesco":			$('#grauParentescoCad option:selected').text(),
	        "percentualParticipacao":	$('#percentCad').val(),
	        "situacao":     			"Novo"
	    } ).draw();
	
	//$(tabelaDep, window.parent.document).val(table);
	
	$('.select-item').html('');
	
	$('#modalCadastroBen').modal('hide');
	
}

function excluirBeneficiario(){
	var table = $('#table-beneficiarios').dataTable();
	 
	table.fnDeleteRow(rowIndexDelete);
	
	//$(tabelaDep, window.parent.document).val(table);
	
	$('.select-item').html('');
	
}

function editarBeneficiario(){
	var table = $('#table-beneficiarios').dataTable();

	table.fnUpdate(  {
        "codBeneficiario":			0,
        "codContrato":				0,
        "nomeBeneficiario":			$('#nomeCad').val(),
        "dataNascimento":			$('#dataNascCad').val(),
        //"dataNascimento":			formatDate($('#dataNascCad').val()+ " 00:00:00"),
        "codParentesco":			$('#grauParentescoCad option:selected').val(),
        "grauParentesco":			$('#grauParentescoCad option:selected').text(),
        "percentualParticipacao":	$('#percentCad').val(),
        "situacao":     			"Alterado",
    }, rowIndexEdit);
	
	//$(tabelaDep, window.parent.document).val(table);
	
	$('.select-item').html('');
	
	$('#modalCadastroBen').modal('hide');
	
}

// function popularDadosClienteDep(){
// 	//$('#cadDependentes').contents().find('#depPlasNumCartao').val();
	
// 	$('#depPlasNumCartao').val( $('#plasNumCartao', window.parent.document).val() );
// 	$('#depCliNome').val( $('#cliNome', window.parent.document).val() );
// 	$('#depStaCredito').val( $('#staCredito', window.parent.document).val() );
// 	$('#depStaPlastico').val( $('#staPlastico', window.parent.document).val() );
	
// }

function popularDadosSeguroBen(){

	var infosProduto = {};
	if(dataAcaoPagina == 'editar'){
		infosProduto = getObjectArrayProdutosContratados(codServicoEditando);
	}else{
		infosProduto = getObjectArrayProdutosDisponiveis(codServicoContratando);
	}

	$('#cadVlrMensalSeguro').val('R$ ' + formatMoney(infosProduto.valorPremio,2,',','.'));
	$('#cadCoberturaDescr').val(infosProduto.servico);
	$('#cadCoberturaValor').val('R$ ' + formatMoney(infosProduto.valorCobertura,2,',','.'));
	
}

function popularDadosInforAdicionaisPrev(){
	
	console.log('######### - LAVANDOSK 629')
	
	popularComboboxProficao();
	popularComboboxEndOutroPais();
	popularComboboxGrauParentesco();
	
	if(dataAcaoPagina == 'editar'){ // contratar

		var infosProduto = getObjectArrayProdutosContratados(codServicoEditando);
		
		var dados = getDadosInforAdicionaisPrev(infosProduto.codigoContratoCliente);
		
		$('#cadCliProfissao').val(dados.profissao);
		$('#cadCliIdadeSaida').val(dados.idadeSaida);
		$('#cadCliEndOutroPais').val((dados.endOutroPais != "0"? dados.endOutroPais: ""));
		$('#cadCliVisitaPais').val(dados.visitaPais);
		$('#cadCliPolExposto').val(dados.politicoExposto);
		
		// desabilitar os botões
		setTimeout(function () {
			$('#btnIncluir').prop('disabled','disabled');
			$('#submitBeneficiarios').prop('disabled','disabled');
			var btnTemp = $('#table-beneficiarios').find('.btn');
			$.each(btnTemp, function(index, campo) {
				btnTemp[index].disabled = true;
				$(btnTemp[index]).css('cursor','not-allowed');
			});
		}, 1000);
		
	}else{
		
		$('#cadCliProfissao').val('');
		$('#cadCliIdadeSaida').val('');
		$('#cadCliEndOutroPais').val('');
		$('#cadCliVisitaPais').val('');
		$('#cadCliPolExposto').val('');
		
		// habilitar os botões
		setTimeout(function () {
			$('#btnIncluir').removeProp('disabled');
			$('#submitBeneficiarios').removeProp('disabled');
			var btnTemp = $('#table-beneficiarios').find('.btn');
			$.each(btnTemp, function(index, campo) {
				btnTemp[index].disabled = false;
				$(btnTemp[index]).css('cursor','');
			});
		}, 1000);		
		
	}

}

function popularDadosInforPessoal(){
	
	$('#cadCliResDdd').val(_INFO_CLIENTE.foneResDddCliente);
	$('#cadCliResFone').val(_INFO_CLIENTE.foneResCliente);
	$('#cadCliEmail').val(_INFO_CLIENTE.emailCliente);
	$('#cadCliIdentidade').val(_INFO_CLIENTE.numeroRgCliente);
	$('#cadCliOrgEmissor').val(_INFO_CLIENTE.orgaoEmissorCliente);
	$('#cadCliDtExped').val(_INFO_CLIENTE.rgDtExpedCliente);
	$('#cadCliSalario').val(formatMoney(_INFO_CLIENTE.salarioCliente,2,',','.'));
	
	if(validarFoneCelular("" + _INFO_CLIENTE.foneResDddCliente + _INFO_CLIENTE.foneResCliente)){
		$('#cadCliTipoFone').val('C');
	}else{
		$('#cadCliTipoFone').val('F');
	}
	var tipo = $("#cadCliTipoFone").val();
	if( tipo == "C" ){
		$('#cadCliResFone').unmask();
		$('#cadCliResFone').mask("99999-9999");
	} else {
		$('#cadCliResFone').unmask();
		$('#cadCliResFone').mask("9999-9999");
	}
}

function popularComboboxGrauParentesco() {
	
	var parameters = {};	
	
	var result = clientService.callPostService("previdencia", "beneficiario/obtemGrauParentesco", parameters); 
	if (result["codigoRetorno"] == 200) {
		
		var objLista = result["objectsReturn"]["ComboboxGrauParentesco"];

		logging("ComboboxGrauParentesco: ", objLista);

		var grauParentescoCad = $('#grauParentescoCad');
		grauParentescoCad.find('option').remove(); 
		$('<option>').val("").text("Selecione...").appendTo(grauParentescoCad); // Criando a opção selecione...
		$.each(objLista, function(index, objeto) { // Iterando as opções
			$('<option>').val(objeto.cod).text(objeto.descr).appendTo(grauParentescoCad);
		});
		
	} else if (result["codigoRetorno"] != 204) {
		logging("Warning");
		marisaMessage.showDialog(marisaMessageType_WARNING, "Atenção", result["codigoRetorno"] + " - " +  result["descricaoRetorno"] + "!");
		return null;
	} else {
		logging("Error");
		marisaMessage.showDialog(marisaMessageType_ERROR, "Erro", result["codigoRetorno"] + " - " +  result["descricaoRetorno"] + "!");
		return null;
	}

}

function popularComboboxEndOutroPais() {
	
	var parameters = {};	
	
	var result = clientService.callPostService("previdencia", "obtemPais", parameters); 
	if (result["codigoRetorno"] == 200) {
		
		var objLista = result["objectsReturn"]["ComboboxPais"];
		
		logging("ComboboxPais: ", objLista);
		
		var cadCliEndOutroPais = $('#cadCliEndOutroPais');
		cadCliEndOutroPais.find('option').remove(); 
		$('<option>').val("").text("Não").appendTo(cadCliEndOutroPais); // Criando a opção selecione...
		$.each(objLista, function(index, objeto) { // Iterando as opções
			$('<option>').val(objeto.codDesc).text(objeto.descr).appendTo(cadCliEndOutroPais);
		});
		
	} else if (result["codigoRetorno"] != 204) {
		logging("Warning");
		marisaMessage.showDialog(marisaMessageType_WARNING, "Atenção", result["codigoRetorno"] + " - " +  result["descricaoRetorno"] + "!");
		return null;
	} else {
		logging("Error");
		marisaMessage.showDialog(marisaMessageType_ERROR, "Erro", result["codigoRetorno"] + " - " +  result["descricaoRetorno"] + "!");
		return null;
	}
	
}

function popularComboboxProficao() {
	
	var parameters = {};	
	
	var result = clientService.callPostService("previdencia", "obtemProfissao", parameters); 
	if (result["codigoRetorno"] == 200) {
		
		var objLista = result["objectsReturn"]["ComboboxProfissao"];
		
		logging("ComboboxProfissao: ", objLista);
		
		var cadCliProfissao = $('#cadCliProfissao');
		cadCliProfissao.find('option').remove(); 
		$('<option>').val("").text("Selecione...").appendTo(cadCliProfissao); // Criando a opção selecione...
		$.each(objLista, function(index, objeto) { // Iterando as opções
			$('<option>').val(objeto.cod).text(objeto.descr).appendTo(cadCliProfissao);
		});
		
	} else if (result["codigoRetorno"] != 204) {
		logging("Warning");
		marisaMessage.showDialog(marisaMessageType_WARNING, "Atenção", result["codigoRetorno"] + " - " +  result["descricaoRetorno"] + "!");
		return null;
	} else {
		logging("Error");
		marisaMessage.showDialog(marisaMessageType_ERROR, "Erro", result["codigoRetorno"] + " - " +  result["descricaoRetorno"] + "!");
		return null;
	}
	
}

function getDadosInforAdicionaisPrev(codigoContrato) {
	
	var parameters = {
			"codigoContrato" 	: codigoContrato + ""
	};	
	
	var result = clientService.callPostService("previdencia", "getDadosInforAdicionais", parameters); 
	if (result["codigoRetorno"] == 200) {
		var obj = result["objectsReturn"]["PrevidenciaInfoAdicionais"];
		
		logging("PrevidenciaInfoAdicionais: ", obj);
		
		return obj;
		
	} else if (result["codigoRetorno"] != 204) {
		logging("Warning");
		marisaMessage.showDialog(marisaMessageType_WARNING, "Atenção", result["codigoRetorno"] + " - " +  result["descricaoRetorno"] + "!");
		return null;
	} else {
		logging("Error");
		marisaMessage.showDialog(marisaMessageType_ERROR, "Erro", result["codigoRetorno"] + " - " +  result["descricaoRetorno"] + "!");
		return null;
	}
	
}

function getBeneficiarios(){
	var TrasformObjBen = [];
	var getObj = $("#table-beneficiarios").DataTable().rows().data();
	objBeneficiariosExtraida = [];
	console.log('Pegando o getBeneficiarios',getObj)
	for (var i = 0; i < getObj.length; i++) {
		console.log('For ----',getObj[i])
		objBeneficiariosExtraida.push(getObj[i]);
	}


	$.each(objBeneficiariosExtraida, function(index, val) {
		console.log('index',index,val.cpf)
		
//		var objListaParentesco = $("#modalCadastroBen #grauParentescoCad").find('option');
//		var parentesco;
//		$.each(objListaParentesco, function(index, item) {
//			if (val.grauParentesco == item.text) {
//				parentesco = item.value;
//			}										        		
//		});

		TrasformObjBen.push({
		 	'codBeneficiario':(val.codBeneficiario + "").replace(/[^\d]+/g,""),
		 	'codContrato':val.codContrato,
		 	'nomeBeneficiario':val.nomeBeneficiario,
		 	'dataNascimento':getDate(val.dataNascimento),
		 	'codParentesco':val.codParentesco,
		 	'grauParentesco':val.grauParentesco,
		 	'percentualParticipacao':val.percentualParticipacao
		 })
	});
  
	console.log('TrasformObjBen',TrasformObjBen);
	return TrasformObjBen;
}

//function getCodParentesco(txt){
//	var objListaParentesco = $("#modalCadastroBen #grauParentescoCad").find('option');
//	var parentesco;
//	$.each(objListaParentesco, function(index, item) {
//		if (txt == item.text) {
//			parentesco = item.value;
//		}										        		
//	});
//	return parseInt(parentesco);
//}

function validarPercentual(percent){
	console.log(percent);
	
	if(parseInt(percent) < 1){
		return false;
	}
	
	var resp = true;
	var percentTotal = 0;
	var getObj = $("#table-beneficiarios").DataTable().rows().data();
	if(getObj.length > 0){
		for (var i = 0; i < getObj.length; i++) {
			percentTotal += parseInt(getObj[i].percentualParticipacao);
		}
	}
	percentTotal += parseInt(percent);
	if(formCadastroMode == 2 && percentTotal - parseInt(percent) > 100){
		resp = false;
	}else if(formCadastroMode == 1 && percentTotal > 100){
		resp = false;
	}
	return resp;	
	
}

function validarPercentualTotal(){
	
	var resp = true;
	var percentTotal = 0;
	var getObj = $("#table-beneficiarios").DataTable().rows().data();
	if(getObj.length > 0){
		for (var i = 0; i < getObj.length; i++) {
			percentTotal += parseInt(getObj[i].percentualParticipacao);
		}
		if(percentTotal < 100 || percentTotal > 100){
			resp = false;
		}
	}
	return resp;	
	
}


function tratarIdade(dataNasc, idadeSaida){
	if(calcularIdade(dataNasc) < idadeSaida){
		return true;
	}else{
		marisaMessage.showDialog(marisaMessageType_WARNING, "Idade", "Idade de saída precisa ser maior que a idade atual do cliente: " + calcularIdade(dataNasc) + " anos" );
        document.getElementById("cadCliIdadeSaida").value = "";
        document.getElementById("cadCliIdadeSaida").focus();
        return false;	
    }
}

function tratarDataNascimento(data){
	//if(calcularIdade(data) > -1){
	if(new Date(Date.parse(desformatDate(data) + " 23:59:59")) < new Date()){
		if(calcularIdade(data) > calcularIdade('01/01/1900')){
			marisaMessage.showDialog(marisaMessageType_WARNING, "Idade", "Data de Nascimento Inválida!");
			document.getElementById("dataNascCad").value = "";
			document.getElementById("dataNascCad").focus();
			return false;			
		}else{
			return true;
		}
		
	}else{
		marisaMessage.showDialog(marisaMessageType_WARNING, "Idade", "Data de Nascimento Inválida!");
		document.getElementById("dataNascCad").value = "";
		document.getElementById("dataNascCad").focus();
		return false;	
	}
}

function tratarDataExpedicao(data){
	//if(calcularIdade(data) > -1){
	if(new Date(Date.parse(desformatDate(data) + " 23:59:59")) < new Date()){
		if(calcularIdade( _INFO_CLIENTE.dtNascCliente ) < calcularIdade(data)){
			marisaMessage.showDialog(marisaMessageType_WARNING, "Idade", "Data de Expedição Inválida!");
			document.getElementById("cadCliDtExped").value = "";
			document.getElementById("cadCliDtExped").focus();
			return false;			
		}else{
			return true;
		}		
	}else{
		marisaMessage.showDialog(marisaMessageType_WARNING, "Idade", "Data de Expedição Inválida!");
		document.getElementById("cadCliDtExped").value = "";
		document.getElementById("cadCliDtExped").focus();
		return false;	
	}
}


//calcular a idade de uma pessoa
//recebe a data como um string em formato portugues
//devolve um inteiro com a idade. Devolve false em caso de que a data seja incorreta ou maior que o dia atual
function calcularIdade(data) {

	//calculo a data de hoje
	hoje = new Date();
	var i = 0;

	//calculo a data que recebo
	//descomponho a data em um array
	var array_data = data.split("/");
	//se o array nao tem tres partes, a data eh incorreta
	if (array_data.length != 3) {
		return false
	}

	//comprovo que o ano, mes, dia sao corretos
	var ano
	ano = parseInt(array_data[2]);
	if (isNaN(ano))
		return false

	var mes
	mes = parseFloat(array_data[1]);
	if (isNaN(mes))
		return false

	var dia
	dia = parseFloat(array_data[0]);

	if (isNaN(dia))
		return false

		//se o ano da data que recebo so tem 2 cifras temos que muda-lo a 4
	if (ano <= 99) {
		ano += 1900
	}

	//subtraio os anos das duas datas
	idade = hoje.getYear() + 1900 - ano - 1; //-1 porque ainda nao fez anos durante este ano

	//se subtraio os meses e for menor que 0 entao nao cumpriu anos. Se for maior sim ja cumpriu
	if (hoje.getMonth() + 1 - mes < 0) { //+ 1 porque os meses comecam em 0
		return idade;
	}

	if (hoje.getMonth() + 1 - mes >= 0) {
		if (hoje.getMonth() + 1 - mes > 0) {
			return idade + 1;
		} else if (hoje.getUTCDate() >= dia) {
			return idade + 1;
		}
	}

	//entao eh porque sao iguais. Vejo os dias
	//se subtraio os dias e der menor que 0 entao nao cumpriu anos. Se der maior ou igual sim que ja cumpriu

	return idade;
}



function confirmaCancelamentoCadastroBeneficiarios(){
	if (tipoEdicaoAdicional == 1) {
		
		setTimeout(function(){
			var msg = "Deseja realmente cancelar o Cadastro de Beneficiários?";
			var resp = marisaMessage.questionDialog("Cadastro de Beneficiários",
					msg, "Sim", "Não", cancelamentoCadastroBeneficiariosConfirmada, abortaPerguntaOnly, marisaMessageType_WARNING);
		}, 100);
		
	}else{
		
		setTimeout(function(){
			var msg = "Deseja realmente cancelar o Cadastro de Beneficiários?";
			var resp = marisaMessage.questionDialog("Cadastro de Beneficiários",
					msg, "Sim", "Não", cancelamentoCadastroBeneficiariosConfirmada, abortaPerguntaOnly, marisaMessageType_WARNING);
		}, 100);
		
	}
	return false;		
}

var cancelamentoCadastroBeneficiariosConfirmada = function(e) {
	showPainel($('#panelDashboard')); 
	clickMenu('menuDashboard');
}

function validarFone(value) {
	var tipo = $("#cadCliTipoFone").val();
	value = value.replace(/[^\d]+/g,"") + "";
	value = $('#cadCliResDdd').val() + value;
	console.log("Validar Fone ...");
	console.log(value);
	if( tipo == "C" ){
		check = validarFoneCelular(value); 
	}else{
		check = validarFoneFixo(value);
	}
	return check;
	
}

function validarEmail(value) {
	console.log("Validar Email ...", value);
	check = isEmail(value); 
	return check;
}

function isEmail(field){
	
	usuario = field.substring(0, field.indexOf("@"));
	dominio = field.substring(field.indexOf("@")+ 1, field.length);
	 
	if ((usuario.length >=1) &&
	    (dominio.length >=3) && 
	    (usuario.search("@")==-1) && 
	    (dominio.search("@")==-1) &&
	    (usuario.search(" ")==-1) && 
	    (dominio.toUpperCase().search(".BR.BR")==-1) &&
	    (dominio.search(" ")==-1) &&
	    (dominio.search(".")!=-1) &&      
	    (dominio.indexOf(".") >=1)&& 
	    (dominio.lastIndexOf(".") < dominio.length - 1)) {
		return true;
	}else{
		return false;
	}
}

function validarFoneCelular(telefone){
	telefone = telefone.replace(/\D/g,'');
	
	if(!(telefone.length >= 10 && telefone.length <= 11)) return false;
	
	if (telefone.length == 11 && parseInt(telefone.substring(2, 3)) != 9) return false;
	if (telefone.length == 10 && [7].indexOf(parseInt(telefone.substring(2, 3))) == -1) return false;
	
	for(var n = 0; n < 10; n++){
		if(telefone == new Array(11).join(n) || telefone == new Array(12).join(n)) return false;
	}
	
	//DDDs validos
    var codigosDDD =   [11, 12, 13, 14, 15, 16, 17, 18, 19,
						21, 22, 24, 27, 28, 31, 32, 33, 34,
						35, 37, 38, 41, 42, 43, 44, 45, 46,
						47, 48, 49, 51, 53, 54, 55, 61, 62,
						64, 63, 65, 66, 67, 68, 69, 71, 73,
						74, 75, 77, 79, 81, 82, 83, 84, 85,
						86, 87, 88, 89, 91, 92, 93, 94, 95,
						96, 97, 98, 99];
	if(codigosDDD.indexOf(parseInt(telefone.substring(0, 2))) == -1) return false;
	
	return true;
	
}

function validarFoneFixo(telefone){
    telefone = telefone.replace(/\D/g,'');
    
    if(!(telefone.length == 10)) return false;
	if( [2, 3, 4, 5, 7].indexOf(parseInt(telefone.substring(2, 3))) == -1) return false;
    
    for(var n = 0; n < 10; n++){
    	if(telefone == new Array(11).join(n)) return false;
    }
    
    //DDDs validos
    var codigosDDD =   [11, 12, 13, 14, 15, 16, 17, 18, 19,
						21, 22, 24, 27, 28, 31, 32, 33, 34,
						35, 37, 38, 41, 42, 43, 44, 45, 46,
						47, 48, 49, 51, 53, 54, 55, 61, 62,
						64, 63, 65, 66, 67, 68, 69, 71, 73,
						74, 75, 77, 79, 81, 82, 83, 84, 85,
						86, 87, 88, 89, 91, 92, 93, 94, 95,
						96, 97, 98, 99];
	if(codigosDDD.indexOf(parseInt(telefone.substring(0, 2))) == -1) return false;
	
	return true;
  
}

$("#cadCliTipoFone").change(function(){
	var tipo = $("#cadCliTipoFone").val();
	if( tipo == "C" ){
		$('#cadCliResFone').unmask();
		$('#cadCliResFone').mask("99999-9999");
	} else {
		$('#cadCliResFone').unmask();
		$('#cadCliResFone').mask("9999-9999");
	}
	$('#cadCliResFone').val('');
	$('#cadCliResFone').focus();
});

//$('#modalQuestion').on('click', function (e) {
//	
//	objCadastroCliente = {
//			"cliResDdd" 		: $('#cliResDdd').val(),
//			"cliResFone"		: $('#cliResFone').val(),
//			"cliEmail" 			: $('#cliEmail').val(),
//			"cliIdentidade"	    : $('#cliIdentidade').val(),
//			"cliIdeOrgaoEmi" 	: $('#cliIdeOrgaoEmi').val(),
//			//""		    = _INFO_CLIENTE.valorCobertura;
//			"cliEmpSalario" 	: $('#cliEmpSalario').val()
//			
//	}
//	//objCadastroCliente.tipoFoneForm		= $('#tipoFoneForm').val();
//	//
//	console.log('Teste de divisão de func',objCadastroCliente);
//	
//	
//});
//
//$('#submitBeneficiarios').on('click', function (e) {
//		e.preventDefault();
//		
//		objCadastroCliente = {
//			"cliResDdd" 		: $('#cadCliResDdd').val(),
//			"cliResFone"		: $('#cadCliResFone').val(),
//			"cliEmail" 			: $('#cadCliEmail').val(),
//			"cliIdentidade"	    : $('#cadCliIdentidade').val(),
//			"cliIdeOrgaoEmi" 	: $('#cadCliOrgEmissor').val(),
//			"cliIdeDtExped" 	: $('#cadCliDtExped').val(),
//			"cliEmpSalario" 	: $('#cadCliSalario').val()
//		}
//		
//		objInfoAdicionais = {
//				"profissao"			: $('#cadCliProfissao').val() + "",
//				"idadeSaida"		: $('#cadCliIdadeSaida').val() + "",
//				"endOutroPais"		: $('#cadCliEndOutroPais').val() + "",
//				"visitaPais"		: $('#cadCliVisitaPais').prop( "checked" ) + "",
//				"politicoExposto"	: $('#cadCliPolExposto').prop( "checked" ) + "",
//		}
//		
//		objBeneficiariosExtraida = getBeneficiarios();
//
//		// $('#submitBeneficiarios').prop("disabled", false);
//		console.log("Vamos Gravar tudo!! ",objBeneficiariosExtraida);
//		if(dataAcaoPagina === 'editar'){
//			console.log('Vamos Salvar a edição!!')
//			confirmarEdicaoServico();
//		}
//		else{
//			confirmaContratacaoServico();
//		}
//		
//});

