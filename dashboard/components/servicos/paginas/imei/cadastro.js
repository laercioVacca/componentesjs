
require("../../../../resources/libs/jquery-maskedinput/jquery.mask.js");
require("../../../../resources/libs/bootstrap-validator/validator.min.js");
require("../../../../resources/libs/underscore/lodash.min.js");
require("../../../../resources/libs/jquery-ui-1.12.1.custom/jquery-ui.min.js");
require("../../../../resources/libs/jquery-ui-1.12.1.custom/i18n/datepicker-pt-BR.js");

var formCadastroMode = 1; // 1 = incluir,  2 = editar
var objCelular;

function require(script) {
	$.ajax({
		url : script,
		dataType : "script",
		async : false, // <-- This is the key
		success : function() {
			// all good...
		},
		error : function() {
			throw new Error("Could not load script " + script);
		}
	});
}

function jsonReplaceAll(data, node, find, replace) {
	data.forEach(function(record) {
		var line = record;
		for ( var key in line) {
			if (key === node && line[key] === find) {
				line[key] = replace;
			}
		}
	});
	return data;
}

function removeFromArray(array, prop, value) {
	logging("procurando "+value);
	var i = 0;
	array.forEach(function(record) {
		var line = record;
		for ( var key in line) {
			if (key === prop && line[key] === value) {
				logging("removendo "+value);
				array.splice(i, 1);
			}
		}
		i++;
	});	
    return array;
}


$(document).ready(function() {
	
	logging("Iniciando Celular: "+globalDataAcaoPagina);
	
	if(globalDataAcaoPagina == 'editar'){
		formCadastroMode = 2;
	}

	// carregar combo de marcas
	popularComboboxMarcas();
	
	$("#marcaCel").on('change', function (e) {
	    e.preventDefault();
	    // carregar combo de modelos
	    popularComboboxModelos();
	});	

	$("#modeloCel").on('change', function (e) {
		e.preventDefault();
		// carregar valor seguro e valor cobertura
		popularValoresModelo();
	});	
	
    
    var validatorOptions = {
            delay: 300,
            html: true,
            disable: true,
            focus: true,
            custom: {
                "imei": function ($el) {
                	logging($el);
                    var checkImei = $el.val().trim();
                    var check = (checkImei.length == 15) && (!/^(\d)\1+$/.test(checkImei));
                    logging(check);
                    return !check;
                },
			    "imeiConfirm": function ($el) {
			    	logging($el);
			    	var checkImei = $el.val().trim();
			    	var check = (checkImei == $('#imeiCel').val());
			    	logging(check);
			    	return !check;
			    }
            },
            errors: {
                imei: "IMEI inválido!",
                imeiConfirm: "Confirmação do IMEI inválido!"
            },
            feedback: {
            	    success: 'glyphicon-ok',
            	    error: 'glyphicon-remove'
            	  }

    }
    $('#formCelular').validator(validatorOptions);    
    
	$('#formCelular').validator().on('submit', function (e) {
		logging("formCelular: "+formCadastroMode);
		if (e.isDefaultPrevented()) {
			// handle the invalid form...
		} else {
			if(formCadastroMode == 1){ // incluir
				incluirCelular();
			}else{ // editar
				editarCelular();
			}
			return false;
		}
	});
	
	$('#submitCelular').on('click', function (e) {
		e.preventDefault();

		$('#formCelular').submit();
		
	});
	
	if(formCadastroMode == 2){
		popularFormEdicao(codServicoEditando);
	}

    
});

function incluirCelular() {

	objCelular = getCel();
	getObjectArrayProdutosDisponiveis(codServicoContratando).valorMinimoPremio = objCelular.valorSeguro;
	getObjectArrayProdutosDisponiveis(codServicoContratando).valorMinimoCobertura = objCelular.valorCelular;
	
	// todo: verificar se existe o imei em algum contrato ativo e vigente
	var resp = validarImei(objCelular.imeiCelular, null);
	
	if(resp == true){
		confirmaContratacaoServico();
	}else{
		marisaMessage.showDialog(marisaMessageType_WARNING, "Atenção","Esse Imei encontra-se cadastrado em outro contrato!");
	}
	
}

function editarCelular() {
	
	objCelular = getCel();
	getObjectArrayProdutosContratados(codServicoEditando).valorMinimoPremio = objCelular.valorSeguro;
	getObjectArrayProdutosContratados(codServicoEditando).valorMinimoCobertura = objCelular.valorCelular;
	
	// todo: verificar se existe o imei em algum contrato ativo e vigente
	var resp = validarImei(objCelular.imeiCelular, getObjectArrayProdutosContratados(codServicoEditando).codigoContratoCliente);
	
	if(resp == true){
		confirmarEdicaoServico();
	}else{
		marisaMessage.showDialog(marisaMessageType_WARNING, "Atenção","Esse Imei encontra-se cadastrado em outro contrato!");
	}
	
}

function validarImei(pImei, pContrato){
	
	var parameters = {
			'codContrato': pContrato + "",
			'numImei': pImei
	};
	
	var result = clientService.callPostService("dashboard/utilities", "validarImei", parameters);
	if (result["codigoRetorno"] == 200) {
		var obj = result["objectsReturn"]["validarImei"];
		
		if(obj == true){
			return true;
		}else{
			return false;
		}

	} else {
		logging("Error");
		marisaMessage.showDialog(marisaMessageType_ERROR, "Erro","Falha ao tentar validar o Imei!");
		return false;
	}	
	
}

function popularFormEdicao(codServico){
	
	var imei = getObjectArrayProdutosContratados(codServico).imei;
	$('#marcaCel').val(imei.marcaCelular);
	popularComboboxModelos();
	var valorCelular = imei.valorCelular + "";
	var valorSeguro = imei.valorSeguro + "";
	$('#modeloCel').val(imei.idModelo + '|' + valorCelular.replace(/\./g, ',') + '|' + valorSeguro.replace(/\./g, ','));
	if($('#modeloCel').val() == null || $('#modeloCel').val().length == 0){
		$('#modeloCel').val(imei.idModelo + '|' + valorCelular.replace(/\,/g, '.') + '|' + valorSeguro.replace(/\,/g, '.'));
	}
	$('#modeloCel').focusout();
	$('#valorCoberturaCel').val('R$ ' + formatMoney( imei.valorCelular, 2, ',', '.'));
	$('#valorSeguroCel').val('R$ ' + formatMoney( imei.valorSeguro, 2, ',', '.'));
	$('#imeiCel').val(imei.imeiCelular);
	$('#imeiConfirmCel').val(imei.imeiCelular);
	$('#imeiCel').focus();
	
}


function popularComboboxMarcas(){
	
	var combo = $('#marcaCel');// Atribuindo referencias na variavel
	combo.find('option').remove(); // Limpando as opções do combo

	var config = {};
	var parameters = {
	};
	
	var result = clientService.callPostService("dashboard/utilities", "getComboboxMarcasCelular", parameters);
	if (result["codigoRetorno"] == 200) {
		var objLista = result["objectsReturn"]["ComboboxMarcas"];

		$('<option>').val("").text("Selecione...").appendTo(combo); // Criando a opção selecione...
		$.each(objLista, function(index, objeto) { // Iterando as opções
			$('<option>').val(objeto.cod).text(objeto.descr).appendTo(combo);
		});
		combo.focusout();// Necessário para atualizar o botão de submit
	} else {
		logging("Error");
		marisaMessage.showDialog(marisaMessageType_ERROR, "Erro","Falha ao tentar recuper a lista de Marcas de Celular!");
	}
	
}

function popularComboboxModelos(){
	
	var combo = $('#modeloCel');// Atribuindo referencias na variavel
	combo.find('option').remove(); // Limpando as opções do combo

	var config = {};
	var parameters = {
			'codProduto': 152,
			'marca': $('#marcaCel').val()
	};
	
	var result = clientService.callPostService("dashboard/utilities", "getComboboxModelosCelular", parameters);
	if (result["codigoRetorno"] == 200) {
		var objLista = result["objectsReturn"]["ComboboxModelos"];

		$('<option>').val("").text("Selecione...").appendTo(combo); // Criando a opção selecione...
		$.each(objLista, function(index, objeto) { // Iterando as opções
			$('<option>').val(objeto.cod).text(objeto.descr).appendTo(combo);
		});
		combo.focusout();// Necessário para atualizar o botão de submit
	} else {
		logging("Error");
		marisaMessage.showDialog(marisaMessageType_ERROR, "Erro","Falha ao tentar recuper a lista de Modelos de Celular!");
	}
	
}

function popularValoresModelo(){
	var vlrCobertura = $('#modeloCel').val().split('|')[1].replace(',','.');
	$('#valorCoberturaCel').val('R$ ' + formatMoney( vlrCobertura, 2, ',', '.'));
	var vlrSeguro = $('#modeloCel').val().split('|')[2].replace(',','.');
	$('#valorSeguroCel').val('R$ ' + formatMoney( vlrSeguro, 2, ',', '.'));
	$('#imeiCel').focus();
}

function getCel(){
	
	var celular = {
		'marcaCelular': $('#marcaCel').val(), 	
		'idModelo': $('#modeloCel').val().split('|')[0], 	
		'valorCelular': $('#modeloCel').val().split('|')[1].replace(',','.'), 	
		'valorSeguro': $('#modeloCel').val().split('|')[2].replace(',','.'), 	
		'imeiCelular': $('#imeiCel').val() 	
	}
	
	logging('celular',celular);
	return celular;
	
}

function getCodParentesco(txt){
	var objListaParentesco = $("#modalCadastroDep #grauParentescoCad").find('option');
	var parentesco;
	$.each(objListaParentesco, function(index, item) {
		if (txt == item.text) {
			parentesco = item.value;
		}										        		
	});
	return parseInt(parentesco);
}

function confirmaCancelamentoCadastroCelular(){
	
	if(tipoEdicaoAdicional == 1){
		
		setTimeout(function(){
			var msg = "Deseja realmente cancelar o Cadastro de Celular?";
			var resp = marisaMessage.questionDialog("Cadastro de Celular",
					msg, "Sim", "Não", cancelamentoCadastroCelularConfirmada, abortaPerguntaOnly);
		}, 100);
		
	}else{
		
		setTimeout(function(){
			var msg = "Deseja realmente cancelar a alteração de Celular?";
			var resp = marisaMessage.questionDialog("Cadastro de Celular",
					msg, "Sim", "Não", cancelamentoCadastroCelularConfirmada, abortaPerguntaOnly, marisaMessageType_WARNING);
		}, 100);
		
	}
	return false;		
}

var cancelamentoCadastroCelularConfirmada = function(e) {
	// logging("Callback cancelamentoCadastroCelularConfirmada");
	showPainel($('#panelDashboard')); 
	clickMenu('menuDashboard');
}



