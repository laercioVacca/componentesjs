require("../../../../resources/libs/jquery-maskedinput/jquery.mask.js");
require("../../../../resources/libs/bootstrap-validator/validator.min.js");
require("../../../../resources/libs/underscore/lodash.min.js");
require("../../../../resources/libs/jquery-ui-1.12.1.custom/jquery-ui.min.js");
require("../../../../resources/libs/jquery-ui-1.12.1.custom/i18n/datepicker-pt-BR.js");

require("../../../../resources/libs/datatable/js/jquery.dataTables.min.js");
require("../../../../resources/libs/datatable/js/dataTables.bootstrap.js");

require("../../../../resources/libs/datatable/js/dataTables.buttons.min.js");
require("../../../../resources/libs/datatable/js/buttons.flash.min.js");

require("../../../../resources/libs/datatable/js/dataTables.fixedHeader.min.js");
require("../../../../resources/libs/datatable/js/dataTables.responsive.min.js");
require("../../../../resources/libs/datatable/js/responsive.bootstrap.min.js");
require("../../../../resources/libs/datatable/js/dataTables.select.min.js");
require("../../../../resources/libs/datatable/js/dataTables.fixedColumns.min.js");



//seta o valor do atrubuto dependendo do valor da variavel passada do servicos
if(globalDataAcaoPagina === 'editar'){
	document.getElementById('card.cadastro_local_risco').setAttribute("data-acao", 'editar');

}
else{
	document.getElementById('card.cadastro_local_risco').setAttribute("data-acao", 'contratar');
}
//Pega qual ação a pagina vai realizar e armazena nesta Variavel dataAcaoPagina
var dataAcaoPagina =  document.getElementById('card.cadastro_local_risco').getAttribute("data-acao");


var objLocalExtraido = [];
function require(script) {
	$.ajax({
		url : script,
		dataType : "script",
		async : false, // <-- This is the key
		success : function() {
			// all good...
			// logging("Loadding "+script);
		},
		error : function() {
			throw new Error("Could not load script " + script);
		}
	});
}
// 
// logging('arrayProdutosContratados',arrayProdutosContratados)



var exclusaoConfirmada = function() {
	logging("Callback exclusaoConfirmada");
	excluirDependente(registroId);
}

var exclusaoCancelada = function() {
	// logging("Callback exclusaoCancelada");
}

function confirmarExclusao(idRegistro, descrRegistro) {
	registroId = idRegistro;
	registroDescr = descrRegistro;
	var resp = marisaMessage.questionDialog("Excluir", "Deseja realmente excluir o registro \'" + idRegistro + " - "
					+ descrRegistro + "\'?", "Sim", "Não", exclusaoConfirmada, exclusaoCancelada);
}

function listarProdutosContratados(codServicoEditando){
	var infosProdutoContratado = [];
	$.each(arrayProdutosContratados, function(index, val) {
		if(val.cod == codServicoEditando ){
			infosProdutoContratado.push(val.objeto);
		}
		
	});
	return infosProdutoContratado[0];
}




$(document).ready( function() {
	
	logging("Iniciando carregamento do local...",codServicoContratando);
	
	// popularDadosClienteDep();
	
	if(dataAcaoPagina == 'editar'){
		$('#tituloDinamico').html('Editar Local de Risco');
		popularDadosLocal(codServicoEditando);
		// listarProdutosContratados(codServicoEditando)
	}else{
		popularDadosSeguroDep(codServicoContratando);
	}
	
	
	$('#locCep').mask("99.999-999");
	$("#locCep").focus();

	$('#formLocal').on( 'click', '#btnInformar', function(e) {
		logging("btnInformar clicked");
		formCadastroMode = 1;
		e.preventDefault();
		$('#formCadastroLoc').trigger("reset");
		$('#modalCadastroLoc #formModalTitle').attr( 'innerText', 'Buscar CEP por Logradouro');

		$('#modalCadastroLoc').modal('show');

	});
	
    var validatorOptions = {
            delay: 300,
            html: true,
            disable: true,
            focus: true,
            custom: {
                "cpf": function ($el) {
                	logging($el);
                    var checkCpf = $el.val().trim();
                    var check = validarCPF(checkCpf);
                    logging(check);
                    return !check;
                }
            },
            errors: {
                cpf: "CPF inválido!"
            },
            feedback: {
            	    success: 'glyphicon-ok',
            	    error: 'glyphicon-remove'
            	  }
    }
    $('#formCadastroLoc').validator(validatorOptions);
    
    $('#btnPesquisar').on( 'click', function (e) {
    	logging("btnPesquisar clicked");
    	e.preventDefault();
    	if($('#locCep').val() != undefined && $('#locCep').val().replace(/[^\d]+/g,'').length == 8){
    		logging($('#locCep').val());
    		var logradouro = pesquisarLogradouro($('#locCep').val().replace(/[^\d]+/g,'')); 
    		
    		$("#locRua").val(logradouro.ceplogLograCep);
    		$("#locComplemento").val(logradouro.ceplogComplCep);
    		$("#locBairro").val(logradouro.ceplogBaiIniCep);
    		$("#locMunicipio").val(logradouro.ceplogLocalidadeCep);
    		$("#locUf").val(logradouro.ceplogUfCep);
    		$("#locNumero").focus();
    		
    		
    	}else{
    		
    		marisaMessage.showDialog(marisaMessageType_WARNING, "Atenção", "Preencha corretamente o campo CEP!");
    		$("#locCep").focus();
    		
    	}
    	
		
    } );    
    
    $('#btnBuscar').on( 'click', function (e) {
    	logging("btnBuscar clicked");
    	e.preventDefault();

    	if(
    			($('#pesqEstado').val() != undefined && $('#pesqEstado').val().length == 2)
    			|| ($('#pesqMunicipio').val() != undefined && $('#pesqMunicipio').val().length > 0)
    			|| ($('#pesqLogradouro').val() != undefined && $('#pesqLogradouro').val().length > 0)
    	){
    		logging($('#pesqEstado').val());
    		var logradouros = pesquisarCepLogradouro($('#pesqEstado').val(), $('#pesqMunicipio').val(), $('#pesqLogradouro').val());
    		
    		logging(logradouros);
    		
    		loadTableLocais(logradouros);
    		
    	}else{
    		
    		marisaMessage.showDialog(marisaMessageType_WARNING, "Atenção", "Preencha corretamente os campos de pesquisa!");
    		$("#pesqEstado").focus();
    		
    	}
    	
    } );  
    
    $('#btnSelecionar').on( 'click', function (e) {
    	logging("btnSelecionar clicked");
    	e.preventDefault();
    	
    	var local = $("#table-local").DataTable().rows({selected:  true}).data()[0];
    	
    	if(local != undefined){
    		
    		$("#locCep").val(local.ceplogCodCep);
    		$("#locRua").val(local.ceplogLograCep);
    		$("#locComplemento").val(local.ceplogComplCep);
    		$("#locBairro").val(local.ceplogBaiIniCep);
    		$("#locMunicipio").val(local.ceplogLocalidadeCep);
    		$("#locUf").val(local.ceplogUfCep);
    		$("#locNumero").focus();
    		
    		$('#modalCadastroLoc').modal('hide');
    		
    	}else{
    		marisaMessage.showDialog(marisaMessageType_WARNING, "Atenção", "Selecione um registro na tabela!");
    	}
    	
    } );  
    
    
    
});

function loadTableLocais(locais){
	var objDataTable = $("#table-local").DataTable();
    objDataTable.destroy();	
    var objLista;

	//logging(locais);
	if(locais != undefined){
		objLista = [];
		
		locais.forEach(function(entry) {
			
		    //logging('local:');
		    //logging(entry);

		    var local = {};
		    
		    local.ceplogBaiIniCep = entry.ceplogBaiIniCep.toUpperCase();
		    local.ceplogCodCep = formatCep(entry.ceplogCodCep);
		    //local.ceplogComplCep = entry.ceplogComplCep.toUpperCase();
		    //local.ceplogEspecialCep = entry.ceplogEspecialCep.toUpperCase();
		    //local.ceplogLocCod = entry.ceplogLocCod;
		    local.ceplogLocalidadeCep = entry.ceplogLocalidadeCep.toUpperCase();
		    local.ceplogLograCep = entry.ceplogLograCep.toUpperCase();
		    //local.ceplogPtoScore = entry.ceplogPtoScore;
		    //local.ceplogTpLocalCep = entry.ceplogTpLocalCep.toUpperCase();
		    //local.ceplogTpLograCep = entry.ceplogTpLograCep.toUpperCase();
		    local.ceplogUfCep = entry.ceplogUfCep.toUpperCase();		    
		    //local.acao = "";
		    
		    objLista.push(local);
		
		})
		
	    
	}else{
		objLista = {};
	    
	    objLista.ceplogBaiIniCep = "";
	    objLista.ceplogCodCep = "";
	    objLista.ceplogComplCep = "";
	    objLista.ceplogEspecialCep = "";
	    objLista.ceplogLocCod = "";
	    objLista.ceplogLocalidadeCep = "";
	    objLista.ceplogLograCep = "";
	    objLista.ceplogPtoScore = "";
	    objLista.ceplogTpLocalCep = "";
	    objLista.ceplogTpLograCep = "";
	    objLista.ceplogUfCep = "";
	    
	}
    
	logging(objLista);
    
	if (!$.fn.DataTable.isDataTable( "#table-local" )) {
		logging("Init #table-local");
		var table = $('#table-local').dataTable( {
	        language: language,	 
	        responsive: true,
			bFilter : false,               
			bLengthChange: false,
			columns: [
				{title : "&nbsp;", 
					width: "5%",
				    render: function( data, type, full, meta ) {
				    	
				        return '';
				    }
				},
				{ title: "CEP" , data: "ceplogCodCep"},
	            { title: "Logradouro" , data: "ceplogLograCep"},
	            { title: "Bairro" , data: "ceplogBaiIniCep"},
	            { title: "Cidade", data: "ceplogLocalidadeCep" },
	            { title: "UF", data: "ceplogUfCep" }
	        ],			
			columnDefs: [ 
		        {
		            orderable: false,
		            className: 'select-checkbox',
		            targets:   0
		        }
			],
			data: objLista,
	        select: {
	        	style: 'single'
	        },
	        fnCreatedRow: function( nRow, aData, iDataIndex ) {
	        	$(nRow).children("td").css("overflow", "hidden");
	        	$(nRow).children("td").css("white-space", "nowrap");
	        	$(nRow).children("td").css("text-overflow", "ellipsis");
	        	$(nRow).addClass('RowDinamica');
	        }
		
	    } );
	}	    
	
	objDataTable = $("#table-local").DataTable();
	new $.fn.dataTable.FixedHeader( objDataTable );	
	
	$('.select-item').html('');
}



function getLocal(){
	var local = {};
	
	local.cep = $("#locCep").val().replace(/[^\d]+/g,'');
	local.rua = $("#locRua").val();
	local.complemento = $("#locComplemento").val();
	local.bairro = $("#locBairro").val();
	local.municipio = $("#locMunicipio").val();
	local.uf = $("#locUf").val();
	local.numero = $("#locNumero").val();

	return local;
}


function pesquisarLogradouro(cep) { // novo
  
 var parameters = {
   "ceplogCodCep" : cep.replace(/[^\d]+/g,"")  
 };
 //"ceplogCodCep" : cep.replace( /\D/g , "")
 var result = centralServicosCCMServicesRS.callPostService("/endereco", "consultaCep", parameters); // Buscando os dados cadastrados
 
 result = JSON.parse(result);
 
 
 if (result["codigoRetorno"] == 200) { 
  
  var objLista = result["objectsReturn"]["cep"];
  return  objLista; 
 } else {
  logging("Error");
  marisaMessage.showDialog(marisaMessageType_ERROR, "Erro","Falha ao tentar recuper o Cep!");
 }
}



function pesquisarCepLogradouro(uf, municipio, logra){
 
 var logradouro = {
   "ceplogUfCep" : uf,
   "ceplogLocalidadeCep" : municipio,
   "ceplogLograCep" : logra 
 };
 
 var parameters = {
   "CepLogradouro" : logradouro
 };
 
 var result = centralServicosCCMServicesRS.callPostService("/endereco", "getLogradouroByLocal", parameters);
 result = JSON.parse(result);
 logging(result);
 logging(result["codigoRetorno"]);
 if (result["codigoRetorno"] == 200) {
  var objLista = result["objectsReturn"]["CepLogradouro"];

  return objLista;
 } else {
  logging("CepLogradouro Error: "+result["codigoRetorno"] + " - " +  result["descricaoRetorno"] + "!");
  marisaMessage.showDialog(marisaMessageType_ERROR, "Erro", result["codigoRetorno"] + " - " +  result["descricaoRetorno"] + "!");
  return null;
 }
 
}

function popularDadosClienteDep(){
	
	$('#locPlasNumCartao').val( $('#plasNumCartao', window.parent.document).val() );
	$('#locCliNome').val( $('#cliNome', window.parent.document).val() );
	$('#locStaCredito').val( $('#staCredito', window.parent.document).val() );
	$('#locStaPlastico').val( $('#staPlastico', window.parent.document).val() );
	
}

function popularDadosSeguroDep(codServicoContratando){
	logging('load func ')
	var infosProduto = [];
	$.each(arrayProdutosDisponiveis, function(index, val) {
	
		if(val.cod === codServicoContratando ){
			infosProduto.push(val.objeto);
		}
		
	});
	var descricaoCoberturaValor = infosProduto[0].descricaoCobertura;
	var valorSeguro             = infosProduto[0].valorMinimoPremio;
	var valorCober			    = infosProduto[0].valorCobertura;

	$('#locVlrMensalSeguro').val('R$ ' + formatMoney(valorSeguro,2,',','.'));
	$('#locCoberturaDescr').val(descricaoCoberturaValor);
	$('#locCoberturaValor').val('R$ ' + formatMoney(valorCober,2,',','.'));
	
}





function popularDadosLocal(codServicoEditando){
	
	try{
		var local = listarProdutosContratados(codServicoEditando);
	}catch (e) {
		// alert("não encontrou o produto");
	}
	if(local != undefined){
		infosLocalRisco = local.localRisco;
		$("#locCep").val(formatCep(infosLocalRisco.cep));
		$("#locRua").val(infosLocalRisco.rua);
		$("#locComplemento").val(infosLocalRisco.complemento);
		$("#locBairro").val(infosLocalRisco.bairro);
		$("#locMunicipio").val(infosLocalRisco.municipio);
		$("#locUf").val(infosLocalRisco.uf);
		$("#locNumero").val(infosLocalRisco.numero);
	}

	logging('local local',local.localRisco)

	var descricaoCoberturaValor = local.servico;
	var valorSeguro             = local.valorPremio;
	var valorCober			    = local.valorCobertura;


	$('#locVlrMensalSeguro').val('R$ ' + formatMoney(valorSeguro,2,',','.'));
	$('#locCoberturaDescr').val(descricaoCoberturaValor);
	$('#locCoberturaValor').val('R$ ' + formatMoney(valorCober,2,',','.'));
	
}


$('#formLocal').validator().on('submit', function (e) {
		//logging("opa Laercio ",dataAcaoPagina);
		if (e.isDefaultPrevented()) {
			// handle the invalid form...
		} else {
			objLocalExtraido = getLocal();
			if(dataAcaoPagina == 'editar'){
				confirmarEdicaoServico();
			}
			else{
				//logging('debug!!')
				confirmaContratacaoServico();	
			}
			
			return false;//Bloqueando o submit do form
		}
	});

function confirmaCancelamentoCadastroLocalRisco(){
	if(dataAcaoPagina == 'editar'){
		setTimeout(function(){
			var msg = "Deseja realmente cancelar a edição de Local de Risco?";
			var resp = marisaMessage.questionDialog("Editar Local de Risco",
					msg, "Sim", "Não", cancelamentoCadastroLocalRiscoConfirmada, abortaPerguntaOnly);
		}, 100);

	}
	else if (tipoEdicaoAdicional == 1) {
		
		setTimeout(function(){
			var msg = "Deseja realmente cancelar a edição de Local de Risco?";
			var resp = marisaMessage.questionDialog("Editar Local de Risco",
					msg, "Sim", "Não", cancelamentoCadastroLocalRiscoConfirmada, abortaPerguntaOnly);
		}, 100);
		
	}else{
		
		setTimeout(function(){
			var msg = "Deseja realmente cancelar a edição de Local de Risco?";
			var resp = marisaMessage.questionDialog("Editar Local de Risco",
					msg, "Sim", "Não", cancelamentoCadastroLocalRiscoConfirmada, abortaPerguntaOnly, marisaMessageType_WARNING);
		}, 100);
		
	}
	return false;		
}

var cancelamentoCadastroLocalRiscoConfirmada = function(e) {
	showPainel($('#panelDashboard')); 
	clickMenu('menuDashboard');
}