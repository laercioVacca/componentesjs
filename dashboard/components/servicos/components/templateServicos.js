
function TemplateServicos() {

	if (!(this instanceof TemplateServicos)){
		return new TemplateServicos();      
    }

	this.getHtmlServicosDisponiveis = function(cardReload,ObjConsultaServicos){
		
		var flagReload = cardReload;
		var colunas; 
		var servicos = ObjConsultaServicos;
		var acoes = true;
		var retorno;
			if(flagReload != undefined ){
				retorno = '<div class="cardReload">';
			}else {
				retorno = '<div id="cardServicosDisponiveis" class="cardServicos grd_sac">';
			}
			retorno += '<div class="card menu" id="menucard.id" style="margin: 0px;border: 5px solid #fbfaf6;max-width: 100%;">';
			retorno += '<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="false">';
			retorno += '<div class="panel panel-default">';
			retorno += '<div class="panel-heading" role="tab" id="heading card.id">';
			retorno +=	'<h4 class="panel-title">';
			retorno +=	'<a class="collapsed" id="menuCollapsecard.id" role="button" style="padding-right: 5px;color: #75104c;" data-toggle="" data-parent="#accordion" aria-expanded="true" aria-controls="collapsecard.id"> ';
			retorno +=	'<span class="glyphicon glyphicon-thumbs-up" style="left: -30px;"></span>';
			retorno +=	'Serviços Disponíveis';
			retorno +=	'</a>';
			retorno +=	'</h4>';
			retorno += '</div>';
			retorno += '<div id="collapsecard.id" class="panel-collapse collapse show" style="height: 197px;" ';
			retorno += 'role="tabpanel" aria-labelledby="headingcard.id">';
			retorno += '<div class="panel-body" style="visibility: visible;padding: 5px;height: 197px;overflow: auto;">';
			retorno += '<ul class="list-group"> ';
		if(servicos != null && servicos.codigoRetorno == "200" && servicos.servicos.length > 0){
				var countServ = 0; 
				$.each(servicos.servicos, function(index, servico) {
					var servTemp = businessServicos.getObjectArrayProdutosDisponiveis(servico.cod);
						countServ = countServ + 1;
						retorno += '<li class="list-group-item row" style="padding: 0px;margin: 3px;">';
						retorno += '<div class="col-xs-7 col-sm-7 col-md-7 col-lg-7" style="padding: 0px 5px 0px 5px;font-size:12px">'+ servico.desc + '</div>';
						retorno += '<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="padding: 0px 5px 0px 5px;">'+ servico.valor + '</div>';
						if(acoes == null || acoes == undefined || acoes == "" || acoes == false || acoes == "false"){
							retorno = retorno + "";
						}else{
							retorno += '<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2" style="padding: 0px 5px 0px 5px;text-align: right;">';
							retorno += '<a  data-cod-servico="'+ servico.cod +'" class="btnContratarServico btn btn-success btn-xs" style="margin: 0px 0px 0px 0px;height: 20px;color: white;">';
							retorno += '<span class="glyphicon glyphicon-ok"></span>';
							retorno += '</a>';
							retorno += '</div>';
						}
						retorno += '</li>'; 
				});
				
				if(countServ == 0){

						retorno += '<li class="list-group-item row" style="padding: 0px;margin: 3px;">'; 
						retorno += '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding: 0px 5px 0px 5px;TEXT-ALIGN: CENTER;font-weight: bold;">ATENÇÃO: NENHUM SERVIÇO DISPONÍVEL!</div>'; 
						retorno += '</li>';
				}
		}else if(servicos != null && servicos.codigoRetorno == "401"){
		        retorno += '<li class="list-group-item row" style="padding: 0px;margin: 3px;">';
		        retorno += '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding: 0px 5px 0px 5px;TEXT-ALIGN: CENTER;font-weight: bold;">ATENÇÃO:'+ servicos.descricaoRetorno.toUpperCase() + '!</div>';
		        retorno += '</li>';
		}else{
			 retorno += '<li class="list-group-item row" style="padding: 0px;margin: 3px;">';
			 retorno += '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding: 0px 5px 0px 5px;TEXT-ALIGN: CENTER;font-weight: bold;">ATENÇÃO: NENHUM SERVIÇO DISPONÍVEL!</div>';
			 retorno += '</li>'; 
		}
			retorno += '</ul>';  
			
			retorno += '</div>';
			retorno +=	'<div class="panel-footer col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding: 2px 5px 2px 2px;background-color: #dddddd;"> ';
			retorno +=	'<a href="cobranca" class="renderPage btn btn-info btn" style="margin: 3px 3px 3px 3px;height: 20px;float: right;">';
			retorno +=	'<span class="glyphicon glyphicon-th-list"> HISTÓRICO</span> ';
			retorno +=	'</a>';
			retorno +=	'</div> ';
			retorno += '</div>';
			retorno += '</div>';  
			retorno += '</div>'; 
			retorno += '</div>';  
			retorno += '</div>';
		return retorno;
	}

	this.getHtmlServicosContratados = function(cardReload,ObjConsultaServicosContratados){
		var flagReload = cardReload;
		var colunas; 
		var servicos = ObjConsultaServicosContratados;
		var acoes = true;
		var retorno;
		if(flagReload != undefined ){
			retorno = '<div class="cardReload">';
		}else {
			retorno = '<div id="cardServicosContratados" class="cardServicos grd_sac">';
		}
		retorno += '<div class="referenciaModalcard"></div>';
		retorno += '<div class="card menu" id="menucard.id" style="margin: 0px;border: 5px solid #fbfaf6;max-width: 100%;">'; 
		retorno += '<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="false">'; 
		retorno += '<div class="panel panel-default">'; 
		retorno += '<div class="panel-heading" role="tab" id="heading servcontratados">'; 
		retorno += '<h4 class="panel-title">'; 
		retorno += '<a class="collapsed" id="menuCollapsecard.id" role="button" style="padding-right: 5px;color: #75104c;" data-toggle="" data-parent="#accordion" aria-expanded="true" aria-controls="collapsecard.id">';
		retorno += '<span class="glyphicon glyphicon-briefcase" style="left: -30px;"></span>'; 
		retorno += 'Serviços Contratados'; 
		retorno += '</a>'; 
		retorno += '</h4>'; 
		retorno += '</div>'; 
		retorno += '<div id="collapsecard.id" class="panel-collapse collapse show" style="height: 197px;" role="tabpanel" aria-labelledby="headingcard.id">'; 
		retorno += '<div class="panel-body" style="visibility: visible;padding: 5px;height: 197px;overflow: auto;">'; 
		retorno += '<ul class="list-group col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding: 0px;">';  
		if(servicos != null){
			$.each(servicos, function(index, servico) {
				retorno += '<li class="list-group-item row" style="padding: 0px;margin: 3px;">'; 
				retorno += '<div class="col-xs-7 col-sm-7 col-md-7 col-lg-7" style="padding: 0px 5px 0px 5px;">'+ servico.desc +'</div>'; 
				retorno += '<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="padding: 0px 0px 0px 0px;">'+ servico.valor +'</div>';
				if(acoes == null || acoes == undefined || acoes == "" || acoes == false || acoes == "false"){
					retorno = retorno + "";
				}
				else{
					retorno += '<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2" style="padding: 0px 0px 0px 0px;text-align: right;">';
					var servTemp = businessServicos.getObjectArrayProdutosContratados(servico.cod);
					if(servTemp.permiteIncDependente || servTemp.permiteIncLocalRisco || servTemp.pssFlgImei){ // previdencia nao permite edicao
					    retorno += '<a onclick="javascript: editarServico('+ servico.cod +')" class="btn btn-warning btn-xs" style="margin: 0px 0px 0px 0px;height: 20px;color: white;">'; 
					    retorno += '<span class="glyphicon glyphicon-pencil"></span>';
					    retorno += '</a>';
					}
					if(servTemp.permiteIncBeneficiario ){ 
						retorno += '<a onclick="javascript: editarServico('+servico.cod +')" class="btn btn-info btn-xs" style="margin: 0px 0px 0px 0px;height: 20px;color: white;">';
						retorno += '<span class="fs1" aria-hidden="true" data-icon="&#xe0f7;" style="margin-left: -3px;margin-right: -3px;"></span>'; 
						retorno += '</a>';
					}
				    retorno += '<a data-cod-servico="'+servico.cod +'" class="btnCancelarServico btn btn-danger btn-xs" style="margin: 0px 0px 0px 0px;height: 20px;color: white;">';
				    retorno += '<span class="glyphicon glyphicon-remove"></span>';
				    retorno += '</a>';
				    retorno += '</div>';
				}
			    retorno += '</li>'; 
			}); // each
		}
		else{
	        retorno += '<li class="list-group-item row" style="padding: 0px;margin: 3px;">';
	        retorno += '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding: 0px 5px 0px 5px;TEXT-ALIGN: CENTER;font-weight: bold;">ATENÇÃO: NENHUM SERVIÇO CONTRATADO!</div>';
	        retorno += '</li>'; 
		}
		retorno += '</ul>'; 
		retorno += '</div>'; 
		retorno += '</div>'; 
		retorno += '</div>';
		retorno += '</div>'; 
		retorno += '</div>'; 
		retorno += '</div>';
		return retorno;
	}

	this.getHtmlServicosCancelados = function(servicos, colunas){
		// colunas = 1 ou 2
		var retorno =  
			"" ;
		if(colunas == null || colunas == undefined || colunas == "" || colunas == 2){
			retorno = retorno +
			"		 	<div id=\"card.id\" class=\"col-xs-12 col-sm-6 col-md-6 col-lg-6\"" ;
		}else{
			retorno = retorno +
			"		 	<div id=\"card.id\" class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\"" ;
		}
		retorno = retorno +
		"" + 
		"				style=\"padding: 0px; display: inline-table;\">" + 
		"				<div class=\"card menu\" id=\"menucard.id\" style=\"margin: 0px;border: 5px solid #fbfaf6;max-width: 100%;\">" + 
		"					<div class=\"panel-group\" id=\"accordion\" role=\"tablist\"" + 
		"						aria-multiselectable=\"false\">" + 
		"						<div class=\"panel panel-default\">" + 
		"							<div class=\"panel-heading\" role=\"tab\" id=\"heading servcancelados + \"\">" + 
		"								<h4 class=\"panel-title\">" + 
		"									<a class=\"collapsed\" id=\"menuCollapsecard.id\" role=\"button\" style=\"padding-right: 5px;color: #75104c;\"" + 
		"										data-toggle=\"\" data-parent=\"#accordion\" aria-expanded=\"true\"" + 
		"										aria-controls=\"collapsecard.id\"> <span" + 
		"										class=\"glyphicon glyphicon-briefcase\" style=\"left: -30px;\"></span>" + 
		"										Serviços Cancelados" + 
		"									</a>" + 
		"								</h4>" + 
		"							</div>" + 
		"							<div id=\"collapsecard.id\" class=\"panel-collapse collapse show\" " + // style=\"height: 197px;\"
		"								role=\"tabpanel\" aria-labelledby=\"headingcard.id\">" + 
		"								<div class=\"panel-body\" style=\"visibility: visible;padding: 5px;height: 197px;overflow: auto;\">" + 
		"									" + 
		"									<ul class=\"list-group\">" ; 
		
		if(servicos != null){
			retorno = retorno +
			"" + 
			"                                     <li class=\"list-group-item row\" style=\"padding: 0px;margin: 3px;background-color: #c6cace;font-weight: bold;\">" + 
			"                                         <div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\" style=\"padding: 0px 5px 0px 5px;\">Serviço</div>" + 
			"                                         <div class=\"col-xs-2 col-sm-2 col-md-2 col-lg-2\" style=\"padding: 0px 5px 0px 0px;\">Adesão</div>" +
			"                                         <div class=\"col-xs-2 col-sm-2 col-md-2 col-lg-2\" style=\"padding: 0px 5px 0px 0px;\">Cancelamento</div>" +
			"                                         <div class=\"col-xs-2 col-sm-2 col-md-2 col-lg-2\" style=\"padding: 0px 5px 0px 0px;\">Fornecedor</div>" +
			"                                         <div class=\"col-xs-2 col-sm-2 col-md-2 col-lg-2\" style=\"padding: 0px 5px 0px 0px;\">Motivo</div>" ;
			retorno = retorno +
			"                                     </li>" + 
			"";
			
			
			$.each(servicos, function(index, servico) { // Iterando as opções
				retorno = retorno +
				"" + 
				"                                     <li class=\"list-group-item row\" style=\"padding: 0px;margin: 3px;\">" + 
				"                                         <div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\" style=\"padding: 0px 5px 0px 5px;\">"+ servico.servico +"</div>" + 
				"                                         <div class=\"col-xs-2 col-sm-2 col-md-2 col-lg-2\" style=\"padding: 0px 5px 0px 0px;\">"+ servico.dataAquisicao +"</div>" +
				"                                         <div class=\"col-xs-2 col-sm-2 col-md-2 col-lg-2\" style=\"padding: 0px 5px 0px 0px;\">"+ servico.dataCancelamento +"</div>" +
				"                                         <div class=\"col-xs-2 col-sm-2 col-md-2 col-lg-2\" style=\"padding: 0px 5px 0px 0px;\">"+ servico.fornecedor +"</div>" +
				"                                         <div class=\"col-xs-2 col-sm-2 col-md-2 col-lg-2\" style=\"padding: 0px 5px 0px 0px;\">"+ servico.motivoCancelamento +"</div>" ;
				
				
				retorno = retorno +
				"                                     </li>" + 
				"";
				
			}); // each
			
			
		}else{
			
			retorno = retorno +
			"" + 
			"                                     <li class=\"list-group-item row\" style=\"padding: 0px;margin: 3px;\">" + 
			"                                         <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\" style=\"padding: 0px 5px 0px 5px;TEXT-ALIGN: CENTER;font-weight: bold;\">ATENÇÃO: NENHUM SERVIÇO CANCELADO!</div>" + 
			"                                     </li>" + 
			"";
			
		}
		
		retorno = retorno +
		"" + 
		"								    </ul>" + 
		"								</div>" + 
		"							</div>" + 
		"						</div>" + 
		"					</div>" + 
		"				</div>" + 
		"			</div>" + 		"";
		return retorno;
	}	
}

if(templateServicos == undefined || templateServicos == null){
	var templateServicos = new TemplateServicos();
}