
function ContratarServicos() {
	if (!(this instanceof ContratarServicos)){
        return new ContratarServicos();
    }

	// Função para contratar Serviço
	this.contratarServico = function(codServico){
		globalDataAcaoPagina  = 'contratar';
		codServicoContratando = codServico;
		flagModalContrata = false;
		permiteDependentes 		= businessServicos.getObjectArrayProdutosDisponiveis(codServicoContratando).permiteIncDependente;
		permiteIncLocalRisco 	= businessServicos.getObjectArrayProdutosDisponiveis(codServicoContratando).permiteIncLocalRisco;
		permiteIncImeiCelular 	= businessServicos.getObjectArrayProdutosDisponiveis(codServicoContratando).pssFlgImei;
		permiteIncBeneficiario 	= businessServicos.getObjectArrayProdutosDisponiveis(codServicoContratando).permiteIncBeneficiario;
		permiteIncCapitalizacao = businessServicos.getObjectArrayProdutosDisponiveis(codServicoContratando).pssFlgCapitalizacao;

		if(permiteDependentes){
			renderPageComponente.render('servicos/paginas/dependentes','cadastro');
		}else if(permiteIncLocalRisco){
			renderPageComponente.render('servicos/paginas/local','cadastro');
		}else if(permiteIncImeiCelular){
			renderPageComponente.render('servicos/paginas/imei','cadastro');
		}else if(permiteIncBeneficiario){
			renderPageComponente.render('servicos/paginas/beneficiarios','cadastro');
		}else{
			this.contratacaoServicoModal(codServicoContratando)
		}
			
	}

	// refatoração
	this.infosServico = function(codServicoContratando){
		var serv = businessServicos.getObjectArrayProdutosDisponiveis(codServicoContratando);
		var msg
		msg  = 'Deseja realmente contratar o serviço abaixo?';
		msg += 'Serviço:' + serv.servico + '<br>';
		msg += 'Valor Premio:' + 'R$ ' + formatMoney( serv.valorMinimoPremio, 2, ',', '.') + '<br>';
		msg += 'Valor Cobertura:' + 'R$ ' + formatMoney( serv.valorMinimoCobertura, 2, ',', '.') + '<br>';
		msg += 'Periodicidade:'+ serv.periodicidadeCobranca;
		if(serv.pssFlgCapitalizacao ){
			msg += '<div id="PoliticamenteEspostoBox" class="form-group has-feedback">';
			msg += '<label class="control-label" style="margin-top: 0px; float:left" for="checkPoliticamenteEsposto">Politicamente exposto?</label>';
			msg += '<input type="checkbox" id="checkPoliticamenteEsposto" style="width:70px; float:left; height:20px"  placeholder=\"Politicamente exposto?\">';
			msg += '</div>';
		}
		msg += '<br>';
		msg += '<br>';
		msg += '<br>';
		msg += '<div id="groupCpfVendedor" class="form-group has-feedback">';
		msg += '<label class="control-label" style="margin-top: 0px;" for="cpfVendedor">CPF Vendedor</label><br>';
		msg += '<input type="text" id="cpfVendedor" class="form-control" placeholder="CPF Vendedor">';
		msg += '</input>';
		msg += '<div id="erroCpfVendedor" class="help-block with-errors" style="display: inline;color: red;font-size: 11px;"></div>';
		msg += '</div>';
		return msg;
	}

	this.contratacaoServicoModal = function(codServicoContratando){
		var conteudo = contratarServicos.infosServico(codServicoContratando);
		var infosClienteEmAtendimento = controllStorageSac.getinfosCliEmAtendimento();
		var vlrMiniPremio = businessServicos.getObjectArrayProdutosDisponiveis(codServicoContratando).valorMinimoPremio;
		var cliCpf = infosClienteEmAtendimento.cpf;

		var funcRealizarContratacao  = function(){return contratarServicos.efetuarContratacaoPSS(cliCpf, codServicoContratando, vlrMiniPremio)};
		var callBackRealizarContratacao = function(){return contratarServicos.callbackFuncOk()}
		var elementoRaiz = $('.referenciaModalcard');
		propsModal = {
			"elementoRaiz":elementoRaiz,
			"modalServico":true,
			"tipoModal":"modalPersonalizado",
			"titulo":"Confirmação de Dados",
			"texto":"Deseja realmente Realizar este Acordo?",
			"conteudoPersonalizado":conteudo,
			"callback":callBackRealizarContratacao,
			"funcao":funcRealizarContratacao
		}
		customModalSac.load(propsModal);
	}

	// this.contratacaoServicoConfirmada = function(e) {
	// 	// var self = this;
	// 	// setTimeout(function(){
	// 	// 	waitingDialog.show('Carregando...', {dialogSize : 'sm', progressType : 'info'});
	// 	// }, 10);

	// 	var infosClienteEmAtendimento = controllStorageSac.getinfosCliEmAtendimento();
	// 	var vlrMiniPremio = businessServicos.getObjectArrayProdutosDisponiveis(codServicoContratando).valorMinimoPremio;
	// 	var cliCpf = infosClienteEmAtendimento.cpf;
	// 	var resp = contratarServicos.efetuarContratacaoPSS(cliCpf, codServicoContratando, vlrMiniPremio);
		
	// 	if(resp){

	// 		marisaMessage.showDialog(marisaMessageType_SUCCESS, "Contratação", "Serviço contratado com sucesso!");
	// 		setTimeout(function(){
	// 			contratarServicos.callbackFuncOk();
	// 		},300)
			
	// 	}else{
	// 		marisaMessage.showDialog(marisaMessageType_WARNING, "Alerta", "Ocorreu um problema ao contratar o serviço!");
	// 		waitingDialog.hide();
	// 	}
		
	// }

	// this.confirmaContratacaoServico = function(){

	// 	var serv = businessServicos.getObjectArrayProdutosDisponiveis(codServicoContratando);
	// 	var msg
	// 	msg  = 'Deseja realmente contratar o serviço abaixo?';
	// 	msg += 'Serviço:' + serv.servico + '<br>';
	// 	msg += 'Valor Premio:' + 'R$ ' + formatMoney( serv.valorMinimoPremio, 2, ',', '.') + '<br>';
	// 	msg += 'Valor Cobertura:' + 'R$ ' + formatMoney( serv.valorMinimoCobertura, 2, ',', '.') + '<br>';
	// 	msg += 'Periodicidade:'+ serv.periodicidadeCobranca;
	// 	if(serv.pssFlgCapitalizacao ){
	// 		msg += '<div id="PoliticamenteEspostoBox" class="form-group has-feedback">';
	// 		msg += '<label class="control-label" style="margin-top: 0px; float:left" for="checkPoliticamenteEsposto">Politicamente exposto?</label>';
	// 		msg += '<input type="checkbox" id="checkPoliticamenteEsposto" style="width:70px; float:left; height:20px"  placeholder=\"Politicamente exposto?\">';
	// 		msg += '</div>';
	// 	}
	// 	msg += '<div id="groupCpfVendedor" class="form-group has-feedback">';
	// 	msg += '<label class="control-label" style="margin-top: 0px;" for="cpfVendedor">CPF Vendedor</label><br>';
	// 	msg += '<input type="text" id="cpfVendedor" class="form-control" placeholder="CPF Vendedor">';
	// 	msg += '</input>';
	// 	msg += '<div id="erroCpfVendedor" class="help-block with-errors" style="display: inline;color: red;font-size: 11px;"></div>';
	// 	msg += '</div>';

	// 	var resp = marisaMessage.questionDialog("Confirmação de Contratação de Serviço ",msg, "Sim", "Não", contratacaoServicoConfirmada, abortaPerguntaOnly);
			
		
		
		
	// 	var abortaPerguntaOnly;

	// 	return false;		
	// }









	// Função de confirmação de contratação de serviço
	// this.confirmaContratacaoServico = function(){
	// 	var serv = businessServicos.getObjectArrayProdutosDisponiveis(codServicoContratando);
	// 	console.log('SERV!!!>>',serv)
	// 	setTimeout(function(){
	// 		var msg = "Deseja realmente contratar o serviço abaixo?" +
	// 		  "<br>" +
	// 		  "<br>" +
	// 		  "Serviço: " + serv.servico + "<br>" +
	// 		  "Valor Premio: " + 'R$ ' + formatMoney( serv.valorMinimoPremio, 2, ',', '.') + "<br>" +
	// 		  "Valor Cobertura: " + 'R$ ' + formatMoney( serv.valorMinimoCobertura, 2, ',', '.') + "<br>" +
	// 		  "Periodicidade: " + serv.periodicidadeCobranca + "";

	// 			if(serv.pssFlgCapitalizacao ){
	// 			msg = msg + 
	// 			  "<br>" +
	// 			  "<br>" +
	// 			  "<div id=\"PoliticamenteEspostoBox\" class=\"form-group has-feedback\">" +
	// 			  "	    <label class=\"control-label\" style=\"margin-top: 0px; float:left\" for=\"checkPoliticamenteEsposto\">Politicamente exposto?</label>" +
	// 			  "	    <input type=\"checkbox\" id=\"checkPoliticamenteEsposto\" style=\"width:70px; float:left; height:20px\"  placeholder=\"Politicamente exposto?\"> " +
	// 			  "</div>"
				
	// 		}
	// 		msg = msg + 
	// 		  "<br>" +
	// 		  "<br>" +
	// 		  "<div id=\"groupCpfVendedor\" class=\"form-group has-feedback\">" +
	// 		  "	    <label class=\"control-label\" style=\"margin-top: 0px;\" for=\"cpfVendedor\">CPF Vendedor</label><br>" +
	// 		  "	    <input type=\"text\" id=\"cpfVendedor\" class=\"form-control\" placeholder=\"CPF Vendedor\"> " +
	// 		  "     </input>";
			  
	// 		msg = msg + 
	// 		  "		<div id=\"erroCpfVendedor\" class=\"help-block with-errors\" style=\"display: inline;color: red;font-size: 11px;\"></div>" +
	// 		  "</div>" +


			  
			  
	// 		var resp = marisaMessage.questionDialog("Confirmação de Contratação de Serviço ",
	// 						msg, "Sim", "Não", contratacaoServicoConfirmada, abortaPerguntaOnly);
			
	// 	}, 100);
		
	// 	var contratacaoServicoConfirmada = function(e) {
	// 		// var self = this;
	// 		// setTimeout(function(){
	// 		// 	waitingDialog.show('Carregando...', {dialogSize : 'sm', progressType : 'info'});
	// 		// }, 10);

	// 		var infosClienteEmAtendimento = controllStorageSac.getinfosCliEmAtendimento();
	// 		var vlrMiniPremio = businessServicos.getObjectArrayProdutosDisponiveis(codServicoContratando).valorMinimoPremio;
	// 		var cliCpf = infosClienteEmAtendimento.cpf;
	// 		var resp = contratarServicos.efetuarContratacaoPSS(cliCpf, codServicoContratando, vlrMiniPremio);
			
	// 		if(resp){
	// 			marisaMessage.showDialog(marisaMessageType_SUCCESS, "Contratação", "Serviço contratado com sucesso!");
	// 			setTimeout(function(){
	// 				contratarServicos.callbackFuncOk();
	// 			},300)
				
	// 		}else{
	// 			marisaMessage.showDialog(marisaMessageType_WARNING, "Alerta", "Ocorreu um problema ao contratar o serviço!");
	// 			waitingDialog.hide();
	// 		}
			
	// 	}
	// 	var abortaPerguntaOnly;

	// 	return false;		
	// }
	this.callbackFuncOk = function(){
		renderDashBoard.load('cardServicosContratados');
		renderDashBoard.load('cardServicosDisponiveis');
	}
	this.efetuarContratacaoPSS = function(cpfCliente, codServico, vlrMinimoPremio){
		
		var parameters = {};
		parameters.cli_cpf = cpfCliente;
		
		// parameters.usu_cpf = $('#cpfVendedor').val().replace(/[^\d]+/g,"") + "";
		parameters.usu_cpf = '90817702849';
		parameters.fil_cod = webLink.getVar("filial") + "";
		parameters.pss_prd_codigo = codServico + "";
		//parameters.pss_prd_codigo = getObjectArrayProdutosContratados($('#produtosContratados option:selected').val()).pssPrdCodigo + "";
		parameters.valor_premio = vlrMinimoPremio + "";
		parameters.data_adesao = new Date(); // formatDateTime(new Date());
		//parameters.id_contrato = getObjectArrayProdutosContratados($('#produtosContratados option:selected').val()).codigoContratoCliente + "";
		parameters.flg_processo = "R";
		parameters.flg_memofile = "true";
		parameters.flg_email = "true";
		
		if(permiteDependentes){
			parameters.valor_total = ((vlrMinimoPremio * objDependentesExtraida.length) + vlrMinimoPremio) + "";
			parameters.dependentes = objDependentesExtraida;
			if(informacoesAdicionaisCliente != undefined && informacoesAdicionaisCliente != null){
				parameters.numero_rg_cliente = informacoesAdicionaisCliente[0];
				parameters.orgao_emissor_cliente = informacoesAdicionaisCliente[1];
				parameters.estado_emissor_cliente = informacoesAdicionaisCliente[2];
			}
		}else if(permiteIncLocalRisco){
			parameters.valor_total = vlrMinimoPremio + "";
			parameters.localRisco = objLocalExtraido;
		}else if(permiteIncImeiCelular){
			parameters.imei = getCel();
			parameters.valor_total = vlrMinimoPremio + "";
		}else if(permiteIncBeneficiario){
			//parameters.valor_total = ((vlrMinimoPremio * objBeneficiariosExtraida.length) + vlrMinimoPremio) + "";
			parameters.valor_total = vlrMinimoPremio + "";
			parameters.beneficiarios = objBeneficiariosExtraida;
			if(objCadastroCliente != undefined && objCadastroCliente != null){
				parameters.numero_rg_cliente = objCadastroCliente.cliIdentidade;
				parameters.orgao_emissor_cliente = objCadastroCliente.cliIdeOrgaoEmi;
				parameters.estado_emissor_cliente = _INFO_CLIENTE.estadoEmissorCliente;
				parameters.expedicao_rg_cliente = objCadastroCliente.cliIdeDtExped;
				parameters.ddd_res_cliente = objCadastroCliente.cliResDdd;
				parameters.fone_res_cliente = objCadastroCliente.cliResFone;
				parameters.salario_cliente = objCadastroCliente.cliEmpSalario;
				parameters.email_cliente = objCadastroCliente.cliEmail;
			}
			if(objInfoAdicionais != undefined && objInfoAdicionais != null){
				parameters.cod_ocupacao = objInfoAdicionais.profissao;
				parameters.cod_pais = objInfoAdicionais.endOutroPais;
				parameters.flg_pais = objInfoAdicionais.visitaPais;
				parameters.idade_saida = objInfoAdicionais.idadeSaida;
			}
			parameters.flg_pol_exposto = $('#cadCliPolExposto').prop( "checked" ) + "";
		}else if(permiteIncCapitalizacao){
			parameters.valor_total = vlrMinimoPremio + "";
			parameters.flg_pol_exposto = $('#checkPoliticamenteEsposto').prop( "checked" ) + "";
		}else{
			parameters.valor_total = vlrMinimoPremio + "";
		}
		pssService.showDialog = true;
		var hashProcesso      =   controllStorageSac.criacaoHashUnicaProcesso("proc_");
		var result = serviceServicos.contratarProduto(hashProcesso,parameters)
		logging('LOG RESULT', result);
		pssService.showDialog = false;
		if (result["codigoRetorno"] == 200) {
			logging("PSS Success: Contratacao realizado com sucesso!");
			return true;
		} else {
			logging("PSS Error: "+result["codigoRetorno"] + " - " +  result["descricaoRetorno"] + "!");
			return false;
		}
	}

	
}




if(contratarServicos == undefined || contratarServicos == null){
	var contratarServicos = new ContratarServicos();
}
