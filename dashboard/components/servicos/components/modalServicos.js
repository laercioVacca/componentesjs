function ModalServicosSac() {
	this.load = function(propsModal){
		this.propsModal = propsModal;

		this.isServico    = this.propsModal.modalServico;//Parametro que indica que este modal retorna response de servico
		this.modalCard    = this.propsModal.modalcard;
		this.modalPage    = this.propsModal.modalpage;
		this.callbackFunc = this.propsModal.callback;

		this.idProduto    = this.propsModal.idProduto;
		console.log('this.idProduto',this.idProduto)

		this.construtorModal();
		return;
	};
	this.construtorModal = function(){
		var elementoRaiz = this.propsModal.elementoRaiz;
		
		var tituloModal  = this.propsModal.titulo;
		var textoModal   = this.propsModal.texto;
		var tipoModal    = this.propsModal.tipoModal;
		this.propsModal.callback;
		$(elementoRaiz).html("")
		var content = document.createElement('div');
		$(elementoRaiz).append(this.template(tipoModal,tituloModal,textoModal));
		this.acaoPositivaModal();
		this.acaoNegativaModal();
		$('#ModalPadraoSac').modal({
		  keyboard: false
		})
		$('#ModalPadraoSac').modal('show');

		
	}
	this.template = function(tipoModal,titulo,texto){
		var html;
			html  = '<div id="ModalPadraoSac" class="modal fade modalSAC" tabindex="-1" role="dialog">';
			html += '<div class="modal-dialog modal-dialog-centered" role="document">';
			html += '<div id="modalContent" class="modal-content">';
			html += this.templatesConteudoModal(tipoModal,titulo,texto);
			html += '</div>';
			html += '</div>';
			html += '</div>';
		return html;
	};
	this.templatesConteudoModal = function(tipoModal,titulo,texto){
		var conteudoModal;
		var self = this;
		switch (tipoModal) {
			case "modalCancelar":
				conteudoModal = conteudoModalCancelamento();
				break;
			case "modalContratar":
				conteudoModal = conteudoModalContratacao();
				break;
			default:
				conteudoModal = conteudoModalContratacao();
				break;
			}
		function conteudoModalContratacao(){
			var conteudoPersonalizado = self.propsModal.conteudoPersonalizado;
			var html;
				html  = '<div class="modal-header" id="messageHeader" style="display: block; background-color: rgba(33, 129, 212, 0.45);">';
				html += '<button type="button" id="btntesteevento" class="close" data-dismiss="modal" aria-label="Close" style="padding-top: 0px;"><span aria-hidden="true">×</span></button>';
				html += '<h4 id="messageTitle" class="modal-title">'+titulo+'</h4>';
				html += '</div>';
				html += '<div class="modal-body" style="display: inline-flex;font-size: 14px;">';
				html += conteudoPersonalizado;
				html += '</div>';
				html += '<div class="modal-footer" style="background-color: rgb(243, 243, 243);padding: 10px;">';
				html += '<button type="button" id="btnContratar" class="btn btn-primary" style="font-size: 14px;">SIM</button>';
				html += '<button type="button" id="btnNaoContratar" style="font-size: 14px;" onclick="" class="btn btn-default">Não</button>';
				html += '</div>';
			return html;
		}
		function conteudoModalCancelamento(){
			// var conteudoPersonalizado = self.propsModal.conteudoPersonalizado;

			console.log('this.idProduto >>>',self.idProduto)

			var serv = businessServicos.getObjectArrayProdutosContratados(self.idProduto);
			console.log('<<<<<<<<<<<<<<serv>>>>>>>>>>>',serv)
			arrayMotivosCancelamento = cancelarServicos.getMotivosSolicitacoesCancelamentos(self.idProduto);
			var html;
				html  = '<div class="modal-header" id="messageHeader" style="display: block; background-color: rgba(33, 129, 212, 0.45);">';
				html += '<button type="button" id="btntesteevento" class="close" data-dismiss="modal" aria-label="Close" style="padding-top: 0px;"><span aria-hidden="true">×</span></button>';
				html += '<h4 id="messageTitle" class="modal-title">'+titulo+'</h4>';
				html += '</div>';
				html += '<div class="modal-body" style="display: inline-flex;font-size: 14px;">';
				
					html  = 'Deseja realmente cancelar o contrato do serviço abaixo?';
					html += '<br>';
					html += '<br>';
					html += 'Serviço: " + serv.servico + "<br>';
					html += 'Valor Premio:'+ formatMoney( serv.valorPremio, 2, ',', '.') + '<br>';
					html += 'Valor Cobertura:' + formatMoney( serv.valorCobertura, 2, ',', '.') + '<br>';
					html += 'Aquisição:' + formatDate(serv.dataAquisicao)
					html += '<br>';
					html += '<br>';
					html += '<div class="form-inline has-feedback">';
						html += '<label class="control-label" style="margin-top: 0px;" for="motivoSolicitacaoCancelamento">Motivo Solicitação do Cancelamento</label><br>';
						html += '<select class="form-control" id="motivoSolicitacaoCancelamento" style="width: 100%;" required="required" data-error="Valor inválido!" pattern="^[123]{0,1}$">';
						html += '<option value="">Selecione...</option>';
						$.each(arrayMotivosCancelamento, function(index, objeto) { 
							html += '<option value="'+ objeto.cod+'">'+ objeto.descr +'</option>';
						});
						html += '</select>';
						html += '<div class="help-block with-errors" style="display: inline;"></div>';
					html += '</div>';
				html += '</div>';

				html += '<div class="modal-footer" style="background-color: rgb(243, 243, 243);padding: 10px;">';
				html += '<button type="button" id="btnContratar" class="btn btn-primary" style="font-size: 14px;">SIM</button>';
				html += '<button type="button" id="btnNaoContratar" style="font-size: 14px;" onclick="" class="btn btn-default">Não</button>';
				html += '</div>';
			return html;
		}
		return conteudoModal;
	}
	this.acaoPositivaModal = function(){
		var self = this;
		var disparaAcaoPositivaModal = $('#btnContratar').on('click', function(event) {
			event.preventDefault();
   			var motivoCancelamento  = $('#motivoSolicitacaoCancelamento').val();
			self.acaoModalCancelarProduto(motivoCancelamento)


			// if(self.propsModal.funcao){
			// 	if(self.isServico){
			// 		self.completarAcaoRetornoServico(respostaServico);
			// 	}
			// 	else{
			// 		self.propsModal.funcao.call();
			// 		self.completarAcao();
			// 	}
			// }
			// else {
			// 	self.retornoAcaoPositiva();
			// }
		});
		return disparaAcaoPositivaModal;
	};
	this.acaoNegativaModal = function(){
		var self = this;
		var disparaAcaoNegativaModal = $('#btnNaoContratar').on('click', function(event) {
			event.preventDefault();
			var flagFinalizar = $(this).attr('data-finalizaAcao');
			self.retornoAcaoNegativa(flagFinalizar);
		});
		return disparaAcaoNegativaModal;
	};
	this.retornoAcaoPositiva = function(){
		this.completarAcao();
		return;
	};
	this.retornoAcaoNegativa = function(flagFinalizar){
		// console.log('TESTE CALBACK FUNCAO this.callbackFunc',flagFinalizar)
		// Ultimo callback realizado no fechamento do modal
		if(flagFinalizar != undefined || flagFinalizar == "true"){
			if(this.callbackFunc != undefined){
				var self = this;
				// Função Callback chamada quando o modal acaba de ser oculto ao usuário (espera as transições CSS finalizarem).
				$('#ModalPadraoSac').on('hidden.bs.modal', function (e) {
					var testeRetorno = "retorno teste func"
				 	 self.callbackFunc.call(testeRetorno);
				 	 return testeRetorno
				})
			}
		}
		if(this.modalPage){
			$('#ModalPadraoSac').modal('hide');
			setTimeout(function(){
				var content = $('#panelDashboard');
				var componets  = '<div id="panelCardsAviso" nome-cp="card_aviso" class="stats col-xs-12 col-sm-12 col-md-12 col-lg-12"> </div>';
				componets += '<div id="panelCardsServicos"  nome-cp="card_servico" class="stats col-xs-12 col-sm-12 col-md-12 col-lg-12"></div>';
				$(content).html(componets)
				components.render();
			},200)
		}
		else {
			$('#ModalPadraoSac').modal('hide');
		}
		
		return;
	};
	this.completarAcao = function(){
		var self = this;
		var tmpLoad = '<div class="master_load" style="width:100%;padding-top:20px; text-align:center">';
		tmpLoad += '<div class="loaderPequeno"></div>';
		tmpLoad += '</div>';
		$('#messageHeader #messageTitle').html("Aguarde")
		$('#messageHeader #messageTitle').css('text-align', 'center');
		$('.modal-body').html(tmpLoad);
		$('.modal-footer').fadeTo( "slow", 0.33 );
			setTimeout(function(){
				msgsucesso = 'Solicitação Realizada com Sucesso!';
				var tmpSucesso = '<h1 style="margin-top: 4px;">';
					tmpSucesso += '<i id="dialog-icon" style="color: rgba(93, 181, 93, 0.89);" class="fa fa-check-circle col-lg-1"></i>';
					tmpSucesso += '</h1>';
					tmpSucesso += '<div>';
					tmpSucesso += '<label id="dialog-message" style="padding-top: 7px;">';
					tmpSucesso += '<div class="form-inline has-feedback">';
					tmpSucesso += '<span>'+msgsucesso+'</span>';
					tmpSucesso += '</div>';
					tmpSucesso += '</label>';
					tmpSucesso += '</div>';
				$('#messageHeader #messageTitle').html("Atenção")
				$('#messageHeader #messageTitle').css('text-align', 'center');
				$('.modal-body').html(tmpSucesso);
				$('.modal-footer #btnContratar').hide();
				
				$('.modal-footer #btnNaoContratar').text("Fechar").attr({
					title:"Fechar"
				});

				$('.modal-footer').fadeTo( "slow", 1 );
				$('#ModalPadraoSac #messageHeader').css("background-color","rgba(93, 181, 93, 0.39)");
			},800);
	}
	this.completarAcaoRetornoServico = function(respostaServico){
		console.log('completarAcao',respostaServico)
		console.log('completarAcao Laercio',respostaServico.codigoRetorno)
		var self = this;
		var tmpLoad = '<div class="master_load" style="width:100%;padding-top:20px; text-align:center">';
		tmpLoad += '<div class="loaderPequeno"></div>';
		tmpLoad += '</div>';
		$('#messageHeader #messageTitle').html("Aguarde")
		$('#messageHeader #messageTitle').css('text-align', 'center');
		$('.modal-body').html(tmpLoad);
		$('.modal-footer').fadeTo( "slow", 0.33 );
		var flagFinalizaCallBack;
		if(respostaServico.codigoRetorno != 200){
			flagFinalizaCallBack = false;
			setTimeout(function(){
				msgsucesso = respostaServico.descricaoRetorno;
				var tmpSucesso = '<h1 style="margin-top: 4px;">';
					tmpSucesso += '<i id="dialog-icon" style="color: rgba(93, 181, 93, 0.89);" class="fa fa-check-circle col-lg-1"></i>';
					tmpSucesso += '</h1>';
					tmpSucesso += '<div>';
					tmpSucesso += '<label id="dialog-message" style="padding-top: 7px;">';
					tmpSucesso += '<div class="form-inline has-feedback">';
					tmpSucesso += '<span>'+msgsucesso+'</span>';
					tmpSucesso += '</div>';
					tmpSucesso += '</label>';
					tmpSucesso += '</div>';
				$('#messageHeader #messageTitle').html("Atenção")
				$('#messageHeader #messageTitle').css('text-align', 'center');
				$('.modal-body').html(tmpSucesso);
				$('.modal-footer #btnContratar').hide();
				$('.modal-footer #btnNaoContratar').attr('data-finalizaAcao', flagFinalizaCallBack);
				$('.modal-footer #btnNaoContratar').text("Fechar").attr({
					title:"Fechar"
				});
				$('.modal-footer').fadeTo( "slow", 1 );
				$('#ModalPadraoSac #messageHeader').css("background-color","rgba(240, 173, 78, 0.5)");
	    		$("#ModalPadraoSac #dialog-icon").removeAttr('class');
	    		$('#ModalPadraoSac #dialog-icon').addClass('fa fa-exclamation-circle col-lg-1');
	    		$('#ModalPadraoSac #dialog-icon').css("color","rgb(240, 173, 78)");
			},800);
		}else if (respostaServico.codigoRetorno == 500) {
			flagFinalizaCallBack = false;
			setTimeout(function(){
				msgsucesso = respostaServico.descricaoRetorno;
				var tmpSucesso = '<h1 style="margin-top: 4px;">';
					tmpSucesso += '<i id="dialog-icon" style="color: rgba(93, 181, 93, 0.89);" class="fa fa-check-circle col-lg-1"></i>';
					tmpSucesso += '</h1>';
					tmpSucesso += '<div>';
					tmpSucesso += '<label id="dialog-message" style="padding-top: 7px;">';
					tmpSucesso += '<div class="form-inline has-feedback">';
					tmpSucesso += '<span>'+msgsucesso+'</span>';
					tmpSucesso += '</div>';
					tmpSucesso += '</label>';
					tmpSucesso += '</div>';
				$('#messageHeader #messageTitle').html("Atenção")
				$('#messageHeader #messageTitle').css('text-align', 'center');
				$('.modal-body').html(tmpSucesso);
				$('.modal-footer #btnContratar').hide();
				$('.modal-footer #btnNaoContratar').attr('data-finalizaAcao', flagFinalizaCallBack);
				$('.modal-footer #btnNaoContratar').text("Fechar").attr({
					title:"Fechar"
				});
				$('.modal-footer').fadeTo( "slow", 1 );
	    		$('#ModalPadraoSac #messageHeader').css("background-color","rgba(217, 83, 79, 0.5)");
	    		$("#ModalPadraoSac #dialog-icon").removeAttr('class');
	    		$('#ModalPadraoSac #dialog-icon').addClass('fa fa-times-circle col-lg-1');
	    		$('#ModalPadraoSac #dialog-icon').css("color","rgb(217, 83, 79)");
			},800);
		}
		else {
			flagFinalizaCallBack = true;
			setTimeout(function(){
				msgsucesso = respostaServico.descricaoRetorno;
				var tmpSucesso = '<h1 style="margin-top: 4px;">';
					tmpSucesso += '<i id="dialog-icon" style="color: rgba(93, 181, 93, 0.89);" class="fa fa-check-circle col-lg-1"></i>';
					tmpSucesso += '</h1>';
					tmpSucesso += '<div>';
					tmpSucesso += '<label id="dialog-message" style="padding-top: 7px;">';
					tmpSucesso += '<div class="form-inline has-feedback">';
					tmpSucesso += '<span>'+msgsucesso+'</span>';
					tmpSucesso += '</div>';
					tmpSucesso += '</label>';
					tmpSucesso += '</div>';
				$('#messageHeader #messageTitle').html("Atenção")
				$('#messageHeader #messageTitle').css('text-align', 'center');
				$('.modal-body').html(tmpSucesso);
				$('.modal-footer #btnContratar').hide();
				$('.modal-footer #btnNaoContratar').attr('data-finalizaAcao', flagFinalizaCallBack);
				$('.modal-footer #btnNaoContratar').text("Fechar").attr({
					title:"Fechar"
				});
				$('.modal-footer').fadeTo( "slow", 1 );
				$('#ModalPadraoSac #messageHeader').css("background-color","rgba(93, 181, 93, 0.39)");
			},800);
		}
	}
	// 


	this.acaoModalCancelarProduto = function(motivoCancelamento){
		var infosClienteEmAtendimento = controllStorageSac.getinfosCliEmAtendimento();
		var cliCpf 					  = infosClienteEmAtendimento.cpf;
		var codContraCli 			  = businessServicos.getObjectArrayProdutosContratados(this.idProduto).codigoContratoCliente
		var conteudo                  = cancelarServicos.contentInfosCancelamento(this.idProduto)
		cancelarServicos.efetuarCancelamentoPSS(cliCpf, this.idProduto,codContraCli, motivoCancelamento)
		// var callBackRealizarContratacao = function(){return contratarServicos.callbackFuncOk()}
	}
}
var modalServicosSac = new ModalServicosSac();

