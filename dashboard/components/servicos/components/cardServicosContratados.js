function CardServicosContratados() {
	this.load = function(){
		this.eventosCard();
		this.getServico();
	}
	this.getServico = function(){
		require("components/cobranca/components/serviceCobranca.js");
		var respostaConsultaCli;
		var hashProcesso      =   controllStorageSac.criacaoHashUnicaProcesso("proc_");
        var propsParcelaAcordo = {
                "cliCpf":this.infosClienteEmAtendimento.cpf,
                "dtVencimento":this.getDataAtual()
        }
		var respostaConsultaCli = serviceCobranca.consultaParcelasAcordo(hashProcesso,propsParcelaAcordo);
		return respostaConsultaCli;
	
	}
	this.getDataAtual = function(){
		var dataAtual;
		var dataHoraAtual = retornaDataHoraAtual();
		var dataAtualArr = dataHoraAtual.split(" ");
		var dataAtual    = dataAtualArr[0];
		return dataAtual;
	}
	this.getTemplate = function(reload){
		this.flagReload = reload;
		this.infosClienteEmAtendimento = controllStorageSac.getinfosCliEmAtendimento();
		if(this.infosClienteEmAtendimento.situacaoContaCod == "21"){
			var objServico = this.getServico();
			var card = this.templateCard(objServico);
		}else {
			var card = this.templateCardfalse();
		}
		return card;
	}
	this.templateCard = function (objServico) {
		var flagReload = cardReload;
		var colunas; 
		var servicos = ObjConsultaServicosContratados;
		var acoes = true;
		var retorno;
		if (this.flagReload != undefined) {
			retorno = '<div class="cardReload">';
		}else {
			retorno = '<div id="cardServicosContratados" class="cardServicos grd_sac">';
		}
		retorno += '<div class="referenciaModalcard"></div>';
		retorno += '<div class="card menu" id="menucard.id" style="margin: 0px;border: 5px solid #fbfaf6;max-width: 100%;">'; 
		retorno += '<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="false">'; 
		retorno += '<div class="panel panel-default">'; 
		retorno += '<div class="panel-heading" role="tab" id="heading servcontratados">'; 
		retorno += '<h4 class="panel-title">'; 
		retorno += '<a class="collapsed" id="menuCollapsecard.id" role="button" style="padding-right: 5px;color: #75104c;" data-toggle="" data-parent="#accordion" aria-expanded="true" aria-controls="collapsecard.id">';
		retorno += '<span class="glyphicon glyphicon-briefcase" style="left: -30px;"></span>'; 
		retorno += 'Serviços Contratados'; 
		retorno += '</a>'; 
		retorno += '</h4>'; 
		retorno += '</div>'; 
		retorno += '<div id="collapsecard.id" class="panel-collapse collapse show" style="height: 197px;" role="tabpanel" aria-labelledby="headingcard.id">'; 
		retorno += '<div class="panel-body" style="visibility: visible;padding: 5px;height: 197px;overflow: auto;">'; 
		retorno += '<ul class="list-group col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding: 0px;">';  
		if(servicos != null){
			this.listaInfosCard(servicos)
		}
		else{
	        retorno += '<li class="list-group-item row" style="padding: 0px;margin: 3px;">';
	        retorno += '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding: 0px 5px 0px 5px;TEXT-ALIGN: CENTER;font-weight: bold;">ATENÇÃO: NENHUM SERVIÇO CONTRATADO!</div>';
	        retorno += '</li>'; 
		}
		retorno += '</ul>'; 
		retorno += '</div>'; 
		retorno += '</div>'; 
		retorno += '</div>';
		retorno += '</div>'; 
		retorno += '</div>'; 
		retorno += '</div>';
		return retorno;
	}
	this.listaInfosCard = function(servicos){
		$.each(servicos, function(index, servico) {
			retorno += '<li class="list-group-item row" style="padding: 0px;margin: 3px;">'; 
			retorno += '<div class="col-xs-7 col-sm-7 col-md-7 col-lg-7" style="padding: 0px 5px 0px 5px;">'+ servico.desc +'</div>'; 
			retorno += '<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="padding: 0px 0px 0px 0px;">'+ servico.valor +'</div>';
			if(acoes == null || acoes == undefined || acoes == "" || acoes == false || acoes == "false"){
				retorno = retorno + "";
			}
			else{
				retorno += '<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2" style="padding: 0px 0px 0px 0px;text-align: right;">';
				var servTemp = businessServicos.getObjectArrayProdutosContratados(servico.cod);
				if(servTemp.permiteIncDependente || servTemp.permiteIncLocalRisco || servTemp.pssFlgImei){ // previdencia nao permite edicao
					retorno += '<a onclick="javascript: editarServico('+ servico.cod +')" class="btn btn-warning btn-xs" style="margin: 0px 0px 0px 0px;height: 20px;color: white;">'; 
					retorno += '<span class="glyphicon glyphicon-pencil"></span>';
					retorno += '</a>';
				}
				if(servTemp.permiteIncBeneficiario ){ 
					retorno += '<a onclick="javascript: editarServico('+servico.cod +')" class="btn btn-info btn-xs" style="margin: 0px 0px 0px 0px;height: 20px;color: white;">';
					retorno += '<span class="fs1" aria-hidden="true" data-icon="&#xe0f7;" style="margin-left: -3px;margin-right: -3px;"></span>'; 
					retorno += '</a>';
				}
				retorno += '<a data-cod-servico="'+servico.cod +'" class="btnCancelarServico btn btn-danger btn-xs" style="margin: 0px 0px 0px 0px;height: 20px;color: white;">';
				retorno += '<span class="glyphicon glyphicon-remove"></span>';
				retorno += '</a>';
				retorno += '</div>';
			}
			retorno += '</li>'; 
		});
	}
	this.retornoMensagem = function(txt){
		var html;
		html  = '<div class="Index_elemento_9 list-blocos-group lista_grid_02 margin_controle itemMensagemCobranca ">'; 
		html += '<div class="alt_din list-group-item list-group-item-primary" style="padding:5px;">';
		html += '<div class="alert_info_finaceira alert alert-warning alert-dismissible fade show" role="alert">';
		html += '<strong>'+txt+'</strong>';
		html += '</div>';
		html += '</div>';	
		html += '</div>';
		return html; 
	}
	this.templateCardfalse = function () {
		var textoClienteSemCobranca = 'Cliente não encontrado na fila de cobrança. '
		var html ;
		html  =	'<div id="card_08" class="cardServicos grd_sac">';
			html +=	'<div class="card menu" id="menucard.id" style="margin: 0px;border: 5px solid #fbfaf6;max-width: 100%;">';
				html +=	'<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="false">';
					html +=	'<div class="panel panel-default">';
						html +=	'<div class="panel-heading" role="tab" id="heading card.id + "">';
							html +=	'<h4 class="panel-title">';
								html +=	'<a class="collapsed" id="menuCollapsecard.id" role="button" style="padding-right: 5px;color: #75104c;" data-toggle="" data-parent="#accordion" aria-expanded="true" aria-controls="collapsecard.id"> ';
									html +=	'<span class="glyphicon glyphicon-comment" style="left: -30px;"></span>';
									html +=	'Acordos';
								html +=	'</a>';
							html +=	'</h4>';
						html +=	'</div>';
						html +=	'<div id="collapsecard.id" class="panel-collapse collapse show" style="height: 200px;" role="tabpanel" aria-labelledby="headingcard.id">';
							html +=	'<div class="panel-body" style="visibility: visible;padding: 5px;height: 170px">';
								html += '<div class="Index_elemento_9 list-blocos-group lista_grid_02 margin_controle itemMensagemCobranca ">'; 
								
									html += '<div class="alt_din list-group-item list-group-item-primary" style="padding:5px;">';
										html += '<div class="alert_info_finaceira alert alert-warning alert-dismissible fade show" role="alert">';
											html += '<strong>'+textoClienteSemCobranca+'</strong>';
										html += '</div>';
									html += '</div>';
								html += '</div>';
							html +=	'</div>';
						html +=	'</div>';
					html +=	'</div>';
				html +=	'</div>';
			html +=	'</div>';
		html +=	'</div>';
		return html;
	}
	// 
	this.eventosCard = function(){
		$('.panel-body').on("mouseenter", '.item_lista_tooltipsac', function(event) {
            event.preventDefault();
            $(this).find('.tooltipsac').show().animate({opacity:1},function(){})
        });
        $('.panel-body').on("mouseleave", '.item_lista_tooltipsac', function(event) {
            event.preventDefault();
            $(this).find('.tooltipsac').hide().animate({opacity:0},function(){})
        });
	}

}
var cardServicosContratados = new CardServicosContratados();
