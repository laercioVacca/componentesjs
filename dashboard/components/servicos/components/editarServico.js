
function EditarServicos() {
	if (!(this instanceof EditarServicos)){
        return new EditarServicos();
    }	
	this.efetuarAlteracaoPSS = function(cpfCliente, servico){
		var parameters = {};
		parameters.cli_cpf = cpfCliente;
		parameters.usu_cpf = servico.cpfVendedor + "";
		parameters.fil_cod = servico.filCodVenda + "";
		parameters.pss_prd_codigo = servico.pssPrdCodigo + "";
		parameters.valor_premio = servico.valorPremio + "";
		parameters.data_adesao = servico.dataAquisicao; 
		parameters.flg_processo = "R";
		parameters.flg_memofile = "false";
		parameters.flg_email = "false";
		if(permiteDependentes){
			parameters.valor_total = ((servico.valorPremio * objDependentesExtraida.length) + servico.valorPremio) + "";
			parameters.dependentes = objDependentesExtraida;
			if(informacoesAdicionaisCliente != undefined && informacoesAdicionaisCliente != null){
				parameters.numero_rg_cliente = informacoesAdicionaisCliente[0];
				parameters.orgao_emissor_cliente = informacoesAdicionaisCliente[1];
				parameters.estado_emissor_cliente = informacoesAdicionaisCliente[2];
			}
		}else if(permiteIncLocalRisco){
			parameters.valor_total = servico.valorPremio + "";
			parameters.localRisco = objLocalExtraido;
		}else if(permiteIncImeiCelular){
			parameters.imei = getCel();
			parameters.valor_total = servico.valorPremio + "";
		}else if(permiteIncBeneficiario){
			parameters.valor_total = ((vlrMinimoPremio * objBeneficiariosExtraida.length) + vlrMinimoPremio) + "";
			parameters.beneficiarios = objBeneficiariosExtraida;
			if(objCadastroCliente != undefined && objCadastroCliente != null){
				parameters.numero_rg_cliente = objCadastroCliente.cliIdentidade;
				parameters.orgao_emissor_cliente = objCadastroCliente.cliIdeOrgaoEmi;
				parameters.estado_emissor_cliente = _INFO_CLIENTE.estadoEmissorCliente;
				parameters.expedicao_rg_cliente = objCadastroCliente.cliIdeDtExped;
				parameters.ddd_res_cliente = objCadastroCliente.cliResDdd;
				parameters.fone_res_cliente = objCadastroCliente.cliResFone;
				parameters.salario_cliente = objCadastroCliente.cliEmpSalario;
				parameters.email_cliente = objCadastroCliente.cliEmail;
			}
			if(objInfoAdicionais != undefined && objInfoAdicionais != null){
				parameters.cod_ocupacao = objInfoAdicionais.profissao;
				parameters.cod_pais = objCadastroCliente.endOutroPais;
				parameters.flg_pais = objCadastroCliente.visitaPais;
				parameters.idade_saida = objCadastroCliente.idadeSaida;
			}
			parameters.flg_pol_exposto = $('#cadCliPolExposto').prop( "checked" ) + "";		
		}else{
			parameters.valor_total = servico.valorPremio + "";
		}
		pssService.showDialog = true;
		var result = pssService.callPostService("/produto", "contratarProduto", parameters);
		pssService.showDialog = false;
		if (result["codigoRetorno"] == 200) {
			logging("PSS Success: Contratacao realizado com sucesso!");
			return true;
		} else {
			logging("PSS Error: "+result["codigoRetorno"] + " - " +  result["descricaoRetorno"] + "!");
			return false;
		}
	}
	this.editarServico = function(codServico){
		globalDataAcaoPagina    = 'editar';
		codServicoEditando 	    = codServico;
		permiteDependentes      = getObjectArrayProdutosContratados(codServicoEditando).permiteIncDependente;
		permiteIncLocalRisco    = getObjectArrayProdutosContratados(codServicoEditando).permiteIncLocalRisco;
		permiteIncImeiCelular   = getObjectArrayProdutosContratados(codServicoEditando).pssFlgImei;
		permiteIncBeneficiario  = getObjectArrayProdutosContratados(codServicoEditando).permiteIncBeneficiario;
		if(permiteDependentes){
			showPainel($('#panelDetalhes'));
			$('#panelDetalhes').empty();
			$('#panelDetalhes').load("./dependentes/cadastro.html",codServicoEditando,function(){
				document.getElementById('card.cadastro_dependente').setAttribute('data-acao','editar');
			});
		}else if(permiteIncLocalRisco){
			showPainel($('#panelDetalhes'));
			$('#panelDetalhes').empty();
			$('#panelDetalhes').load("./local/cadastro.html?24248",codServicoEditando,function(){
				document.getElementById('card.cadastro_local_risco').setAttribute('data-acao','editar');
			});
		}else if(permiteIncImeiCelular){
			showPainel($('#panelDetalhes'));
			$('#panelDetalhes').empty();
			$('#panelDetalhes').load("./imei/cadastro.html");
		}else if(permiteIncBeneficiario){
			showPainel($('#panelDetalhes'));
			$('#panelDetalhes').empty();
			$('#panelDetalhes').load("./beneficiarios/cadastro.html",codServicoEditando,function(){
				//document.getElementById('card.cadastro_beneficiario').setAttribute('data-acao','editar');
			});
		}else{
			$('#panelDetalhes').empty();
		}	
	}
	this.confirmarEdicaoServico = function(){
		var msg = "Deseja realmente salvar as informações alteradas";
		var resp = marisaMessage.questionDialog("Confirmação de Edição",
							msg, "Sim", "Não", edicaoServicoConfirmada, abortaPerguntaOnly);
	}
	var edicaoServicoConfirmada = function edicaoServicoConfirmada(){
		logging("Callback edicaoServicoConfirmada");
		setTimeout(function(){
			waitingDialog.show('Carregando...', {dialogSize : 'sm', progressType : 'info'});
		}, 10);
		setTimeout(function(){
			var resp = efetuarAlteracaoPSS(cpf, getObjectArrayProdutosContratados(codServicoEditando));
			logging('resp do servico', resp)
			if(resp){
				//setTimeout(function(){
					waitingDialog.hide();
				//}, 100);
				setTimeout(function(){
					marisaMessage.showDialog(marisaMessageType_SUCCESS, "Alteração", "Serviço alterado com sucesso!");
					search();
				}, 500);
			}else{
				setTimeout(function(){
					marisaMessage.showDialog(marisaMessageType_WARNING, "Alerta", "Ocorreu um problema ao alterar o serviço!");
					waitingDialog.hide();
				}, 1000);
			}
		}, 300);
	}
	
}

if(editarServicos == undefined || editarServicos == null){
	var editarServicos = new EditarServicos();
}
