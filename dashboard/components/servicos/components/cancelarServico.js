function CancelarServicos() {
	if (!(this instanceof CancelarServicos)){
        return new CancelarServicos();
    }

    this.cancelarServico = function(codServico){
		// this.confirmaCancelamentoServico(codServico);
	}

	this.contentInfosCancelamento = function(codServicoCancelando){
		var msg
		var serv = businessServicos.getObjectArrayProdutosContratados(codServicoCancelando);
		msg  = 'Deseja realmente cancelar o contrato do serviço abaixo?';
		msg += '<br>';
		msg += '<br>';
		msg += 'Serviço: " + serv.servico + "<br>';
		msg += 'Valor Premio:'+ formatMoney( serv.valorPremio, 2, ',', '.') + '<br>';
		msg += 'Valor Cobertura:' + formatMoney( serv.valorCobertura, 2, ',', '.') + '<br>';
		msg += 'Aquisição:' + formatDate(serv.dataAquisicao)
		msg += '<br>';
		msg += '<br>';
		msg += '<div class="form-inline has-feedback">';
		msg += '<label class="control-label" style="margin-top: 0px;" for="motivoSolicitacaoCancelamento">Motivo Solicitação do Cancelamento</label><br>';
		msg += '<select class="form-control" id="motivoSolicitacaoCancelamento" style="width: 100%;" required="required" data-error="Valor inválido!" pattern="^[123]{0,1}$">';
		msg += '<option value="">Selecione...</option>';
		
		arrayMotivosCancelamento = cancelarServicos.getMotivosSolicitacoesCancelamentos(codServicoCancelando);

		$.each(arrayMotivosCancelamento, function(index, objeto) { 
			msg += '<option value="'+ objeto.cod+'">'+ objeto.descr +'</option>';
		});
		msg += '</select>';
		msg += '<div class="help-block with-errors" style="display: inline;"></div>';
		msg += '</div>';
		// var resp = marisaMessage.questionDialog("Confirmação de Cancelamento de Serviço",msg, "Sim", "Não", cancelamentoServicoConfirmada, abortaPerguntaOnly);
		return msg;
	}
	







	this.getMotivosSolicitacoesCancelamentos = function(codServico) {
		var parameters = {
				"pss_acd_codigo" 	: codServico + ""
		};
		var hashProcesso      =   controllStorageSac.criacaoHashUnicaProcesso("proc_");
		var result = serviceServicos.getMotivosCancelamento(hashProcesso,parameters)	
		console.log('result++++++++++',result)
		if (result["codigoRetorno"] == 200) {
			var objLista = result["objectsReturn"]["Motivos"];
			console.log('objLista ====================',objLista)
			return objLista;
		} else if (result["codigoRetorno"] != 204) {
			logging("Warning");
			marisaMessage.showDialog(marisaMessageType_WARNING, "Atenção", result["codigoRetorno"] + " - " +  result["descricaoRetorno"] + "!");
			return null;
		} else {
			logging("Error");
			marisaMessage.showDialog(marisaMessageType_ERROR, "Erro", result["codigoRetorno"] + " - " +  result["descricaoRetorno"] + "!");
			return null;
		}
	}





	this.efetuarCancelamentoPSS = function(cpfCliente, codServico, codContrato, codMotivo){
		var dadosAtendimento = controllStorageSac.getDataFlagAtendimento()
		var protocolo = dadosAtendimento.atendimentoInfos.idProtocolo
		var parameters = {
				"cli_cpf" 			: cpfCliente,
				"pss_prd_codigo" 	: codServico + "", 
				"id_contrato" 		: codContrato + "",
				"fil_cod" 			: webLink.getVar("filial") + "",
				"usu_cpf" 			: webLink.getVar("cpfUsuario") + "",
				"pss_acd_codigo" 	: codMotivo + "",
				"flg_isencao_multa" : "N",
				"flg_email"			: "true",
				"protocolo"			: protocolo + "",
				"flg_memofile"		: "true"
		};	
		pssService.showDialog = true;
		var hashProcesso      =   controllStorageSac.criacaoHashUnicaProcesso("proc_");
		var result = serviceServicos.cancelarContrato(hashProcesso,parameters)
		pssService.showDialog = false;
		if (result["codigoRetorno"] == 200) {
			logging("PSS Success: Cancelamento realizado com sucesso!");
			var txtAuto = "O SERVICO "+businessServicos.getObjectArrayProdutosContratados(codServico).servico + " FOI CANCELADO";
			lancamentosManuais.setLogAutomatico(txtAuto);
			renderDashBoard.load('cardServicosContratados');
			renderDashBoard.load('cardServicosDisponiveis');
			return true;
		} else {
			logging("PSS Error: "+result["codigoRetorno"] + " - " +  result["descricaoRetorno"] + "!");
			return false;
		}
	}



}

if(cancelarServicos == undefined || cancelarServicos == null){
	var cancelarServicos = new CancelarServicos();
}	