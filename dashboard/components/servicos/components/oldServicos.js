require("../../../../resources/js/Util.js");
require("../../../../resources/js/pssServices.js");
require("../../../../resources/js/centralServicosCCMServicesRS.js");
require("../../../../resources/js/marisaServices.js");
require("../../../../resources/js/clientServices.js");
//require("../../../../resources/js/marisaMessages.js");
//require("../../../../resources/js/loadWebLink.js");

var arrayProdutosCancelados = null;
var arrayProdutosContratados = null;
var arrayProdutosDisponiveis = null;
var permiteDependentes = null;
var permiteIncLocalRisco = null;
var permiteIncImeiCelular = null;
var permiteIncBeneficiario = null;
var codServicoContratando = null;
var codServicoCancelando = null;
var codServicoEditando = null;
var motivoCancelamento = null;
var arrayMotivosCancelamento = null;
var informacoesAdicionaisCliente = [];



var globalDataAcaoPagina = null;
//var inforCadastraoCliente = null;

logging('Esta carregando o weblink na pagina servicos.js',webLink)

function require(script) {
    $.ajax({
        url: script,
        dataType: "script",
        async: false,           // <-- This is the key
        success: function () {
            // all good...
        },
        error: function () {
            throw new Error("Could not load script " + script);
        }
    });
}

function getObjectArrayProdutosContratados(valor){
	// getObjectArrayProdutosContratados(cod_104).codigoContratoCliente + ""
	var obj =  $.grep(arrayProdutosContratados, function(e){ return e.cod == valor; });
	if(obj[0] != undefined){
		obj = obj[0].objeto;
	}else{
		obj = null;
	}
	return obj;
}

function getObjectArrayProdutosDisponiveis(valor){
	var obj =  $.grep(arrayProdutosDisponiveis, function(e){ return e.cod == valor; });
	if(obj[0] != undefined){
		obj = obj[0].objeto;
	}else{
		obj = null;
	}
	return obj;
}

function getServicosCancelados(cpf) {
	arrayProdutosCancelados = [];
	var servicosCancelados = [];

	var parameters = {
		"cli_cpf" : cpf
	};
		
	pssService.showDialog = false;
	var result = pssService.callPostService("/produto", "produtosCancelados", parameters); 
	
	if (result["codigoRetorno"] == 200) {
		var objLista = result["objectsReturn"]["listaProdutosCancelados"];

		logging("listaProdutosCancelados: ");
		logging(objLista);
		
		$.each(objLista, function(index, objeto) { // Iterando as opções
			
			//$('<option>').val(objeto.pssPrdCodigo).text(objeto.servico + ' - ' + 'R$ ' + formatMoney( objeto.valorPremio,2,',','.')).appendTo(contratados);
			var servicos = {};
			
			servicos.codigoContrato = objeto.codigoContratoCliente;
			servicos.codProduto = objeto.pssPrdCodigo;
			servicos.servico = objeto.servico;
			servicos.dataAquisicao = formatDate(objeto.dataAquisicao);
			servicos.dataCancelamento = formatDate(objeto.dataCancelamento);
			servicos.fornecedor = objeto.fornecedor;
			servicos.motivoCancelamento = objeto.motivoCancelamento;
			
			servicosCancelados.push(servicos); 
			
			var produto = {};
			produto.cod = objeto.pssPrdCodigo;
			produto.objeto = objeto;
			arrayProdutosCancelados.push(produto); 
			
		});
		
		return servicosCancelados;
	} else if (result["codigoRetorno"] != 500) {
		//logging("Warning");
		//marisaMessage.showDialog(marisaMessageType_WARNING, "Atenção", result["codigoRetorno"] + " - " +  result["descricaoRetorno"] + "!");
		return null;
	} else {			
		logging("Error");
		marisaMessage.showDialog(marisaMessageType_ERROR, "Erro", result["codigoRetorno"] + " - " +  result["descricaoRetorno"] + "!");
		return null;
	}

}

function getServicosContratados(cpf) {
	arrayProdutosContratados = [];
	servicosContratados = [];
	
	var parameters = {
			"cli_cpf" : cpf
	};
	
	pssService.showDialog = false;
	var result = pssService.callPostService("/produto", "produtosContratados", parameters); 
	
	if (result["codigoRetorno"] == 200) {
		var objLista = result["objectsReturn"]["listaProdutosContratados"];
		
		logging("listaProdutosContratados: ");
		logging(objLista);
		
	//		console.log('dadosCadastroCliente',dadosCadastroCliente);	
	//		inforCadastraoCliente = dadosCadastroCliente;
		
		$.each(objLista, function(index, objeto) { // Iterando as opções
			
			//$('<option>').val(objeto.pssPrdCodigo).text(objeto.servico + ' - ' + 'R$ ' + formatMoney( objeto.valorPremio,2,',','.')).appendTo(contratados);
			var servicos = {};
			servicos.cod = objeto.pssPrdCodigo;
			servicos.desc = objeto.servico;
			servicos.valor = 'R$ ' + formatMoney( objeto.valorPremio, 2, ',', '.');
			servicosContratados.push(servicos); 
			
			var produto = {};
			produto.cod = objeto.pssPrdCodigo;
			produto.objeto = objeto;
			arrayProdutosContratados.push(produto); 
			
		});
		
		return servicosContratados;
	} else if (result["codigoRetorno"] != 500) {
		//logging("Warning");
		//marisaMessage.showDialog(marisaMessageType_WARNING, "Atenção", result["codigoRetorno"] + " - " +  result["descricaoRetorno"] + "!");
		return null;
	} else {			
		logging("Error");
		marisaMessage.showDialog(marisaMessageType_ERROR, "Erro", result["codigoRetorno"] + " - " +  result["descricaoRetorno"] + "!");
		return null;
	}
	
}

function getServicosDisponiveis(cpf, filial) {
	arrayProdutosDisponiveis = [];
	servicosDisponiveis = {};
	
	
	var parameters = {
			"cli_cpf" : cpf,
			"fil_cod" : filial + ""
			//"cod_produto" : codProduto
		};	
	
	pssService.showDialog = false;
	var result = pssService.callPostService("/produto", "produtosDisponiveisPassiveisVenda", parameters); 
	//var result = pssService.callPostService("/produto", "produtosDisponiveis", parameters); 
	
	if (result["codigoRetorno"] == 200) {
		var objLista = result["objectsReturn"]["listProdutosDisponiveis"];
		
		servicosDisponiveis.codigoRetorno = result["codigoRetorno"];
		servicosDisponiveis.descricaoRetorno = result["descricaoRetorno"];
		servicosDisponiveis.servicos = [];
		
		logging("listProdutosDisponiveis: ");
		logging(objLista);
		
		$.each(objLista, function(index, objeto) { // Iterando as opções
			
			//$('<option>').val(objeto.pssPrdCodigo).text(objeto.servico + ' - ' + 'R$ ' + formatMoney( objeto.valorPremio,2,',','.')).appendTo(contratados);
			var servicos = {};
			servicos.cod = objeto.codigo;
			servicos.desc = objeto.servico;
			servicos.valor = 'R$ ' + formatMoney( objeto.valorMinimoPremio, 2, ',', '.');
			servicosDisponiveis.servicos.push(servicos); 
			
			var produto = {};
			produto.cod = objeto.codigo;
			produto.objeto = objeto;
			arrayProdutosDisponiveis.push(produto); 
			
		});
		
		return servicosDisponiveis;
		
	} else if (result["codigoRetorno"] == 204) {
		servicosDisponiveis.codigoRetorno = result["codigoRetorno"];
		servicosDisponiveis.descricaoRetorno = result["descricaoRetorno"];
		servicosDisponiveis.servicos = [];
		return servicosDisponiveis;
	
	} else if (result["codigoRetorno"] == 401) {
		servicosDisponiveis.codigoRetorno = result["codigoRetorno"];
		servicosDisponiveis.descricaoRetorno = result["descricaoRetorno"];
		servicosDisponiveis.servicos = [];
		return servicosDisponiveis;
		
	} else if (result["codigoRetorno"] == 500) {
		servicosDisponiveis.codigoRetorno = result["codigoRetorno"];
		servicosDisponiveis.descricaoRetorno = result["descricaoRetorno"];
		servicosDisponiveis.servicos = [];
		return servicosDisponiveis;
	} else {			
		logging("Error");
		marisaMessage.showDialog(marisaMessageType_ERROR, "Erro", result["codigoRetorno"] + " - " +  result["descricaoRetorno"] + "!");
		return null;
	}
	
}

function contratarServico(codServico){
	globalDataAcaoPagina = 'contratar';
	codServicoContratando = codServico;
	
	permiteDependentes = getObjectArrayProdutosDisponiveis(codServicoContratando).permiteIncDependente;
	permiteIncLocalRisco = getObjectArrayProdutosDisponiveis(codServicoContratando).permiteIncLocalRisco;
	permiteIncImeiCelular = getObjectArrayProdutosDisponiveis(codServicoContratando).pssFlgImei;
	permiteIncBeneficiario = getObjectArrayProdutosDisponiveis(codServicoContratando).permiteIncBeneficiario;
	permiteIncCapitalizacao  = getObjectArrayProdutosDisponiveis(codServicoContratando).pssFlgCapitalizacao;
	logging('FlagCapitalizacao',permiteIncCapitalizacao)
	if(permiteDependentes){
		
		showPainel($('#panelDetalhes'));
		$('#panelDetalhes').empty();
		$('#panelDetalhes').load("./dependentes/cadastro.html",codServicoContratando);
		
	}else if(permiteIncLocalRisco){
		
		showPainel($('#panelDetalhes'));
		$('#panelDetalhes').empty();
		$('#panelDetalhes').load("./local/cadastro.html",codServicoContratando);
		
	}else if(permiteIncImeiCelular){
		
		showPainel($('#panelDetalhes'));
		$('#panelDetalhes').empty();
		$('#panelDetalhes').load("./imei/cadastro.html");
		
	}else if(permiteIncBeneficiario){
		
		showPainel($('#panelDetalhes'));
		$('#panelDetalhes').empty();
		$('#panelDetalhes').load("./beneficiarios/cadastro.html",codServicoContratando);
		
	}else{
		confirmaContratacaoServico();
	}	
	
}

function confirmaContratacaoServico(){
	var serv = getObjectArrayProdutosDisponiveis(codServicoContratando);
	logging('debug',serv);
	setTimeout(function(){
		var msg = "Deseja realmente contratar o serviço abaixo?" +
		  "<br>" +
		  "<br>" +
		  "Serviço: " + serv.servico + "<br>" +
		  "Valor Premio: " + 'R$ ' + formatMoney( serv.valorMinimoPremio, 2, ',', '.') + "<br>" +
		  "Valor Cobertura: " + 'R$ ' + formatMoney( serv.valorMinimoCobertura, 2, ',', '.') + "<br>" +
		  "Periodicidade: " + serv.periodicidadeCobranca + "";
			if(serv.pssFlgCapitalizacao ){
			msg = msg + 
			  "<br>" +
			  "<br>" +
			  "<div id=\"PoliticamenteEspostoBox\" class=\"form-group has-feedback\">" +
			  "	    <label class=\"control-label\" style=\"margin-top: 0px; float:left\" for=\"checkPoliticamenteEsposto\">Politicamente exposto?</label>" +
			  "	    <input type=\"checkbox\" id=\"checkPoliticamenteEsposto\" style=\"width:70px; float:left; height:20px\"  placeholder=\"Politicamente exposto?\"> " +
			  "</div>"
			
		}
		msg = msg + 
		  "<br>" +
		  "<br>" +
		  "<div id=\"groupCpfVendedor\" class=\"form-group has-feedback\">" +
		  "	    <label class=\"control-label\" style=\"margin-top: 0px;\" for=\"cpfVendedor\">CPF Vendedor</label><br>" +
		  "	    <input type=\"text\" id=\"cpfVendedor\" class=\"form-control\" placeholder=\"CPF Vendedor\"> " +
		  //"         " + formatCpf(webLink.getVar("cpfUsuario")) + " " +
		  "     </input>";
		  
		msg = msg + 
		  "		<div id=\"erroCpfVendedor\" class=\"help-block with-errors\" style=\"display: inline;color: red;font-size: 11px;\"></div>" +
		  "</div>" +
		  "<script type=\"text/javascript\"> " +
		  "" +
		  "$(\"#cpfVendedor\").keyup(function(){ " +
		  "    if(validarCPF($(\"#cpfVendedor\").val().replace(/[^\\d]+/g,\"\"))){" +
		  "        $(\"#cpfVendedor\").blur();" +
		  "    }" +
	  	  "	});" +
	  	  "" +
		  "$(\"#cpfVendedor\").blur(function(){ " +
		  "		var txtCpfVendedor = $(\"#cpfVendedor\").val().replace(/[^\\d]+/g,\"\"); " +
		  "		if( txtCpfVendedor.length > 0 && validarCPF(txtCpfVendedor) ){ " +
		  "         $(\"#erroCpfVendedor\").text(\"\"); " +
		  "         $(\"#groupCpfVendedor\").removeClass(\"has-error\"); " +
		  "			txtCpfVendedor = addZeroEsquerda(txtCpfVendedor,11); " +
		  "			$('#cpfVendedor').val(txtCpfVendedor); " +
		  "			$('#cpfVendedor').unmask(); " +
		  "			$('#cpfVendedor').mask(\"999.999.999-99\"); " +
		  "			$('#modalQuestion #btnYesOk').removeAttr('disabled'); " +
		  "		} else { " +
		  "         $(\"#groupCpfVendedor\").addClass(\"has-error\"); " +
		  "         $(\"#erroCpfVendedor\").text(\"CPF Inválido!\"); " +
	//		  "			$('#cpfVendedor').val(\"\"); " +
	//		  "			$('#cpfVendedor').unmask(); " +
	//		  "			$('#cpfVendedor').mask(\"999.999.999-99\"); " +
		  "         $('#modalQuestion #btnYesOk').attr( 'disabled', 'disabled' ); " +
	  	  "		} " +
	  	  "		logging(txtCpfVendedor); " +
	  	  "	});" +
	  	  "" +
		  "$('#modalQuestion #btnYesOk').attr( 'disabled', 'disabled' ); " +
		  "$('#cpfVendedor').mask(\"999.999.999-99\");" +
		  "$('#cpfVendedor').val(\"" + formatCpf(webLink.getVar("cpfUsuario")) + "\");" +
		  "$('#cpfVendedor').blur();" +
		  "$('#cpfVendedor').focus();" +
		  "" +
		  "</script>" ;
		  
		  
		var resp = marisaMessage.questionDialog("Confirmação de Contratação de Serviço ",
						msg, "Sim", "Não", contratacaoServicoConfirmada, abortaPerguntaOnly);
		
	}, 100);
	return false;		
}

var contratacaoServicoConfirmada = function(e) {
	logging("Callback contratacaoServicoConfirmada");
	setTimeout(function(){
		waitingDialog.show('Carregando...', {dialogSize : 'sm', progressType : 'info'});
	}, 10);
	setTimeout(function(){
		
		var resp = efetuarContratacaoPSS(cpf, codServicoContratando, getObjectArrayProdutosDisponiveis(codServicoContratando).valorMinimoPremio);
		logging('resp do servico', resp);
		if(resp){

			//setTimeout(function(){
				waitingDialog.hide();
			//}, 100);
			setTimeout(function(){
				marisaMessage.showDialog(marisaMessageType_SUCCESS, "Contratação", "Serviço contratado com sucesso!");
				$('#tipoSearch').val('CPF');
				$('#cpfSearch').unmask();
				$('#cpfSearch').mask("9999.9999.9999.9999");
				$('#cpfSearch').val(cpf);
				$('#cpfSearch').blur();
				search();
			}, 500);
		}else{
			setTimeout(function(){
				marisaMessage.showDialog(marisaMessageType_WARNING, "Alerta", "Ocorreu um problema ao contratar o serviço!");
				waitingDialog.hide();
			}, 1000);
		}
		
	
	}, 300);
	
	//return false;
}

function efetuarContratacaoPSS(cpfCliente, codServico, vlrMinimoPremio){
	
	var parameters = {};
	parameters.cli_cpf = cpfCliente;
	
	parameters.usu_cpf = $('#cpfVendedor').val().replace(/[^\d]+/g,"") + "";
	
	parameters.fil_cod = webLink.getVar("filial") + "";
	parameters.pss_prd_codigo = codServico + "";
	//parameters.pss_prd_codigo = getObjectArrayProdutosContratados($('#produtosContratados option:selected').val()).pssPrdCodigo + "";
	parameters.valor_premio = vlrMinimoPremio + "";
	parameters.data_adesao = new Date(); // formatDateTime(new Date());
	//parameters.id_contrato = getObjectArrayProdutosContratados($('#produtosContratados option:selected').val()).codigoContratoCliente + "";
	parameters.flg_processo = "R";
	parameters.flg_memofile = "true";
	parameters.flg_email = "true";
	console.log('confirmando parametros politicamente',permiteIncCapitalizacao)
	if(permiteDependentes){
		parameters.valor_total = ((vlrMinimoPremio * objDependentesExtraida.length) + vlrMinimoPremio) + "";
		parameters.dependentes = objDependentesExtraida;
		if(informacoesAdicionaisCliente != undefined && informacoesAdicionaisCliente != null){
			parameters.numero_rg_cliente = informacoesAdicionaisCliente[0];
			parameters.orgao_emissor_cliente = informacoesAdicionaisCliente[1];
			parameters.estado_emissor_cliente = informacoesAdicionaisCliente[2];
		}
	}else if(permiteIncLocalRisco){
		parameters.valor_total = vlrMinimoPremio + "";
		parameters.localRisco = objLocalExtraido;
	}else if(permiteIncImeiCelular){
		parameters.imei = getCel();
		parameters.valor_total = vlrMinimoPremio + "";
	}else if(permiteIncBeneficiario){
		//parameters.valor_total = ((vlrMinimoPremio * objBeneficiariosExtraida.length) + vlrMinimoPremio) + "";
		parameters.valor_total = vlrMinimoPremio + "";
		parameters.beneficiarios = objBeneficiariosExtraida;
		if(objCadastroCliente != undefined && objCadastroCliente != null){
			parameters.numero_rg_cliente = objCadastroCliente.cliIdentidade;
			parameters.orgao_emissor_cliente = objCadastroCliente.cliIdeOrgaoEmi;
			parameters.estado_emissor_cliente = _INFO_CLIENTE.estadoEmissorCliente;
			parameters.expedicao_rg_cliente = objCadastroCliente.cliIdeDtExped;
			parameters.ddd_res_cliente = objCadastroCliente.cliResDdd;
			parameters.fone_res_cliente = objCadastroCliente.cliResFone;
			parameters.salario_cliente = objCadastroCliente.cliEmpSalario;
			parameters.email_cliente = objCadastroCliente.cliEmail;
		}
		if(objInfoAdicionais != undefined && objInfoAdicionais != null){
			parameters.cod_ocupacao = objInfoAdicionais.profissao;
			parameters.cod_pais = objInfoAdicionais.endOutroPais;
			parameters.flg_pais = objInfoAdicionais.visitaPais;
			parameters.idade_saida = objInfoAdicionais.idadeSaida;
		}
		parameters.flg_pol_exposto = $('#cadCliPolExposto').prop( "checked" ) + "";
	}else if(permiteIncCapitalizacao){
		parameters.valor_total = vlrMinimoPremio + "";
		parameters.flg_pol_exposto = $('#checkPoliticamenteEsposto').prop( "checked" ) + "";
	}else{
		parameters.valor_total = vlrMinimoPremio + "";
	}
	
	logging("Parametros Contratacao: ");
	// logging(parameters);
	logging('Paramentos Enviados',parameters)
	
	//return true;
	pssService.showDialog = true;
	var result = pssService.callPostService("/produto", "contratarProduto", parameters);
	logging('LOG RESULT', result);
	pssService.showDialog = false;
	if (result["codigoRetorno"] == 200) {
		logging("PSS Success: Contratacao realizado com sucesso!");
		//marisaMessage.showDialog(marisaMessageType_SUCCESS, "Retenção", "Cancelamento realizado com sucesso!");
		return true;
	} else {
		logging("PSS Error: "+result["codigoRetorno"] + " - " +  result["descricaoRetorno"] + "!");
		//marisaMessage.showDialog(marisaMessageType_ERROR, "Erro", result["codigoRetorno"] + " - " +  result["descricaoRetorno"] + "!");
		return false;
	}
	
}

function efetuarAlteracaoPSS(cpfCliente, servico){
	
	
	var parameters = {};
	parameters.cli_cpf = cpfCliente;
	
	parameters.usu_cpf = servico.cpfVendedor + "";
	parameters.fil_cod = servico.filCodVenda + "";
	parameters.pss_prd_codigo = servico.pssPrdCodigo + "";
	parameters.valor_premio = servico.valorPremio + "";
	parameters.data_adesao = servico.dataAquisicao; 
	parameters.flg_processo = "R";
	parameters.flg_memofile = "false";
	parameters.flg_email = "false";
	
	if(permiteDependentes){
		parameters.valor_total = ((servico.valorPremio * objDependentesExtraida.length) + servico.valorPremio) + "";
		parameters.dependentes = objDependentesExtraida;
		if(informacoesAdicionaisCliente != undefined && informacoesAdicionaisCliente != null){
			parameters.numero_rg_cliente = informacoesAdicionaisCliente[0];
			parameters.orgao_emissor_cliente = informacoesAdicionaisCliente[1];
			parameters.estado_emissor_cliente = informacoesAdicionaisCliente[2];
		}
	}else if(permiteIncLocalRisco){
		parameters.valor_total = servico.valorPremio + "";
		parameters.localRisco = objLocalExtraido;
	}else if(permiteIncImeiCelular){
		parameters.imei = getCel();
		parameters.valor_total = servico.valorPremio + "";
	}else if(permiteIncBeneficiario){
		parameters.valor_total = ((vlrMinimoPremio * objBeneficiariosExtraida.length) + vlrMinimoPremio) + "";
		parameters.beneficiarios = objBeneficiariosExtraida;
		if(objCadastroCliente != undefined && objCadastroCliente != null){
			parameters.numero_rg_cliente = objCadastroCliente.cliIdentidade;
			parameters.orgao_emissor_cliente = objCadastroCliente.cliIdeOrgaoEmi;
			parameters.estado_emissor_cliente = _INFO_CLIENTE.estadoEmissorCliente;
			parameters.expedicao_rg_cliente = objCadastroCliente.cliIdeDtExped;
			parameters.ddd_res_cliente = objCadastroCliente.cliResDdd;
			parameters.fone_res_cliente = objCadastroCliente.cliResFone;
			parameters.salario_cliente = objCadastroCliente.cliEmpSalario;
			parameters.email_cliente = objCadastroCliente.cliEmail;
		}
		if(objInfoAdicionais != undefined && objInfoAdicionais != null){
			parameters.cod_ocupacao = objInfoAdicionais.profissao;
			parameters.cod_pais = objCadastroCliente.endOutroPais;
			parameters.flg_pais = objCadastroCliente.visitaPais;
			parameters.idade_saida = objCadastroCliente.idadeSaida;
		}
		parameters.flg_pol_exposto = $('#cadCliPolExposto').prop( "checked" ) + "";		
	}else{
		parameters.valor_total = servico.valorPremio + "";
	}
	
	logging("Parametros Aletracao: ");
	logging('Paramentos Enviados!>>',parameters)
	
	pssService.showDialog = true;
	var result = pssService.callPostService("/produto", "contratarProduto", parameters);
	
	pssService.showDialog = false;
	if (result["codigoRetorno"] == 200) {
		logging("PSS Success: Contratacao realizado com sucesso!");
		return true;
	} else {
		logging("PSS Error: "+result["codigoRetorno"] + " - " +  result["descricaoRetorno"] + "!");
		return false;
	}
	
}

function cancelarServico(codServico){
	
	codServicoCancelando = codServico; 
	
	confirmaCancelamentoServico();
	
}

function editarServico(codServico){
	globalDataAcaoPagina = 'editar';
	codServicoEditando = codServico;
	
	permiteDependentes = getObjectArrayProdutosContratados(codServicoEditando).permiteIncDependente;
	permiteIncLocalRisco = getObjectArrayProdutosContratados(codServicoEditando).permiteIncLocalRisco;
	permiteIncImeiCelular = getObjectArrayProdutosContratados(codServicoEditando).pssFlgImei;
	permiteIncBeneficiario = getObjectArrayProdutosContratados(codServicoEditando).permiteIncBeneficiario;
	
	if(permiteDependentes){
		
		showPainel($('#panelDetalhes'));
		$('#panelDetalhes').empty();
		$('#panelDetalhes').load("./dependentes/cadastro.html",codServicoEditando,function(){
			document.getElementById('card.cadastro_dependente').setAttribute('data-acao','editar');
		});
		
	}else if(permiteIncLocalRisco){
		
		
		showPainel($('#panelDetalhes'));
		$('#panelDetalhes').empty();
		$('#panelDetalhes').load("./local/cadastro.html?24248",codServicoEditando,function(){
			document.getElementById('card.cadastro_local_risco').setAttribute('data-acao','editar');
		});
		
	}else if(permiteIncImeiCelular){
		
		showPainel($('#panelDetalhes'));
		$('#panelDetalhes').empty();
		$('#panelDetalhes').load("./imei/cadastro.html");
		
	}else if(permiteIncBeneficiario){
		
		showPainel($('#panelDetalhes'));
		$('#panelDetalhes').empty();
		$('#panelDetalhes').load("./beneficiarios/cadastro.html",codServicoEditando,function(){
			//document.getElementById('card.cadastro_beneficiario').setAttribute('data-acao','editar');
		});
		
	}else{
		$('#panelDetalhes').empty();
	}	
	

}


function confirmarEdicaoServico(){
	var msg = "Deseja realmente salvar as informações alteradas";
	var resp = marisaMessage.questionDialog("Confirmação de Edição",
						msg, "Sim", "Não", edicaoServicoConfirmada, abortaPerguntaOnly);
}

var edicaoServicoConfirmada = function edicaoServicoConfirmada(){
	
	logging("Callback edicaoServicoConfirmada");
	setTimeout(function(){
		waitingDialog.show('Carregando...', {dialogSize : 'sm', progressType : 'info'});
	}, 10);
	setTimeout(function(){
		
		var resp = efetuarAlteracaoPSS(cpf, getObjectArrayProdutosContratados(codServicoEditando));
		logging('resp do servico', resp)
		if(resp){

			//setTimeout(function(){
				waitingDialog.hide();
			//}, 100);
			setTimeout(function(){
				marisaMessage.showDialog(marisaMessageType_SUCCESS, "Alteração", "Serviço alterado com sucesso!");
				search();
			}, 500);
		}else{
			setTimeout(function(){
				marisaMessage.showDialog(marisaMessageType_WARNING, "Alerta", "Ocorreu um problema ao alterar o serviço!");
				waitingDialog.hide();
			}, 1000);
		}
		
	
	}, 300);
	
}

function confirmaCancelamentoServico(){
	var serv = getObjectArrayProdutosContratados(codServicoCancelando);
	setTimeout(function(){
		var msg = "Deseja realmente cancelar o contrato do serviço abaixo?" +
		  "<br>" +
		  "<br>" +
		  "Serviço: " + serv.servico + "<br>" +
		  "Valor Premio: " + 'R$ ' + formatMoney( serv.valorPremio, 2, ',', '.') + "<br>" +
		  "Valor Cobertura: " + 'R$ ' + formatMoney( serv.valorCobertura, 2, ',', '.') + "<br>" +
		  "Aquisição: " + formatDate(serv.dataAquisicao) + "";
		
		msg = msg + 
		  "<br>" +
		  "<br>" +
		  "<div class=\"form-inline has-feedback\">" +
		  "	<label class=\"control-label\" style=\"margin-top: 0px;\" for=\"motivoSolicitacaoCancelamento\">Motivo Solicitação do Cancelamento</label><br>" +
		  "		<select class=\"form-control\" id=\"motivoSolicitacaoCancelamento\" style=\"width: 100%;\" required=\"required\" data-error=\"Valor inválido!\" pattern=\"^[123]{0,1}$\"> " +
		  "			<option value=\"\">Selecione...</option> " ;
		
		arrayMotivosCancelamento = getMotivosSolicitacoesCancelamentos(codServicoCancelando);
		$.each(arrayMotivosCancelamento, function(index, objeto) { 
			msg = msg + 
			  "			<option value=\""+ objeto.cod +"\">"+ objeto.descr +"</option> " ;
		});
		  
		msg = msg + 
		  "		</select>" +
			  "		<div class=\"help-block with-errors\" style=\"display: inline;\"></div>" +
			  "</div>" + 
	//		  "<script type=\"text/javascript\">" +
	//		  "		$(\"#motivoSolicitacaoCancelamento\").on('change', function (e) {" +
	//		  "			e.preventDefault();" +
	//		  "			if($(this).val().length > 0){" +
	//		  "				motivoCancelamento = $(this).val();" +
	//		  "			}" +
	//		  "		});" +
	//		  "</script>" +
		  "";

		var resp = marisaMessage.questionDialog("Confirmação de Cancelamento de Serviço",
						msg, "Sim", "Não", cancelamentoServicoConfirmada, abortaPerguntaOnly);
		
	}, 100);
	return false;		
}

function getMotivosSolicitacoesCancelamentos(codServico) {
	
	
	var parameters = {
			"pss_acd_codigo" 	: codServico + ""
	};	
	
	var result = clientService.callPostService("/dashboard", "getMotivosCancelamento", parameters); 
	if (result["codigoRetorno"] == 200) {
		var objLista = result["objectsReturn"]["Motivos"];

		logging("Motivos: ");
		logging(objLista);
		
		return objLista;
	} else if (result["codigoRetorno"] != 204) {
		logging("Warning");
		marisaMessage.showDialog(marisaMessageType_WARNING, "Atenção", result["codigoRetorno"] + " - " +  result["descricaoRetorno"] + "!");
		return null;
	} else {
		logging("Error");
		marisaMessage.showDialog(marisaMessageType_ERROR, "Erro", result["codigoRetorno"] + " - " +  result["descricaoRetorno"] + "!");
		return null;
	}

}

var cancelamentoServicoConfirmada = function(e) {
	logging("Callback cancelamentoServicoConfirmada");
	
	motivoCancelamento = $('#motivoSolicitacaoCancelamento').val();
	
	if(motivoCancelamento == null || motivoCancelamento == ""){
		marisaMessage.showDialog(marisaMessageType_WARNING, "Atenção", "Para cancelamento de serviços é necessário informar o motivo da solicitação!<br>Favor tentar novamente.");
	}else{
	
		setTimeout(function(){
			waitingDialog.show('Carregando...', {dialogSize : 'sm', progressType : 'info'});
		}, 10);
		setTimeout(function(){
			var resp = efetuarCancelamentoPSS(cpf, codServicoCancelando, getObjectArrayProdutosContratados(codServicoCancelando).codigoContratoCliente, motivoCancelamento);
			
			if(resp){
				waitingDialog.hide();
				setTimeout(function(){
					marisaMessage.showDialog(marisaMessageType_SUCCESS, "Cancelamento", "Serviço cancelado com sucesso!");
					$('#tipoSearch').val('CPF');
					$('#cpfSearch').unmask();
					$('#cpfSearch').mask("9999.9999.9999.9999");
					$('#cpfSearch').val(cpf);
					$('#cpfSearch').blur();
					search();
				}, 1000);
			}else{
				setTimeout(function(){
					marisaMessage.showDialog(marisaMessageType_WARNING, "Alerta", "Ocorreu um problema ao cancelar o serviço!");
					waitingDialog.hide();
				}, 1000);
			}
		
		}, 300);
	}
	
	//return false;
}

function efetuarCancelamentoPSS(cpfCliente, codServico, codContrato, codMotivo){
	
	//protocolo = "22529066";
	protocolo = getNumProtocolo(cpfCliente, webLink.getVar("cpfUsuario"), true);
	
	var parameters = {
			"cli_cpf" 			: cpfCliente,
			"pss_prd_codigo" 	: codServico + "", 
			"id_contrato" 		: codContrato + "",
			"fil_cod" 			: webLink.getVar("filial") + "",
			"usu_cpf" 			: webLink.getVar("cpfUsuario") + "",
			"pss_acd_codigo" 	: codMotivo + "",
			"flg_isencao_multa" : "N",
			"flg_email"			: "true",
			"protocolo"			: protocolo + "",
			"flg_memofile"		: "true"
	};	
	pssService.showDialog = true;
	
	logging("Parametros Cancelamento: ");
	logging(parameters);
	
	var result = pssService.callPostService("/produto", "cancelarContrato", parameters);
	pssService.showDialog = false;
	if (result["codigoRetorno"] == 200) {
		logging("PSS Success: Cancelamento realizado com sucesso!");
		//marisaMessage.showDialog(marisaMessageType_SUCCESS, "Retenção", "Cancelamento realizado com sucesso!");
		
		addHistoricoProtocolo(protocolo, cpfCliente, webLink.getVar("cpfUsuario"), "O SERVICO " + getObjectArrayProdutosContratados(codServico).servico + " FOI CANCELADO");
		encerrarProtocolo(protocolo, cpfCliente, webLink.getVar("cpfUsuario"));
		
		return true;
	} else {
		logging("PSS Error: "+result["codigoRetorno"] + " - " +  result["descricaoRetorno"] + "!");
		//marisaMessage.showDialog(marisaMessageType_ERROR, "Erro", result["codigoRetorno"] + " - " +  result["descricaoRetorno"] + "!");
		return false;
	}
	
}


