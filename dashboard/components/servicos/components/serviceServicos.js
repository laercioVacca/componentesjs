
/*
 * Classe utilizada para implementacao das funcoes de chamadas a servicos
 */
function ServiceServicos() {
    if (!(this instanceof ServiceServicos)){
        return new ServiceServicos();
    }
	this.consultaServicosDisponiveis = function(idProcesso,parameters){
    	// console.log('consultaServicosDisponiveis parameters',parameters)
    	this.servicosDisponiveis    = new GenericService('pssservices');
		this.getServicosDisponiveis = this.servicosDisponiveis.callPostService("/produto", "produtosDisponiveisPassiveisVenda",idProcesso, parameters);
		return this.getServicosDisponiveis;
    }
    this.consultaServicosContratados = function(idProcesso,parameters){
    	// console.log('consultaServicosDisponiveis parameters',parameters)
    	this.servicosContratados    = new GenericService('pssservices');
		this.getServicosContratados = this.servicosContratados.callPostService("/produto", "produtosContratados",idProcesso, parameters);
		return this.getServicosContratados;
    }
	
	this.contratarProduto = function(idProcesso,parameters){
    	console.log('consultaServicosDisponiveis parameters',parameters)
    	this.servicosContratarProduto    = new GenericService('pssservices');
		this.getServicosContratarProduto = this.servicosContratarProduto.callPostService("/produto", "contratarProduto",idProcesso, parameters);
		return this.getServicosContratarProduto;
    }

    this.cancelarContrato = function(idProcesso,parameters){
    	console.log('cancelarContrato parameters',parameters)
    	this.servicosCancelarContrato    = new GenericService('pssservices');
		this.getServicosCancelarContrato = this.servicosCancelarContrato.callPostService("/produto", "cancelarContrato",idProcesso, parameters);
		return this.getServicosCancelarContrato;
    }
	
    this.getMotivosCancelamento = function(idProcesso,parameters){
    	console.log('cancelarContrato parameters',parameters)
    	this.servicosMotivosCancelamento    = new GenericService('psfclientservices');
		this.getMotivosCancelamento = this.servicosMotivosCancelamento.callPostService("/dashboard", "getMotivosCancelamento",idProcesso, parameters);
		return this.getMotivosCancelamento;
    }	


	
	
	
	
	
	
	
}


if(serviceServicos == undefined || serviceServicos == null){
	var serviceServicos = new ServiceServicos();
}



