
/*
 * Classe utilizada para implementação de funcoes de negocio
 */
function BusinessServicos() {
    
	if (!(this instanceof BusinessServicos)){
        return new BusinessServicos();
    }	

	this.load = function(objVars){
		this.cliCpf = objVars.cpf;
		this.filial = objVars.currentFilial;
		this.objServicosDisponiveis = this.getServicosDisponiveis();
		this.objServicosContratados = this.getServicosContratados();
	};
	this.getObjectArrayProdutosDisponiveis = function(valor){
		var obj =  $.grep(arrayProdutosDisponiveis, function(e){ return e.cod == valor; });
		if(obj[0] != undefined){
			obj = obj[0].objeto;
		}else{
			obj = null;
		}
		return obj;
	}
	this.getObjectArrayProdutosContratados = function(valor){
		var servicosContratadosGrupo = this.objServicosContratados;
		var obj =  $.grep(arrayProdutosContratados, function(e){ return e.cod == valor; });
		console.log('LAERCIO 12/06/2019 !!!!!',obj)
		if(obj[0] != undefined){
			obj = obj[0].objeto;
		}else{
			obj = null;
		}
		return obj;
	}

	// 
	this.loadCardServicosDisponiveis = function(cardReload){
		return templateServicos.getHtmlServicosDisponiveis(cardReload,this.getServicosDisponiveis());
	}
	this.loadCardServicosContratados = function(cardReload){
		return templateServicos.getHtmlServicosContratados(cardReload,this.getServicosContratados());
	}
	this.loadEventosCard = function(){
		eventServicos.load();
	}
	this.getServicosCancelados = function(cpf) {
		arrayProdutosCancelados = [];
		var servicosCancelados = [];
		var parameters = {
			"cli_cpf" : cpf
		};
		pssService.showDialog = false;
		var result = pssService.callPostService("/produto", "produtosCancelados", parameters); 
		if (result["codigoRetorno"] == 200) {
			var objLista = result["objectsReturn"]["listaProdutosCancelados"];
			logging("listaProdutosCancelados: ");
			logging(objLista);
			$.each(objLista, function(index, objeto) { // Iterando as opções
				var servicos = {};
				servicos.codigoContrato = objeto.codigoContratoCliente;
				servicos.codProduto = objeto.pssPrdCodigo;
				servicos.servico = objeto.servico;
				servicos.dataAquisicao = formatDate(objeto.dataAquisicao);
				servicos.dataCancelamento = formatDate(objeto.dataCancelamento);
				servicos.fornecedor = objeto.fornecedor;
				servicos.motivoCancelamento = objeto.motivoCancelamento;
				servicosCancelados.push(servicos); 
				var produto = {};
				produto.cod = objeto.pssPrdCodigo;
				produto.objeto = objeto;
				arrayProdutosCancelados.push(produto); 
			});
			return servicosCancelados;
		} else if (result["codigoRetorno"] != 500) {
			return null;
		} else {			
			logging("Error");
			marisaMessage.showDialog(marisaMessageType_ERROR, "Erro", result["codigoRetorno"] + " - " +  result["descricaoRetorno"] + "!");
			return null;
		}
	}
	this.getServicosDisponiveis = function() {
		var hashProcesso      =   controllStorageSac.criacaoHashUnicaProcesso("proc_");
		arrayProdutosDisponiveis = [];
		servicosDisponiveis = {};
		var parameters = {
				"cli_cpf" : this.cliCpf,
				"fil_cod" : this.filial + ""
				//"cod_produto" : codProduto
			};	
		var result = serviceServicos.consultaServicosDisponiveis(hashProcesso,parameters);
		if (result["codigoRetorno"] == 200) {
			var objLista = result["objectsReturn"]["listProdutosDisponiveis"];
			servicosDisponiveis.codigoRetorno = result["codigoRetorno"];
			servicosDisponiveis.descricaoRetorno = result["descricaoRetorno"];
			servicosDisponiveis.servicos = [];
			$.each(objLista, function(index, objeto) { // Iterando as opções
				var servicos = {};
				servicos.cod = objeto.codigo;
				servicos.desc = objeto.servico;
				servicos.valor = formatMoney( objeto.valorMinimoPremio, 2, ',', '.');
				servicosDisponiveis.servicos.push(servicos); 
				var produto = {};
				produto.cod = objeto.codigo;
				produto.objeto = objeto;
				arrayProdutosDisponiveis.push(produto); 
			});
			return servicosDisponiveis;
		} else if (result["codigoRetorno"] == 204) {
			servicosDisponiveis.codigoRetorno = result["codigoRetorno"];
			servicosDisponiveis.descricaoRetorno = result["descricaoRetorno"];
			servicosDisponiveis.servicos = [];
			return servicosDisponiveis;
		} else if (result["codigoRetorno"] == 401) {
			servicosDisponiveis.codigoRetorno = result["codigoRetorno"];
			servicosDisponiveis.descricaoRetorno = result["descricaoRetorno"];
			servicosDisponiveis.servicos = [];
			return servicosDisponiveis;
		} else if (result["codigoRetorno"] == 500) {
			servicosDisponiveis.codigoRetorno = result["codigoRetorno"];
			servicosDisponiveis.descricaoRetorno = result["descricaoRetorno"];
			servicosDisponiveis.servicos = [];
			return servicosDisponiveis;
		} else {			
			marisaMessage.showDialog(marisaMessageType_ERROR, "Erro", result["codigoRetorno"] + " - " +  result["descricaoRetorno"] + "!");
			return null;
		}
	}
	this.getServicosContratados = function() {
		var hashProcesso      =   controllStorageSac.criacaoHashUnicaProcesso("proc_");
		arrayProdutosContratados = [];
		servicosContratados = [];
		var parameters = {
				"cli_cpf" : this.cliCpf
		};
		var result = serviceServicos.consultaServicosContratados(hashProcesso,parameters);
		if (result["codigoRetorno"] == 200) {
			var objLista = result["objectsReturn"]["listaProdutosContratados"];
			$.each(objLista, function(index, objeto) { // Iterando as opções
				var servicos = {};
				servicos.cod = objeto.pssPrdCodigo;
				servicos.desc = objeto.servico;
				servicos.valor = 'R$ ' + formatMoney( objeto.valorPremio, 2, ',', '.');
				servicosContratados.push(servicos); 
				var produto = {};
				produto.cod = objeto.pssPrdCodigo;
				produto.objeto = objeto;
				arrayProdutosContratados.push(produto); 
			});
			
			return servicosContratados;
		} else if (result["codigoRetorno"] != 500) {
			//logging("Warning");
			//marisaMessage.showDialog(marisaMessageType_WARNING, "Atenção", result["codigoRetorno"] + " - " +  result["descricaoRetorno"] + "!");
			return null;
		} else {			
			logging("Error");
			marisaMessage.showDialog(marisaMessageType_ERROR, "Erro", result["codigoRetorno"] + " - " +  result["descricaoRetorno"] + "!");
			return null;
		}
	}
	
	
}

if(businessServicos == undefined || businessServicos == null){
	var businessServicos = new BusinessServicos();
}
