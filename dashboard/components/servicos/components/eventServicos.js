
/*
 * CLasse utilizada para implementacao dos eventos de tela e forms
 */
function EventServicos() {
    if (!(this instanceof EventServicos)){
        return new EventServicos();
    }
	
	this.load = function(){
		
		this.eventosComponent();
		
	};	

	this.eventosComponent = function(){
		console.log('EVENTOS COMPONENTSESSS')

		var $btnContratarServico = $('.btnContratarServico');
		var $btnCancelarServico  = $('.btnCancelarServico');

		$("#cpfVendedor").keyup(function(){
			if(validarCPF($("#cpfVendedor").val().replace(/[^d]+/g,""))){
				$("#cpfVendedor").blur();
			}
		});

		$("#cpfVendedor").blur(function(){
			var txtCpfVendedor = $("#cpfVendedor").val().replace(/[^d]+/g,"");
			if( txtCpfVendedor.length > 0 && validarCPF(txtCpfVendedor) ){
				$("#erroCpfVendedor").text("");
				$("#groupCpfVendedor").removeClass("has-error");
				txtCpfVendedor = addZeroEsquerda(txtCpfVendedor,11);
				$('#cpfVendedor').val(txtCpfVendedor);
				$('#cpfVendedor').unmask();
				$('#cpfVendedor').mask("999.999.999-99");
				$('#modalQuestion #btnYesOk').removeAttr('disabled');
			} 
			else {
				$("#groupCpfVendedor").addClass("has-error");
				$("#erroCpfVendedor").text("CPF Inválido!");
				$('#modalQuestion #btnYesOk').attr( 'disabled', 'disabled' );
			}
		});

		$('#modalQuestion #btnYesOk').attr( 'disabled', 'disabled' );
		$('#cpfVendedor').mask("999.999.999-99");
		$('#cpfVendedor').val("" + formatCpf(webLink.getVar("cpfUsuario")) + "");
		$('#cpfVendedor').blur();
		$('#cpfVendedor').focus();

		$btnContratarServico.on('click', function(event) {
			event.preventDefault();
			var codigoServicoContratado = $(this).attr('data-cod-servico');
			console.log('codigoServicoContratado ==>',codigoServicoContratado)
			contratarServicos.contratarServico(codigoServicoContratado);
			




		});

		$btnCancelarServico.on('click', function(event) {
			event.preventDefault();
			var codigoServicoContratado = $(this).attr('data-cod-servico');
			console.log('codigoServicoContratado ==>',codigoServicoContratado)
			// motivoCancelamento = $('#motivoSolicitacaoCancelamento').val();
			var elementoRaiz = $('.referenciaModalcard');
			propsModal = {
				"elementoRaiz":elementoRaiz,
				"tipoModal":"modalCancelar",
				"idProduto":codigoServicoContratado,
			}
			modalServicosSac.load(propsModal);

			// cancelarServicos.cancelarServico(codigoServicoContratado);
		});
	    
	}; // fim do this.eventosComponent();
	
	

}

if(eventServicos == undefined || eventServicos == null){
	var eventServicos = new EventServicos();
}

