function DeclaracaoQuitacaoDebito() {
	this.load = function(container) {
		console.log('DeclaracaoQuitacaoDebito load ==========>!!',container)
		this.container = container;
		this.montarComponente()
		this.eventos()
	}
	this.montarComponente = function(){
		this.container.html(this.templateComponente())
	}
	this.serviceDeclaraQuitacaoDebito = function(hashProcesso,parameters){
		var getServico  	= new GenericServiceV2('financialservices');
		var respostaServico = getServico.callPostService("v2/infofinanceiras", "getInformacoesFinanceiras", hashProcesso, parameters)
		return respostaServico;
	}
	// deletar
	this.mockObj = function(){
		result = {
	    	"codigoRetorno": 200,
	    	"descricaoRetorno": "Declaração enviada com exito",
	    		"objectsReturn": {
			        "ClientDashboard": {
			        	"teste":"teste01"
			        }
	    		}
			}

	    return result;
	}
	// 
	this.loadServiceQuitaDebito = function(){
		var infosClienteEmAtendimento = utilSac.getInfosCurrentCliente(controllStorageSac.getCurrentCliCpf());
		var hashProcesso      =   controllStorageSac.criacaoHashUnicaProcesso("proc_");
        var propsServico = {
                "cliCpf":infosClienteEmAtendimento.cpf,
        }
		var respServicoQuitaDebito = this.serviceDeclaraQuitacaoDebito(hashProcesso,propsServico);
		return respServicoQuitaDebito;
	}
	this.templateComponente = function(){
		var html;
		html  = '<div id="tab_conta" class="left_box_grid" style="display: block">';
		html += '<ul class="list-group">';
		html += '<li class="list-group-item list-group-item-info">Declaração de Quitação de Debito</li>';
		html += '</ul>';
		html += '<div id="quitaDebitoInfos" class="conteudo_dinamico_tabs">';

		html += '<div class="alt_din list-group-item list-group-item-primary">';
		html += '<p style="font-size:15px">Envio da Declaração de Quitação de Debito</p>';
		html += '<div id="retornoServicoQuitaDebito"></div>';

		html += '<div class="panel-footer col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding: 2px 5px 2px 2px;">'; 
		html += '<a id="btnEnviarDeclaracao" href="#" class="sidebar_link btn btn-success btn" style="margin: 3px 3px 3px 3px;height: 20px;float: right;">';
		html += '<span class="glyphicon glyphicon-ok"> ENVIAR</span>'; 
		html += '</a>';
		html += '</div>';
		html += '<div class="lista_grupo_clear"></div>';
		html += '</div>';
		html += '</div>';
		html += '</div>';
		return html;
	}
	this.enviarDeclaracao = function(){
		var retServQuitaDebito = this.mockObj();
		var textoRetorno;
		var boxInfosDeclaraDebito = $('#retornoServicoQuitaDebito');
		if(retServQuitaDebito.codigoRetorno != 200){
			textoRetorno = retServQuitaDebito.descricaoRetorno
		}else{
			textoRetorno = retServQuitaDebito.descricaoRetorno
		}
		
		loadGif(boxInfosDeclaraDebito);
		var strResposta = '<span style="padding:15px 0 15px 0; display:block">'+textoRetorno+'</span>';
		setTimeout(function(){
			boxInfosDeclaraDebito.html(strResposta)
			$("#btnEnviarDeclaracao").hide();
		},300)
	
	}
	this.eventos = function(){
		var btnEnviarDeclaracao = $('#btnEnviarDeclaracao');
		var self  = this;
		btnEnviarDeclaracao.on('click', function(event) {
			event.preventDefault();
			self.enviarDeclaracao()
			console.log('enviar Declaracao')
			
		});
	}
	
}

var declaracaoQuitacaoDebito = new DeclaracaoQuitacaoDebito()