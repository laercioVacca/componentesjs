/**
 * Escripts Laercio Vacca Filho
 * Dia 14/05/2018
 */
 // console.log('LOAD MODULO CONTA',obj_Conta)


tratarValor = new TratarValor();
var $containerLista = document.getElementById('conteudo_dinamico_conta');



if(JSON.stringify(obj_Conta) != '[]')
{
	var removeDia = true;
	var dataConta 		    = new TrataData(obj_Conta.dtAbertura,removeDia);
	var dataUltimaMudVenc   = new TrataData(obj_Conta.diavctoUltalt);
	var dataValidade        = new TrataData(obj_Conta.dtValidFin,removeDia);
	var dataLimiteCompra    = new TrataData(obj_Conta.dtAlterLimite);
	var dataVendaDireta     = new TrataData(obj_Conta.dtAlterSaque);
	var dataUltimaTransMone = new TrataData(obj_Conta.dtUltTrans);
	var dataUltimaMudStatus = new TrataData(obj_Conta.statDtAnt,removeDia);
	var dataUltimatransPag  = new TrataData(obj_Conta.fncDtAltLimite)

	var limiteCompra = tratarValor.converter(obj_Conta.vrLimiteCompra);
	var limiteCompraAnt = tratarValor.converter(obj_Conta.limiteCprAnt);
	var limiteSaque = tratarValor.converter(obj_Conta.vrLimiteSaqueFacil);
	var limiteSaqueDisponivel = tratarValor.converter(obj_Conta.vrDisponivelSaqueFacil);
	var limiteCelular  = tratarValor.converter(obj_Conta.limiteTotalCelular);
	var limiteCompraDisponivel = tratarValor.converter(obj_Conta.valorLimiteDisp);
	var limiteCelularDisponivel = tratarValor.converter(obj_Conta.limiteDisponivelCelular);

	function tratarAcao(){
		var acaoTxt;
		if(obj_header_infos.StatCodDescr == ""){
			acaoTxt = "----";
		}
		else{
			
			acaoTxt = obj_header_infos.StatCodDescr;
		}
		return acaoTxt;
	}

	OBJ_CONTA = {
		"Possui Adicionais":obj_header_infos.possuiAdicionais,
		"Ação":tratarAcao(),
		"Abertura" : dataConta.retiraAno(),
		"Nº última fatura":obj_Conta.numFatura,
		"Última mudança dia vencimento":dataUltimaMudVenc.retiraAno(),
		"Última mudança status":dataUltimaMudStatus.retiraAno(),
		"Status da conta":obj_Conta.statCodDesc,
		"Limite compra": limiteCompra,
		"Limite compra anterior":limiteCompraAnt,
		"Limite Saque":limiteSaque,
		"Limite saque disponível":limiteSaqueDisponivel,
		"Limite Celular":limiteCelular,
		"Qtdade cartões ativos":obj_Conta.cartoesAtivos,
		"Data última transação pagamento":dataUltimatransPag.retiraAno(),
		"Pontos fidelização":obj_Conta.qtPontos,
		"Tipo de Conta": '<span class="destaque_vermelho">'+obj_Conta.tpConta+'</span>',
		"Validade":dataValidade.retiraAno(),
		"Dia vencimento":obj_Conta.diaVcto,
		"Dia vencimento anterior":obj_Conta.ultVenc,
		"Dia corte":obj_Conta.diaCorte,
		"Status anterior da conta":obj_Conta.statAntDesc,
		"Limite compra disponível":limiteCompraDisponivel,
		"Última alteração limite de compra":dataLimiteCompra.retiraAno(),
		"Limite Celular disponível":limiteCelularDisponivel,
		"Dia vencto alterado (nºvezes)":obj_Conta.qtdVctoAlter,
		"Data última transação monetária":dataUltimaTransMone.retiraAno(),
		"Categoria":obj_Conta.tpContaDesc,
		// "Última alteração limite de saques":obj_Conta.fncDtAltLimite,
		"Status SAX": tratarStatusSax(obj_Conta.statusSAX)
		
	}

	function tratarStatusSax(statusSAX){
		var campoSaxDescricao;
		if(statusSAX){
			campoSaxDescricao =  '<span class="destaque_vermelho">'+obj_Conta.descricaoStatusSAX+'</span>' ;
		}
		else{
			campoSaxDescricao =  '----' ;
		}
		return campoSaxDescricao;
	}
	
	function varreduraObj(OBJ_CONTA){
		it=0;
		$.each(OBJ_CONTA, function(index, value){ 
	  		it++;
	    	criarlista(index,value, it);
		});

		$('#conteudo_dinamico_conta').append('<div class="clear"></div>');
		var itensConta = new SeparadorDeColunas(it)
		var $elementoSeparador = document.createElement('div');
		$elementoSeparador.setAttribute('class', 'separador_dinamico');
		$('.Index_elemento_'+itensConta.numeroDivisor()).append($elementoSeparador);
		
	}
 
	function criarlista(index,value,it){
		var $listaDinamica  = document.createElement('div');
		
		if(it%2==0){
			$listaDinamica.setAttribute('class', 'Index_elemento_'+it +' list-blocos-group lista_grid_02' + addClasseFullCAmp(index));
		}
		else{
			$listaDinamica.setAttribute('class','Index_elemento_'+it + ' list-blocos-group lista_grid_02 margin_controle '+ addClasseFullCAmp(index));
		}	
		$listaDinamica.innerHTML = TemplateListaHtml(index,value);
		$($containerLista).append($listaDinamica);
	}

	function addClasseFullCAmp(valorCp){

		if(valorCp.indexOf('SAX') > -1){
			classeFullCamp = ' ';
		}
		else{
			classeFullCamp = '';
		}
		
		return classeFullCamp;
	}


	function TemplateListaHtml(index,value){
		var _HTML;
		_HTML = ' <div class="alt_din list-group-item list-group-item-primary" style="height:60px">';
		_HTML += '<span class="item_lista_titulo controle_altura_dinamico_titulo">'+index+'</span>';
		_HTML += '<span class="item_lista_valor controle_altura_dinamico_valor">'+value+'</span>';
		_HTML += '<div class="lista_grupo_clear"></div>';
		_HTML += '</div>';
		return _HTML;
	}
  	varreduraObj(OBJ_CONTA);
}
else
{
	$($containerLista).append(mensagem.montaTemplate('Dados não encontrados!')) ;
}





