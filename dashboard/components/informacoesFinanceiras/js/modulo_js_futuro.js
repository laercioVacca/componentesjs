var elemFuturos     = document.getElementById('conteudo_dinamico_futuros');
// logging('obj_Futuro<<>>',obj_Futuro)
tratarValor = new TratarValor();
function criarIdentificador(){
	var identificadorUnico = Math.floor(Math.random() * 65536);
	return identificadorUnico
}
if(JSON.stringify(obj_Futuro) != '[]')
{
	function varreduraObj(objGenerico,elementoGenerico){
		for (var i = 0; i < objGenerico.length; i++) {
			var _informacoesFinanceirasFuturo  = objGenerico[i].informacoesFinanceirasFuturo;
			var _dtVencimento	       		   = objGenerico[i].dtVencimento;
			var _totalCompraTranqAux           = tratarValor.converter(objGenerico[i].totalCompraTranqAux);
			var _totalComprasAux               = tratarValor.converter(objGenerico[i].totalComprasAux);
			var _totalGeralAux                 = tratarValor.converter(objGenerico[i].totalGeralAux);
			var _totalSaquesAux                = tratarValor.converter(objGenerico[i].totalSaquesAux);
			var _valorTotalCompra    	       = tratarValor.converter(objGenerico[i].valorTotalCompra);
			var _identificadorUnico            = criarIdentificador();
			criarCard(_dtVencimento,_valorTotalCompra,elementoGenerico,_identificadorUnico,_informacoesFinanceirasFuturo,_totalGeralAux);
		}
		clicarModal(elementoGenerico,_identificadorUnico);
	}
	 
	function criarCard(dtVencimento,_valorTotalCompra,elementoGenerico,_identificadorUnico,_informacoesFinanceirasFuturo,_totalGeralAux){
	    
	    var $containerLista = elementoGenerico;
		var $listaDinamica  = document.createElement('div');
		var $modalLvf = document.createElement('div');
		var conatinerListaExtrato = document.createElement('div');

		$modalLvf.innerHTML = templateModalServicos(_identificadorUnico,dtVencimento,_informacoesFinanceirasFuturo);
	    $listaDinamica.innerHTML = templateCardServicos(dtVencimento,_valorTotalCompra,_identificadorUnico,_totalGeralAux);

	    $($containerLista).prepend($modalLvf);
	    $($containerLista).append($listaDinamica); 
	    clickAcordion();  
	}

	function templateCardServicos(dtVencimento,_valorTotalCompra,_identificadorUnico,_totalGeralAux){
		var htmlCard;
			htmlCard = '<div class="card bg-light mb-3 custom_card">';
			htmlCard += '<div class="card-header sea-green-bg">';
			htmlCard += '<span class="fs1 icone_card_futuro" aria-hidden="true" data-icon=""></span>';  
			htmlCard += '<span class="titulo_card_custom">Vencimento</span>';
			htmlCard += '</div>';
			htmlCard += '<div class="card-body">';
			htmlCard += '<div class="card-text card_custom_texto">';
			htmlCard += '<span class="titulo">Data Vencimento</span>';
			htmlCard += '<span class="descricao">'+dtVencimento+'</span>';
			htmlCard += '</div>';
			htmlCard += '<div class="card-text card_custom_texto border_controle">';
			htmlCard += '<span class="titulo">Valor a pagar</span>';
			htmlCard += '<span class="descricao">'+_totalGeralAux+'</span>';
			htmlCard += '</div>';
			htmlCard += '</div>';
			htmlCard += '<div class="card-header card_custom_saiba_mais">';
			htmlCard += '<a href="#" data-identificador="'+_identificadorUnico+'" class="titulo_card_custom card_btn_saiba_mais isDown" data-toggle="modal" data-target="#modal_futuro">Saiba Mais</a>';
			htmlCard += '<span class="fs1 saiba_mais_icon " aria-hidden="true" data-icon="&#xe102;"></span>';
			htmlCard += '</div>';
			htmlCard += '</div>';
		return htmlCard
	}


	function templateModalServicos(_identificadorUnico,dtVencimento,_informacoesFinanceirasFuturo){
		var htmlModal;
		htmlModal = '<div class="LVF_custom_modal modal fade" id="'+_identificadorUnico+'" tabindex="-1" role="dialog">';
		htmlModal += '<div class="modal-dialog modal-dialog-centered LVF_custom_modal_dialog" role="document">';
		htmlModal += '<div class="modal-content">';
		htmlModal += '<div class="modal-header sea-green-bg">';
		htmlModal += '<h5 class="modal-title" id="exampleModalLongTitle">';
		htmlModal += '<span class="fs1 icone_card_ativo" aria-hidden="true" data-icon=""></span>'; 
		htmlModal += 'Total em '+dtVencimento+'';
		htmlModal += '</h5>';
		htmlModal += '</div>';
		htmlModal += '<div id="testeLOAD" class="modal-body futuro_modal_infos">';
		htmlModal += '<div id="accordion_Futuro_modal_'+_identificadorUnico+'" class="accordion_Futuro_modal">';
		$.each(_informacoesFinanceirasFuturo, function(index, val) {

			var dataExtrato = val.dtRealizacao;
			var descricao   = val.descricao;
			var parcelas    = val.parcelas;
			var valor       = tratarValor.converter(val.valor);
			var valorCompra = tratarValor.converter(val.vrCompra);
			var valorSaque  = tratarValor.converter(val.vrSaque);
			var valorCompraTranquila = tratarValor.converter(val.vlCompraTranquila);

			htmlModal += listarExtratoFuturoModal(dataExtrato,descricao,parcelas,valor,valorCompra,valorSaque,valorCompraTranquila);
			
			
			
		});

		htmlModal += '</div>';
		htmlModal += '<div class="modal-footer">';
		htmlModal += '<button type="button" class="btn btn-secondary" data-dismiss="modal">fechar</button>';
		htmlModal += '</div>';
		htmlModal += '</div>';
		htmlModal += '</div>';
		htmlModal += '</div>'; 
		return htmlModal;
		
	}


	function listarExtratoFuturoModal(dataExtrato,descricao,parcelas,valor,valorCompra,valorSaque,valorCompraTranquila){
		var htmlListaExtrato;
		htmlListaExtrato =  '<div class="card">'
		htmlListaExtrato +=	'<div class="card-header btn_show_accordion" id="headingThree">';
		htmlListaExtrato +=	'<h5 class="mb-0">';
		htmlListaExtrato +=	'<a href="#collapseThree" class="btn btn-link card_btn_saiba_mais">';
		htmlListaExtrato +=	'Extrato referente a ' + dataExtrato;
		htmlListaExtrato +=	'</a>';
		htmlListaExtrato +=	'</h5>';
		htmlListaExtrato +=	'</div>';
		htmlListaExtrato +=	'<div id="collapseThree" class="collapset box_fechado">';
		htmlListaExtrato +=	'<div class="card-body">';
		htmlListaExtrato +=	'<div class="infos_extrato_card">';
		htmlListaExtrato +=	'<div class="card-text card_custom_texto">';
		htmlListaExtrato +=	'<span class="titulo">Descrição</span>';
		htmlListaExtrato +=	'<span class="descricao">'+descricao+'</span>';
		htmlListaExtrato +=	'</div>';
		htmlListaExtrato +=	'<div class="card-text card_custom_texto">';
		htmlListaExtrato +=	'<span class="titulo">Parcelas </span>';
		htmlListaExtrato +=	'<span class="descricao">'+parcelas+'</span>';
		htmlListaExtrato +=	'</div>';
		htmlListaExtrato +=	'<div class="card-text card_custom_texto border_controle">';
		htmlListaExtrato +=	'<span class="titulo">Valor Total Da Compra</span>';
		htmlListaExtrato +=	'<span class="descricao">'+valor+'</span>';
		htmlListaExtrato +=	'</div>';
		htmlListaExtrato +=	'<div class="card-text card_custom_texto border_controle">';
		htmlListaExtrato +=	'<span class="titulo">Valor Saque</span>';
		htmlListaExtrato +=	'<span class="descricao">'+valorSaque+'</span>';
		htmlListaExtrato +=	'</div>';
		htmlListaExtrato +=	'<div class="card-text card_custom_texto border_controle">';
		htmlListaExtrato +=	'<span class="titulo">Valor serviço</span>';
		htmlListaExtrato +=	'<span class="descricao">'+valorCompraTranquila+'</span>';
		htmlListaExtrato +=	'</div>';
		htmlListaExtrato +=	'<div class="card-text card_custom_texto border_controle">';
		htmlListaExtrato +=	'<span class="titulo">Valor da Parcela</span>';
		htmlListaExtrato +=	'<span class="descricao">'+valorCompra+'</span>';
		htmlListaExtrato +=	'</div>';
		// htmlListaExtrato +=	'<div class="card-text card_custom_texto border_controle">';
		// htmlListaExtrato +=	'<span class="titulo">Data vencimento </span>';
		// htmlListaExtrato +=	'<span class="descricao">25/12/2017</span>';
		// htmlListaExtrato +=	'</div>';
		htmlListaExtrato +=	'</div>';
		htmlListaExtrato +=	'</div>';
		htmlListaExtrato +=	'</div>';
		htmlListaExtrato +=	'</div>';
		return htmlListaExtrato;
	}
	function clicarModal(elBtn,_identificadorUnico){
		$(elBtn).on('click', '.card_btn_saiba_mais', function(event) {
			event.preventDefault();
			var ATT = this.getAttribute("data-identificador");
			var ValorVazio = "";
			var boxL = '#accordion_Futuro_modal_'+ATT;
			$('#'+ATT).modal('show');
		});
	}

	function clickAcordion(){
		  $('.accordion_Futuro_modal').on('click', '.btn_show_accordion', function(event) {
	    		event.preventDefault();
	    		$('.box_fechado').hide();
	    		$(this).parent().find('.box_fechado').show();
	  		});

		  $('.modal-footer').on('click', '.btn-secondary', function(event) {
		    event.preventDefault();
		    $('.box_fechado').hide();
		  });

	}
	varreduraObj(obj_Futuro,elemFuturos);
}
else
{
	$(elemFuturos).append(mensagem.montaTemplate('Sem Lançamentos Futuros')) ;
}


