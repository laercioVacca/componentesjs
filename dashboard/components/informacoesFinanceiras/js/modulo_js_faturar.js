var $containerLista = document.getElementById('conteudo_dinamico_faturar');

var tratarValor = new TratarValor();

if(JSON.stringify(obj_Faturar) != '[]')
{
  OBJ_FATURAR = {
  	"Valor compras":tratarValor.converter(obj_Faturar.vrComprasAcum),
  	"Valor parcelado":tratarValor.converter(obj_Faturar.vrPrc),
  	"Valor pagamentos":tratarValor.converter(obj_Faturar.vrPgtoAcum),
  	"Estornos à crédito":tratarValor.converter(obj_Faturar.vrCredito),
  	"Estornos à débito":tratarValor.converter(obj_Faturar.vrDebito),
  	"Valor antecipado":tratarValor.converter(obj_Faturar.vrTotalAntec),
  }
  function varreduraObj(OBJ_ANUIDADE_DINAMICA){
  	it=0;
  	$.each(OBJ_ANUIDADE_DINAMICA, function(index, value){ 
    		it++;
      	criarlista(index,value, it);
  	});
  	$('#conteudo_dinamico_faturar').append('<div class="clear"></div>');
  }
  
  
  function criarlista(index,value,it){
     
      var $listaDinamica  = document.createElement('div');
      var $elementoSeparador = document.createElement('div');

      if(it==4)
      {
      	$elementoSeparador.setAttribute('class', 'separador_dinamico');
      	$($containerLista).append($elementoSeparador);
      }
  	if(it%2==0)
  	{

    		$listaDinamica.setAttribute('class','list-blocos-group lista_grid_02');
  	}
  	else
  	{
    		$listaDinamica.setAttribute('class','list-blocos-group lista_grid_02 margin_controle');
  	}	

      
      
      $listaDinamica.innerHTML = TemplateListaHtml(index,value);
      $($containerLista).append($listaDinamica);
    }

    function TemplateListaHtml(index,value){
      var _HTML;
        _HTML = ' <div class="alt_din list-group-item list-group-item-primary" style="height:50px">';
        _HTML += '<span class="item_lista_titulo controle_altura_dinamico_titulo">'+index+'</span>';
        _HTML += '<span class="item_lista_valor controle_altura_dinamico_valor">'+value+'</span>';
        _HTML += '<div class="lista_grupo_clear"></div>';
        _HTML += '</div>';
        return _HTML;
    }
    varreduraObj(OBJ_FATURAR);
}

else
{
  $($containerLista).append(mensagem.montaTemplate('Sem Valores para Faturar')) ;
}
