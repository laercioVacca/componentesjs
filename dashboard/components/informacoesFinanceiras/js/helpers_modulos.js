// Carregamento do Helpers JS das tabs.




function detectar_mobile() { 
	 if( navigator.userAgent.match(/Android/i)
	 || navigator.userAgent.match(/webOS/i)
	 || navigator.userAgent.match(/iPhone/i)
	 || navigator.userAgent.match(/iPad/i)
	 || navigator.userAgent.match(/iPod/i)
	 || navigator.userAgent.match(/BlackBerry/i)
	 || navigator.userAgent.match(/Windows Phone/i)
	 )
	 {
    	return true;
  	}
 else {
    return false;
  }
}



function loadGif(container){
    $(container).html('<div class="master_load" style="width:100%;padding-top:20%; text-align:center"><div class="loaderCss"></div></div>');
}

function criarIdentificador(){
	var identificadorUnico = Math.floor(Math.random() * 65536);
	return identificadorUnico
}

function Redirecionamento(){
	this.url  = window.location.url; 
	// var absoluto = url.split("/")[url.split("/").length -1];
	this.destino = function(destino){
		var l = this.url;
		var destino = window.location.l;

		return destino;
	}
	
}
function DataFormat(){
	var anoSeparado,Dia,Mes,Ano,inputData
	this.dataRetornada = function(inputData){
		this.inputData = inputData;
		if(this.inputData){
			anoSeparado = this.inputData.split('/');
			Dia = anoSeparado[0];
			Mes = anoSeparado[1];
			Ano = anoSeparado[2];
			
			if(Ano.indexOf("1901") == -1){
				inputData = Dia+'/'+Mes+'/'+Ano;
			}
			else
			{
				inputData = '----';	
			}
		}
		else {
			inputData = '----';
		}
		return inputData;
	}

}
//############################## 
function TrataData(_data,removeDia){
	var dataCompleta,removeDia,anoSeparado,Dia,Mes,Ano,_novaData

	this.dataCompleta = _data;
	this.removeDia    = removeDia;
	this.retiraAno = function(semOdia){
		
		if(this.dataCompleta){
			anoSeparado = this.dataCompleta.split('/');
			Dia = anoSeparado[0];
			Mes = anoSeparado[1];
			Ano = anoSeparado[2];
			
			if(Ano.indexOf("1901") == -1){
				if(this.removeDia == true){
					_novaData = Mes+'/'+Ano;
				}
				else{
					_novaData = Dia+'/'+Mes+'/'+Ano;
				}
				
			}
			else
			{
				_novaData = '----';	
			}
		}
		else {
			_novaData = '----';
		}
		return _novaData;
	}
	this.tratarHora = function(_boleano){
		var Hor,HoraTratada,novaHoraCampo,horaFormatada,horaCampo
		Hor = _boleano;
		HoraTratada = this.dataCompleta.split(' ');
		novaHoraCampo = HoraTratada[1].split(':');
		horaFormatada = 'Hr'+ novaHoraCampo[0]+':'+novaHoraCampo[1];
		if(Hor)
		{
			horaCampo = horaFormatada;
		}
		else
		{
			horaCampo = false;
		}
		return horaCampo;
	}
}
// 
function SeparadorDeColunas(NumeroElemtos){
	var posicaoDivisa,numClasseTratado
	this.NumElemtos = NumeroElemtos;
	this.numeroDivisor = function(){

		posicaoDivisa =  this.NumElemtos/2;
		numClasseTratado = posicaoDivisa.toString().split('.')
	
		return numClasseTratado[0];
	}
}
// 
function regularAltura(objElementos,elParaAplicarAltura){
	this.obj_elementos = objElementos;
	this.ele_aplicaltura = elParaAplicarAltura;
	this.aplicaAltura = function(){
		var arrauAlturaElementos = [];
	  	var arrayObjetos         = [];
	  	for (var i = 0; i < this.obj_elementos.length; i++) {
	  		var itenLista = this.obj_elementos[i];
	    	arrayObjetos.push({classe:itenLista});
	  	}
	  	$.each(arrayObjetos, function(index, value){ 
	    	var alturaUnica = $('.Index_elemento_'+index).height();
	    	arrauAlturaElementos.push(alturaUnica)
	    	
		});
		var NV = 0;
		for (var i = 0; i < arrauAlturaElementos.length; i++) {
			if(arrauAlturaElementos[i] > NV)
			{
				NV = arrauAlturaElementos[i];
			}
		}
		$(this.ele_aplicaltura).height(NV-20);
		$('.item_lista_titulo').addClass('controle_altura_dinamico_titulo');
		$('.item_lista_valor').addClass('controle_altura_dinamico_valor');
	}
}

function mensagem(){
	this.montaTemplate = function(texto){
		this.texto = texto;
		var templateMensage;
		templateMensage  = '<div class="alert_info_finaceira alert alert-warning alert-dismissible fade show" role="alert">';
		templateMensage += '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'
	  	templateMensage += '<span aria-hidden="true">&times;</span>';
	  	templateMensage += '</button>';
	  	templateMensage += '<strong>'+texto+'</strong>';
	  	templateMensage += '</div>';
	  	return templateMensage;
	}
};



function TrataRetorno(valorRetorno,conjuntoArgs){
	this.valorRetorno = this.valorRetorno;
	this.conjuntoArgs = this.conjuntoArgs;
	this.retornoTratado = function(){
		var argumentos = this.conjuntoArgs;
		var objAgrupados200 = [];
		var objAgrupados500 = [];
		var agrupados = [];
		var flagVazio  = false;
		
		if (this.valorRetorno == 500) 
		{
			objAgrupados500 = "Erro";
			return objAgrupados500;
		}
		else if(this.valorRetorno == 204)
		{
			for (var i = 0; i < argumentos.length; i++) {
				if(JSON.stringify(argumentos[i])==[]){
					flagVazio = true;
					var obj = argumentos[i];
					objAgrupados200.push({obj:flagVazio});
				}
			}
		}
		return objAgrupados200;
	}	
}

function TratarValor(){
	this.converter = function(n, c, d, t, putMoeda){
		c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "," : d, t = t == undefined ? "." : t, s = n < 0 ? "-" : "", i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
		var retorno = s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
		if(putMoeda != undefined && putMoeda != null && (putMoeda == true || putMoeda == 'true')){
			return 'R$ ' + retorno;
		}else{
			return retorno;
		}
	}
}

function VerificaVazios(objEntrada){
	this.objEntrada = objEntrada;
	this.varrerObjEntrada = function(){
		// var objSaida = [];
		$.each(this.objEntrada, function(index, value){ 
	  		
		});
		
		// return objSaida;
	}

}

function carregandoEnvio(msg){
	this.msg = msg;
	var htmlLoad  = '<div>';
		htmlLoad += '<p>' + this.msg + '</p>';
	    htmlLoad += '<img src="#">';
	    htmlLoad += '</div>';
	    return htmlLoad;
}



mensagem = new mensagem();
