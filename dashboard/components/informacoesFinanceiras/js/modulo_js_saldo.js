
var objSaldoFaturaAtual 	      = obj_Saldo.informacoesFinanceirasFaturaSaldo;
var objSaldoPosicao 			  = obj_Saldo.informacoesFinanceirasPosicaoSaldo;
var elementoAPopularPosicao 	  = document.getElementById('conteudo_dinamico_saldo_posicao');
var elementoAPopularFatura  	  = document.getElementById('conteudo_dinamico_saldo_atual');
var elementoAPopularConsolidados  = document.getElementById('conteudo_dinamico_saldo_consolidados');

tratarValor = new TratarValor();

// logging('OBJ Saldo!',obj_Saldo)

if(JSON.stringify(obj_Saldo) != '[]')
{
	
	var dataVencimentoFatura = new TrataData(objSaldoFaturaAtual.dtVcto);
	logging('dataVencimentoFatura debug',objSaldoFaturaAtual,dataVencimentoFatura)
	var PosicaoSaldo = {
		"Saldo devedor até a data atual":tratarValor.converter(objSaldoPosicao.vrUtilizCompra),
		"Saldo devedor Total":tratarValor.converter(objSaldoPosicao.saldoDevTotal),
		"Pgtos efetuado até data atual":tratarValor.converter(objSaldoPosicao.vrPgtoAcum),
		"Saldo devedor futuro":tratarValor.converter(objSaldoPosicao.vrPrc),
		"Compras próx fat até data atual":tratarValor.converter(objSaldoPosicao.vrMmPrc),
		"Adesão serviços no pdv":tratarValor.converter(objSaldoPosicao.adesaoServicosPdv),
		"Pagto pendente até data atual":tratarValor.converter(objSaldoPosicao.pgtoPend),
		"Serviços p/próxima fatura":tratarValor.converter(objSaldoPosicao.saldoServCiclo),
		"Compra Pendente até data atual":tratarValor.converter(objSaldoPosicao.vrComprasPend),
		"Pagamento de acordo":tratarValor.converter(objSaldoPosicao.pgtoMinAcordo)
	} 

	var FaturaSaldo = {
		"Valor da ultima fatura":tratarValor.converter(objSaldoFaturaAtual.vrTotFatura),
		"Valor mínimo última fatura":tratarValor.converter(objSaldoFaturaAtual.vrMinTotal),
		"Saldo anterior":tratarValor.converter(objSaldoFaturaAtual.vrSaldoAnt),
		"Valor dos serviços":tratarValor.converter(objSaldoFaturaAtual.saldoServCorte),
		"Valor das compras":tratarValor.converter(objSaldoFaturaAtual.vrComprasAcum),
		"Valor dos créditos":tratarValor.converter(objSaldoFaturaAtual.vrCred),
		"Data do vcto da fatura":dataVencimentoFatura.retiraAno(),
		"Valor dos encargos":tratarValor.converter(objSaldoFaturaAtual.vrEncargos),
		// "Saldo devedor até a data atual":tratarValor.converter(objSaldoFaturaAtual.saldoDevAteDataAtual),
		// "Pgtos efetuado até data atual":tratarValor.converter(objSaldoFaturaAtual.fatSaqVrPagto),
		// "Saldo devedor futuro":tratarValor.converter(objSaldoFaturaAtual.totalSaqueAux)
	}
	var consolidadosSaldo = {
		"Saldo devedor total até a data atual":tratarValor.converter(objSaldoFaturaAtual.saldoDevAteDataAtual),
		"Saldo devedor Geral":tratarValor.converter(objSaldoFaturaAtual.totalSaqueAux),
		"Saldo devedor futuro total":tratarValor.converter(objSaldoPosicao.vrPrc),
		"Pgtos total até data atual":tratarValor.converter(objSaldoFaturaAtual.fatSaqVrPagto)
		
	}
	function varreduraObj(objGenerico,elementoGenerico){
		it=0;
		$.each(objGenerico, function(index, value){ 
	  		it++;
	    	criarlista(index,value,it,elementoGenerico);
		});

		$(elementoGenerico).append('<div class="clear"></div>');
		var itensConta = new SeparadorDeColunas(it)
		var $elementoSeparador = document.createElement('div');
		$elementoSeparador.setAttribute('class', 'separador_dinamico');

		$($elementoSeparador).insertAfter('.Index_elemento_'+itensConta.numeroDivisor());
		
	}
 
	function criarlista(index,value,it,elementoGenerico){
	    var $containerLista = elementoGenerico;
		var $listaDinamica  = document.createElement('div');
	   
		if(it%2==0)
		{

	  		$listaDinamica.setAttribute('class', 'Index_elemento_'+it +' list-blocos-group lista_grid_02');
		}
		else
		{
	  		$listaDinamica.setAttribute('class','Index_elemento_'+it + ' list-blocos-group lista_grid_02 margin_controle');
		}	
	    $listaDinamica.innerHTML = TemplateListaHtml(index,value);
	    $($containerLista).append($listaDinamica);
	}



	function TemplateListaHtml(index,value){
		var _HTML;
		_HTML = ' <div class="alt_din list-group-item list-group-item-primary" style="height:60px">';
		_HTML += '<span class="item_lista_titulo controle_altura_dinamico_titulo">'+index+'</span>';
		_HTML += '<span class="item_lista_valor controle_altura_dinamico_valor">'+value+'</span>';
		_HTML += '<div class="lista_grupo_clear"></div>';
		_HTML += '</div>';
		return _HTML;
	}
	varreduraObj(PosicaoSaldo,elementoAPopularPosicao);
	varreduraObj(FaturaSaldo,elementoAPopularFatura);
	varreduraObj(consolidadosSaldo,elementoAPopularConsolidados);
}
else
{
   $(elementoAPopularPosicao).append(mensagem.montaTemplate('Sem Informações')) ;
   $('#titulo_header_saldo_atual,#titulo_header_saldo_consolidados').hide();
}
