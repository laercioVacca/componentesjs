   
// var _URL_JASON                  =  _URL_SCRIPTS + "json02/ReturnServiceInformacoesFinanceirasJSON.json";
// var _URL_JASON_COMPLEMENTO      =  _URL_SCRIPTS + "json02/ReturnServiceInformacoesFinanceirasComplementoJSON.json";
// console.log('LOAD SERVICOS');
//require("../../../../resources/js/genericServices.js");



var obj_header_infos;
var obj_Anuidade;
var obj_Conta;
var obj_Saldo;
var obj_Inadimplencia;
var obj_Faturar;
var obj_servicos;
var obj_Futuro;
var obj_ComplementoSaldo;
var obj_Inadimplencia_acordo;


var servicoTabsComplemento = new GenericServiceV2('financialservices');
var servicoTabs 		   = new GenericServiceV2('financialservices');

//var userDf = '4764741911';
//var userAdr = '6708575990';
//var userDan = '3930600471';
//var userGenerico = '3158725509';
//var userAcordo = '98261908100'

parameters = {
	"cliCPF" : utilSac.getInfosCurrentCliente(controllStorageSac.getCurrentCliCpf()).cpf,
	
};

var hashProcesso = controllStorageSac.criacaoHashUnicaProcesso("prc_");

var obj_getInfoFinaceiras = servicoTabs.callPostService("v2/infofinanceiras", "getInformacoesFinanceiras", hashProcesso, parameters); 
var obj_getInfoFinaceirasComplemento = servicoTabsComplemento.callPostService("v2/infofinanceiras", "getInformacoesFinanceirasComplemento", hashProcesso, parameters);

// console.log('obj_getInfoFinaceiras ======>>>',obj_getInfoFinaceiras)
// console.log('obj_getInfoFinaceirasComplemento =====>>>',obj_getInfoFinaceirasComplemento)


var servRetorno     = obj_getInfoFinaceiras.codigoRetorno;
var servRetornoComp = obj_getInfoFinaceirasComplemento.codigoRetorno;


function limparObj(){
	obj_header_infos = [];
	obj_Anuidade = [];
	obj_Conta = [];
	obj_Saldo = [];
	obj_Inadimplencia = [];
	obj_Faturar = [];
	obj_servicos = [];
	obj_Futuro = [];
	obj_ComplementoSaldo = [];
	obj_Inadimplencia_complemento = [];
}

function cargaService(){
	
	if(obj_getInfoFinaceiras.objectsReturn.Header != undefined){
		obj_header_infos  = obj_getInfoFinaceiras.objectsReturn.Header; 
		obj_Conta		  = obj_getInfoFinaceiras.objectsReturn.Header.informacoesFinanceirasContaVO;
		obj_Saldo		  = obj_getInfoFinaceiras.objectsReturn.Header.informacoesFinanceirasSaldoVO;
		obj_Anuidade	  = obj_getInfoFinaceiras.objectsReturn.Header.informacoesFinanceirasAnuidadeVO;
		obj_Inadimplencia = obj_getInfoFinaceiras.objectsReturn.Header.informacoesFinanceirasInadimplenciaVO;
		obj_Faturar		  = obj_getInfoFinaceiras.objectsReturn.Header.informacoesFinanceirasFaturaVO;
	}
	
	if(obj_getInfoFinaceirasComplemento.codigoRetorno = 200){
		obj_ComplementoSaldo = obj_getInfoFinaceirasComplemento.objectsReturn.InformacoesFinanceirasComplemento.informacoesFinanceirasInadimplenciaVO;
		obj_servicos = obj_getInfoFinaceirasComplemento.objectsReturn.InformacoesFinanceirasComplemento.informacoesFinanceirasServicosVO;
		obj_Futuro = obj_getInfoFinaceirasComplemento.objectsReturn.InformacoesFinanceirasComplemento.informacoesFinanceirasAgrupFuturoVO;
		obj_Inadimplencia_complemento = obj_getInfoFinaceirasComplemento.objectsReturn.InformacoesFinanceirasComplemento.informacoesFinanceirasInadimplenciaVO;
	}
	
}


function tratarRetornoService(servRetorno){
	var resultadoRetorno = servRetorno;
	switch (resultadoRetorno) {
		case 204:
			limparObj();
			// window.location.href = "/psfloja/paginas/cockpit/dashboard/responsive/page.html?token=6A3868386736703860306E39612F763E7747763F77457E477B48784A814A";
			marisaMessage.showDialog(marisaMessageType_WARNING, "Alerta","Cliente não encontrado!");
			
			break;
		case 500:
			limparObj();
			marisaMessage.showDialog(marisaMessageType_WARNING, "Alerta","Erro ao buscar as informações!");
		default:
			limparObj();
			cargaService();
			break;
	}
}



tratarRetornoService(servRetorno);

$('#btnSearch').on('click', function(event) {
	event.preventDefault();
	limparObj();
});








  
 