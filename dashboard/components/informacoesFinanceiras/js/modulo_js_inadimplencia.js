var $containerLista = document.getElementById('conteudo_dinamico_inadimplencia');
var containerlistaAcordos = document.getElementById('conteudo_dinamico_inadimplencia_acordo');


// logging('.razaoSocial',obj_Inadimplencia_complemento.razaoSocial)
// logging('.ddd',obj_Inadimplencia_complemento.ddd);
// logging('.fone',obj_Inadimplencia_complemento.fone);

var razaoSocial 	= obj_Inadimplencia_complemento.razaoSocial;
var razaoSocialDdd 	= obj_Inadimplencia_complemento.ddd;
var razaoSocialFone = obj_Inadimplencia_complemento.fone;
var origemInadimplencia;

if(razaoSocial != '' || razaoSocialDdd != ''){
	origemInadimplencia = "ASSESSORIA - " + obj_Inadimplencia_complemento.razaoSocial + "(" + obj_Inadimplencia_complemento.ddd + ") " + obj_Inadimplencia_complemento.fone ;
}
else{
	origemInadimplencia = "----";
}




var obj_Acordos = obj_Inadimplencia_complemento.informacoesFinanceirasAcordoVO;

var tratarValor = new TratarValor();
var inadimplenciaCnpj
var inadimplenciaDdd;

logging('obj_Inadimplencia obj_Inadimplencia_complemento',obj_Inadimplencia,obj_Inadimplencia_complemento)


if(JSON.stringify(obj_Inadimplencia) != '[]')
{
	$('#lista_acordos').hide();
	var dataEncargos = new TrataData(obj_Inadimplencia.dtParaEncarg);
	var dataCobranca = new TrataData(obj_Inadimplencia.dtCobranca);
    var dtVenciAtraso = new TrataData(obj_Inadimplencia_complemento.dtvctoAtrAnt);
    var dtVencMaiorAtraso = new TrataData(obj_Inadimplencia_complemento.dtvctoAtr);
    var dtPgMaiorAtraso   = new TrataData(obj_Inadimplencia_complemento.dtpgtoAtr)
    var dtPgMaiorAtrasoAnte   = new TrataData(obj_Inadimplencia_complemento.dtpgtoAtrAnt);

	ARRAY_OBJ_INADIMPLENCIA = {
		"Situação cobrança":obj_Inadimplencia.sitCobr,
		"Origem":origemInadimplencia,
		"Dias atraso":obj_Inadimplencia.diasAtraso,
		"Encargos paralisados":obj_Inadimplencia.flgParaEncarg,
		"Data paralisação encargos":dataEncargos.retiraAno(),
		"Data cobrança":dataCobranca.retiraAno(),
		"Valor parcela acordo":tratarValor.converter(obj_Inadimplencia.vlAcordo),
		"Valor entrada acordo":tratarValor.converter(obj_Inadimplencia.vrParcAcdEnt),
		"Esteve em cobrança(nº vezes)": obj_Inadimplencia_complemento.qtAtr,
		"Maior fase":obj_Inadimplencia_complemento.faseAtr,
		"Valor em compras":tratarValor.converter(obj_Inadimplencia_complemento.vrSaldoRota),
		"Valor multa":tratarValor.converter(obj_Inadimplencia_complemento.vrMulta),
		"Valor juros":tratarValor.converter(obj_Inadimplencia_complemento.vrJuros),
		"Vencimento maior atraso":dtVencMaiorAtraso.retiraAno(),
		"Data pagamento maior atraso":dtPgMaiorAtraso.retiraAno(), 
		"Nº fatura maior atraso":obj_Inadimplencia_complemento.fatAtr,
		"Vencimento maior atraso anterior":dtVenciAtraso.retiraAno(),   
		"Data pagamento maior atraso anterior":dtPgMaiorAtrasoAnte.retiraAno()
	}
	if(JSON.stringify(obj_Acordos) != '[]'){
		
		var array_obj_acordos = [];
		var pacoteAcordos = document.createElement('div');

		$.each(obj_Acordos,function(index, val) {
			logging('index e Valor',index,val)
			var situAcordo 		= val.acdSituacaoDescr;
			var parcQuant 		= val.prcCod + '/'+ val.prcNum;
			var dtVencimento 	= val.prcDtVencto;
			var vlParc 			= tratarValor.converter(val.prcVr);
			var dtPagamento		= new TrataData(val.prcDtPagto);
			var vlpago 			= tratarValor.converter(val.prcVrPago);

			var pacoteAcordos = document.createElement('div');

			pacoteAcordos.innerHTML = templateAcordos(situAcordo,parcQuant,dtVencimento,vlParc,dtPagamento,vlpago);
			
			$(containerlistaAcordos).append(pacoteAcordos);
			logging('situAcordo',situAcordo)
			
				$('#lista_acordos').show();
				$('#situacao_acordo').html(situAcordo);
				
			

		});
	}

	function templateAcordos(situAcordo,parcQuant,dtVencimento,vlParc,dtPagamento,vlpago){
		var htmlAcordos;
		htmlAcordos  = '<div class="list-blocos-group lista_acordos">';
		// htmlAcordos += '<ul class="list-group">';
		// htmlAcordos += '<li class="list-group-item list-group-item-info">Situação do acordo - <span id="situacao_acordo">'+situAcordo+'</span></li>';
		// htmlAcordos += '</ul>'; 
		// htmlAcordos += '<div class="alt_din list-group-item list-group-item-primary">';
		// htmlAcordos += '<span class="lista_acordos_titulo">Parcela/Qtde Parcelas</span>';
		// htmlAcordos += '<span class="lista_acordos_titulo">Data Vencimento</span>';
		// htmlAcordos += '<span class="lista_acordos_titulo">Valor Parcela</span>';
		// htmlAcordos += '<span class="lista_acordos_titulo">Data Pagamento</span>';
		// htmlAcordos += '<span class="lista_acordos_titulo">Valor Pago</span>';
		// htmlAcordos += '</div>';
		htmlAcordos += '<div class="alt_din list-group-item list-group-item-primary">';
		htmlAcordos += '<span class="lista_acordos_texto">'+parcQuant+'</span>';
		htmlAcordos += '<span class="lista_acordos_texto">'+dtVencimento+'</span>';
		htmlAcordos += '<span class="lista_acordos_texto">'+vlParc+'</span>';
		htmlAcordos += '<span class="lista_acordos_texto">'+dtPagamento.retiraAno()+'</span>';
		htmlAcordos += '<span class="lista_acordos_texto">'+vlpago+'</span>';
		htmlAcordos += '</div>';
		htmlAcordos += '</div>';
		return htmlAcordos;
	}
	
	
	

	function varreduraObj(ARRAY_OBJ_INADIMPLENCIA){
		it=0;
		$.each(ARRAY_OBJ_INADIMPLENCIA, function(index, value){ 
	  		it++;
	    	criarlista(index,value, it);
		});

		$('#conteudo_dinamico_inadimplencia').append('<div class="clear"></div>');
		var itensConta = new SeparadorDeColunas(it)
		var $elementoSeparador = document.createElement('div');
		$elementoSeparador.setAttribute('class', 'separador_dinamico');
		$('.Index_elemento_'+itensConta.numeroDivisor()).append($elementoSeparador);
		
	}
 
	function criarlista(index,value,it){
	    
		var $listaDinamica  = document.createElement('div');
	   
		if(it%2==0)
		{

	  		$listaDinamica.setAttribute('class', 'Index_elemento_'+it +' list-blocos-group lista_grid_02');
		}
		else
		{
	  		$listaDinamica.setAttribute('class','Index_elemento_'+it + ' list-blocos-group lista_grid_02 margin_controle');
		}	
	    $listaDinamica.innerHTML = TemplateListaHtml(index,value);
	    $($containerLista).append($listaDinamica);
	}
	function TemplateListaHtml(index,value){
		var _HTML;
	    _HTML = ' <div class="alt_din list-group-item list-group-item-primary" style="height:50px">';
	    _HTML += '<span class="item_lista_titulo controle_altura_dinamico_titulo">'+index+'</span>';
	    _HTML += '<span class="item_lista_valor controle_altura_dinamico_valor item_'+index+'">'+value+'</span>';
	    _HTML += '<div class="lista_grupo_clear"></div>';
	    _HTML += '</div>';
	    return _HTML;
	}

	varreduraObj(ARRAY_OBJ_INADIMPLENCIA);

	
}
else
{
	$('#lista_acordos').hide();
	$($containerLista).append(mensagem.montaTemplate('Sem Informações')) ;
}
	

	
	
	


