function TemplatePopUp() {
	// Template PopUps identificacao Positiva
	this.popUpMensagemInterna = function(gridPopUps,tipoPopUp){
		var tmp;
		tmp  = '<div class="mr_cardAviso mr_cardAviso-interno cardAviso '+gridPopUps+'">';
		tmp += '<div class="mr_card">';
		tmp += '<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="false" style="">';
		tmp += '<div class="panel panel-default">';
		tmp += '<div class="panel-collapse collapse show" style="" role="tabpanel" aria-labelledby="headingcard.id">';
		if(tipoPopUp!= null && tipoPopUp != undefined && tipoPopUp != false){
			tmp +=	infosPopUpDash()
		}else {
			tmp += infosPopUpIdentPositiva();
		}
		tmp += '</div>';
		tmp += '</div>';
		tmp += '</div>';
		tmp += '</div>';
		tmp += '</div>';
		function infosPopUpIdentPositiva(){
			var tmp02;
			tmp02 = '<div class="">';
			tmp02 += '<div class="callout callout-warning no-margin">';
			tmp02 += '<a class="mr_botFechar" data-content="interno" href="#"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>';
			tmp02 += '<h4 class="mr_cardAviso-titulo">Mensagem</h4>';
			tmp02 += '<p class="mr_cardAviso-texto">';
			tmp02 += 'Olá Atendente Maria Silva.'; 
			tmp02 += '</p>';
			tmp02 += '</div>';
			tmp02 += '</div>  '; 
			return tmp02; 
		}
		function infosPopUpDash(){
			var tmp;
			tmp  = '<div class="" style="visibility: visible;padding: 5px;overflow: auto;">';
			tmp += '<div class="callout callout-warning no-margin" style=" height: 100%;">';
			tmp += '<h4 style="font-size: 24px;font-weight: bold;margin-bottom: 20px;">Mensagem</h4>';
			tmp += '<p style="font-size: 18px;">';
			tmp += 'Olá Atendente Laercio Vacca Filho.'; 
			tmp += '</p>';
			tmp += '<p style="font-size: 16px; line-height: 19px">Seu treinamento para o novo sistema do SAC foi agendado Para <strong style="color:#d9534f"> 14/05/2019</strong></p>';
			tmp += '</div>';
			tmp += '</div>';                               
			tmp += '<div class="panel-footer col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding: 2px 5px 2px 2px;/* background-color: rgba(0, 0, 255, 0.3); */">';
			tmp += '<a href="cardAviso_01" data-identificador="cardAviso_01" class="gravarLog contaConjuntoCard btn_hide btn btn-danger btn" style="margin: 3px 3px 3px 3px;height: 20px;float: right;">';
			tmp += '<span class="glyphicon icon-pushpin"> OK</span>';
			tmp += '</a>';
			tmp += '</div>';
			return tmp;
		}
		return tmp;
	}
	this.popUpClienteInativo = function(gridPopUps,tipoPopUp,txtPopUp){
		var tmp;
		tmp  = '<div class="mr_cardAviso mr_cardAviso-inativo cardAviso '+gridPopUps+'">';
		tmp += '<div class="mr_card">';
		tmp += '<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="false" style="">';
		tmp += '<div class="panel panel-default">';
		tmp += '<div class="panel-collapse collapse show" style="" role="tabpanel" aria-labelledby="headingcard.id">';
		if(tipoPopUp!= null && tipoPopUp != undefined && tipoPopUp != false){
			tmp +=	clienteInativoDash(txtPopUp)
		}else {
			tmp += clienteInativoIdentificacaoPositiva(txtPopUp);
		}
		tmp += '</div>';
		tmp += '</div>';
		tmp += '</div>';
		tmp += '</div>';
		tmp += '</div>';
		return tmp;
		function clienteInativoIdentificacaoPositiva(txtPopUp){
			var tmp;
			tmp  = '<div class="">';
			tmp += '<div class="callout callout-warning no-margin">';
			tmp += '<a class="mr_botFechar" data-content="inativo" href="#"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>';
			tmp += '<h4 class="mr_cardAviso-titulo">Cliente Inativo</h4>';
			tmp += '<p class="mr_cardAviso-texto">'+txtPopUp+'</p>';
			tmp += '</div>';
			tmp += '</div>';
			return tmp;
		}
		function clienteInativoDash(txtPopUp){
			var tmp;
			tmp = '<div class="" style="visibility: visible;padding: 5px;overflow: auto;">';
			tmp += '<div class="callout callout-warning no-margin" style=" height: 100%;">';
			tmp += '<h4 style="font-size: 24px;font-weight: bold;margin-bottom: 20px;">Cliente Inativo</h4>';
			tmp += '<p style="font-size: 18px;">';
			tmp += 'O cliente encontra-se inativo'; 
			tmp += 'Favor solicitar a reanalise através do botão abaixo.';
			tmp += '</p>';
			tmp += '</div>';
			tmp += '</div>';
			tmp += '<div class="panel-footer col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding: 2px 5px 2px 2px;/* background-color: rgba(0, 0, 255, 0.3); */">';
			tmp += '<a href="cardAviso_02" data-identificador="cardAviso_02" class="contaConjuntoCard btn_modal_SAC btn btn-warning btn" data-modalsac-type="clienteinativo" style="margin: 3px 3px 3px 3px;height: 20px;float: right;">';
			tmp += '<span class="glyphicon glyphicon-flash"> REANALISE</span>';
			tmp += '</a>';
			tmp += '</div>';
			return tmp;
		}
	}
	this.popUpClientePreAprovadoItau = function(gridPopUps,tipoPopUp){
		var tmp;
		tmp  = '<div class="mr_cardAviso  mr_cardAviso-preaprovadoItau cardAviso '+gridPopUps+'">';
		tmp += '<div class="mr_card">';
		tmp += '<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="false" style="">';
		tmp += '<div class="panel panel-default">';
		tmp += '<div class="panel-collapse collapse show" style="" role="tabpanel" aria-labelledby="headingcard.id">';
		if(tipoPopUp!= null && tipoPopUp != undefined && tipoPopUp != false){
			tmp +=	preAprovadoItauDash()
		}else {
			tmp += preAprovadoItauIdentificacaoPositiva();
		}
		tmp += '</div>';
		tmp += '</div>';
		tmp += '</div>';
		tmp += '</div>';
		tmp += '</div>';
		return tmp;
		function preAprovadoItauIdentificacaoPositiva(){
			var tmp;
			tmp  = '<div class="">';
			tmp += '<div class="callout callout-warning no-margin">';
			tmp += '<a class="mr_botFechar" data-content="preaprovadoItau" href="#"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>';
			tmp += '<h4 class="mr_cardAviso-titulo">Pré-Aprovado Itaú</h4>';
			tmp += '<p class="mr_cardAviso-texto">';
			tmp += 'O cliente encontra-se pré-aprovado.'; 
			tmp += '</p>';
			tmp += '</div>';
			tmp += '</div>  '; 
			return tmp;
		}
		function preAprovadoItauDash(){
			var tmp;
			tmp = '<div class="" style="visibility: visible;padding: 5px;overflow: auto;">';
			tmp += '<div class="callout callout-info no-margin" style=" height: 100%;">';
			tmp += '<h4 style="font-size: 24px;font-weight: bold;margin-bottom: 20px;">Pré-Aprovado Itaú</h4>';
			tmp += '<p style="font-size: 18px;">';
			tmp += 'O cliente encontra-se pré-aprovado para migração ao cartão Itaú.';
			tmp += '</p>';
			tmp += '<p style="font-size: 18px;">Limite: R$ 1.568,00</p>';
			tmp += '</div>';
			tmp += '</div>';                               
			tmp += '<div class="panel-footer col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding: 2px 5px 2px 2px;/* background-color: rgba(0, 0, 255, 0.3); */">';
			tmp += '<a href="cardAviso_03" data-identificador="cardAviso_03" class="contaConjuntoCard btn_mod_tombamento btn btn-info btn" style="margin: 3px 3px 3px 3px;height: 20px;float: right;">';
			tmp += '<span class="glyphicon glyphicon-flash"> REALIZAR TOMBAMENTO</span>';
			tmp += '</a>';
			tmp += '</div>';
			return tmp;
		}
	}
	this.popUpClienteEmprestimoPessoal = function(gridPopUps,tipoPopUp){
		var tmp;
		tmp  = '<div class="mr_cardAviso mr_cardAviso-emprestimoPessoal cardAviso '+gridPopUps+'">';
		tmp += '<div class="mr_card">';
		tmp += '<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="false" style="">';
		tmp += '<div class="panel panel-default">';
		tmp += '<div class="panel-collapse collapse show" style="" role="tabpanel" aria-labelledby="headingcard.id">';
		if(tipoPopUp!= null && tipoPopUp != undefined && tipoPopUp != false){
			tmp +=	emprestimoPessoalDash()
		}else {
			tmp += emprestimoPessoalIdentificaPositiva();
		}
		tmp += '</div>';
		tmp += '</div>';
		tmp += '</div>';
		tmp += '</div>';
		tmp += '</div>';
		return tmp;
		function emprestimoPessoalIdentificaPositiva(){
			var tmp;
			tmp  = '<div class="">';
			tmp += '<div class="callout callout-warning no-margin">';
			tmp += '<a class="mr_botFechar" data-content="emprestimoPessoal" href="#"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>';
			tmp += '<h4 class="mr_cardAviso-titulo">Empréstimo Pessoal</h4>';
			tmp += '<p class="mr_cardAviso-texto">';
			tmp += 'O cliente encontra-se pré-aprovado.'; 
			tmp += '</p>';
			tmp += '</div>';
			tmp += '</div>';
			return tmp; 
		}
		function emprestimoPessoalDash(){
			var tmp;
			tmp  = '<div class="" style="visibility: visible;padding: 5px;overflow: auto;">';
			tmp += '<div class="callout callout-success no-margin" style=" height: 100%;">';
			tmp += '<h4 style="font-size: 24px;font-weight: bold;margin-bottom: 20px;">Empréstimo Pessoal</h4>';
			tmp += '<p style="font-size: 18px;">';
			tmp += 'O cliente encontra-se pré-aprovado para empréstimo pessoal.';
			tmp += '</p>';
			tmp += '<p style="font-size: 18px;">Limite: R$ 540,00</p>';
			tmp += '<p style="font-size: 18px;">Validade: 31/05/2018</p>';
			tmp += '</div>';
			tmp += '</div> ';                              
			tmp += '<div class="panel-footer col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding: 2px 5px 2px 2px;/* background-color: rgba(0, 0, 255, 0.3); */">';
			tmp += '<a href="http://128.1.30.127:88/login.asp" data-identificador="cardAviso_04" target="_blank" class="contaConjuntoCard btn btn-success btn" data-modalsac-type="sysin"  style="margin: 3px 3px 3px 3px;height: 20px;float: right;">';
			tmp += '<span class="glyphicon glyphicon-flash">  SYSIN  </span>';
			tmp += '</a>';
			tmp += '</div>';
			return tmp;
		}
	}
	this.popUpClientePreAprovadoSac = function(gridPopUps,tipoPopUp){
		var tmp;
		tmp  = '<div class="mr_cardAviso  mr_cardAviso-preaprovadoSac cardAviso '+gridPopUps+'">';
		tmp += '<div class="mr_card">';
		tmp += '<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="false" style="">';
		tmp += '<div class="panel panel-default">';
		tmp += '<div class="panel-collapse collapse show" style="" role="tabpanel" aria-labelledby="headingcard.id">';
		if(tipoPopUp!= null && tipoPopUp != undefined && tipoPopUp != false){
			tmp +=	preAprovadoSaqueDash()
		}else {
			tmp += preAprovadoSaqueIdentificaPositiva();
		}
		tmp += '</div>';
		tmp += '</div>';
		tmp += '</div>';
		tmp += '</div>';
		tmp += '</div>';
		return tmp;

		function preAprovadoSaqueIdentificaPositiva(){
			var tmp;	
			tmp  = '<div class="">';
			tmp += '<div class="callout callout-warning no-margin">';
			tmp += '<a class="mr_botFechar" data-content="preaprovadoSac" href="#"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>';
			tmp += '<h4 class="mr_cardAviso-titulo">Pré-Aprovado Saque</h4>';
			tmp += '<p class="mr_cardAviso-texto">';
			tmp += 'O cliente possui saques pré-aprovados.'; 
			tmp += '</p>';
			tmp += '</div>';
			tmp += '</div>';
			return tmp;
		}
		function preAprovadoSaqueDash(){
			var tmp;
			tmp = '<div class="" style="visibility: visible;padding: 5px;overflow: auto;">';
			tmp += '<div class="callout callout-yellow no-margin" style=" height: 100%;">';
			tmp += '<h4 style="font-size: 24px;font-weight: bold;margin-bottom: 20px;">Pré-Aprovado Saque</h4>';
			tmp += '<p style="font-size: 18px;">O cliente encontra-se pré-aprovado saques.</p>';
			tmp += '<p style="font-size: 18px;">Limite: R$ 268,00</p>';
			tmp += '<p style="font-size: 18px;">Encaminhe o cliente ao PDV.</p>';
			tmp += '</div>';
			tmp += '</div> ';                              
			tmp += '<div class="panel-footer col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding: 2px 5px 2px 2px;/* background-color: rgba(0, 0, 255, 0.3); */">';
			tmp += '<a href="cardAviso_05" data-identificador="cardAviso_05" class="contaConjuntoCard btn_hide btn btn-success btn" style="margin: 3px 3px 3px 3px;height: 20px;float: right;">';
			tmp += '<span class="glyphicon glyphicon-flash"> OK  </span>';
			tmp += '</a>';
			tmp += '</div>';
			return tmp;
		}
	}


}
var templatePopUp = new TemplatePopUp();