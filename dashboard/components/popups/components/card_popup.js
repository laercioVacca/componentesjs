function CardMemofile() {
	this.templateCard = function(){
		var retorno;
		retorno  = '<div id="card_06" class="col-xs-12 col-sm-6 col-md-6 col-lg-6" style="padding: 0px; display: inline-table;">';
		retorno += '<div class="card menu" id="menucard.id" style="margin: 0px;border: 5px solid #fbfaf6;max-width: 100%;">';
		retorno += '<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="false">';
		retorno += '<div class="panel panel-default">';
		retorno += '<div class="panel-heading" role="tab" id="heading card.id + "">';
		retorno += '<h4 class="panel-title">';
		retorno += '<a class="collapsed" id="menuCollapsecard.id" role="button" style="padding-right: 5px;color: #75104c;" data-toggle="" data-parent="#accordion" aria-expanded="true" aria-controls="collapsecard.id">'; 
		retorno += '<span class="glyphicon glyphicon-th-list" style="left: -30px;"></span>';
		retorno += 'Memo File';
		retorno += '</a>';
		retorno += '</h4>';
		retorno += '</div>';
		retorno += '<div id="card_memofile" class="panel-collapse collapse show" style="height: 200px;" role="tabpanel" aria-labelledby="headingcard.id">';
		retorno += '<div class="panel-body" style="visibility: visible;padding: 5px;height: 170px;overflow: auto;">';
		for (var i = 0; i < 3; i++) {
			var identificador = 'iden_' + i;
			retorno += this.templateListaMemofile(identificador);
		}
		retorno += '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding: 2px 2px 2px 2px;"> ';
		retorno += '<div class="" style="height: 10px;">&nbsp;</div> ';
		retorno += '</div>'; 
		retorno += '</div>';
		retorno += '<div class="panel-footer col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding: 2px 5px 2px 2px;background-color: #dddddd;"> ';
		retorno += '<a id="btnCardHistoricoMemoFile" href="javascript:void(0)" class="render_page btn btn-info btn" style="margin: 3px 3px 3px 3px;height: 20px;float: right;">';
		retorno += '<span class="glyphicon glyphicon-th-list"> HISTÓRICO</span> ';
		retorno += '</a>';
		retorno += '</div>'; 
		retorno += '</div>';
		retorno += '</div>';
		retorno += '</div>';
		retorno += '</div>';
		retorno += '</div>';
		return retorno;
	};
	this.templateListaMemofile = function(identificador){
		var retornoLista;
		retornoLista = '<div style="max-width:800px" class="card">';
		retornoLista += '<div class="card-header" id="headingOne2" style="padding-top:0px;padding-bottom: 0px">';
		retornoLista += '<a class="btn_drop_memo" href="#" data-toggle="collapse" data-target="#'+identificador+'" aria-expanded="true" aria-controls="'+identificador+'">';
		retornoLista += '<span class="titulo_memofile_card_drop">2017</span>';                           
		retornoLista += '</a>';
		retornoLista += '</div>';
		retornoLista += '<div id="'+identificador+'" class="collapse" aria-labelledby="headingOne2" data-parent="#accordion">';
		retornoLista += '<div class="card-body"  style="margin: 0px; padding:0px">';
		retornoLista += '<ul class="list-group" style="margin-bottom: 5px;">';
		retornoLista += '<li class="list-group-item row" style="padding: 0px;margin: 3px;">';
		retornoLista += '<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="padding: 0px 5px 0px 5px;">09/04/2018 08:11:21</div>';
		retornoLista += '<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8" style="padding: 0px 5px 0px 5px;">CLIENTE DEV APROX 7, ...</div>';
		retornoLista += '</li>';
		retornoLista += '<li class="list-group-item row" style="padding: 0px;margin: 3px;">';
		retornoLista += '<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="padding: 0px 5px 0px 5px;">09/04/2018 08:07:28</div>';
		retornoLista += '<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8" style="padding: 0px 5px 0px 5px;">CLIENTE SOLICITOU FALAR COM ...</div>';
		retornoLista += '</li>';
		retornoLista += '</ul>';
		retornoLista += '</div>';
		retornoLista += '</div>';
		retornoLista += '</div>';
		return retornoLista;
	};
}
var cardMemofile = new CardMemofile();

