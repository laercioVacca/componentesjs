require("components/popups/components/templates_popup.js");

function BusinessPopUp() {
	this.load = function(containerPopUps,objInfosPopUps){
		this.flagIDPO           = JSON.parse(localStorage.getItem('setFlagIDPO'))
		this.tipoPopUp          = JSON.parse(localStorage.getItem('flagIdentificacaoPositiva_'+webLink.cpfUsuario))
		this.containerPopUps	= containerPopUps;
		this.objInfosPopUps 	= objInfosPopUps;
		this.inativoCliente 	= this.objInfosPopUps.inativo;

		this.diasCarenciaReanaliseCliente 	= this.objInfosPopUps.diasCarenciaReanalise;
		this.statCodCreditoCliente 			= this.objInfosPopUps.situacaoContaCod;
		this.statCodCartaoCliente 			= this.objInfosPopUps.situacaoCartaoCod;
		this.statCodEmbossamentoCliente 	= this.objInfosPopUps.situacaoEmbossamentoCod;
	
		this.renderPopUpCliente();
	};
	this.quantidadePopUps = function(){
		var avisosCliente = 4;

		var gridPop01 = "col-sm-12 col-md-12 col-lg-12 col-xl-12";
		var gridPop02 = "col-sm-12 col-md-6 col-lg-6 col-xl-6";
		var gridPop03 = "col-sm-12 col-md-6 col-lg-4 col-xl-4";
		var gridPop04 = "col-sm-12 col-md-6 col-lg-4 col-xl-3";
		var gridPop05 = "col-sm-12 col-md-4 col-lg-3 col-xl-3";
		var mr_gridPopup;

		switch (avisosCliente) {
			case 1:
				mr_gridPopup = gridPop01;
				break;
			case 2:
				mr_gridPopup = gridPop02;
				break;
			case 3:
				mr_gridPopup = gridPop03;
				break;
			case 4:
				mr_gridPopup = gridPop04;
				break;
			case 5:
				mr_gridPopup = gridPop05;
				break;
			default:
				mr_gridPopup = gridPop01;
				break;
		}
		return mr_gridPopup
	};
	this.controleTipoGridPopUp = function(){
		var tipoGride;
		// this.tipoPopUp for true quer dizer que estamos no dashBoard 
		// this.flagIDPO e igual a null estamos na identificação Positiva
		console.log('this.flagIDPO',this.flagIDPO)
		// if(this.tipoPopUp!= null && this.tipoPopUp != undefined && this.tipoPopUp != false){
		// 	tipoGride =	"col-sm-12 col-md-12 col-lg-12 col-xl-12";
		// }
		// else {
		// 	tipoGride = this.quantidadePopUps();
		// }

		if(this.flagIDPO != null && this.flagIDPO != undefined && this.flagIDPO != false){
			tipoGride =	"col-sm-12 col-md-12 col-lg-12 col-xl-12";
		}
		else {
			tipoGride = this.quantidadePopUps();
			
		}



		return tipoGride;
	}
	this.controleInativo = function(){
		var diasCarenciaReanalise ;
		if(this.diasCarenciaReanaliseCliente == -1){
			diasCarenciaReanalise = 0;
		}else {
			diasCarenciaReanalise = this.diasCarenciaReanaliseCliente
		}
		var popUpInativo;
		var txtPopUp;
		if(this.inativoCliente == "S"){
			if(diasCarenciaReanalise > 0){
				txtPopUp = 'Nova reanálise em '+ diasCarenciaReanalise +' dias.';
			}else{
				txtPopUp = 'O cliente encontra-se inativo.';
			}
			popUpInativo = templatePopUp.popUpClienteInativo(this.controleTipoGridPopUp(),this.flagIDPO,txtPopUp);
		}else{
			popUpInativo = "";
		}
		return popUpInativo;
	}
	this.controleMensagem = function(){
		var msn = 1;
		var popUpMensagem;
		if(msn > 0){
			popUpMensagem = templatePopUp.popUpMensagemInterna(this.controleTipoGridPopUp(),this.flagIDPO,this.flagIDPO);
		}else {
			popUpMensagem = "";
		}
		return popUpMensagem;
	}
	this.controlePreAprovadoItau = function(){
		var popUpPreItau;
		if(this.statCodCreditoCliente == 1){
			popUpPreItau = templatePopUp.popUpClientePreAprovadoItau(this.controleTipoGridPopUp(),this.flagIDPO);
		}else {
			popUpPreItau = "";
		}
		
		return popUpPreItau;
	}
	this.renderPopUpCliente = function(){
		var grupoPopUps
		    grupoPopUps  = this.controleInativo();
			grupoPopUps += this.controleMensagem();
			grupoPopUps += this.controlePreAprovadoItau();

			// var grupoPopUps  = templatePopUp.popUpClienteInativo(this.controleTipoGridPopUp(),this.flagIDPO,txtPopUp);
			// grupoPopUps += templatePopUp.popUpMensagemInterna(this.controleTipoGridPopUp(),this.tipoPopUp,this.tipoPopUp);
			// grupoPopUps += templatePopUp.popUpClientePreAprovadoItau(this.controleTipoGridPopUp(),this.tipoPopUp);
			// grupoPopUps += templatePopUp.popUpClienteEmprestimoPessoal(this.controleTipoGridPopUp(),this.tipoPopUp);
			// grupoPopUps += templatePopUp.popUpClientePreAprovadoSac(this.controleTipoGridPopUp(),this.tipoPopUp);

		$(this.containerPopUps).html(grupoPopUps);
		this.eventosPopUp();
	}
	this.eventosPopUp = function(){
		$(this.containerPopUps).on('click', '.mr_botFechar', function(event) {
			event.preventDefault();
			var currentPopUp = $(this).attr('data-content');
			$('.mr_cardAviso-'+currentPopUp).remove();
			console.log('mr_botFechar')
		});
		$(this.containerPopUps).on('click', '.gravarLog', function(event) {
			textLogAuto = "Texto gerado automaticamente!!"
			lancamentosManuais.setLogAutomatico(textLogAuto);
			event.preventDefault();
			
			console.log('Gravar log')
		});
	};
}

var businessPopUp = new BusinessPopUp();