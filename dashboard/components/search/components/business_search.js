require("./components/search/components/template_search.js");
require("./components/search/components/service_search.js");
require("./components/controleEstadoComponente/init_ControleEstadoComponente.js");

function BusinessSearch() {
	this.load = function(container,campoCpfSearch,tipoSearch){
		this.container      = container;
		$(this.container).html(templateSearch.busca());
		this.getCampos();
		this.eventosComponente();
		
	};
	this.getCampos = function(campoCpfSearch,tipoSearch){
		this.campoCpfSearch = $('#cpfSearch');
		this.tipoSearch     = $('#tipoSearch');
	}
	this.acaoBuscar = function(){
		var self = this;
		if(this.campoCpfSearch.val() != undefined && this.campoCpfSearch.val().length > 0){
			if($('#tipoSearch').val() == 'CPF' && !validarCPF(this.campoCpfSearch.val())){

				// logging("Warning");
				marisaMessage.showDialog(marisaMessageType_WARNING, "Alerta","CPF inválido!");
				this.campoCpfSearch.focus();
				
				$('#panelCliente').empty();
				$('#panelStats').empty();
				$('#panelCards').empty();

				
				

			}else{
				waitingDialog.show('Processando...', {dialogSize : 'sm',progressType : 'info'});
				setTimeout(function () {
					// corrigi a url
					var windowURL = window.location.href;
					//logging(windowURL);
					if(windowURL.indexOf("cpf=") > -1){
						windowURL = windowURL.replace("cpf=" + getUrlVars()['cpf'], "cpf=" + addZeroEsquerda($("#cpfSearch").val().replace(/[^\d]+/g,""), 11) );
					}else{
						windowURL = windowURL + "&" + "cpf=" + addZeroEsquerda($("#cpfSearch").val().replace(/[^\d]+/g,""), 11);
					}
					//logging(windowURL);
					
					window.history.pushState({},"", windowURL);
					
					// $('#contentDadosCliente').empty();
					// $('#panelPrincipal').empty();
					// $('#panelCards').empty();
					console.log('BUSCA DASHBOARD!!')
					self.consultaServico();
					
				
					setTimeout(function () {
						logging("waitingDialog.hide()");
						waitingDialog.hide();
					}, 500);
		
				}, 300);
			}
		
		}else{
			cpf = '';
			marisaMessage.showDialog(marisaMessageType_WARNING, "Alerta","Preencha corretamente o campo de pesquisa!<br>Utilize um CPF ou número de Cartão.");
			$('#cpfSearch').focus();
		}
	};
	// função que manipula as infos recebidas pelo service
	this.consultaServico = function(){
		var search = {};
		search.tipo = this.tipoSearch[0].value; 
		search.value = this.campoCpfSearch[0].value; 
		var parameters = {
			"Search" : search
		};
		var result = serviceSearch.load(parameters);

		if (result["codigoRetorno"] == 200) {
			this.getInfosCliente(result);
		} 
		else if (result["codigoRetorno"] == 204) {
	        marisaMessage.showDialog(marisaMessageType_WARNING, "Alerta","Cliente não encontrado!");
	        
		} else {
			marisaMessage.showDialog(marisaMessageType_ERROR, "Erro","Falha ao buscar cliente!");
		}
	}
	this.getInfosCliente = function(objResult){
		var objInfosCliente = objResult["objectsReturn"]["ClientDashboard"];
		this.objInfosCliente = objInfosCliente;
		var currentDados = gravarDadosClienteDash.dadosDash();

		var currentCliCpf = currentDados.cpf;
		var NovoCpfCliente = this.objInfosCliente.cpf;
		
		if(currentCliCpf != NovoCpfCliente){
			
			var elementoRaiz = $('.referenciaModalcard');
			var apagarTokem = function(){
               return controllStorageSac.apagarLocalStorageHash();
            }
            var propsModal = {
                "elementoRaiz":elementoRaiz,
                "tipoModal":"acaodupla",
                "titulo":"Confirmação de Dados",
                "texto":"Novo Cpf estamos Redirecionando para identificação Positiva!",
                "funcao":apagarTokem
            }
            customModalSac.load(propsModal)



		}
		else {

			$('#tipoSearch').val('CPF'); 
			$('#cpfSearch').val(addZeroEsquerda(currentCliCpf,11)); 					
			$('#cpfSearch').unmask();
			$('#cpfSearch').mask("999.999.999-99");
			
		}
		return objInfosCliente;
	}
	this.retornoInfosTratadas = function(){
		var objInfosCliente = this.objInfosCliente;
		return objInfosCliente;
	}
	this.eventosComponente = function(){
		var self = this;
		$(this.container).on('click', '#btnSearch', function(event) {
			event.preventDefault();
			self.acaoBuscar()
		});
	};
}

var businessSearch = new BusinessSearch();