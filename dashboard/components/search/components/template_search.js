function TemplateSearch() {
	
	this.busca = function(){
		var _html ;
		_html = '<div class="referenciaModalcard"></div>';
		_html += '<div class="mr_naveBusca page-title clearfix row" style="margin: -5px -10px -10px -10px;padding: 0px 0px 0px 0px;">'; 
		_html += '<ul class="stats col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding: 0px 0px 0px 0px;list-style-type: none;"> ';
		_html += '<li id="menuPesquisa" class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="border-radius: 5px !important;background-color: #b3c9e0ab;"> ';
		_html += '<div class="details">';                        
		_html += '<div class="input-group" style="float: right;"> ';
		_html += '<div class="input-group-btn search-panel"> ';
		_html += '<select class="select_tipo_busca btn btn-default" id="tipoSearch" > ';
		_html += '<option value="">Buscar por:</option>'; 									 
		_html += '<option selected="" value="CPF">CPF</option>'; 									 
		_html += '<option value="CARTAO">CARTÃO</option> ';							 
		_html += '</select>';							 
		_html += '</div>';                            
		_html += '<input type="text" inputmode="tel" class="form-control" placeholder="Entre com o CPF" id="cpfSearch" style="height: 40px;font-size: 20px;border: 1px solid #f9cbe0;" onkeypress="return isNumberKey(event);">';                            
		_html += '<span id="clearSearch" class="glyphicon glyphicon-remove-circle"></span>'; 
		_html += '<input type="hidden" id="scoreCliente" >';
		_html += '<div class="input-group-btn" style="">';                                 
		_html += '<button id="btnSearch" class="btn btn-default" type="submit">'; 
		_html += '<i class="glyphicon glyphicon-search"></i>'; 
		_html += '</button>';                             
		_html += '</div>';                         
		_html += '</div>';                
		_html += '</div>';             
		_html += '</li>';                       
		_html += '</ul>';         
		_html += '</div> '; 
		return _html;
	}

}
var templateSearch = new TemplateSearch();