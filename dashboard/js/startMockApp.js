function StartMockApp() {
	this.load = function(){
		console.log('Start Mock App !!!')
		this.gravaInfosCliMock()
	}
	
	this.gravaInfosCliMock = function(){
		var cliCpf = "98538950720"
		var objCliEmAtendimento = {

				"buscaPorAdicional": "N",
				"celular": "21999247521",
				"cidadeUf": "DUQUE DE CAXIAS/RJ",
				"cliEmpSalario": 900,
				"cliIdade": "12",
				"cliIdeOrgaoEmi": "IFP",
				"cliIdeUfEmi": "RJ",
				"cliIdentidade": "07865709",
				"cliResDdd": "21",
				"cliResFone": "31683646",
				"cpf": cliCpf,
				"cpfUser": "11111111111",
				"dataCadastro": "13/01/2015",
				"dataUltimaAlteracao": "13/01/2015",
				"diaCorte": "21",
				"diaVencimento": "2",
				"diasCarenciaReanalise": "-1",
				"dtNasc": "10/06/1963",
				"email": " ",
				"endereco": "R DOS GIRASSOIS, sn",
				"estadoCivil": "SOLTEIRO",
				"filialRetirada": 39,
				"horaInicio": "13/08/2019 às 15:26:07",
				"identificado": true,
				"inativo": "N",
				"nome": "ALCENIR FERREIRA",
				"nomeMae": "MARIA PAULA FERREIA",
				"nomePai": "NAO DECLARADO",
				"numeroCartao": "6034752037851119",
				"numeroCelular": "21999247521",
				"objProtocolo": "obj",
				"origem": 39,
				"protocoloCliente": 22531738,
				"sexo": "F",
				"situacaoCartao": "CARTAO ATIVO",
				"situacaoCartaoCod": "1",
				"situacaoConta": "CONTA BLOQUEADA INADIMPLENCIA",
				"situacaoContaCod": "21",
				"situacaoEmbossamentoCod": "7",
				"ultFatSaldoLimite": "338.2",
				"ultFatTotalLimite": "350",
				"ultFatValor": "101.3",
				"ultFatVcto": "02/04/2016",
		}

		localStorage.setItem('infosCliEmAtendimento_'+cliCpf,JSON.stringify(objCliEmAtendimento));	
		// this.atendimentosRealizados(this.currentCpfCliente,this.dataCriacaoObj,this.numeroProtocolo)
	};
}

var startMockApp = new StartMockApp();

startMockApp.load()