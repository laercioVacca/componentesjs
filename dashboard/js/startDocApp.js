require("../resources/js/Util.js");
require("../resources/js/pssServices.js");
require("../resources/js/centralServicosCCMServicesRS.js");
require("../resources/js/centralServicosCCMServices.js");
require("../resources/js/marisaServices.js");
require("../resources/js/genericServices.js");
require("../resources/js/clientServices.js");
require("../resources/js/marisaMessages.js");
require("../resources/js/loadWebLink.js");

$(document).ready(function() {
	
	w3IncludeHTML();

	var contentPageSwagger = $('#panelSwagger');
	var contentPageFluxo   = $('#panelFluxo');
	setTimeout(function(){


		$('#navBarSac').on('click', '.docLinkPage', function(event) {
			event.preventDefault();
			var pageName = $(this).attr('href');
			console.log('pageName',pageName)
			if(pageName == "swaggerPage"){
				content = contentPageSwagger;
			}
			else{
				content = contentPageFluxo;
			}
			
			renderPage(content,pageName)
		});

	},200)
	
});
function renderPage(contentBox,nomePag){
	var boxPage = $('#panelDocPsf');
	console.log('pageName',boxPage)
	pagePath = './documentacaoPsf/swagger/'+nomePag;
	contentBox.load(pagePath,function(e){});
}
function require(script) {
    $.ajax({
        url: script,
        dataType: "script",
        async: false,           // <-- This is the key
        success: function () {
            // all good...
        },
        error: function () {
            throw new Error("Could not load script " + script);
        }
    });
}