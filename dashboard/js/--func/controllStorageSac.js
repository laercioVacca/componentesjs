
function ControllStorageSac(){
	this.load = function(){
		this.hashSac = new Date().getTime();
		this.gravacaoInitiStorage();
	};
	this.setFlagIDPO = function(){
		var flagIpo = {
			"initIdentificacaoPositiva":true
		}
		localStorage.setItem('setFlagIDPO',JSON.stringify(flagIpo));
	}
	this.removeFlagIDPO = function(){
		localStorage.removeItem('setFlagIDPO');
	}
	this.criacaoHashUnicaProcesso = function(tipoHash){
		var dateTime = +new Date();
		var timestamp = Math.floor((Math.random() * dateTime) / 1000);
		var hashPro = "";
		var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
		for (var i = 0; i < 6; i++){
			hashPro += possible.charAt(Math.floor(Math.random() * possible.length));
		}
		return tipoHash + timestamp + hashPro;
	}
	this.initAtendimento = function(obj){
		var objAtendimento = obj.objProtocolo; 
		console.log('objAtendimento STORAGE',objAtendimento)
		var dataPrt = objAtendimento.dtAberturaProtocolo;
		var dia = dataPrt.dayOfMonth;
		var mes = dataPrt.month;
		var ano = dataPrt.year;
		var dtCompleta = dia + '/'+ mes +'/'+ ano;
		var flag = {
			"flagTokem":this.flagTokem,
			"idServico":this.criacaoHashUnicaProcesso('srv_'),
			"idProcesso":this.criacaoHashUnicaProcesso('prc_'),
			"atendimentoInfos":{
				"idProtocolo":objAtendimento.numProtocolo,
				"dataAberturaProtocolo":dtCompleta
			},
			
			"showIdenticacaoPositiva":true
		}
		localStorage.setItem('ATEND_'+webLink.token,JSON.stringify(flag));
	}
	this.verificarFlag = function(){
		this.flagTokem = webLink.token;
		var flagPsi = false;
		this.removeFlagIDPO()
		var f = JSON.parse(localStorage.getItem('ATEND_'+webLink.token));
		if(f!= null && f != undefined && f != false){
			if(this.controleFluxo(f)){
				this.setFlagIDPO();
				var flagPsi = true;
			}else{
				this.removeFlagIDPO()
				localStorage.removeItem('ATEND_'+webLink.token)
				var flagPsi = false;
			}
		}
		return flagPsi;
	}
	this.controleFluxo = function(f){
		var liberarFluxo = false;
		if(f.flagTokem == webLink.token){
			var liberarFluxo = true;
		}else{

			var liberarFluxo = false;
		}
		return liberarFluxo;
	}
	this.apagarLocalStorageHash = function(){
		localStorage.removeItem('hashAtendimento_'+webLink.cpfUsuario);
		window.location.reload();
	}
	this.verificacaoStorageAtendimento = function(webLinkOldTokem){
		var atendimentoId = JSON.parse(localStorage.getItem('ATEND_'+webLinkOldTokem));
		return atendimentoId;
	}
	this.limparStorage = function(){
		 localStorage.clear();
	}
	// Gravar infos cliente em atendimento
	this.gravaInfosCliEmAtendido = function(obj){
		this.dadosCliDash = obj
		console.log('this.dadosCliDash dentro do controle storage',this.dadosCliDash)
		this.currentCpfCliente = this.dadosCliDash.cpf;
		var objCliEmAtendimento = {
			"nome":obj.nome,
			"cpf":obj.cpf,
			"cliIdade":"12",
			"situacaoConta":obj.situacaoConta ,
			"situacaoContaCod":obj.situacaoContaCod ,
			"numeroCartao":obj.numeroCartao ,
			"situacaoCartao":obj.situacaoCartao,
			"situacaoCartaoCod":obj.situacaoCartaoCod,
			"situacaoEmbossamentoCod":obj.situacaoEmbossamentoCod,
			"buscaPorAdicional":obj.buscaPorAdicional,
			"endereco":obj.endereco,
			"cidadeUf":obj.cidadeUf,
			"celular":obj.celular,
			"email":obj.email,
			"dtNasc":obj.dtNasc,
			"dataCadastro":obj.dtCadastro,
			"dataUltimaAlteracao":obj.dtUltAlteracao,
			"melhorDia":obj.diaVencimento,
			"sexo":obj.sexo,
			"origem":obj.filCod,
			"filialRetirada":obj.filCodRetirada,
			"nomeMae":obj.nmMae,
			"cliResDdd":obj.cliResDdd,
			"cliResFone":obj.cliResFone,
			"cliIdentidade":obj.cliIdentidade,
			"cliIdeOrgaoEmi":obj.cliIdeOrgaoEmi,
			"cliIdeUfEmi":obj.cliIdeUfEmi,
			"cliEmpSalario":obj.cliEmpSalario,
			"inativo":obj.inativo,
			"diasCarenciaReanalise":obj.diasCarenciaReanalise,

		}
		localStorage.setItem('infosCliEmAtendimento',JSON.stringify(objCliEmAtendimento));	
	};
	// pegar as infos do cliente en atendimento
	this.getinfosCliEmAtendimento = function(){
		this.dadosCliEmAtendimento = JSON.parse(localStorage.getItem('infosCliEmAtendimento'));
		return this.dadosCliEmAtendimento
	}
	this.getDataFlagAtendimento = function(){
		var dataCurrentAtendimento = this.verificacaoStorageAtendimento(this.flagTokem);
		return dataCurrentAtendimento
	}


	
}
var controllStorageSac = new ControllStorageSac()


