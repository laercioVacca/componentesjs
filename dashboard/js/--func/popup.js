function PopUp(){
	this.render = function(elemento,nomeServico){
		var go = this.montar(elemento,nomeServico);
		return go;
	};
	this.montar = function(elemento,nomeServico){
		var maskPopup = document.createElement('div');
		$(maskPopup).addClass('maskPopup');
		var elPai = $(elemento).closest('.cardReferenciaMaster');
		var boxPrincipal = $(".cardReferenciaMaster");
		var elementoSecundario = $(elemento);
		var boundaries = {
		  principal: boxPrincipal.offset(),
		  secundario: elementoSecundario.offset()
		}
		console.log({
		  x: boundaries.secundario.top - boundaries.principal.top,
		  y: boundaries.secundario.left - boundaries.principal.left,
		})
		var elLeft = boundaries.secundario.left - boundaries.principal.left;
		var psTop  = boundaries.secundario.top - boundaries.principal.top;
		var difAltura = psTop - 90;
		var self = this;
		$.each(this.popupProps(), function(index, val) {
			 console.log('PROPS EACH',index,val.titulo)
			 if(index == nomeServico){
			 	elPai.append(self.template(difAltura,elLeft,val.titulo,val.texto));
			 }
			 
		});
		$("#btnFecharinfos").on('click', function(event) {
			event.preventDefault();
			$("#popupVantagens2").remove();
		});
		return;
	};
	this.template = function(elTop,elLeft,titulo,texto){
		var tituloPopUp;
		var html;
			html = '<div id="popupVantagens2" class="card bg-light mb-3 custom_card" style="top:'+elTop+'px;left:150px">';
			html += '<div class="card-header bg-new-color">';
			html += '<span class="iconeTitulo glyphicon glyphicon-ok-sign"></span>';
			html += '<span class="titulo_card_custom">'+titulo+'</span>';
			html += '</div>';
			html += '<div class="card-body">';
			html += '<div class="card-text card_custom_texto" style="border-bottom:none">';
			html += '<div id="marcadorServico"></div>';
			html += '<span class="descricao">';
			html += texto; 
			html += '</span>';
			html += '</div>';
			html += '</div>';
			html += '<div class="card-footer card_custom_saiba_mais">';
			html += '<a id="btnFecharinfos" href="#" class="btn btn-info" >';
			html += '<span style="top:3px" class="glyphicon glyphicon-remove"></span>';
			html += '<span class="tit_btn">Fechar</span> ';
			html += '</a>';
			html += '<div style="clear:both"></div>';
			html += '</div>';
			html += '</div>';
		return html;
	};
	this.popupProps = function(){
		
		var props = {
			"protecaocelular":{
				"titulo":"Proteção Celular",
				"texto":this.infosProtecaoCelular()
			},
			"protecaofinanceira":{
				"titulo":"Proteção Financeira",
				"texto":this.infosProtecaoFinaceira()
			},
			"bolsaprotegida":{
				"titulo":"Bolsa Protegida",
				"texto":this.infosBolsaProtegida()
			},
		}
		return props;
	}
	this.infosProtecaoCelular = function(){
		var txtProtecaoCelular;
		txtProtecaoCelular  = '<h3 style="margin-top:0px">Seguro de Aparelhos Eletrônicos</h3>';
		txtProtecaoCelular += '<p>Agora não tem hora nem lugar para você estar conectada e usar seu celular com tranquilidade</p>';
		txtProtecaoCelular += '<p>O seguro protege seu aparelho contra roubo, furto qualificado mediante arrombamento e quebra acidental.</p>';
		txtProtecaoCelular += '<p>Carência: você estará protegida após 30 dias da contratação.</p>';
		txtProtecaoCelular += '<p>Franquia: se algum imprevisto acontecer, você deverá pagar somente 25% do limite máximo de indenização descrito no seu bilhete.</p>';
		return txtProtecaoCelular;
	}
	this.infosBolsaProtegida = function(){
		var txtBolsaProtegida;
		txtBolsaProtegida  = '<h3 style="margin-top:0px">Seguro de Perda ou Roubo do Cartão Marisa</h3>';
		txtBolsaProtegida += '<p>Agora você pode andar mais tranquila Proteja sua bolsa e seu cartão Marisa.</p>';
		txtBolsaProtegida += '<p>Com o seguro Bolsa Protegida, você tem proteção em até R$ 2.000,00 em transações</p>'; 
		txtBolsaProtegida += '<p>indevidas efetuadas com seu cartão Marisa (efetuadas em até 96 horas anteriores à comunicação).</p>';
		txtBolsaProtegida += '<p>E até R$ 500,00 de indenização por roubo ou furto qualificado de sua bolsa e seus pertences***.</p>';
		txtBolsaProtegida += '<p>Além de concorrer toda semana à R$1.500,00* pela loteria federal.</p>';
		txtBolsaProtegida += '<p>Tudo isso pagando apenas';
		txtBolsaProtegida += '<strong> R$ 5,99** /mês</strong></p>';
		return txtBolsaProtegida
	};
	this.infosProtecaoFinaceira = function(){
		var txtProtecaoFinaceira;
		txtProtecaoFinaceira  = '<h3 style="margin-top:0px">Você preparada quando for pega de surpresa</h3>'
		txtProtecaoFinaceira += '<p>Microsseguro perda de renda</p>';
		txtProtecaoFinaceira += '<p>Fique mais tranquila caso tenha imprevistos.</p>';
		txtProtecaoFinaceira += '<p>Em caso de desemprego involuntário, você receberá até 5 parcelas de R$ 250,00.</p>';
		txtProtecaoFinaceira += '<p>Indenização de R$2.000,00 em caso de invalidez permanente total por acidente ou morte.</p>';
		txtProtecaoFinaceira += '<p>Apenas 30 dias de carência* e sem franquia!</p>';
		txtProtecaoFinaceira += '<p>Além de concorrer toda semana à R$5.000,00* pela loteria federal.</p>';
		return txtProtecaoFinaceira;
	}

}	

var popUp = new PopUp()
