
function Components(){
	var el, i, elemento, file;
  	this.render = function(){
  		var panelControllDashboard = document.querySelector('#panelControllDashboard');
		$(panelControllDashboard).show();
  		el = document.getElementsByTagName("*");
	  	for (i = 0; i < el.length; i++) {
	    	elemento = el[i];
	    	file = elemento.getAttribute("nome-cp");
	 		this.componentPath = './components/'+file+'/'+file+'.html';
	    	if (file) {
	      		// console.log('Arquivos con data encontardos',elemento);
	      		$(elemento).load(this.componentPath,function(e){
					// console.log('Retorno Load!!',e);
				});
	    	}
	  	}
	}
	return;
}
function RenderPages(){
	this.render = function(page,dataCustomTarget){
		var contentPrincipal = document.querySelector('#panelDashboard');
		this.pagePath = './components/page/'+page;
		this.dataCustomTarget = dataCustomTarget;
		$(contentPrincipal).load(this.pagePath,this.dataCustomTarget,function(e){
	});
	return
	}
}



function PageIdentificacaoPositiva(){
	this.render = function(){
		var boxPage = document.querySelector('#panelIdentificacaoPositiva');
		this.identificacaoPath = './components/identificacao_positiva/identificacao_positiva.html';
		$(boxPage).load(this.identificacaoPath,function(e){
			// console.log('Retorno Load!!',e);
		});
	}
}
function PageDashBoard(){
	this.render = function(){
		var boxPage = $('#panelPrincipal');
		this.identificacaoPath = './components/dashBoard/dashBoard.html';
		$(boxPage).load(this.identificacaoPath,function(e){
			// console.log('Retorno Load!!',e);
		});
	}
}
function RenderPageComponente(){
	this.render = function(componenteNome,pageNome){
		this.componenteNome = componenteNome;
		var contentRender = $('#panelDashboard');
		this.pagePath = './components/'+componenteNome+'/'+pageNome+'.html';
		var self = this;
		$(contentRender).load(this.pagePath,function(e){
			// setCurrentPage.setPageName(self.componenteNome);
		});
	}
}



var renderPageComponente = new RenderPageComponente();



var pageIdentificacaoPositiva = new PageIdentificacaoPositiva()
var pageDashBoard = new PageDashBoard();
var renderPages = new RenderPages();
var components = new Components();

