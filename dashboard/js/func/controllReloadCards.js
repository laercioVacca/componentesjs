/*
* Controle de recarregamento dos cards do DashBoard.
* Quando quisermos que um card do dahsboard recarregue
* devemos chamar a função renderDashBoard.load('nomedocard');
* passando o nome do card como parametro
* o nome do card deve ser o mesmo nome do id do box que e carregado o card

 Função que faz o reload do local storage, fazendo uma nova consulta e atualizando o stotage
 reloadLocalStorageSac.load();
*/
function ControllReloadCards(){
    if (!(this instanceof ControllReloadCards)){
        return new ControllReloadCards();
    }
    this.load = function(container,nomeCard){
      this.containerCard  = container;
      this.nomeCardReload = nomeCard;
      
      this.containerCard.addClass('cardReload');
      this.atualizarComponent();
    }
    this.atualizarCardAcordo = function(){
        this.containerCard.html(cardCobranca.getTemplate('reload'));
        cardCobranca.load();
    }
    this.atualizarInfosCliHeader = function(){
      var dadosCurrentCliente = controllStorageSac.getinfosCliEmAtendimento();
      initDadosCliente.load(dadosCurrentCliente);
    }
    this.cardServicosDisponiveis = function(){
      this.containerCard.html(businessServicos.loadCardServicosDisponiveis('reload'));
      businessServicos.loadEventosCard();
    }
    this.cardServicosContratados = function(){
      this.containerCard.html(businessServicos.loadCardServicosContratados('reload'));
      businessServicos.loadEventosCard();
    }
    this.infosCartaoCliHeader = function(){

    }
    this.cardBonificacao = function(){

    }

    this.atualizarComponent = function(){
        switch (this.nomeCardReload) {
          case 'cardCobranca':
            this.atualizarCardAcordo();
            break;
          case 'dadosCliente':
            this.atualizarInfosCliHeader();
            break;
          case 'cardServicosContratados':
            this.cardServicosContratados();
            break;
          case 'cardServicosDisponiveis':
            this.cardServicosDisponiveis();
            break;
          case 'cardBonificacao':
            this.atualizarCardBonificacao();
            break;
          default:
            // statements_def
            break;
        }
    }
}

if(controllReloadCards == undefined || controllReloadCards == null){
  var controllReloadCards = new ControllReloadCards();
}
