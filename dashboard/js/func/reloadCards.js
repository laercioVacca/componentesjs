
function ReloadCards() {
	if (!(this instanceof ReloadCards)){
        return new ReloadCards();
    }
	this.load = function(){
		// this.masterCards = $('#panelDashboard');
		// this.masterCards.empty();

		var panelControllDashboard,
		compProcedimento,
		panelAtendimento,
		currentPageName,
		containerPrincipal
		

		this.carregaCardsDashBoard();
		this.eventosDash();
	}

	this.carregaCardsDashBoard = function(){
		servicosInstance.load();
		var agrupamentoCardsDash;
		var panelCardsServicos = $('#panelCardsServicos');
		var ConteudoCard = document.createElement('div');
		ConteudoCard.setAttribute('class','cardReferenciaMaster row');

		agrupamentoCardsDash  = cardHistoricoChamados.getTemplate();
		agrupamentoCardsDash += cardCartaoBonus.getTemplate();
		agrupamentoCardsDash += cardCobranca.getTemplate();
		
		// agrupamentoCardsDash += cardInformacoesFinanceiras.getTemplate();
		// agrupamentoCardsDash += cardConsultaFaturas.getTemplateAtual();
		// agrupamentoCardsDash += cardConsultaFaturas.getTemplateHistorico();
		// agrupamentoCardsDash += cardMemoFile.getTemplate();
		// agrupamentoCardsDash += plasticosInstance.getTemplateCard();
		// agrupamentoCardsDash += businessServicos.loadCardServicosDisponiveis();
		// agrupamentoCardsDash += businessServicos.loadCardServicosContratados();

		ConteudoCard.innerHTML = agrupamentoCardsDash;
		// panelCardsServicos.append(ConteudoCard);
		panelCardsServicos.html(ConteudoCard);

		cardHistoricoChamados.load();
		cardCartaoBonus.load();
		cardCobranca.load();
		// cardInformacoesFinanceiras.load();
		// cardMemoFile.load();
		// plasticosInstance.loadCard();
		

		
	}
	

	this.limpaComponets = function(){
		var compProcedimento    = $('#panelProcedimentos');
		var panelAtendimento    = $('#panelAtendimento');
		var contentDadosCliente = $('#contentDadosCliente')
		$(compProcedimento).empty();
		$(panelAtendimento).empty();
		$(contentDadosCliente).empty();
	}
	
	this.eventosDash = function(){
		$('#panelCardsServicos').on('click','.renderPage',  function(event) {
            event.preventDefault();
            event.stopPropagation();
            var pageName = $(this).attr('href');
            var componenteNome = $(this).attr('href');
            // ClearApp.clear('#panelDashboard');

            renderPageComponente.render(componenteNome,pageName);
        });
		
		$(function () {
			  $('[data-toggle="tooltip"]').tooltip();
		});
		
	}

}

if(reloadCards == undefined || reloadCards == null){
	var reloadCards = new ReloadCards();
}
