function CustomModalSac() {
	this.load = function(propsModal){
		this.propsModal = propsModal;
		this.isServico    = this.propsModal.modalServico;//Parametro que indica que este modal retorna response de servico
		this.modalCard    = this.propsModal.modalcard;
		this.modalPage    = this.propsModal.modalpage;
		this.callbackFunc = this.propsModal.callback;
		this.construtorModal();
		return;
	};
	this.construtorModal = function(){
		var elementoRaiz = this.propsModal.elementoRaiz;
		
		var tituloModal  = this.propsModal.titulo;
		var textoModal   = this.propsModal.texto;
		var tipoModal    = this.propsModal.tipoModal;

		this.propsModal.callback;


		$(elementoRaiz).html("")
		var content = document.createElement('div');
		$(elementoRaiz).append(this.template(tipoModal,tituloModal,textoModal));
		
		this.acaoPositivaModal();
		this.acaoNegativaModal();
		$('#ModalPadraoSac').modal({
		  keyboard: false
		})
		$('#ModalPadraoSac').modal('show');
	}
	this.template = function(tipoModal,titulo,texto){
		var html;
			html  = '<div id="ModalPadraoSac" class="modal fade modalSAC" tabindex="-1" role="dialog">';
			html += '<div class="modal-dialog modal-dialog-centered" role="document">';
			html += '<div id="modalContent" class="modal-content">';
			html += this.templatesConteudoModal(tipoModal,titulo,texto);
			html += '</div>';
			html += '</div>';
			html += '</div>';
		return html;
	};
	this.templatesConteudoModal = function(tipoModal,titulo,texto){
		var conteudoModal;
		var self = this;
		switch (tipoModal) {
			case "acaodupla":
				conteudoModal = conteudoModalAcaoDupla();
				break;
			case "cadastro":
				conteudoModal = conteudoModalCadastro();
				break;
			case "cadastroCelular":
				conteudoModal = conteudoModalCadastroCelular();
				break;
			case "modalPdf":
				conteudoModal = conteudoModalPdf();
				break;
			case "modalPersonalizado":
				conteudoModal = conteudoModalPersonalizado();
				break;
			case "modalEdicao":
				conteudoModal = conteudoModalEdicao();
				break;
			case "modalCancelarEdicao":
				conteudoModal = conteudoModalCancelarEdicao();
				break;
			case "modalCancelarServico":
				conteudoModal = conteudoModalCancelarServico();
				break;
			default:
				conteudoModal = conteudoModalGenerico();
				break;
			}
		function conteudoModalAcaoDupla(){
			var html;
				html  = '<div class="modal-header" id="messageHeader" style="display: block; background-color: rgba(33, 129, 212, 0.45);">';
				html += '<button type="button" id="btntesteevento" class="close" data-dismiss="modal" aria-label="Close" style="padding-top: 0px;"><span aria-hidden="true">×</span></button>';
				html += '<h4 id="messageTitle" class="modal-title">'+titulo+'</h4>';
				html += '</div>';
				// header modal
				html += '<div class="modal-body" style="display: inline-flex;font-size: 14px;">';
				html += '<h1 style="margin-top: 4px;"><i id="dialog-icon" style="color: rgba(33, 129, 212, 0.95);" class="fa fa-question-circle col-lg-1"></i></h1>';
				html += '<div>';
				html += '<label id="dialog-message" style="padding-top: 7px;">';
				html += '<div class="form-inline has-feedback">';
				html += '<span>'+texto+'</span>';
				html += '</div>';					
				html += '</label>';
				html += '</div>';
				html += '</div>';
				// content Modal
				html += '<div class="modal-footer" style="background-color: rgb(243, 243, 243);padding: 10px;">';
				html += '<button type="button" id="btnContratar" class="btn btn-primary" style="font-size: 14px;">SIM</button>';
				html += '<button type="button" id="btnNaoContratar" style="font-size: 14px;" onclick="" class="btn btn-default">Não</button>';
				html += '</div>';
				// footer Modal
			return html;
		}
		// 
		function conteudoModalCadastro(){
			var html;
				html  = '<div class="modal-header" id="messageHeader" style="display: block; background-color: rgba(33, 129, 212, 0.45);">';
				html += '<button type="button" id="btntesteevento" class="close" data-dismiss="modal" aria-label="Close" style="padding-top: 0px;"><span aria-hidden="true">×</span></button>';
				html += '<h4 id="messageTitle" class="modal-title">'+titulo+'</h4>';
				html += '</div>';
				// header modal
				html += '<div class="modal-body" style="display: inline-flex;font-size: 14px;">';
				html += '<h1 style="margin-top: 4px;"><i id="dialog-icon" style="color: rgba(33, 129, 212, 0.95);" class="fa fa-question-circle col-lg-1"></i></h1>';
				html += '<div>';
				html += '<label id="dialog-message" style="padding-top: 7px;">';
				html += '<div class="form-inline has-feedback">';
				html += '<span>'+texto+'';
				html += '<div class="form-group">';
				html += '<input type="email" class="form-control" id="campoaCadastroEmail" aria-describedby="emailHelp" placeholder="Email" style="width:100%;margin-top:8px">';
				html += '</div>';
				html += '</span>';
				html += '</div>';					
				html += '</label>';
				html += '</div>';
				html += '</div>';
				// content Modal
				html += '<div class="modal-footer" style="background-color: rgb(243, 243, 243);padding: 10px;">';
				html += '<button type="button" cadastro-email="" id="btnContratar" class="btn btn-primary" style="font-size: 14px;">Salvar</button>';
				html += '<button type="button" id="btnNaoContratar" style="font-size: 14px;" onclick="" class="btn btn-default">Cancelar</button>';
				html += '</div>';
				// footer Modal
			return html;
		}
		// 
		function conteudoModalCadastroCelular(){
			var html;
				html  = '<div class="modal-header" id="messageHeader" style="display: block; background-color: rgba(33, 129, 212, 0.45);">';
				html += '<button type="button" id="btntesteevento" class="close" data-dismiss="modal" aria-label="Close" style="padding-top: 0px;"><span aria-hidden="true">×</span></button>';
				html += '<h4 id="messageTitle" class="modal-title">'+titulo+'</h4>';
				html += '</div>';
				// header modal
				html += '<div class="modal-body" style="display: inline-flex;font-size: 14px;">';
				html += '<h1 style="margin-top: 4px;"><i id="dialog-icon" style="color: rgba(33, 129, 212, 0.95);" class="fa fa-question-circle col-lg-1"></i></h1>';
				html += '<div>';
				html += '<label id="dialog-message" style="padding-top: 7px;">';
				html += '<div class="form-inline has-feedback">';
				html += '<span>'+texto+'';
				html += '<div class="form-group">';
				html += '<input type="email" class="form-control" id="campoaCadastroCeluDDD" placeholder="DDD" style="width:15%; float:left;margin:8px 8px 0 0">';
				html += '<input type="email" class="form-control" id="campoaCadastroCeluNumero" placeholder="Celular" style="width:75%;float:left;margin-top:8px">';
				html += '</div>';
				html += '</span>';
				html += '</div>';					
				html += '</label>';
				html += '</div>';
				html += '</div>';
				// content Modal
				html += '<div class="modal-footer" style="background-color: rgb(243, 243, 243);padding: 10px;">';
				html += '<button type="button" cadastro-email="" id="btnContratar" class="btn btn-primary" style="font-size: 14px;">Salvar</button>';
				html += '<button type="button" id="btnNaoContratar" style="font-size: 14px;" onclick="" class="btn btn-default">Cancelar</button>';
				html += '</div>';
				// footer Modal
			return html;
		}
		//
		function conteudoModalEdicao(){
			var html;
				html  = '<div class="modal-header" id="messageHeader" style="display: block; background-color: rgba(33, 129, 212, 0.45);">';
				html += '<button type="button" id="btntesteevento" class="close" data-dismiss="modal" aria-label="Close" style="padding-top: 0px;"><span aria-hidden="true">×</span></button>';
				html += '<h4 id="messageTitle" class="modal-title">'+titulo+'</h4>';
				html += '</div>';
				// header modal
				html += '<div class="modal-body" style="display: inline-flex;font-size: 14px;">';
				html += '<h1 style="margin-top: 4px;"><i id="dialog-icon" style="color: rgba(33, 129, 212, 0.95);" class="fa fa-question-circle col-lg-1"></i></h1>';
				html += '<div>';
				html += '<label id="dialog-message" style="padding-top: 7px;">';
				html += '<div class="form-inline has-feedback">';
				html += '<span>'+texto+'</span>';
				html += '</div>';					
				html += '</label>';
				html += '</div>';
				html += '</div>';
				// content Modal
				html += '<div class="modal-footer" style="background-color: rgb(243, 243, 243);padding: 10px;">';
				html += '<button type="button" id="btnContratar" class="btn btn-primary" style="font-size: 14px;">SIM</button>';
				html += '<button type="button" id="btnNaoContratar" style="font-size: 14px;" onclick="" class="btn btn-default">Não</button>';
				html += '</div>';
				// footer Modal
			return html;
		}
		//
		function conteudoModalCancelarEdicao(){
			var html;
				html  = '<div class="modal-header" id="messageHeader" style="display: block; background-color: rgba(33, 129, 212, 0.45);">';
				html += '<button type="button" id="btntesteevento" class="close" data-dismiss="modal" aria-label="Close" style="padding-top: 0px;"><span aria-hidden="true">×</span></button>';
				html += '<h4 id="messageTitle" class="modal-title">'+titulo+'</h4>';
				html += '</div>';
				// header modal
				html += '<div class="modal-body" style="display: inline-flex;font-size: 14px;">';
				html += '<h1 style="margin-top: 4px;"><i id="dialog-icon" style="color: rgba(33, 129, 212, 0.95);" class="fa fa-question-circle col-lg-1"></i></h1>';
				html += '<div>';
				html += '<label id="dialog-message" style="padding-top: 7px;">';
				html += '<div class="form-inline has-feedback">';
				html += '<span>'+texto+'</span>';
				html += '</div>';					
				html += '</label>';
				html += '</div>';
				html += '</div>';
				// content Modal
				html += '<div class="modal-footer" style="background-color: rgb(243, 243, 243);padding: 10px;">';
				html += '<button type="button" id="btnContratar" class="btn btn-primary" style="font-size: 14px;">SIM</button>';
				html += '<button type="button" id="btnNaoContratar" style="font-size: 14px;" onclick="" class="btn btn-default">Não</button>';
				html += '</div>';
				// footer Modal
			return html;
		}
		//
		function conteudoModalPersonalizado(){
			var conteudoPersonalizado = self.propsModal.conteudoPersonalizado;
			
			var html;
				html  = '<div class="modal-header" id="messageHeader" style="display: block; background-color: rgba(33, 129, 212, 0.45);">';
				html += '<button type="button" id="btntesteevento" class="close" data-dismiss="modal" aria-label="Close" style="padding-top: 0px;"><span aria-hidden="true">×</span></button>';
				html += '<h4 id="messageTitle" class="modal-title">'+titulo+'</h4>';
				html += '</div>';
				// header modal
				html += '<div class="modal-body" style="display: inline-flex;font-size: 14px;">';
				html += conteudoPersonalizado;
				// html += '<h1 style="margin-top: 4px;"><i id="dialog-icon" style="color: rgba(33, 129, 212, 0.95);" class="fa fa-question-circle col-lg-1"></i></h1>';
				// html += '<div>';
				// html += '<label id="dialog-message" style="padding-top: 7px;">';
				// html += '<div class="form-inline has-feedback">';
				// html += '<span>'+texto+'</span>';
				// html += '</div>';					
				// html += '</label>';
				// html += '</div>';

				html += '</div>';
				// content Modal
				html += '<div class="modal-footer" style="background-color: rgb(243, 243, 243);padding: 10px;">';
				html += '<button type="button" id="btnContratar" class="btn btn-primary" style="font-size: 14px;">SIM</button>';
				html += '<button type="button" id="btnNaoContratar" style="font-size: 14px;" onclick="" class="btn btn-default">Não</button>';
				html += '</div>';
				// footer Modal
			return html;
		}
		// 
		function conteudoModalPdf(){
			var html;
				html  = '<div class="modal-header" id="messageHeader" style="display: block; background-color: rgba(33, 129, 212, 0.45);">';
				html += '<button type="button" id="btntesteevento" class="close" data-dismiss="modal" aria-label="Close" style="padding-top: 0px;"><span aria-hidden="true">×</span></button>';
				html += '<h4 id="messageTitle" class="modal-title">'+titulo+'</h4>';
				html += '</div>';
				html += '<div class="modal-body" style="display: inline-flex;font-size: 14px;">';
				html += '<object id="faturaPdfobj" width="100%" height="400" type="application/pdf" data="pdf/fatura.pdf">';
				html += '<embed class="fatura_show" src="pdf/fatura.pdf" width="100%" height="400" type="application/pdf">';
				html += '</object>';
				html += '</div>';
				html += '<div class="modal-footer" style="background-color: rgb(243, 243, 243);padding: 10px;">';
				html += '<button type="button" id="btnNaoContratar" style="font-size: 14px;" onclick="" class="btn btn-default">Fechar</button>';
				html += '</div>';
			return html;
		}
		// conteudoModalGenerico
		function conteudoModalGenerico(){
			var html;
				html  = '<div class="modal-header" id="messageHeader" style="display: block; background-color: rgba(33, 129, 212, 0.45);">';
				html += '<button type="button" id="btntesteevento" class="close" data-dismiss="modal" aria-label="Close" style="padding-top: 0px;"><span aria-hidden="true">×</span></button>';
				html += '<h4 id="messageTitle" class="modal-title">'+titulo+'</h4>';
				html += '</div>';
				html += '<div class="modal-body" style="display: inline-flex;font-size: 14px;">';
				html += '<h1 style="margin-top: 4px;"><i id="dialog-icon" style="color: rgba(33, 129, 212, 0.95);" class="fa fa-question-circle col-lg-1"></i></h1>';
				html += '<div>';
				html += '<label id="dialog-message" style="padding-top: 7px;">';
				html += '<div class="form-inline has-feedback">';
				html += '<span>'+texto+'</span>';
				html += '</div>';					
				html += '</label>';
				html += '</div>';
				html += '</div>';
				html += '<div class="modal-footer" style="background-color: rgb(243, 243, 243);padding: 10px;">';
				html += '<button type="button" id="btnNaoContratar" style="font-size: 14px;" onclick="" class="btn btn-default">Fechar</button>';
				html += '</div>';
			return html;
		}
		//Modal de Cancelamneto de servico
		function conteudoModalCancelarServico(){
			var html;
			html  = '<div class="modal-header" id="messageHeader" style="display: block; background-color: rgba(33, 129, 212, 0.45);">';
			html += '<button type="button" id="btntesteevento" class="close" data-dismiss="modal" aria-label="Close" style="padding-top: 0px;"><span aria-hidden="true">×</span></button>';
			html += '<h4 id="messageTitle" class="modal-title">'+titulo+'</h4>';
			html += '</div>';
			// header modal
			html += '<div class="modal-body" style="display: inline-flex;font-size: 14px;">';
			html += '<h1 style="margin-top: 4px;"><i id="dialog-icon" style="color: rgba(33, 129, 212, 0.95);" class="fa fa-question-circle col-lg-1"></i></h1>';
			html += '<div>';
			html += '<label id="dialog-message" style="padding-top: 7px;">';
			html += 'Deseja realmente cancelar o contrato do serviço abaixo?<br><br>';
			html += 'Serviço: PROTEÇÃO FINANCEIRA<br>';
			html += 'Valor Premio: R$ 10,90<br>';
			html += 'Valor Cobertura: R$ 0,00<br>';
			html += 'Aquisição: 07/04/2016<br><br>';
			html += '<div class="form-inline has-feedback">';	
			html += '<label class="control-label" style="margin-top: 0px;" for="motivoSolicitacaoCancelamento">Motivo Solicitação do Cancelamento</label><br>';		
			html += '<select class="form-control" id="motivoSolicitacaoCancelamento" style="width: 100%;" required="required" data-error="Valor inválido!" pattern="^[123]{0,1}$"> ';			
			html += '<option value="">Selecione...</option>'; 			
			html += '<option value="1132">Obito</option>'; 			
			html += '<option value="1143">Arrependimento em sete dias</option>'; 			
			html += '<option value="1144">Acordo</option> ';			
			html += '<option value="1174">Problemas com atendimento na Assurant</option> ';			
			html += '<option value="913">Obito</option>'; 			
			html += '<option value="917">Arrependimento em sete dias</option>'; 			
			html += '<option value="919">Acordo</option> ';			
			html += '<option value="915">Problemas com atendimento na Assurant</option>'; 			
			html += '<option value="1077">Não solicitou</option> ';	 		
			html += '</select>';
			html += '<div class="help-block with-errors" style="display: inline;"></div>';
			html += '</div>';
			html += '</label>';
			html += '</div>';
			html += '</div>';
			// content Modal
			html += '<div class="modal-footer" style="background-color: rgb(243, 243, 243);padding: 10px;">';
			html += '<button type="button" id="btnContratar" class="btn btn-primary" style="font-size: 14px;">SIM</button>';
			html += '<button type="button" id="btnNaoContratar" style="font-size: 14px;" onclick="" class="btn btn-default">Não</button>';
			html += '</div>';
			// footer Modal
			return html;
		}
		return conteudoModal;
	}
	this.acaoPositivaModal = function(){
		var self = this;
		var disparaAcaoPositivaModal = $('#btnContratar').on('click', function(event) {
			event.preventDefault();
			console.log('OPA VAI AQUI HOJE',self.isServico)
			if(self.propsModal.funcao){
				if(self.isServico){
					var respostaServico = self.propsModal.funcao.call();
					console.log('mostar retorno Servico!!',respostaServico)
					self.completarAcaoRetornoServico(respostaServico);
				}else{
					self.propsModal.funcao.call();
					self.completarAcao();
				}
			}else {
				self.retornoAcaoPositiva();
			}
		});
		return disparaAcaoPositivaModal;
	};
	this.acaoNegativaModal = function(){
		var self = this;
		var disparaAcaoNegativaModal = $('#btnNaoContratar').on('click', function(event) {
			event.preventDefault();
			var flagFinalizar = $(this).attr('data-finalizaAcao');
			self.retornoAcaoNegativa(flagFinalizar);
		});
		return disparaAcaoNegativaModal;
	};
	this.retornoAcaoPositiva = function(){
		this.completarAcao();
		return;
	};
	this.retornoAcaoNegativa = function(flagFinalizar){
		// console.log('TESTE CALBACK FUNCAO this.callbackFunc',flagFinalizar)

		// Ultimo callback realizado no fechamento do modal
		if(flagFinalizar != undefined || flagFinalizar == "true"){
			if(this.callbackFunc != undefined){
				var self = this;
				// Função Callback chamada quando o modal acaba de ser oculto ao usuário (espera as transições CSS finalizarem).
				$('#ModalPadraoSac').on('hidden.bs.modal', function (e) {
					var testeRetorno = "retorno teste func"
				 	 self.callbackFunc.call(testeRetorno);
				 	 return testeRetorno
				})
			}
		}
		if(this.modalPage){
			$('#ModalPadraoSac').modal('hide');
			setTimeout(function(){
				var content = $('#panelDashboard');
				var componets  = '<div id="panelCardsAviso"     nome-cp="card_aviso" class="stats col-xs-12 col-sm-12 col-md-12 col-lg-12"> </div>';
				componets += '<div id="panelCardsServicos"  nome-cp="card_servico" class="stats col-xs-12 col-sm-12 col-md-12 col-lg-12"></div>';
				$(content).html(componets)
				components.render();
			},200)
		}
		else {
			$('#ModalPadraoSac').modal('hide');
		}
		
		return;
	};
	this.goHome = function(){
		
	}
	this.completarAcao = function(){
		var self = this;
		var tmpLoad = '<div class="master_load" style="width:100%;padding-top:20px; text-align:center">';
		tmpLoad += '<div class="loaderPequeno"></div>';
		tmpLoad += '</div>';
		$('#messageHeader #messageTitle').html("Aguarde")
		$('#messageHeader #messageTitle').css('text-align', 'center');
		$('.modal-body').html(tmpLoad);
		$('.modal-footer').fadeTo( "slow", 0.33 );
			setTimeout(function(){
				msgsucesso = 'Solicitação Realizada com Sucesso!';
				var tmpSucesso = '<h1 style="margin-top: 4px;">';
					tmpSucesso += '<i id="dialog-icon" style="color: rgba(93, 181, 93, 0.89);" class="fa fa-check-circle col-lg-1"></i>';
					tmpSucesso += '</h1>';
					tmpSucesso += '<div>';
					tmpSucesso += '<label id="dialog-message" style="padding-top: 7px;">';
					tmpSucesso += '<div class="form-inline has-feedback">';
					tmpSucesso += '<span>'+msgsucesso+'</span>';
					tmpSucesso += '</div>';
					tmpSucesso += '</label>';
					tmpSucesso += '</div>';
				$('#messageHeader #messageTitle').html("Atenção")
				$('#messageHeader #messageTitle').css('text-align', 'center');
				$('.modal-body').html(tmpSucesso);
				$('.modal-footer #btnContratar').hide();
				
				$('.modal-footer #btnNaoContratar').text("Fechar").attr({
					title:"Fechar"
				});

				$('.modal-footer').fadeTo( "slow", 1 );
				$('#ModalPadraoSac #messageHeader').css("background-color","rgba(93, 181, 93, 0.39)");
			},800);
	}
	this.completarAcaoRetornoServico = function(respostaServico){

		console.log('completarAcao',respostaServico)
		console.log('completarAcao Laercio',respostaServico.codigoRetorno)
		var self = this;
		var tmpLoad = '<div class="master_load" style="width:100%;padding-top:20px; text-align:center">';
		tmpLoad += '<div class="loaderPequeno"></div>';
		tmpLoad += '</div>';
		$('#messageHeader #messageTitle').html("Aguarde")
		$('#messageHeader #messageTitle').css('text-align', 'center');
		$('.modal-body').html(tmpLoad);

		$('.modal-footer').fadeTo( "slow", 0.33 );


		var flagFinalizaCallBack;
		if(respostaServico.codigoRetorno != 200){
			flagFinalizaCallBack = false;
			setTimeout(function(){
				msgsucesso = respostaServico.descricaoRetorno;
				var tmpSucesso = '<h1 style="margin-top: 4px;">';
					tmpSucesso += '<i id="dialog-icon" style="color: rgba(93, 181, 93, 0.89);" class="fa fa-check-circle col-lg-1"></i>';
					tmpSucesso += '</h1>';
					tmpSucesso += '<div>';
					tmpSucesso += '<label id="dialog-message" style="padding-top: 7px;">';
					tmpSucesso += '<div class="form-inline has-feedback">';
					tmpSucesso += '<span>'+msgsucesso+'</span>';
					tmpSucesso += '</div>';
					tmpSucesso += '</label>';
					tmpSucesso += '</div>';
				$('#messageHeader #messageTitle').html("Atenção")
				$('#messageHeader #messageTitle').css('text-align', 'center');
				$('.modal-body').html(tmpSucesso);
				$('.modal-footer #btnContratar').hide();
				$('.modal-footer #btnNaoContratar').attr('data-finalizaAcao', flagFinalizaCallBack);
				$('.modal-footer #btnNaoContratar').text("Fechar").attr({
					title:"Fechar"
				});
				$('.modal-footer').fadeTo( "slow", 1 );
				$('#ModalPadraoSac #messageHeader').css("background-color","rgba(240, 173, 78, 0.5)");
	    		$("#ModalPadraoSac #dialog-icon").removeAttr('class');
	    		$('#ModalPadraoSac #dialog-icon').addClass('fa fa-exclamation-circle col-lg-1');
	    		$('#ModalPadraoSac #dialog-icon').css("color","rgb(240, 173, 78)");
			},800);
		}else if (respostaServico.codigoRetorno == 500) {
			flagFinalizaCallBack = false;
			setTimeout(function(){
				msgsucesso = respostaServico.descricaoRetorno;
				var tmpSucesso = '<h1 style="margin-top: 4px;">';
					tmpSucesso += '<i id="dialog-icon" style="color: rgba(93, 181, 93, 0.89);" class="fa fa-check-circle col-lg-1"></i>';
					tmpSucesso += '</h1>';
					tmpSucesso += '<div>';
					tmpSucesso += '<label id="dialog-message" style="padding-top: 7px;">';
					tmpSucesso += '<div class="form-inline has-feedback">';
					tmpSucesso += '<span>'+msgsucesso+'</span>';
					tmpSucesso += '</div>';
					tmpSucesso += '</label>';
					tmpSucesso += '</div>';
				$('#messageHeader #messageTitle').html("Atenção")
				$('#messageHeader #messageTitle').css('text-align', 'center');
				$('.modal-body').html(tmpSucesso);
				$('.modal-footer #btnContratar').hide();
				$('.modal-footer #btnNaoContratar').attr('data-finalizaAcao', flagFinalizaCallBack);
				$('.modal-footer #btnNaoContratar').text("Fechar").attr({
					title:"Fechar"
				});
				$('.modal-footer').fadeTo( "slow", 1 );
	    		$('#ModalPadraoSac #messageHeader').css("background-color","rgba(217, 83, 79, 0.5)");
	    		$("#ModalPadraoSac #dialog-icon").removeAttr('class');
	    		$('#ModalPadraoSac #dialog-icon').addClass('fa fa-times-circle col-lg-1');
	    		$('#ModalPadraoSac #dialog-icon').css("color","rgb(217, 83, 79)");
			},800);
		}
		else {
			flagFinalizaCallBack = true;
			setTimeout(function(){
				msgsucesso = respostaServico.descricaoRetorno;
				var tmpSucesso = '<h1 style="margin-top: 4px;">';
					tmpSucesso += '<i id="dialog-icon" style="color: rgba(93, 181, 93, 0.89);" class="fa fa-check-circle col-lg-1"></i>';
					tmpSucesso += '</h1>';
					tmpSucesso += '<div>';
					tmpSucesso += '<label id="dialog-message" style="padding-top: 7px;">';
					tmpSucesso += '<div class="form-inline has-feedback">';
					tmpSucesso += '<span>'+msgsucesso+'</span>';
					tmpSucesso += '</div>';
					tmpSucesso += '</label>';
					tmpSucesso += '</div>';
				$('#messageHeader #messageTitle').html("Atenção")
				$('#messageHeader #messageTitle').css('text-align', 'center');
				$('.modal-body').html(tmpSucesso);
				$('.modal-footer #btnContratar').hide();
				$('.modal-footer #btnNaoContratar').attr('data-finalizaAcao', flagFinalizaCallBack);
				$('.modal-footer #btnNaoContratar').text("Fechar").attr({
					title:"Fechar"
				});
				$('.modal-footer').fadeTo( "slow", 1 );
				$('#ModalPadraoSac #messageHeader').css("background-color","rgba(93, 181, 93, 0.39)");
				

			},800);

		}

		
		
	}
}
var customModalSac = new CustomModalSac();

