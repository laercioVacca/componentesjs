function AutenticacaoSac() {

	this.setWebLinkTarget = function(){
	    if(webLink.isDesenv()){
	        logging(">>> MODO DESENV <<<");

	        webLink.setDefaults();
	        filial = webLink.getVar("filial") + "";
	        return true;
	    }
	    else{ 
	        logging(">>> MODO PRODUCAO <<<");
	        logging(window.location.href);
	        // Corregar WebLink 
	        webLink.setVar("filial", this.getUrlVars()['filial']);
	        webLink.setVar("sessao", this.getUrlVars()['sessao']);
	        webLink.setVar("weblinkageweb", this.getUrlVars()['weblinkageweb']);
	        webLink.setVar("webapplip", this.getUrlVars()['webapplip']);
	        webLink.setVar("ipUsuario", this.getUrlVars()['ipUsuario']); 
	        webLink.setVar("webfilcod", this.getUrlVars()['webfilcod']);
	        //webLink.setVar("token", getUrlVars()['webfilcod']);
	        webLink.setVar("modulo", this.getUrlVars()['modulo']);
	        webLink.setVar("rotina", this.getUrlVars()['rotina']);
	        webLink.setVar("cpfUsuario", this.getUrlVars()['cpfUsuario']);
	        webLink.setVar("carBin", this.getUrlVars()['carBin']); 
	        webLink.setVar("loginUsuario", this.getUrlVars()['loginUsuario'].trim()); // ???
	        filial = webLink.getVar("filial") + "";
	        //log("Filial: "+ filial + "");
	        logging(webLink.getVar("cpfUsuario") + "");
	        clearLocalStorageStartWith('webLink_');
	        // COLOCA O WEBLINK DO LOCALSTORAGE COM O TOKEN NOVO
	        localStorage.setItem('webLink_'+webLink.getVar("token"), JSON.stringify(webLink));
	        return true;
	    }       
	}

	this.renewSession = function(pToken){
		
		if(webLink.isDesenv()){
			logging(">>> MODO DESENV <<<");
			
			webLink.setDefaults();
			
			filial = webLink.getVar("filial") + "";

			console.log('abaixo aqui',webLink.setDefaults())
			return true;
		}
		else{ 
			logging(">>> MODO PRODUCAO <<<");
			logging(window.location.href);
			if(pToken == undefined){
				// TODO: MENSAGEM DE ERRO
				// TODO: REDIRECIONAR PARA LOGIN
			}
			// PEGAR WEBLINK DO LOCALSTORAGE COM O TOKEN ANTIGO
			var tokenOld = pToken;
			var webLinkOld = JSON.parse(localStorage.getItem('webLink_'+tokenOld));
			if(webLinkOld == null){
				logging("Warning");
				marisaMessage.showDialog(marisaMessageType_WARNING, "Atenção", "Token inválido!"+ 
				"<br><br>Você será redirecionado para a tela de login.");
				window.setTimeout(function(){window.location.href = "/psfsecurity/paginas/security/login/tela/login.html?teste02teste"}, 3000);
				return false;
			}
			else{
				logging("webLinkOld");
				logging(webLinkOld);
				// TODO: RENOVA TOKEN
				//token = renovarToken();
				// Corregar WebLink 
				webLink.setVar("filial", webLinkOld.filial);
				webLink.setVar("sessao", webLinkOld.sessao);
				webLink.setVar("weblinkageweb", webLinkOld.weblinkageweb);
				webLink.setVar("webapplip", webLinkOld.webapplip);
				webLink.setVar("ipUsuario", webLinkOld.ipUsuario);
				webLink.setVar("webfilcod", webLinkOld.webfilcod);
				webLink.setVar("token", token);
				webLink.setVar("modulo", webLinkOld.modulo);
				webLink.setVar("rotina", webLinkOld.rotina);
				webLink.setVar("cpfUsuario", webLinkOld.cpfUsuario);
				webLink.setVar("carBin", webLinkOld.carBin);
				webLink.setVar("loginUsuario", webLinkOld.loginUsuario);
				
				//filial = webLink.getVar("filial") + "";
				
				var filialStorage = localStorage.getItem('login_'+webLink.getVar("loginUsuario"));
				if(filialStorage != null && filialStorage != undefined && filialStorage > 0){
					filial = filialStorage + "";
					webLink.setVar("filial", filial);
					webLink.setVar("webfilcod", filial);
				}else{
					filial = webLink.getVar("filial") + "";
				}
				$('#labelFilial').text(webLink.getVar("filial"));
				localStorage.removeItem('login_'+webLink.getVar("loginUsuario"));
				localStorage.setItem('login_'+webLink.getVar("loginUsuario"), filial);   
				
				logging(filial + "");
				log(" ");
				log(" ");
				log(" ");
				log("Filial: "+ filial + "");
				logging(webLink.getVar("cpfUsuario") + "");
				
				// APAGA LOCALSTORAGE DO TOKEN ANTIGO
				localStorage.removeItem('webLink_'+tokenOld);
				
				// COLOCA O WEBLINK DO LOCALSTORAGE COM O TOKEN NOVO
				localStorage.setItem('webLink_'+webLink.getVar("token"), JSON.stringify(webLink));
				return true;
			}
			
		}       
	}

	this.renewToken = function(pToken){
	    if(webLink.isDesenv()){
	        logging(">>> MODO DESENV <<<");
	        webLink.setDefaults();
	        filial = webLink.getVar("filial") + "";
	    }
	    else{
	        logging(">>> MODO PRODUCAO <<<");
	        logging(window.location.href);
	        if(pToken == undefined){
	            // TODO: MENSAGEM DE ERRO
	            // TODO: REDIRECIONAR PARA LOGIN
	        }
	        
	        // PEGAR WEBLINK DO LOCALSTORAGE COM O TOKEN ANTIGO
	        var tokenOld = webLink.getVar("token");//
	        var webLinkOld;
	        if(tokenOld == undefined){
	            logging("Warning");
	            marisaMessage.showDialog(marisaMessageType_WARNING, "Atenção", "Token inválido!"+ 
	                            "<br><br>Você será redirecionado para a tela de login.");
	            window.setTimeout(function(){window.location.href = "../../login/tela/login.html"}, 2000);          
	        }else{
	            webLinkOld = JSON.parse(localStorage.getItem('webLink_'+tokenOld));
	        
	            token = pToken;
	            
	            logging("webLinkOld");
	            logging(webLinkOld);
	            
	            // Corregar WebLink 
	            webLink.setVar("filial", webLinkOld.filial);
	            webLink.setVar("sessao", webLinkOld.sessao);
	            webLink.setVar("weblinkageweb", webLinkOld.weblinkageweb);
	            webLink.setVar("webapplip", webLinkOld.webapplip);
	            webLink.setVar("ipUsuario", webLinkOld.ipUsuario);
	            webLink.setVar("webfilcod", webLinkOld.webfilcod);
	            webLink.setVar("token", pToken);
	            webLink.setVar("modulo", webLinkOld.modulo);
	            webLink.setVar("rotina", webLinkOld.rotina);
	            webLink.setVar("cpfUsuario", webLinkOld.cpfUsuario);
	            webLink.setVar("carBin", webLinkOld.carBin);
	            webLink.setVar("loginUsuario", webLinkOld.loginUsuario);
	            
	            //filial = webLink.getVar("filial") + "";
	            
				var filialStorage = localStorage.getItem('login_'+webLink.getVar("loginUsuario"));
				if(filialStorage != null && filialStorage != undefined && filialStorage > 0){
					filial = filialStorage + "";
					webLink.setVar("filial", filial);
					webLink.setVar("webfilcod", filial);
				}else{
					filial = webLink.getVar("filial") + "";
				}
				$('#labelFilial').text(webLink.getVar("filial"));
				localStorage.removeItem('login_'+webLink.getVar("loginUsuario"));
				localStorage.setItem('login_'+webLink.getVar("loginUsuario"), filial);            
	            
				
	            logging(filial + "");
	            logging(webLink.getVar("cpfUsuario") + "");
	            
	            // APAGA LOCALSTORAGE DO TOKEN ANTIGO
	            localStorage.removeItem('webLink_'+tokenOld);
	            
	            // COLOCA O WEBLINK DO LOCALSTORAGE COM O TOKEN NOVO
	            localStorage.setItem('webLink_'+webLink.getVar("token"), JSON.stringify(webLink));
	        }
	    }       
	}
}
var autenticacaoSac = new AutenticacaoSac();
