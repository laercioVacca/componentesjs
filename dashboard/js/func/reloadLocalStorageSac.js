function ReloadLocalStorageSac(){
    if (!(this instanceof ReloadLocalStorageSac)){
        return new ReloadLocalStorageSac();
    }

    this.load = function(){
    	this.consultaServico();

    }
    this.consultaServico = function(){
    	var parameters;
    	var hashProcesso;
    	var search = {};
    	var infosCliAtendimento;
    	
		hashProcesso      	=   controllStorageSac.criacaoHashUnicaProcesso("proc_");
    	infosCliAtendimento = controllStorageSac.getinfosCliEmAtendimento();
		search.tipo  = 'CPF'; 
		search.value = infosCliAtendimento.cpf;
		parameters = {
			"search" : search
		};
		var result = this.buscaCliente(hashProcesso,parameters);
		if (result["codigoRetorno"] == 200) {
			controllStorageSac.gravaInfosCliEmAtendido(result.objectsReturn.ClientDashboard);
			console.log('RELOAD DADOS CLI',result.objectsReturn.ClientDashboard );
		} 
	};
    this.buscaCliente = function(idProcesso,parameters){
		this.servicoBuscaCliente        = new GenericServiceV2('psfclientservices');
		this.getBuscaCliente            = this.servicoBuscaCliente.callPostService("v2/dashboard", "searchClient",idProcesso, parameters);
		return this.getBuscaCliente;
	}
    
}

if(reloadLocalStorageSac == undefined || reloadLocalStorageSac == null){
	var reloadLocalStorageSac = new ReloadLocalStorageSac();
}
