function ModalPdfSac() {
	this.load = function(propsModal){
		this.propsModal = propsModal;
		this.construtorModal();
		return;
	};
	this.construtorModal = function(){
		var objPdf 		 = this.propsModal.objServico;
		var elementoRaiz = this.propsModal.elementoRaiz;
		var tituloModal  = this.propsModal.titulo;


		var base64pdf = objPdf.objectsReturn.BoletoRequest;
		var tipoDoc = 'data:application/pdf;base64,';
		this.urlShowPdf = tipoDoc+base64pdf;

		$(this.propsModal.elementoRaiz).html("")
		var content = document.createElement('div');
		$(this.propsModal.elementoRaiz).append(this.template());
		
		this.acaoPositivaModal();
		this.acaoNegativaModal();
		$('#ModalPadraoSac').modal({ keyboard: false})
		$('#ModalPadraoSac').modal('show');
		
	}
	this.template = function(){
		var html;
			html  = '<div id="ModalPadraoSac" class="modal fade modalSAC" tabindex="-1" role="dialog">';
			html += '<div class="modal-dialog modal-dialog-centered" role="document">';
			html += '<div id="modalContent" class="modal-content">';
			html += this.conteudoModal();
			html += '</div>';
			html += '</div>';
			html += '</div>';
		return html;
	};
	this.conteudoModal = function(titulo,base64){
		var html;
		html  = '<div class="modal-header" id="messageHeader" style="display: block; background-color: rgba(93, 181, 93, 0.39);">';
		html += '<button type="button" class="close" data-dismiss="modal" aria-label="Close" style="padding-top: 0px;"><span aria-hidden="true">x</span></button>';
		html += '<h5 class="modal-title" id="exampleModalLongTitle">';
		html += '<span class="fs1 icone_card_ativo" aria-hidden="true" data-icon="&#xe053;"></span> ';
		html += this.propsModal.titulo;
		html += '</h5>';
		html += '</div>';
		html += '<div class="modal-body" style="display: inline-flex;font-size: 14px;">';
		html += '<object id="faturaPdfobj" width="100%" height="400" type="application/pdf" data="'+this.urlShowPdf+'">';
		html += '<embed class="fatura_show" src="'+this.urlShowPdf+'" width="100%" height="400" type="application/pdf">';
		html += '</object>';
		html += '</div>';
		html += '<div class="modal-footer" style="background-color: rgb(243, 243, 243);padding: 10px;">';
		html += '<button type="button" id="btnNaoContratar" style="font-size: 14px;" onclick="" class="btn btn-default">Fechar</button>';
		html += '</div>';
		return html;
	}
	this.acaoPositivaModal = function(){
		var self = this;
		var disparaAcaoPositivaModal = $('#btnContratar').on('click', function(event) {
			event.preventDefault();
			console.log('OPA VAI AQUI HOJE',self.isServico)
			if(self.propsModal.funcao){
				if(self.isServico){
					var respostaServico = self.propsModal.funcao.call();
					console.log('mostar retorno Servico!!',respostaServico)
					self.completarAcaoRetornoServico(respostaServico);
				}else{
					self.propsModal.funcao.call();
					self.completarAcao();
				}
			}else {
				self.retornoAcaoPositiva();
			}
		});
		return disparaAcaoPositivaModal;
	};
	this.acaoNegativaModal = function(){
		var self = this;
		var disparaAcaoNegativaModal = $('#btnNaoContratar').on('click', function(event) {
			event.preventDefault();
			$('#ModalPadraoSac').modal('hide');
		});
		return disparaAcaoNegativaModal;
	};


}
var modalPdfSac = new ModalPdfSac();