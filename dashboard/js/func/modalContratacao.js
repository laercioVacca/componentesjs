function ModalContratacao() {
	this.load = function(local,tipoModal){
		this.tipoModal = tipoModal;
		this.local = local;
		$(this.local).html("")
		this.contrutorModal();
		$('#ModalPadraoSac').modal('show');
		this.acaoPositivaModal();
		this.acaoNegativaModal();
		return;
	};
	this.contrutorModal = function(){
		var self = this;
		var content = document.createElement('div');
		$.each(this.modalProps(), function(index, val) {
			if(index == self.tipoModal ){
			 	$(self.local).append(self.template(val.titulo,val.texto))
			 }
		});
	}
	this.template = function(titulo,texto){
		var html;
			html  = '<div id="ModalPadraoSac" class="modal fade modalSAC" tabindex="-1" role="dialog">';
			html += '<div class="modal-dialog modal-dialog-centered" role="document">';
			html += '<div id="modalContent" class="modal-content">';
			html += '<div class="modal-header" id="messageHeader" style="display: block; background-color: rgba(33, 129, 212, 0.45);">';
			html += '<button type="button" class="close" data-dismiss="modal" aria-label="Close" style="padding-top: 0px;"><span aria-hidden="true">×</span></button>';
			html += '<h4 id="messageTitle" class="modal-title">'+titulo+'</h4>';
			html += '</div>';
			
			html += '<div class="modal-body" style="display: inline-flex;font-size: 14px;">';
			html += '<h1 style="margin-top: 4px;"><i id="dialog-icon" style="color: rgba(33, 129, 212, 0.95);" class="fa fa-question-circle col-lg-1"></i></h1>';
			html += '<div>';
			html += '<label id="dialog-message" style="padding-top: 7px;">';
			html += '<div class="form-inline has-feedback">';
			html += '<span>'+texto+'</span>';
			html += '</div>';					
			html += '</label>';
			html += '</div>';
			html += '</div>';
			
			html += '<div class="modal-footer" style="background-color: rgb(243, 243, 243);padding: 10px;">';
			html += '<button type="button" id="btnContratar" class="btn btn-primary" style="font-size: 14px;">SIM</button>';
			html += '<button type="button" id="btnNaoContratar" style="font-size: 14px;" onclick="" class="btn btn-default">Não</button>';
			html += '</div>';
			html += '</div>';
			html += '</div>';
			html += '</div>';
		return html;
	};
	this.acaoPositivaModal = function(){
		var self =this;
		var disparaAcaoPositivaModal = $('#btnContratar').on('click', function(event) {
			event.preventDefault();
			self.retornoAcaoPositiva();
		});
		return disparaAcaoPositivaModal;
	};
	this.acaoNegativaModal = function(){
		var self =this;
		var disparaAcaoNegativaModal = $('#btnNaoContratar').on('click', function(event) {
			event.preventDefault();
			self.retornoAcaoNegativa();

		});
		return disparaAcaoNegativaModal;
	};
	this.retornoAcaoPositiva = function(){
		console.log('retorno positivo this.tipoModal',this.tipoModal)
		this.completarCompra()
		return;
	};
	this.retornoAcaoNegativa = function(){
		console.log('retorno Negativo')
		$('#ModalPadraoSac').modal('hide');
		return;
	};
	this.completarCompra = function(){
		console.log('completarCompra')
		var self = this;
		var tmpLoad = '<div class="master_load" style="width:100%;padding-top:20px; text-align:center">';
		tmpLoad += '<div class="loaderPequeno"></div>';
		tmpLoad += '</div>';
		$('#messageHeader #messageTitle').html("Aguarde")
		$('#messageHeader #messageTitle').css('text-align', 'center');
		$('.modal-body').html(tmpLoad);
		$('.modal-footer').fadeTo( "slow", 0.33 );
		setTimeout(function(){
			var tmpSucesso = '<h1 style="margin-top: 4px;">';
				tmpSucesso += '<i id="dialog-icon" style="color: rgba(93, 181, 93, 0.89);" class="fa fa-check-circle col-lg-1"></i>';
				tmpSucesso += '</h1>';
				tmpSucesso += '<div>';
				tmpSucesso += '<label id="dialog-message" style="padding-top: 7px;">';
				tmpSucesso += '<div class="form-inline has-feedback">';
				tmpSucesso += '<span>Serviço contratado com sucesso!</span>';
				tmpSucesso += '</div>';
				tmpSucesso += '</label>';
				tmpSucesso += '</div>';

			$('#messageHeader #messageTitle').html("Contratação")
			$('#messageHeader #messageTitle').css('text-align', 'center');
			$('.modal-body').html(tmpSucesso);
			$('.modal-footer #btnContratar').hide();
			$('.modal-footer #btnNaoContratar').text("Fechar").attr({
				title:"Fechar"
			});
			$('.modal-footer').fadeTo( "slow", 1 );
			$('#ModalPadraoSac #messageHeader').css("background-color","rgba(93, 181, 93, 0.39)");
			$('#'+self.tipoModal).remove();
			self.simulacaoServicosContratados()
			
		},800)

	};
	this.simulacaoServicosContratados = function(){
		var self = this;
		function conteudoServicos(){
			var infos ={
				"bolsaprotegida":{
					"titulo":"Bolsa Protegida",
					"valor":"R$ 4,90",
					"btn":"1",
					"idproduto":"001"
				},
				"protecaofinanceira":{
					"titulo":"Proteção Financeira",
					"valor":"R$ 11,90",
					"btn":"2",
					"idproduto":"002"
				},
				"protecaocelular":{
					"titulo":"Proteção Celular",
					"valor":"R$ 7,90",
					"btn":"1",
					"idproduto":"003"
				}
			}
			return infos;
		}
		function template(btn,titulo,valor,idproduto){
			
			var tmp;
			tmp  = '<li id="produtoid_'+idproduto+'" class="list-group-item row" style="padding: 0px;margin: 3px;">';
			tmp += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" style="padding: 0px 5px 0px 5px;">'+titulo+'</div>';
			tmp += '<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="padding: 0px 0px 0px 0px;">'+valor+'</div>';
			tmp += '<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="padding: 0px 5px 0px 5px;text-align: right;">';
			if(btn == 1){
				tmp += '<a href="#" data-id-prod="'+idproduto+'" class="btn_deletar_servico_contratado btn btn-danger btn-xs" style="margin: 0px 0px 0px 5px;height: 20px;">';
				tmp += '<span class="glyphicon glyphicon-remove"></span> ';
				tmp += '</a>';
			}
			else{
				tmp += '<a href="page_editar_sevico.html" class="btn_editar_servico btn btn-warning btn-xs" style="margin: 0px 0px 0px 0px;height: 20px;">';
				tmp += '<span class="glyphicon glyphicon-pencil"></span>';
				tmp += '</a>';
				tmp += '<a href="#" data-id-prod="'+idproduto+'" class="btn_deletar_servico_contratado btn btn-danger btn-xs" style="margin: 0px 0px 0px 5px;height: 20px;">';
				tmp += '<span class="glyphicon glyphicon-remove"></span> ';
				tmp += '</a>';

			}
			
			
			tmp += '</div>';
			tmp += '</li>';
			return tmp;
		}
		function montaListaContratados(){
			$.each(conteudoServicos(), function(index, val) {
				 if(index == self.tipoModal){
				 	$('#listaServicosContratados').append(template(val.btn,val.titulo,val.valor,val.idproduto))
				 }
			});
			
		}
		return montaListaContratados();
	}
	
	this.modalProps = function(){
		var props = {
			"bolsaprotegida":{
				"titulo":"Confirmação de Contratação de Serviço",
				"texto":"Deseja realmente contratar o serviço abaixo?<br>Serviço: Bolsa Protegida<br>Valor Premio: R$ 9,90<br>Valor Cobertura: R$ 0,00 ",
			},
			"protecaofinanceira":{
				"titulo":"Confirmação de Contratação de Serviço",
				"texto":"Deseja realmente contratar o serviço abaixo?<br>Serviço: Proteção Financeira<br>Valor Premio: R$ 9,90<br>Valor Cobertura: R$ 0,00 ",
			},
			"protecaocelular":{
				"titulo":"Confirmação de Contratação de Serviço",
				"texto":"Deseja realmente contratar o serviço abaixo?<br>Serviço: Proteção Celular<br>Valor Premio: R$ 9,90<br>Valor Cobertura: R$ 0,00 ",
			}
		}
		return props;
	}
}
var modalContratacao = new ModalContratacao();
