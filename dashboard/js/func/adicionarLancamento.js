console.log('Load Class adicionar Lançamentos!');

function LancamentosSimultaneos(){


	this.init = function(textLogManual,listaLogManual){
		this.logLancamentos = localStorage.getItem("logLancamentos");
		this.logLancamentos = JSON.parse(this.logLancamentos);
		if(this.logLancamentos == undefined){
			this.logLancamentos = [];
		}else{
			console.log('Tem conteudo no storage',this.logLancamentos)
		}
		this.boxlistaLogManual = listaLogManual;
		this.getValCampoTexto(textLogManual);
		this.deletarLog();
	};
	
	this.getValCampoTexto = function(textLogManual,listaLogManual){
		this.txt = textLogManual;
		this.setInfoStorage(this.txt)
		console.log('Dentro da func',this.txt)
		return this.txt;
	}
	this.setInfoStorage = function(textologManual){
		var idLog = new Date().getTime();
		var chaveLog = idLog;
		var tituloTratadoLog = textologManual.substring(0,15);
		console.log('tituloTratadoLog',tituloTratadoLog)
		var logRecebido = {
			id:chaveLog,
			titulo:tituloTratadoLog,
			textoLog:textologManual
		};
		
		this.logLancamentos.push(logRecebido);
		
		
		localStorage.setItem("logLancamentos",JSON.stringify(this.logLancamentos));
		this.listaLogLancamentos(this.logLancamentos);

		console.log('OBJ ANTES DELET',this.logLancamentos)
		return true;
	};
	this.listaLogLancamentos = function(objLogStorage){
		this.objLogStorage = objLogStorage;
		var timeStamp = new Date().getTime();
		var boxListaLog = document.querySelector('#box_lancamentos_dinamicos');
		var obj_log = this.objLogStorage;
		var textoLog;
		var identificadorCardLog;
		for (var i = 0; i < obj_log.length; i++) {
			idLog  				 = obj_log[i].id; 
			tituloLog            = obj_log[i].titulo
			textoLog             = obj_log[i].textoLog;
			identificadorCardLog = i+timeStamp;
		}
		console.log('identificadorCardLog',identificadorCardLog)
		$(boxListaLog).append(this.templateListaLog(idLog,identificadorCardLog,tituloLog,textoLog));
		return 
	};
	this.deletarLog = function(){
		
		var id_Log;
		var refThs = this;
		$('.card').on('click','.excluir_log',function(e) {
			e.preventDefault();
			e.stopPropagation();
			id_Log = $(this).attr('log-id');
			refThs.getCurrentLog(id_Log);
			$('#carlogID_'+id_Log).remove();
			
		});

		
		return
	};
	this.getCurrentLog = function(ID){
		idLogCurrent = ID;
		var currentLog = [];
		var sobraObj   = [];
		refThis = this;
		$.each(this.logLancamentos, function(index, val) {
			if(val.id == idLogCurrent){
				currentLog.push(val);
				console.log('INDEX >>>',index)
				// refThis.logLancamentos.splice(index);
			}else{
				sobraObj.push(val);
			}
		});
		var RESTO = this.logLancamentos;
		console.log('OBJ APOS DELET',sobraObj)
		localStorage.setItem("logLancamentos",JSON.stringify(sobraObj));
		// console.log('SOBRA DO OBJ APOS DELET',sobraObj)
		// localStorage.setItem("logLancamentos",JSON.stringify(sobraObj));
		// console.log('OBJ APOS DELET',this.logLancamentos)
		return currentLog
	};
	
	this.templateListaLog = function(idLog,identificadorCardLog,tituloLog,textoLog){
		var html;
		html =  '<div id="carlogID_'+idLog+'" class="card">';
		html += '<div class="card-header" id="headingOne2" style="padding-top:0px;padding-bottom: 0px">';
		html += '<a class="att_btn_editar" href="#" data-toggle="collapse" data-target="#'+identificadorCardLog+'" aria-expanded="true" aria-controls="collapseOne2">';
		html += '<span class="nomeLancamento">'+tituloLog+'</span>';				    		
		html += '</a>';
		html += '<a href="#" log-id="'+idLog+'" class="excluir_log btn_editar_log_atendimento btn btn-danger btn-xs" style="margin-top: 3px">';
		html += '<span class="glyphicon glyphicon-remove"></span>'; 
		html += '</a> ';
		html += '</div>';
		html += '<div id="'+identificadorCardLog+'" class="collapse" aria-labelledby="headingOne2" data-parent="#accordion">';
		html += '<div class="card-body">';
		html += '<form>';
		html += '<div class="form-group">';
		html += '<textarea class="textolancamentoEditar form-control" id="campoTexto" rows="5">'+textoLog+'</textarea>';
		html += '</div>';
		html += '<div class="btn_editar_texto form-group">';
		html += '<button id="btnEditartextoLancamento" type="submit" class="btn btn-primary mb-2">Aplicar</button>';
		html += '</div>';
		html += '</form>';
		html += '</div>';
		html += '</div>';
		html += '</div>';
		return html;
	}
	

}
var lancamentosSimultaneos = new LancamentosSimultaneos();

	
