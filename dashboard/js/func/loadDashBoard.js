
function LoadDashBoard() {
	this.load = function(dadosCurrentCliente){
		var panelControllDashboard,
		compProcedimento,
		panelAtendimento,
		currentPageName,
		containerPrincipal

		this.cliCurrent            = dadosCurrentCliente;
		panelControllDashboard = $('#panelControllDashboard');
		compProcedimento       = $('#panelProcedimentos');
		panelAtendimento       = $('#panelAtendimento');
		containerPrincipal     = $('#containerPrincipal');

		$(panelControllDashboard).show();

		currentPageName    = containerPrincipal.attr('page-name');

		this.limpaComponets()

		initDadosCliente.load(this.cliCurrent);
		initNavegacao.load();
		initProcedimento.load(compProcedimento);
		initAtendimento.load(panelAtendimento);

		this.carregaCardsDashBoard();
		this.eventosDash();
	}

	this.carregaCardsDashBoard = function(){
		servicosInstance.load();
		var agrupamentoCardsDash;
		var panelCardsServicos = $('#panelCardsServicos');
		var ConteudoCard = document.createElement('div');
		ConteudoCard.setAttribute('class','cardReferenciaMaster row');

		// agrupamentoCardsDash  = cardHistoricoChamados.getTemplate();
		// agrupamentoCardsDash += cardCartaoBonus.getTemplate();
		// agrupamentoCardsDash = cardCobranca.getTemplate();
		
		// agrupamentoCardsDash += cardInformacoesFinanceiras.getTemplate();
		// agrupamentoCardsDash += cardConsultaFaturas.getTemplateAtual();
		// agrupamentoCardsDash += cardConsultaFaturas.getTemplateHistorico();
		// agrupamentoCardsDash += cardMemoFile.getTemplate();
		// agrupamentoCardsDash += plasticosInstance.getTemplateCard();
		// agrupamentoCardsDash += businessServicos.loadCardServicosDisponiveis();
		// agrupamentoCardsDash += businessServicos.loadCardServicosContratados();

		ConteudoCard.innerHTML = agrupamentoCardsDash;
		panelCardsServicos.append(ConteudoCard);

		// cardHistoricoChamados.load();
		// cardCartaoBonus.load();
		// cardCobranca.load();
		// cardInformacoesFinanceiras.load();
		// businessServicos.loadEventosCard();
		// cardMemoFile.load();
		// plasticosInstance.loadCard();
	}
	

	this.limpaComponets = function(){
		var compProcedimento    = $('#panelProcedimentos');
		var panelAtendimento    = $('#panelAtendimento');
		var contentDadosCliente = $('#contentDadosCliente')
		$(compProcedimento).empty();
		$(panelAtendimento).empty();
		$(contentDadosCliente).empty();
	}
	
	this.eventosDash = function(){
		$('#panelCardsServicos').on('click','.renderPage',  function(event) {
            event.preventDefault();
            event.stopPropagation();
            var $panelDashboard = $('#panelDashboard');
            var pageName = $(this).attr('href');
            var componenteNome = $(this).attr('href');
            $panelDashboard.hide();
            // ClearApp.clear('#panelDashboard');
            renderPageComponente.render(componenteNome,pageName);
        });
		
		$(function () {
			  $('[data-toggle="tooltip"]').tooltip();
		});
		
	}

}

var loadDashBoard = new LoadDashBoard();