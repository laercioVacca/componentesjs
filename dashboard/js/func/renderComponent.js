
function Components(){
	var el, i, elemento, file;
  	this.render = function(){
  		var panelControllDashboard = document.querySelector('#panelControllDashboard');
		$(panelControllDashboard).show();
  		el = document.getElementsByTagName("*");
	  	for (i = 0; i < el.length; i++) {
	    	elemento = el[i];
	    	file = elemento.getAttribute("nome-cp");
	 		this.componentPath = './components/'+file+'/'+file+'.html';
	    	if (file) {
	      		// console.log('Arquivos con data encontardos',elemento);
	      		$(elemento).load(this.componentPath,function(e){
					// console.log('Retorno Load!!',e);
				});
	    	}
	  	}
	}
	return;
}
function RenderPages(){
	this.render = function(page,dataCustomTarget){
		var contentPrincipal = document.querySelector('#panelPages');
		this.pagePath = './components/page/'+page;
		this.dataCustomTarget = dataCustomTarget;
		$(contentPrincipal).load(this.pagePath,this.dataCustomTarget,function(e){
	});
	return
	}
}



function PageIdentificacaoPositiva(){
	this.render = function(){
		var boxPage = document.querySelector('#panelIdentificacaoPositiva');
		this.identificacaoPath = './components/identificacao_positiva/identificacao_positiva.html';
		$(boxPage).load(this.identificacaoPath,function(e){
			// console.log('Retorno Load!!',e);
		});
	}
}
function RenderDashBoard(){
	this.load = function(cardAlterado){
		console.log('cardAlterado>>>>>>>',cardAlterado)
		var $panelDashboard = $('#panelDashboard');
		var $contentPages  = $('#panelPages');
		if(cardAlterado != undefined){
			var $boxCard = $('#'+cardAlterado);
			var nomeCard = cardAlterado;
			$contentPages.hide();
			$panelDashboard.show();
			loadGif($boxCard);
			setTimeout(function(){
				controllReloadCards.load($boxCard,nomeCard);
			},500)
		}
		else {
			$contentPages.hide();
			$panelDashboard.show();
		}
	}
}

function RenderPageComponente(){
	this.render = function(componenteNome,pageNome){
		console.log('RenderPageComponente => 01',componenteNome,pageNome)
		this.componenteNome = componenteNome;
		var panelDashboard = $('#panelDashboard');
		var contentRender = $('#panelPages');
		contentRender.show();
		this.pagePath = './components/'+componenteNome+'/'+pageNome+'.html';
		var self = this;
		loadGif(contentRender)
		console.log('RenderPageComponente =>02',contentRender,this.pagePath)
		contentRender.load(this.pagePath,function(e){
			self.setNamePage();
			comportamentoPaginas.load();
			panelDashboard.hide();
		});
	}
	this.setNamePage = function(){
		var masterContent = $('#containerPrincipal');
		masterContent.attr("page-name",null);
		masterContent.attr("page-name",this.componenteNome);
	}
}



var renderPageComponente = new RenderPageComponente();



var pageIdentificacaoPositiva = new PageIdentificacaoPositiva()
var renderDashBoard = new RenderDashBoard();
var renderPages = new RenderPages();
var components = new Components();

