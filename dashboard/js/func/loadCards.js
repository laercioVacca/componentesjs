function LoadCards() {
	this.load = function(dadosCurrentCliente){
		require("components/popups/init_popup.js");
		this.cliCurrentCard = dadosCurrentCliente;
		
		var masterContentCards  = $('#panelDashboard');
		this.contentPopUpsDash  = $('#panelCardsAviso');
		this.contentPopUpsDash.empty();
		this.renderPopUpsDash();
	}
	this.renderPopUpsDash = function(){
		initPopUp.load(this.contentPopUpsDash,this.cliCurrentCard)
	}
	this.retornoServico = function(){
		var retornoObj = controleEstadoComponent.setResponseSearch();
		return retornoObj;
	}
}
var loadCards = new LoadCards();
