require("js/startMockApp.js");

require("../../resources/js/Util.js");
require("../../resources/js/pssServices.js");
require("../../resources/js/centralServicosCCMServicesRS.js");
require("../../resources/js/centralServicosCCMServices.js");
require("../../resources/js/marisaServices.js");
require("../../resources/js/genericServices.js");
require("../../resources/js/genericServicesV2.js");
require("../../resources/js/clientServices.js");
require("../../resources/js/marisaMessages.js");
require("../../resources/js/loadWebLink.js");



require("../../resources/libs/jquery-maskedinput/jquery.mask.js");
require("../../resources/libs/bootstrap-validator/validator.min.js");
require("../../resources/libs/underscore/lodash.min.js");
require("../../resources/libs/jquery-ui-1.12.1.custom/jquery-ui.min.js");
// require("../../resources/libs/moment/moment.js");

require("../../resources/libs/datatable/js/jquery.dataTables.min.js");
require("../../resources/libs/datatable/js/dataTables.bootstrap.js");
require("../../resources/libs/datatable/js/dataTables.buttons.min.js");
require("../../resources/libs/datatable/js/buttons.flash.min.js");
require("../../resources/libs/datatable/js/dataTables.fixedHeader.min.js");
require("../../resources/libs/datatable/js/dataTables.fixedColumns.min.js");
require("../../resources/libs/datatable/js/dataTables.responsive.min.js");
require("../../resources/libs/datatable/js/responsive.bootstrap.min.js");	





require("js/func/renderComponent.js");
require("js/func/clearApp.js");
require("js/func/adicionarLancamento.js");
require("js/func/popup.js");
require("js/func/modalContratacao.js");
require("js/func/customModalSac.js");
require("js/func/modalPdfSac.js");
require("js/func/loadDashBoard.js");
require("js/func/autenticacao.js");
require("js/func/controllStorageSac.js");
require("js/func/loadCards.js");
require("js/func/comportamentoPaginas.js");
require("js/func/reloadLocalStorageSac.js");
require("js/func/reloadCards.js");
require("js/func/controllReloadCards.js");

// componets inicializados primeiro
require("components/dadosCliente/init_dadosCliente.js");
require("components/search/init_search.js");
require("components/navegacao/init_navegacao.js");

require("components/procedimento/init_procedimento.js");
require("components/atendimento/init_atendimento.js");

require("components/acessoAltitude/init_acessoAltitude.js");



// incluir os cards do DashBoard

// require("components/historicoChamados/components/cardHistoricoChamados.js");
// require("components/cartaoBonus/components/cardCartaoBonus.js");
require("components/cobranca/components/cardCobranca.js");

// require("components/memofile/components/cardMemofile.js");

// require("components/informacoesFinanceiras/js/cardInformacoesFinanceiras.js");
// require("components/consultaFaturas/js/cardConsultaFaturas.js");
// require("components/plasticos/plasticosInstance.js");
require("components/servicos/servicosInstance.js");



require("components/atendimento/components/lancamentos_manuais.js");







var token = null;
var target = null;
var cpf = null;
var dtNasc = null;
var sexo = null;
var nomeCliente = null;
var inativo = null;
var diasCarenciaReanalise = null;
var cartao = null;
var filial = null;
var statCodCredito = null;
var protocolo = null;
var dadosCadastroCliente = null;
var visualizacaoFuncionalidadesPorStatusCredito = null;

sacGlobalInfos = {};

$(document).ready(function() {
	console.log('MOCKKKKKKKKKKKKKKKKKKKKK',controllStorageSac.getinfosCliEmAtendimento().identificado)
	w3IncludeHTML();
	
	loadRestricoesStatusCredito();
	
	token = getUrlVars()["token"];
	idServico = getUrlVars()["idServico"];
	// Verifica se esta tendo atendimento ainda
	// tokem antigo igual a flag do atendimento 
	var idAtendimento = controllStorageSac.verificacaoStorageAtendimento(token)
	
	if(idAtendimento!= null && idAtendimento != undefined && idAtendimento != false){
		console.log('Atendimento flagTokem',idAtendimento.flagTokem);
		console.log('Tokem antigo',token);
	}
	if(idServico){
		initAcessoAltitude.load(getUrlVars())
	}
	else {
		var renew = autenticacaoSac.renewSession(token);
		var DASH = true;
		if(renew){
			if(waitingDialog.show('Carregando.', {dialogSize : 'sm', progressType : 'info'})){
				window.setTimeout(function(){
					if(controllStorageSac.getinfosCliEmAtendimento().identificado){
						var dadosCurrentCliente = controllStorageSac.getinfosCliEmAtendimento();
						// loadCards.load(dadosCurrentCliente);
						loadDashBoard.load(dadosCurrentCliente);
					}else{
						pageIdentificacaoPositiva.render();
						
					}
					waitingDialog.hide();
				}, 1000);
			}
		}
	}
	
		
});


function renderDashBoard(){
	console.log('Novo render Dash')
	
}

function require(script) {
    $.ajax({
        url: script,
        dataType: "script",
        async: false,           // <-- This is the key
        success: function () {
            // all good...
        },
        error: function () {
            throw new Error("Could not load script " + script);
        }
    });
}

function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi,    
    function(m,key,value) {
      vars[key] = value;
    });
    return vars;
}

// ####

function envioEmailTeste(){
	var parameters = {
			"numeroProtocolo" : 7354311,
			"cliCpf" : 6708575990,
			"email" : "lavandosk@gmail.com",
			"usuCpf" : 11111111111,
			"nomeSeguro" : "Seguro Teste Email",
			"atualizarEmailCliente" : "false"
	};
	var parametrosEmail = {
		"parametrosEmail": JSON.stringify(parameters)
    }
	var result = centralServicosCCMServices.callPostService("emailservice", "enviarEmailCancelamentoSeguros", parametrosEmail, webLink);
	//result = JSON.parse(result);
	logging(result);
	logging(result["codigoRetorno"]);

}

function clickMenu(menu){
	$('#'+menu).find('ul').css('display', 'none');
	$('.current-page').removeClass('current-page');
	$('#'+menu+'Active').addClass('current-page');
	//$('.active').find('span').css('color', '#333333');
	$('.active').removeClass('active');
	$('#'+menu).addClass('active');
	//$('#'+menu).find('span').css('color', '#75104c'); 
	$('.without-children').find('span').css('color', ''); 
}

function log(msg) {
    var p = document.getElementById('log');
    p.innerHTML = msg + "\n" + p.innerHTML;
}

function loadScripts() {
	$('#labelFilial').text(webLink.getVar("filial"));
	$("#clearSearch").click(function(){
	    $("#cpfSearch").val('');
	    $("#cpfSearch").focusin();
	});
    $("#cpfSearch").keypress(function(event){
    	if (event.keyCode === 13) {
    		$('#btnSearch').click();
    	}else{
			var txt = $("#cpfSearch").val().replace(/[^\d]+/g,"");
			var tipo = $("#tipoSearch").val();
			if( txt.indexOf('603475') == 0){
				$('#cpfSearch').unmask();
				$('#cpfSearch').mask("9999.9999.9999.9999");
				$('#tipoSearch').val('CARTAO');
				cartao = text;
			} else {
				if( tipo == "CPF" ){
					$('#cpfSearch').unmask();
					$('#cpfSearch').mask("999.999.999-99");
					$('#tipoSearch').val('CPF');
					cpf = txt;
				}else{
		    		$('#cpfSearch').unmask();
		    		$('#cpfSearch').mask("9999999999");
					$('#tipoSearch').val('PROTOCOLO');
				}
			}
			//logging(txt);
			$('#cpfSearch').val(txt);
    	}
	});
    $("#cpfSearch").blur(function(){
		var txt = $("#cpfSearch").val().replace(/[^\d]+/g,"");
		var tipo = $("#tipoSearch").val();
		if( txt.length <= 11 ){
			if( tipo == "CPF" ){
				txt = addZeroEsquerda(txt,11);
				$('#cpfSearch').val(txt);
				$('#cpfSearch').unmask();
				$('#cpfSearch').mask("999.999.999-99");
				$('#tipoSearch').val('CPF');
				if(txt == '00000000000'){
					txt = '';
					$('#cpfSearch').val(txt);
				}
			}else{
				txt = addZeroEsquerda(txt,10);
				$('#cpfSearch').val(txt);
	    		$('#cpfSearch').unmask();
	    		$('#cpfSearch').mask("9999999999");
				$('#tipoSearch').val('PROTOCOLO');
			}
		} else { // cartao
			$('#cpfSearch').unmask();
			$('#cpfSearch').mask("9999.9999.9999.9999");
			$('#tipoSearch').val('CARTAO');
		}
		logging(txt);
	});
    $("#tipoSearch").change(function(){
    	var txt = $("#tipoSearch").val();
    	if( txt == "CPF" ){
    		$('#cpfSearch').unmask();
    		$('#cpfSearch').mask("999.999.999-99");
    	} else if( txt == "CARTAO" ){
    		$('#cpfSearch').unmask();
    		$('#cpfSearch').mask("9999.9999.9999.9999");
    	} else{ // PROTOCOLO
    		$('#cpfSearch').unmask();
    		$('#cpfSearch').mask("9999999999");
    	}
    	$('#cpfSearch').val('');
    	$('#cpfSearch').focusin();
    });
    var pCpf = getUrlVars()['cpf'];
	if(pCpf != undefined && pCpf != null){
		$('#cpfSearch').mask("999.999.999-99");
		$('#cpfSearch').val(formatCpf(pCpf));
		cpf = pCpf;
		$('#btnSearch').click();
	}else{
		cpf = '';
	}
    $('#cpfDadosCliente').text('CPF: '+formatCpf(cpf));
    
}

function loadScriptsDashboard() {
	$('#menuDashboard a').unbind("click").on('click',function(e) { 
		e.preventDefault();
		showPainel($('#panelDashboard'));
		search();
	});
	$('#menuCartao').unbind("click").on('click',function(e) { 
		e.preventDefault();
		showPainel($('#panelDetalhes'));
		$('#panelDetalhes').empty();
        loadGif('#panelDetalhes');
		$('#panelDetalhes').load("./components/informacoesFinanceiras/consulta_financeira.html");
	});
	$('#menuProxFatura').unbind("click").on('click',function(e) { 
		e.preventDefault();
		showPainel($('#panelDetalhes'));
		$('#panelDetalhes').empty();
        loadGif('#panelDetalhes');
		$('#panelDetalhes').load("./consulta_fatura/consulta_fatura.html");
	});
	$('.btnPrint').unbind("click").on('click',function(e) { 
		e.preventDefault();
		showPainel($('#panelFatura'));
	});
	$('.btnMail').unbind("click").on('click',function(e) { 
		e.preventDefault();
		showPainel($('#panelFatura'));
	});
	$('#menuRetencao a').unbind("click").on('click',function(e) { 
		e.preventDefault();
		showRetencao();
	});
	$('#card_memofile').unbind("click").on('click','#btnCardHistoricoMemoFile',function(e) { 
        e.preventDefault();
        showMemofile();
    });
}

function loadGif(container){
    $(container).html('<div class="master_load" style="width:100%;padding-top:20%; text-align:center"><div class="loaderCss"></div></div>');
}

function sparkLines(){
	try{
		 $('#currentSale').sparkline('html', {
		   type: 'bar',
		   barColor: '#fff',
		   barWidth: 8,
		   height: 30,
		 });
		 $('#currentBalance').sparkline('html', {
		   type: 'bar',
		   barColor: '#fff',
		   barWidth: 8,
		   height: 30,
		 });
	}catch(e){
		  
	}
}

function setup(){
    var p = waitingDialog.show('Carregando...', {dialogSize : 'sm', progressType : 'info'});
    loadPage();
}

function loadRestricoesStatusCredito(){
	visualizacaoFuncionalidadesPorStatusCredito = {};
	visualizacaoFuncionalidadesPorStatusCredito.adicionais = [
        { status:"1",  permitido:"S" },
        { status:"21", permitido:"N" },
        { status:"36", permitido:"N" },
        { status:"92", permitido:"N" },
        { status:"93", permitido:"N" },
    ];
	visualizacaoFuncionalidadesPorStatusCredito.plasticos = [
		{ status:"1",  permitido:"S" },
		{ status:"21", permitido:"N" },
		{ status:"36", permitido:"N" },
		{ status:"92", permitido:"N" },
		{ status:"93", permitido:"N" },
	];
}

function filterByStatus(array, status, permite) {
	var retorno = [];
	for (var i = 0; i < array.length ; i++) {
	    if (array[i].status==status && array[i].permitido==permite) {
	    	retorno.push(array[i]);
	    }
	}	
	
	return retorno;
}

function exibeFuncionalidadeStatusCredito(func, status){
	status = status + '';
	var exibe = false;
	if(func == 'adicionais'){
	    exibe = (filterByStatus(visualizacaoFuncionalidadesPorStatusCredito.adicionais, status, 'S').length > 0)
		|| (filterByStatus(visualizacaoFuncionalidadesPorStatusCredito.adicionais, status, 'N').length == 0);
	}
	if(func == 'plasticos'){
	    exibe = (filterByStatus(visualizacaoFuncionalidadesPorStatusCredito.plasticos, status, 'S').length > 0)
		|| (filterByStatus(visualizacaoFuncionalidadesPorStatusCredito.plasticos, status, 'N').length == 0);
	}
	return exibe;
}

function getStats(cpf) {
	
	var clientStats = null;
	
	if(cpf != undefined && cpf.length > 0){
		
		var stats = {};
		stats.cpf = cpf; 
		
		var parameters = {
				"Stats" : stats
		};
		
		var result = marisaService.callPostService("/dashboard", "getStats", parameters); 
		if (result["codigoRetorno"] == 200) {
			
			clientStats = result["objectsReturn"]["Stats"];
			
			logging("clientStats: ");
			logging(clientStats);
			
		} else if (result["codigoRetorno"] == 204) {
			logging("Warning");
			marisaMessage.showDialog(marisaMessageType_WARNING, "Alerta","Não foram encontrados dados para esse CPF!");
		} else {
			logging("Error");
			marisaMessage.showDialog(marisaMessageType_ERROR, "Erro","Falha ao buscar dados do cliente!");
		}
		
		return clientStats;
			
	}else{
		marisaMessage.showDialog(marisaMessageType_WARNING, "Alerta","Informe corretamente o CPF!");
		return clientStats;
	}
	
}

function executarReanalise() {
	
	var reanalise = null; 
		
	if(cpf != undefined && cpf.length > 0){
		
		waitingDialog.show('Processando...', {dialogSize : 'sm',progressType : 'info'});
		
		setTimeout(function () {
		
			var parameters = {
					"cpfCliente" : cpf,
					"codFilial" : filial
			};
			
			var result = marisaService.callPostService("dashboard/utilities", "executarReanalise", parameters); 
			if (result["codigoRetorno"] == 200) {
				
				var reanalise = result["objectsReturn"]["Reanalise"];
				
				logging("reanalise: ", reanalise);
				
				marisaMessage.showDialog(marisaMessageType_SUCCESS, "Alerta", result["codigoRetorno"] + " - " +  result["descricaoRetorno"] + "!");
				
				$('#cardReanalise').hide();
				
				inativo = "N";
				diasCarenciaReanalise = "0";
				var temp = controllStorageSac.getinfosCliEmAtendimento();
				temp.inativoCliente = "N";
				temp.diasCarenciaReanaliseCliente = "0";
				controllStorageSac.gravaInfosCliEmAtendido(temp);
				
			} else if (result["codigoRetorno"] == 401) {
				logging("Warning: "+result["codigoRetorno"] + " - " +  result["descricaoRetorno"] + "!");
				marisaMessage.showDialog(marisaMessageType_WARNING, "Alerta", result["codigoRetorno"] + " - " +  result["descricaoRetorno"] + "!");
			} else if (result["codigoRetorno"] == 204) {
				logging("Warning: "+result["codigoRetorno"] + " - " +  result["descricaoRetorno"] + "!");
				marisaMessage.showDialog(marisaMessageType_WARNING, "Alerta", result["codigoRetorno"] + " - " +  result["descricaoRetorno"] + "!");
			} else {
				logging("Error");
				marisaMessage.showDialog(marisaMessageType_ERROR, "Erro","Falha ao realizar a reanalise!");
			}
			
			setTimeout(function () {
				logging("waitingDialog.hide()");
				waitingDialog.hide();
			}, 500);
			
			return reanalise;
		
		}, 300);
		
	}else{
		marisaMessage.showDialog(marisaMessageType_WARNING, "Alerta","CPF não encontrado!");
		return reanalise;
	}
	
}

