var gulp        = require('gulp');
var sass        = require('gulp-sass');
var rename      = require('gulp-rename')
var watch       = require('gulp-watch');;
var browserSync = require('browser-sync').create();
// var reload      = browserSync.reload;



/*
 * Variables
 */
// Sass Source
var scssFiles = './dashboard/scss/style.scss';

// CSS destination
var cssDest = './dashboard/css/componentes/css_unico_componentes/';

// Options for development
var sassDevOptions = {
  outputStyle: 'expanded'
}

// Options for production
var sassProdOptions = {
  outputStyle: 'compressed'
}

/*
 * Tasks
 */
gulp.task('serve', function () {
    browserSync.init({
        server: {
            baseDir: "./"
        }
    });
   
});




// Task 'sassdev' - Run with command 'gulp sassdev'
gulp.task('sassdev', function() {
  return gulp.src(scssFiles)
    .pipe(sass(sassDevOptions).on('error', sass.logError))
    .pipe(gulp.dest(cssDest));
});

// Task 'sassprod' - Run with command 'gulp sassprod'
gulp.task('sassprod', function() {
  return gulp.src(scssFiles)
    .pipe(sass(sassProdOptions).on('error', sass.logError))
    .pipe(rename('style.min.css'))
    .pipe(gulp.dest(cssDest));
});

// Task 'watch' - Run with command 'gulp watch'
gulp.task('watch', function() {
  gulp.watch(scssFiles, ['sassdev', 'sassprod']);
});

// Default task - Run with command 'gulp'
gulp.task('default', ['serve','sassdev', 'sassprod', 'watch']);


















