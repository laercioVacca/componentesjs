require("../resources/js/Util.js");
require("../resources/js/frontConfig.js");

/**
 * Features of manipulating services
 */

function require(script) {
	$.ajax({
		url : script,
		dataType : "script",
		async : false, // <-- This is the key
		success : function() {
			// all good...
		},
		error : function() {
			throw new Error("Could not load script " + script);
		}
	});
}
console.log('TESTE INTERNO||',urlService)
function GenericService(pPath){
	
	var path = pPath; // psfsecurity
	
	
	//Call Get Rest Service
	this.callGetService =  function(service, functionality, parameters) {
		var result;
		//Call GetObject Rest Service
		
		// Check if already "/"
		service = service.indexOf("/") == 0 ? service.substr(1, service.length) : service;
		
	    $.ajax({
	        url: urlService+"/" + path + "/rest/" + service + "/" + functionality,
	    	type: "GET",
	    	async: false,
	    	data: parameters,
	    	success: function(data) {    		
	    		result = JSON.parse(data);	    
	    	},
			error: function (msg, url, line) {
				result = {"ERROR": msg, "url": url, "line": line};
	        }
	    });
	    
	    return result;
	    
	};
	
	//Call Get Rest Service
	this.callPostService =  function(service, functionality, parameters) {
		var result;
		
		var requestService = {};
		requestService.cpfUsuario		= webLink.getVar("cpfUsuario");
		requestService.loginUsuario		= webLink.getVar("loginUsuario");
		requestService.ipUsuario		= webLink.getVar("ipUsuario");
		requestService.modulo			= webLink.getVar("modulo");
		requestService.rotina			= webLink.getVar("rotina");
		requestService.service			= service;
		requestService.functionality	= functionality;
		requestService.filial			= webLink.getVar("filial");
		
		requestService.requestObjects	= parameters;
		
		// Check if already "/"
		service = service.indexOf("/") == 0 ? service.substr(1, service.length) : service;
		
		//Call GetObject Rest Service
	    $.ajax({
	        url: urlService+"/" + path + "/rest/" + service + "/" + functionality,
	    	type: "POST",
	    	async: false,
	    	//dataType: "json",
//	    	contentType: "application/json",
	    	data: {requestService: JSON.stringify(requestService)},
	    	     	    	
	    	success: function(data) { 
	    		result = JSON.parse(data);
	    	},
			error: function (msg, url, line) {
				try{
					result = JSON.parse(msg.responseText);
				}catch (e) {
					result = "ERROR: msg = " + msg + ", url = " + url + ", line = " + line;
				}	        
			}
	    });
	    
	    return result;
	    
	};
	
	//Call Get Rest Service
	this.callPutService =  function(service, functionality, parameters) {
		var result;
		
		var requestServiceVO = {};
		requestService.cpfUsuario		= webLink.getVar("cpfUsuario");
		requestService.loginUsuario		= webLink.getVar("loginUsuario");
		requestService.ipUsuario		= webLink.getVar("ipUsuario");
		requestService.modulo			= webLink.getVar("modulo");
		requestService.rotina			= webLink.getVar("rotina");
		requestService.service			= service;
		requestService.functionality	= functionality;
		requestService.filial			= webLink.getVar("filial");
		
		requestServiceVO.requestObjects = parameters;
		
		// Check if already "/"
		service = service.indexOf("/") == 0 ? service.substr(1, service.length) : service;
		
		//Call GetObject Rest Service
	    $.ajax({
	        url: urlService+"/" + path + "/rest/" + service + "/" + functionality,
	    	type: "PUT",
	    	async: false,
	    	data: JSON.stringify(requestServiceVO),
	    	success: function(data) {    		
	    		result = JSON.parse(data);
	    	},
			error: function (msg, url, line) {
				result = "ERROR: msg = " + msg + ", url = " + url + ", line = " + line;
	        }
	    });
	    
	    return result;
	    
	};
}

