require("../resources/js/Util.js");
require("../resources/js/frontConfig.js");

function require(script) {
	$.ajax({
		url : script,
		dataType : "script",
		async : false, 
		success : function() {
			
		},
		error : function() {
			throw new Error("Could not load script " + script);
		}
	});
}

function PssService(){
	
	//var showDialog = false;
	
	//Call Get Rest Service
	this.callGetService =  function(service, functionality, parameters) {
		var result;
		//var vAuthentication = marisaSecurity.getAuthentication();		

		// Check if already "/"
		service = service.indexOf("/") == 0 ? service.substr(1, service.length) : service;

		//Call GetObject Rest Service
	    $.ajax({
	        url: urlService+"/pssservices/rest/" + service + "/" + functionality,
	    	type: "GET",
	    	async: false,
	    	data: parameters,
	    	beforeSend: function(){
//	    		if(showDialog){
//	    			waitingDialog.show('Processando...', {dialogSize : 'sm',progressType : 'info'});
//	    		}
	    	},
	    	success: function(data) {    		
	    		//$("#objectJson_save").val(data);
	    		result = JSON.parse(data);
//	    		if(showDialog){waitingDialog.hide();}
	    	},
			error: function (msg, url, line) {
	        	//alert('error trapped in error: function(msg, url, line)');
				result = {"ERROR": msg, "url": url, "line": line};
	        }
	    });
	    logging(result);
	    
	    return result;
	    
	};
	
	//Call Get Rest Service
	this.callPostService =  function(service, functionality, parameters) {
		var result;
		//var vAuthentication = marisaSecurity.getAuthentication();
		//parameters["login"] = {};
		
		var requestService = {};
		requestService.cpfUsuario		= webLink.getVar("cpfUsuario");
		requestService.loginUsuario		= webLink.getVar("loginUsuario");
		requestService.ipUsuario		= webLink.getVar("ipUsuario");
		requestService.modulo			= webLink.getVar("modulo");
		requestService.rotina			= webLink.getVar("rotina");
		requestService.service			= service;
		requestService.functionality	= functionality;
		requestService.filial			= webLink.getVar("filial");
		requestService.cpfCliente		= webLink.getVar("cpfCliente");
		requestService.protocolo		= webLink.getVar("protocolo");
//		requestService.cpfUsuario		= 11111111111;
//		requestService.loginUsuario		= "CCM";
//		requestService.ipUsuario		= "127.0.0.1";
//		requestService.modulo			= 10;
//		requestService.rotina			= 18;
//		requestService.service			= service;
//		requestService.functionality	= functionality;
//		requestService.filial			= 900;
//		requestService.cpfCliente		= 12270371801;
//		requestService.protocolo		= 122703718;
		
		requestService.requestObjects	= parameters;
		
		logging(JSON.stringify(requestService));
		
		// Check if already "/"
		service = service.indexOf("/") == 0 ? service.substr(1, service.length) : service;
		
		//Call GetObject Rest Service
	    $.ajax({
	        url: urlService+"/pssservices/rest/" + service + "/" + functionality,
	    	type: "POST",
	    	async: false,
	    	//dataType: "json",
//	    	contentType: "application/json",
	    	data: {requestService: JSON.stringify(requestService)},	    	
	    	beforeSend: function(){
//	    		if(this.showDialog){
//	    			var s = waitingDialog.show('Processando...', {dialogSize : 'sm',progressType : 'info'});
//	    			logging(s);
//	    		}
	    	},
	    	success: function(data) {    		
	    		//$("#objectJson_save").val(data);
	    		logging(data);
	    		result = data;
//	    		if(showDialog){waitingDialog.hide();}
	    		//result = JSON.parse(data);
	    	},
			error: function (msg, url, line) {
	        	//alert('error trapped in error: function(msg, url, line)');
				result = "ERROR: msg = " + msg + ", url = " + url + ", line = " + line;
	        }
	    });
	    
	    return result;
	    
	};
	
	//Call Get Rest Service
	this.callPutService =  function(service, functionality, parameters) {
		var result;
		//var vAuthentication = marisaSecurity.getAuthentication();
		//parameters["login"] = {};
		
		var requestServiceVO = {};
		requestService.cpfUsuario		= webLink.getVar("cpfUsuario");
		requestService.loginUsuario		= webLink.getVar("loginUsuario");
		requestService.ipUsuario		= webLink.getVar("ipUsuario");
		requestService.modulo			= webLink.getVar("modulo");
		requestService.rotina			= webLink.getVar("rotina");
		requestService.service			= service;
		requestService.functionality	= functionality;
		requestService.filial			= webLink.getVar("filial");
		requestService.cpfCliente		= webLink.getVar("cpfCliente");
		requestService.protocolo		= webLink.getVar("protocolo");
//		requestService.cpfUsuario		= 11111111111;
//		requestService.loginUsuario		= "CCM";
//		requestService.ipUsuario		= "127.0.0.1";
//		requestService.modulo			= 10;
//		requestService.rotina			= 18;
//		requestService.service			= service;
//		requestService.functionality	= functionality;
//		requestService.filial			= 900;
//		requestService.cpfCliente		= 12270371801;
//		requestService.protocolo		= 122703718;
		
		requestServiceVO.requestObjects = parameters;
		
		logging(requestServiceVO);
		
		// Check if already "/"
		service = service.indexOf("/") == 0 ? service.substr(1, service.length) : service;
		
		//Call GetObject Rest Service
	    $.ajax({
	        url: urlService+"/pssservices/rest/" + service + "/" + functionality,
	    	type: "PUT",
	    	async: false,
	    	data: JSON.stringify(requestServiceVO),
	    	beforeSend: function(){
//	    		if(showDialog){
//	    			waitingDialog.show('Processando...', {dialogSize : 'sm',progressType : 'info'});
//	    		}
	    	},
	    	success: function(data) {    		
	    		//$("#objectJson_save").val(data);
	    		result = JSON.parse(data);
//	    		if(showDialog){waitingDialog.hide();}
	    	},
			error: function (msg, url, line) {
	        	//alert('error trapped in error: function(msg, url, line)');
				result = "ERROR: msg = " + msg + ", url = " + url + ", line = " + line;
	        }
	    });
	    
	    return result;
	    
	};
}

var pssService = new PssService();