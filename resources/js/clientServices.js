require("../resources/js/Util.js");
//require("../../../../resources/js/frontConfig.js");


/**
 * Features of manipulating services
 */

function require(script) {
	$.ajax({
		url : script,
		dataType : "script",
		async : false, // <-- This is the key
		success : function() {
			// all good...
		},
		error : function() {
			throw new Error("Could not load script " + script);
		}
	});
}

function ClientService(){
	
	//Call Get Rest Service
	this.callGetService =  function(service, functionality, parameters) {
		var result;
		//var vAuthentication = marisaSecurity.getAuthentication();	
		
		// Check if already "/"
		service = service.indexOf("/") == 0 ? service.substr(1, service.length) : service;
		
		//Call GetObject Rest Service
	    $.ajax({
	        url: urlService+"/psfclientservices/rest/" + service + "/" + functionality,
	    	type: "GET",
	    	async: false,
	    	data: parameters,
	    	success: function(data) {    		
	    		//$("#objectJson_save").val(data);
	    		result = JSON.parse(data);	    
	    	},
			error: function (msg, url, line) {
	        	//alert('error trapped in error: function(msg, url, line)');
				result = {"ERROR": msg, "url": url, "line": line};
	        }
	    });
	    //logging(result);
	    
	    return result;
	    
	};
	
	//Call Get Rest Service
	this.callPostService =  function(service, functionality, parameters) {
		var result;
		//var vAuthentication = marisaSecurity.getAuthentication();
		//parameters["login"] = {};
		
		var requestService = {};
		requestService.cpfUsuario		= webLink.getVar("cpfUsuario");
		requestService.loginUsuario		= webLink.getVar("loginUsuario");
		requestService.ipUsuario		= webLink.getVar("ipUsuario");
		requestService.modulo			= webLink.getVar("modulo");
		requestService.rotina			= webLink.getVar("rotina");
		requestService.service			= service;
		requestService.functionality	= functionality;
		requestService.filial			= webLink.getVar("filial");
		requestService.cpfCliente		= webLink.getVar("cpfCliente");
		requestService.protocolo		= webLink.getVar("protocolo");
//		requestService.cpfUsuario		= 11111111111;
//		requestService.loginUsuario		= "CCM";
//		requestService.ipUsuario		= "127.0.0.1";
//		requestService.modulo			= 10;
//		requestService.rotina			= 18;
//		requestService.service			= service;
//		requestService.functionality	= functionality;
//		requestService.filial			= 900;
//		requestService.cpfCliente		= 12270371801;
//		requestService.protocolo		= 122703718;
		
		requestService.requestObjects	= parameters;
		
		// Check if already "/"
		service = service.indexOf("/") == 0 ? service.substr(1, service.length) : service;
		
		//Call GetObject Rest Service
	    $.ajax({
	        url: urlService+"/psfclientservices/rest/" + service + "/" + functionality,
	    	type: "POST",
	    	async: false,
	    	//dataType: "json",
//	    	contentType: "application/json",
	    	data: {requestService: JSON.stringify(requestService)},	    	
	    	success: function(data) {    		
	    		//$("#objectJson_save").val(data);
	    		result = JSON.parse(data);
	    	},
			error: function (msg, url, line) {
	        	//alert('error trapped in error: function(msg, url, line)');
				try{
					result = JSON.parse(msg.responseText);
				}catch (e) {
					result = "ERROR: msg = " + msg + ", url = " + url + ", line = " + line;
				}	        }
	    });
	    
	    return result;
	    
	};
	
	//Call Get Rest Service
	this.callGetFileService =  function(service, functionality, parameters) {
		var result;
		
		var requestService = {};
		requestService.cpfUsuario		= webLink.getVar("cpfUsuario");
		requestService.loginUsuario		= webLink.getVar("loginUsuario");
		requestService.ipUsuario		= webLink.getVar("ipUsuario");
		requestService.modulo			= webLink.getVar("modulo");
		requestService.rotina			= webLink.getVar("rotina");
		requestService.service			= service;
		requestService.functionality	= functionality;
		requestService.filial			= webLink.getVar("filial");
		requestService.cpfCliente		= webLink.getVar("cpfCliente");
		requestService.protocolo		= webLink.getVar("protocolo");
		
		requestService.requestObjects	= parameters;
		
		// Check if already "/"
		service = service.indexOf("/") == 0 ? service.substr(1, service.length) : service;
		
		$.ajax({
			url: urlService+"/psfclientservices/rest/" + service + "/" + functionality,
			type: "POST",
			async: false,
			data: {requestService: JSON.stringify(requestService)},	    	
			success: function(data) {    		
				//result = JSON.parse(data);
				var blob=new Blob([data]);
			    var link=document.createElement('a');
			    link.href=window.URL.createObjectURL(blob);
			    link.download="proposta.pdf";
			    link.click();
			    result = "Success";
			},
			error: function (msg, url, line) {
				try{
					result = JSON.parse(msg.responseText);
				}catch (e) {
					result = "ERROR: msg = " + msg + ", url = " + url + ", line = " + line;
				}	        }
		});
		
		return result;
		
	};
	
	//Call Get Rest Service
	this.callPutService =  function(service, functionality, parameters) {
		var result;
		//var vAuthentication = marisaSecurity.getAuthentication();
		//parameters["login"] = {};
		
		var requestServiceVO = {};
		requestService.cpfUsuario		= webLink.getVar("cpfUsuario");
		requestService.loginUsuario		= webLink.getVar("loginUsuario");
		requestService.ipUsuario		= webLink.getVar("ipUsuario");
		requestService.modulo			= webLink.getVar("modulo");
		requestService.rotina			= webLink.getVar("rotina");
		requestService.service			= service;
		requestService.functionality	= functionality;
		requestService.filial			= webLink.getVar("filial");
		requestService.cpfCliente		= webLink.getVar("cpfCliente");
		requestService.protocolo		= webLink.getVar("protocolo");
//		requestService.cpfUsuario		= 11111111111;
//		requestService.loginUsuario		= "CCM";
//		requestService.ipUsuario		= "127.0.0.1";
//		requestService.modulo			= 10;
//		requestService.rotina			= 18;
//		requestService.service			= service;
//		requestService.functionality	= functionality;
//		requestService.filial			= 900;
//		requestService.cpfCliente		= 12270371801;
//		requestService.protocolo		= 122703718;
		
		requestServiceVO.requestObjects = parameters;
		
		//logging(requestServiceVO);
		
		// Check if already "/"
		service = service.indexOf("/") == 0 ? service.substr(1, service.length) : service;
		
		//Call GetObject Rest Service
	    $.ajax({
	        url: urlService+"/psfclientservices/rest/" + service + "/" + functionality,
	    	type: "PUT",
	    	async: false,
	    	data: JSON.stringify(requestServiceVO),
	    	success: function(data) {    		
	    		//$("#objectJson_save").val(data);
	    		result = JSON.parse(data);
	    	},
			error: function (msg, url, line) {
	        	//alert('error trapped in error: function(msg, url, line)');
				result = "ERROR: msg = " + msg + ", url = " + url + ", line = " + line;
	        }
	    });
	    
	    return result;
	    
	};
}

var clientService = new ClientService();
