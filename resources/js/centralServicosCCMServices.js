require("../resources/js/Util.js");
require("../resources/js/frontConfig.js");

function require(script) {
	$.ajax({
		url : script,
		dataType : "script",
		async : false, // <-- This is the key
		success : function() {
			// all good...
		},
		error : function() {
			throw new Error("Could not load script " + script);
		}
	});
}

function CentralServicosCCMServices(){
	
	//var showDialog = false;
	
	//Call Get Rest Service
	this.callGetService =  function(service, functionality, parameters) {
		var result;
		//var vAuthentication = marisaSecurity.getAuthentication();	
		
		// Check if already "/"
		service = service.indexOf("/") == 0 ? service.substr(1, service.length) : service;
		
		//Call GetObject Rest Service
	    $.ajax({
	        url: urlService+"/centralservicosccm/rest/" + service + "/" + functionality,
	    	type: "GET",
	    	async: false,
	    	data: parameters,
	    	beforeSend: function(){
//	    		if(showDialog){
//	    			waitingDialog.show('Processando...', {dialogSize : 'sm',progressType : 'info'});
//	    		}
	    	},
	    	success: function(data) {    		
	    		//$("#objectJson_save").val(data);
	    		result = JSON.parse(data);
//	    		if(showDialog){waitingDialog.hide();}
	    	},
			error: function (msg, url, line) {
	        	//alert('error trapped in error: function(msg, url, line)');
				result = {"ERROR": msg, "url": url, "line": line};
	        }
	    });
	    logging(result);
	    
	    return result;
	    
	};
	
	//Call Get Rest Service
	this.callPostService =  function(service, functionality, parameters, webk) {
		
		if(webk == undefined){
			webk = webLink;
		}
		var result;
		//var vAuthentication = marisaSecurity.getAuthentication();
		//parameters["login"] = {};
		
		var postData = {};
		var login = {};
        login["usuCpf"] = webk.getVar("cpfUsuario");
        login["usuNomeChave"] = webk.getVar("loginUsuario");
        login["modCod"] = webk.getVar("modulo");
        login["atriCod"] = webk.getVar("rotina");
        login["nomeServico"] = service;
        login["nomeMetodo"] = functionality;
        login["filCod"] = webk.getVar("filial");
        
        postData["login"] = JSON.stringify(login);
        
        Object.keys(parameters).forEach(function(key) {

        	postData[key] = parameters[key];
        	logging(key, parameters[key]);

        });
        
        console.log(postData);
        

        // Check if already "/"
		service = service.indexOf("/") == 0 ? service.substr(1, service.length) : service;
		

		//Call GetObject Rest Service
	    $.ajax({
	        url: urlService+"/centralservicosccm/rest/" + service + "/" + functionality,
	    	type: "POST",
	    	async: false,
	    	//dataType: "json",
//	    	contentType: "application/json",
	    	data: postData,	    	
	    	beforeSend: function(){
//	    			logging(s);
	    	},
	    	success: function(data) {    		
	    		//$("#objectJson_save").val(data);
	    		logging(data);
	    		//result = JSON.parse(data);
	    		result = data;
//	    		if(showDialog){waitingDialog.hide();}
	    		//result = JSON.parse(data);
	    	},
			error: function (msg, url, line) {
	        	//alert('error trapped in error: function(msg, url, line)');
				result = "ERROR: msg = " + msg + ", url = " + url + ", line = " + line;
	        }
	    });
	    
	    return result;
	    
	};
	
	//Call Get Rest Service
	this.callPutService =  function(service, functionality, parameters) {
		var result;
		//var vAuthentication = marisaSecurity.getAuthentication();
		//parameters["login"] = {};
		
		var postData = {};
		var login = {};
        login["usuCpf"] = webLink.getVar("cpfUsuario");
        login["usuNomeChave"] = webLink.getVar("loginUsuario");
        login["modCod"] = webLink.getVar("modulo");
        login["atriCod"] = webLink.getVar("rotina");
        login["nomeServico"] = service;
        login["nomeMetodo"] = functionality;
        login["filCod"] = webLink.getVar("filial");
        
        postData["login"] = JSON.stringify(login);
        
        Object.keys(parameters).forEach(function(key) {

        	postData[key] = parameters[key];
        	logging(key, parameters[key]);

        });
        
        logging(postData);
        
        // Check if already "/"
		service = service.indexOf("/") == 0 ? service.substr(1, service.length) : service;
		
        // Check if already "/"
		service = service.indexOf("/") == 0 ? service.substr(1, service.length) : service;
		
		//Call GetObject Rest Service
	    $.ajax({
	        url: urlService+"/centralservicosccm/rest/" + service + "/" + functionality,
	    	type: "PUT",
	    	async: true,
	    	//dataType: "json",
//	    	contentType: "application/json",
	    	data: postData,	    	
	    	beforeSend: function(){
//	    			logging(s);
	    	},
	    	success: function(data) {    		
	    		//$("#objectJson_save").val(data);
	    		logging(data);
	    		result = JSON.parse(data);
	    		//result = data;
//	    		if(showDialog){waitingDialog.hide();}
	    		//result = JSON.parse(data);
	    	},
			error: function (msg, url, line) {
	        	//alert('error trapped in error: function(msg, url, line)');
				result = "ERROR: msg = " + msg + ", url = " + url + ", line = " + line;
	        }
	    });
	    
	    return result;
	    
	};
}

var centralServicosCCMServices = new CentralServicosCCMServices();