//Load deve ser feito pelas páginas devido ao path

function require(script) {
	$.ajax({
		url : script,
		dataType : "script",
		async : false, // <-- This is the key
		success : function(data) {
		},
		error : function() {
			throw new Error("Could not load script " + script);
		}
	});
}

function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    return !(charCode > 31 && (charCode < 48 || charCode > 57));
}	

function isNotNumberKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode;
	return !isNumberKey(evt);
}	

function changeFilial(){
	setTimeout(function(){
		var msg = "Deseja realmente trocar de filial?" +
		  "<br>" +
		  "" ;
		msg = msg + 
		  "<br>" +
		  "<div id=\"groupChangeFilial\" class=\"form-group has-feedback\">" +
		  "	    <label class=\"control-label\" style=\"margin-top: 0px;\" for=\"changeFilial\">Filial</label><br>" +
		  "	    <select type=\"text\" id=\"changeFilial\" class=\"form-control\" placeholder=\"Filial\"> " +
		  "     </select>";
		  
		msg = msg + 
		  "		<div id=\"erroChangeFilial\" class=\"help-block with-errors\" style=\"display: inline;color: red;font-size: 11px;\"></div>" +
		  "</div>" +
		  "<script type=\"text/javascript\"> " +
		  "" +
		  "loadFiliaisToChange();" +
		  "$(\"#changeFilial\").val(webLink.getVar(\"filial\"));" +
		  		"" +
		  "$(\"#changeFilial\").change(function(){ " +
		  "		var txtChangeFilial = $(\"#changeFilial\").val().replace(/[^\\d]+/g,\"\"); " +
		  "		if( txtChangeFilial.length > 0 ){ " +
		  "         $(\"#erroChangeFilial\").text(\"\"); " +
		  "         $(\"#groupChangeFilial\").removeClass(\"has-error\"); " +
		  "			txtChangeFilial = txtChangeFilial + \"\"; " +
		  "			$('#modalQuestion #btnYesOk').removeAttr('disabled'); " +
		  "		} else { " +
		  "         $(\"#groupChangeFilial\").addClass(\"has-error\"); " +
		  "         $(\"#erroChangeFilial\").text(\"Filial inválida!\"); " +
		  "         $('#modalQuestion #btnYesOk').attr( 'disabled', 'disabled' ); " +
	  	  "		} " +
	  	  "		logging(txtChangeFilial); " +
	  	  "	});" +
	  	  "" +
		  "$('#modalQuestion #btnYesOk').attr( 'disabled', 'disabled' ); " +
		  "$('#changeFilial').focus();" +
		  "" +
		  "</script>" ;
		  
		  
		var resp = marisaMessage.questionDialog("Confirmação de troca de filial ",
						msg, "Confirmar", "Cancelar", trocaFilialConfirmada, abortaPerguntaOnly2);
		
	}, 100);
	return false;		
	
}

	
function trocaFilialConfirmada(){
	
	var filialStorage = $("#changeFilial").val();
	if(filialStorage != null && filialStorage != undefined && filialStorage > 0){
		filial = filialStorage + "";
		webLink.setVar("filial", filial);
		webLink.setVar("webfilcod", filial);
		$('#labelFilial').text(webLink.getVar("filial"));
		localStorage.removeItem('login_'+webLink.getVar("loginUsuario"));
		localStorage.setItem('login_'+webLink.getVar("loginUsuario"), filial);
	}else{
		marisaMessage.showDialog(marisaMessageType_WARNING, "Alerta","Não foi possível trocar a filial.");
	}
	
}

function loadFiliaisToChange(){
	
	var combo = $("#changeFilial");// Atribuindo referencias na variavel
	combo.find('option').remove(); // Limpando as opções do combo

	var config = {};
	var parameters = {
	};
	
	var result = marisaService.callPostService("dashboard/utilities", "getComboboxFiliais", parameters);
	if (result["codigoRetorno"] == 200) {
		var objLista = result["objectsReturn"]["ComboboxFiliais"];

		$('<option>').val("").text("Selecione...").appendTo(combo); // Criando a opção selecione...
		$.each(objLista, function(index, objeto) { // Iterando as opções
			$('<option>').val(objeto.cod).text(objeto.descr).appendTo(combo);
		});
		combo.focusout();// Necessário para atualizar o botão de submit
	} else {
		logging("Error");
		marisaMessage.showDialog(marisaMessageType_ERROR, "Erro","Falha ao tentar recuper a lista de Filiais!");
	}
	
}

function abortaPerguntaOnly2(){
	return false;
}
	


function goUrl(url){
    window.setTimeout(function(){
        //window.location.href = "../../../../../" + url;
        //window.location.href = window.location.host + url;
        window.location.pathname = url;
    }, 10);
}

function goHref(url, param, value){
	window.setTimeout(function(){
		if(value != undefined && value != null){
			location.href = url + '?' + param + '=' + value + '&' + 'token='+token;
		}else{
			location.href = url + '?' + 'token='+token;
		}
		//window.location.href = window.location.host + url;
		//window.location.pathname = url;
	}, 10);
}


function getSession(){

	if((document.referrer.indexOf("z_action=MENU") > 0) 
		|| (document.referrer.indexOf("servlet/LoginServlet") > 0)
		|| (document.referrer.indexOf("servlet/ccm0200001") > 0)
		|| (document.referrer.indexOf("neurotech/") > 0
		|| webLink.isDesenv)
		){

	
		$('#formCCM')[0].webfilcod.value = localStorage.getItem('webfilcod');
		$('#formCCM')[0].ipmenu.value = localStorage.getItem('ipmenu');
		$('#formCCM')[0].z_action.value = localStorage.getItem('z_action');
		$('#formCCM')[0].login.value = localStorage.getItem('login');
		$('#formCCM')[0].acao.value = localStorage.getItem('acao');
		$('#formCCM')[0].weblinkageweb.value = localStorage.getItem('weblinkageweb');
		$('#formCCM')[0].sessao.value = localStorage.getItem('sessao');
		$('#formCCM')[0].webapplip.value = localStorage.getItem('webapplip');
		$('#formCCM')[0].ipCobol.value = localStorage.getItem('ipCobol');
		
		webLink.setSession();
		
		logging($('#formCCM')[0].weblinkageweb.value);
		
	}else{
		localStorage.clear();
		document.location = 'http://' + document.location.host;
	}
	
//	var parameters = {};
//	var result = marisaService.callPostService("/retencao/config/utilities", "getSession", parameters); 
//	if (result["codigoRetorno"] == 200) {
//		logging("objectsReturn");
//		logging(result);
//		var objLista = result["objectsReturn"]["Session"];
//		logging("Session");
//		logging(objLista);
//		
//	} else {
//		logging("Error");
//		marisaMessage.showDialog(marisaMessageType_ERROR, "Erro","Falha ao tentar recuper a lista de Produtos!");
//	}	
}

function isIE(){
	return ((navigator.userAgent.indexOf("MSIE") != -1 ) || (!!document.documentMode == true ));
}

function isLogging(){
	return true;
}

function logging(msg, obj){
	if(isLogging() && window['console']){
		
		try {
			if(obj == undefined){
				console.log(JSON.stringify(msg));
			}else{
				console.log(JSON.stringify(msg), obj);
			}
			// use o JSON aqui
		} catch(ex) {
			// trate o erro aqui
			//console.log(msg);
		}
	}
}

function getContentWindow(elemento){
	if(isIE()){
		return document.getElementById(elemento).contentWindow;
	}else{
		return document.getElementById(elemento).window;
	}
}

function addZeroEsquerda(str, length) {
  var resto = length - String(str).length;
  var aux = str;
  for (var i = 0; i < resto; i++) {
      aux = "0" + aux;
  }
  //return '0'.repeat(resto > 0 ? resto : '0') + str;
  return aux;
}

function formatarFone(date){
	date=date.replace(/\D/g,"")                 //Remove tudo o que não é dígito
	date=date.replace(/^(\d\d)(\d)/g,"($1) $2") //Coloca parênteses em volta dos dois primeiros dígitos
	date=date.replace(/(\d{4})(\d)/,"$1-$2")    //Coloca hífen entre o quarto e o quinto dígitos
	return date
}

function formatarCelular(date){
	date=date.replace(/\D/g,"")                 //Remove tudo o que não é dígito
	date=date.replace(/^(\d\d)(\d)/g,"($1) $2") //Coloca parênteses em volta dos dois primeiros dígitos
	date=date.replace(/(\d{5})(\d)/,"$1-$2")    //Coloca hífen entre o quinto e o sexto dígitos
	return date
}


/*Função que padroniza CEP*/
function formatarCep(v){
	v=v.replace(/\D/g,"")                 
    v=v.replace(/(\d{5})(\d)/,"$1-$2") 
    return v
}



function formatDate(data){
	//data = data + " 00:00:00";
	var dtNew = new Date(Date.parse(data));
	//logging("formatDate: "+data);
	//logging("formatDate: "+addZeroEsquerda(dtNew.getDate(),2) + "/" + addZeroEsquerda(dtNew.getMonth() + 1,2) + "/" + dtNew.getFullYear());
	return addZeroEsquerda(dtNew.getDate(),2) + "/" + addZeroEsquerda(dtNew.getMonth() + 1,2) + "/" + dtNew.getFullYear();
}

function getDate(data){
	return new Date(Date.parse(desformatDate(data))+(1000*60*60*3));
}

function formatCep(data){
	data = data + "";
	data = addZeroEsquerda(data,8);
	return data.substr(0, 2) + "." + data.substr(2, 3) + "-" + data.substr(5, 3);
}

function desformatDate(data){ // 01/01/2017 para 2017-01-01
	var data2 = addZeroEsquerda(data.slice(6,10),4) + "-" + addZeroEsquerda(data.slice(3,5),2) + "-" + addZeroEsquerda(data.slice(0,2),2);
	//var dtNew = new Date(Date.parse(data2));
	return data2;
}

function formatDateTime(data){
	var dtNew = new Date(Date.parse(data));
	return addZeroEsquerda(dtNew.getDate(),2) + "/" + addZeroEsquerda(dtNew.getMonth() + 1,2) + "/" + dtNew.getFullYear() + " " + 
	       addZeroEsquerda(dtNew.getHours(),2) + ":" + addZeroEsquerda(dtNew.getMinutes() + 1,2) + ":" + dtNew.getSeconds();
}

function formatCartao(v) {
	v = v + "";
    v = v.replace( /\D/g , ""); //Remove tudo o que não é dígito
    v = v.replace( /(\d{4})(\d)/ , "$1.$2"); //Coloca um ponto entre o terceiro e o quarto dígitos
    v = v.replace( /(\d{4})(\d)/ , "$1.$2"); //Coloca um ponto entre o terceiro e o quarto dígitos
    v = v.replace( /(\d{4})(\d)/ , "$1.$2"); //Coloca um ponto entre o terceiro e o quarto dígitos
    //de novo (para o segundo bloco de números)
    //v = v.replace( /(\d{3})(\d{1,2})$/ , "$1-$2"); //Coloca um hífen entre o terceiro e o quarto dígitos
    return v;
}

function formatCpf(v) {
	v = v + "";
	v = v.replace( /\D/g , ""); //Remove tudo o que não é dígito
	v = addZeroEsquerda(v, 11);
	v = v.replace( /(\d{3})(\d)/ , "$1.$2"); //Coloca um ponto entre o terceiro e o quarto dígitos
	v = v.replace( /(\d{3})(\d)/ , "$1.$2"); //Coloca um ponto entre o terceiro e o quarto dígitos
	//de novo (para o segundo bloco de números)
	v = v.replace( /(\d{3})(\d{1,2})$/ , "$1-$2"); //Coloca um hífen entre o terceiro e o quarto dígitos
	return v;
}

function formatNumberWithPoints(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
}

function formatCnpj(v) {
	v = v + "";
    v = v.replace( /\D/g , ""); //Remove tudo o que não é dígito
    v = v.replace( /^(\d{2})(\d)/ , "$1.$2"); //Coloca ponto entre o segundo e o terceiro dígitos
    v = v.replace( /^(\d{2})\.(\d{3})(\d)/ , "$1.$2.$3"); //Coloca ponto entre o quinto e o sexto dígitos
    v = v.replace( /\.(\d{3})(\d)/ , ".$1/$2"); //Coloca uma barra entre o oitavo e o nono dígitos
    v = v.replace( /(\d{4})(\d)/ , "$1-$2"); //Coloca um hífen depois do bloco de quatro dígitos
    return v;
}

function formatMoney(num, c, d, t) { // formatMoney( valor, 2, ',', '.')
//	var n = this, 
	var n = num, 
    c = isNaN(c = Math.abs(c)) ? 2 : c, 
    d = d == undefined ? "." : d, 
    t = t == undefined ? "," : t, 
    s = n < 0 ? "-" : "", 
    i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))), 
    j = (j = i.length) > 3 ? j % 3 : 0;
	return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};

function validarCPF(value) {
	// retira mascara e nao numeros
	value = value + "";
	var cpfValidando = value.replace(/[^\d]+/g,"");
	if (cpfValidando == '')
		return false; // não pode ser nulo

	while(cpfValidando.length < 11) cpfValidando = "0"+ cpfValidando;

	// testa se os 11 digitos são iguais, que não pode.
	var valido = 0;
	for (i = 1; i < 11; i++) {
		if (cpfValidando.charAt(0) != cpfValidando.charAt(i))
			valido = 1;
	}
	if (valido == 0)
		return false;

	// calculo primeira parte
	aux = 0;
	for (i = 0; i < 9; i++)
		aux += parseInt(cpfValidando.charAt(i)) * (10 - i);
	check = 11 - (aux % 11);
	if (check == 10 || check == 11)
		check = 0;
	if (check != parseInt(cpfValidando.charAt(9)))
		return false;

	// calculo segunda parte
	aux = 0;
	for (i = 0; i < 10; i++)
		aux += parseInt(cpfValidando.charAt(i)) * (11 - i);
	check = 11 - (aux % 11);
	if (check == 10 || check == 11)
		check = 0;
	if (check != parseInt(cpfValidando.charAt(10)))
		return false;
	return true;
}

function strip_tags(str) {
    str = str.toString();
    return str.replace(/<\/?[^>]+>/gi, '');
}

function reduzirNome( $texto, $tamanho ){
    // Se o nome for maior que o permitido
    if( ( $texto.length ) > ( $tamanho - 2 ) ){
        $texto = strip_tags( $texto );

        // Pego o primeiro nome
        $palavas    = $texto.split(' ');
        $nome       = $palavas[0];

        // Pego o ultimo nome
        $palavas    = $texto.split(' ');
        $sobrenome  = $palavas[$palavas.length - 1];

        // Vejo qual e a posicao do ultimo nome
        $ult_posicao= $palavas.length - 1;

        // Crio uma variavel para receber os nomes do meio abreviados
        $meio = '';

        // Listo todos os nomes do meios e abrevio eles
        for( $a = 1; $a < $ult_posicao; $a++ ){

            // Enquanto o tamanho do nome nao atingir o limite de caracteres
            // completo com o nomes do meio abreviado
            if( ( $nome + ' ' + $meio + ' ' + $sobrenome ).length <= $tamanho ){
                $meio += ' ' + ( $palavas[$a].substr(0, 1).toUpperCase() );
            }
            
        };

    }else{
       $nome       = $texto;
       $meio       = '';
       $sobrenome  = '';
    }

    return ( $nome + $meio + ' ' + $sobrenome );
}

var language = {
    lengthMenu: "Exibir _MENU_ registros",
    zeroRecords: "Nenhum registro encontrado!",
    info: "Exibindo de _START_ à _END_ de _TOTAL_, página _PAGE_ de _PAGES_",
    infoEmpty: "Nenhum registro encontrado!",
    loadingRecords: "Carregando...",
    processing:     "Processando...",
    search:         "Pesquisar:",
    thousands:      ".",
    decimal:        ",",
    paginate: {
        first:      "Primeira",
        last:       "Última",
        next:       "Próximo",
        previous:   "Anterior"
    },
	infoFiltered: "(filtrado do total de _MAX_ registros)",
    aria: {
        sortAscending:  ": ordenação ascendente",
        sortDescending: ": ordenação descendente"
    },
    select: {
        rows: {
            _: '-&nbsp;&nbsp; %d registros selecionados',
            0: '-&nbsp;&nbsp; Click no quadrado do registro para selecionar',
            1: '-&nbsp;&nbsp; Um registro selecionado'
        }
    }
};
    


var waitingDialog = waitingDialog || (function ($) {
    'use strict';

    var showing = false;
    
	// Creating modal dialog's DOM
	var $dialog = $(
		'<div class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true" style="padding-top:15%; overflow-y:visible;background-color: rgba(0, 0, 0, 0.5);">' +
		'<div class="modal-dialog modal-m">' +
		'<div class="modal-content">' +
			'<div class="modal-header"><h3 style="margin:0;"></h3></div>' +
			'<div class="modal-body">' +
				'<div class="progress progress-striped active" style="margin-bottom:0;"><div class="progress-bar" style="width: 100%"></div></div>' +
			'</div>' +
		'</div></div></div>');

	return {
		/**
		 * Opens our dialog
		 * @param message Custom message
		 * @param options Custom options:
		 * 				  options.dialogSize - bootstrap postfix for dialog size, e.g. "sm", "m";
		 * 				  options.progressType - bootstrap postfix for progress bar type, e.g. "success", "warning".
		 */
		show: function (message, options) {
			// Assigning defaults
			if (typeof options === 'undefined') {
				options = {};
			}
			if (typeof message === 'undefined') {
				message = 'Loading';
			}
			var settings = $.extend({
				dialogSize: 'm',
				progressType: '',
				onHide: null // This callback runs after the dialog was hidden
			}, options);

			// Configuring dialog
			$dialog.find('.modal-dialog').attr('class', 'modal-dialog').addClass('modal-' + settings.dialogSize);
			$dialog.find('.progress-bar').attr('class', 'progress-bar');
			if (settings.progressType) {
				$dialog.find('.progress-bar').addClass('progress-bar-' + settings.progressType);
			}
			$dialog.find('h3').text(message);
			// Adding callbacks
			if (typeof settings.onHide === 'function') {
				$dialog.off('hidden.bs.modal').on('hidden.bs.modal', function (e) {
					settings.onHide.call($dialog);
				});
			}
			// Opening dialog
			$dialog.modal();
			showing = true;
			return true;
		},
		/**
		 * Closes dialog
		 */
		hide: function () {
			$dialog.modal('hide');
			showing = false;
		}
	};

})(jQuery);

function reduzirNome( $texto, $tamanho ){
    // Se o nome for maior que o permitido
    if( ( $texto.length ) > ( $tamanho - 2 ) ){
        $texto = strip_tags( $texto );

        // Pego o primeiro nome
        $palavas    = $texto.split(' ');
        $nome       = $palavas[0];

        // Pego o ultimo nome
        $palavas    = $texto.split(' ');
        $sobrenome  = $palavas[$palavas.length - 1];

        // Vejo qual e a posicao do ultimo nome
        $ult_posicao= $palavas.length - 1;

        // Crio uma variavel para receber os nomes do meio abreviados
        $meio = '';

        // Listo todos os nomes do meios e abrevio eles
        for( $a = 1; $a < $ult_posicao; $a++ ){

            // Enquanto o tamanho do nome nao atingir o limite de caracteres
            // completo com o nomes do meio abreviado
            if( ( $nome + ' ' + $meio + ' ' + $sobrenome ).length <= $tamanho ){
                $meio += ' ' + ( $palavas[$a].substr(0, 1).toUpperCase() );
            }
            
        };

    }else{
       $nome       = $texto;
       $meio       = '';
       $sobrenome  = '';
    }

    return ( $nome + $meio + ' ' + $sobrenome );
}

function strip_tags(str) {
    str = str.toString();
    return str.replace(/<\/?[^>]+>/gi, '');
}

function validaCpf(value) {
	
	value = retirarMascara(value);
	
	if(value.length == 0 || value == ''){
		return true;
	}
	
	cpfValidando = value;
		
	while(cpfValidando.length < 11) cpfValidando = "0"+ cpfValidando;
	var expReg = /^0+$|^1+$|^2+$|^3+$|^4+$|^5+$|^6+$|^7+$|^8+$|^9+$/;
	var a = [];
	var b = new Number;
	var c = 11;
	for (i=0; i<11; i++){
		a[i] = cpfValidando.charAt(i);
		if (i < 9) b += (a[i] * --c);
	}
	if ((x = b % 11) < 2) { a[9] = 0; } else { a[9] = 11-x; }
	b = 0;
	c = 11;
	for (y=0; y<10; y++) b += (a[y] * c--);
	if ((x = b % 11) < 2) { a[10] = 0; } else { a[10] = 11-x; }
	if ((cpfValidando.charAt(9) != a[9]) || (cpfValidando.charAt(10) != a[10]) || cpfValidando.match(expReg)) return false;
	return true;
//, "Informe um CPF válido.")
}; 

function retirarMascara(Valorcampo){
	
	var valor = Valorcampo;

/*
	while (valor.indexOf("_") != -1) { valor = valor.replace("_", "");}
	while (valor.indexOf("-") != -1) { valor = valor.replace("-", "");}
	while (valor.indexOf(".") != -1) { valor = valor.replace(".", "");}
	while (valor.indexOf(",") != -1) { valor = valor.replace(",", "");}
	while (valor.indexOf("(") != -1) { valor = valor.replace("(", "");}
	while (valor.indexOf(")") != -1) { valor = valor.replace(")", "");}
	while (valor.indexOf("/") != -1) { valor = valor.replace("/", "");}
	while (valor.indexOf(" ") != -1) { valor = valor.replace(" ", "");}
	while (valor.indexOf("R") != -1) { valor = valor.replace("R", "");}
	while (valor.indexOf("$") != -1) { valor = valor.replace("$", "");}
*/	
	return valor.replace(/[^\d]+/g,"");
}

function clearLocalStorageStartWith(filtro){
	var arr = []; // Array to hold the keys
	// Iterate over localStorage and insert the keys that meet the condition into arr
	for (var i = 0; i < localStorage.length; i++){
	    logging(localStorage.key(i));
	    logging(localStorage.key(i).substring(0,filtro.length));
	    if (localStorage.key(i).substring(0,filtro.length) == filtro) {
	        arr.push(localStorage.key(i));
	    }
	}
	//logging(arr);
	// Iterate over arr and remove the items by key
	for (var i = 0; i < arr.length; i++) {
	    localStorage.removeItem(arr[i]);
	}
	//logging(arr);
}

function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi,    
    function(m,key,value) {
      vars[key] = value;
    });
    return vars;
}

function removeParam(parameter) {
	var url = document.location.href;
	var urlparts = url.split('?');

	if (urlparts.length >= 2) {
		var urlBase = urlparts.shift();
		var queryString = urlparts.join("?");

		var prefix = encodeURIComponent(parameter) + '=';
		var pars = queryString.split(/[&;]/g);
		for (var i = pars.length; i-- > 0;)
			if (pars[i].lastIndexOf(prefix, 0) !== -1)
				pars.splice(i, 1);
		url = urlBase + '?' + pars.join('&');
		window.history.pushState('', document.title, url); // added this line to push the new url directly to url bar .

	}
	return url;
}


function retornaDataHoraAtual(){
	var data = new Date();
	
	var dia     = data.getDate();           // 1-31
	var dia_sem = data.getDay();            // 0-6 (zero=domingo)
	var mes     = data.getMonth();          // 0-11 (zero=janeiro)
	var ano2    = data.getYear();           // 2 dígitos
	var ano4    = data.getFullYear();       // 4 dígitos
	var hora    = data.getHours();          // 0-23
	var min     = data.getMinutes();        // 0-59
	var seg     = data.getSeconds();        // 0-59
	var mseg    = data.getMilliseconds();   // 0-999
	var tz      = data.getTimezoneOffset(); // em minutos

	var mesAjustado = mes+1;
	var diaTratado  = addZeroEsquerda(dia,2);
	var mesTratado  = addZeroEsquerda(mesAjustado,2);
	var horaTratada = addZeroEsquerda(hora,2);
	var minTratado  = addZeroEsquerda(min,2);
	var segTratado  = addZeroEsquerda(seg,2);


	
	var str_data = diaTratado + '/' +mesTratado + '/' + ano4;
	var str_hora = horaTratada + ':' + minTratado + ':' + segTratado;


	var localdate = str_data + ' às ' + str_hora;

	return localdate;
}