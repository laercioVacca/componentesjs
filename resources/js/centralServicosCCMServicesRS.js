require("../resources/js/Util.js");
require("../resources/js/loadWebLink.js");
require("../resources/js/frontConfig.js");

function require(script) {
	$.ajax({
		url : script,
		dataType : "script",
		async : false, // <-- This is the key
		success : function() {
			// all good...
		},
		error : function() {
			throw new Error("Could not load script " + script);
		}
	});
}

function CentralServicosCCMServicesRS(){
	
	//var showDialog = false;
	
	//Call Get Rest Service
	this.callGetService =  function(service, functionality, parameters) {
		var result;
		//var vAuthentication = marisaSecurity.getAuthentication();
		
		var requestService = {};
		requestService.cpfUsuario		= webLink.getVar("cpfUsuario");
		requestService.loginUsuario		= webLink.getVar("loginUsuario");
		requestService.ipUsuario		= webLink.getVar("ipUsuario");
		requestService.modulo			= webLink.getVar("modulo");
		requestService.rotina			= webLink.getVar("rotina");
		requestService.service			= service;
		requestService.functionality	= functionality;
		requestService.filial			= webLink.getVar("filial");
		requestService.cpfCliente		= webLink.getVar("cpfCliente");
				
		requestService.requestObjects	= parameters;
		
		logging(JSON.stringify(requestService));
		// Check if already "/"
		service = service.indexOf("/") == 0 ? service.substr(1, service.length) : service;
		
		//Call GetObject Rest Service
	    $.ajax({
	        url: urlService+"/centralservicosccm/rest/" + service + "/" + functionality,
	    	type: "GET",
	    	async: false,
	    	data: {requestService: JSON.stringify(requestService)},
	    	beforeSend: function(){
//	    		if(showDialog){
//	    			waitingDialog.show('Processando...', {dialogSize : 'sm',progressType : 'info'});
//	    		}
	    	},
	    	success: function(data) {    		
	    		//$("#objectJson_save").val(data);
	    		result = data;
//	    		if(showDialog){waitingDialog.hide();}
	    	},
			error: function (msg, url, line) {
	        	//alert('error trapped in error: function(msg, url, line)');
				result = {"ERROR": msg, "url": url, "line": line};
	        }
	    });
	    logging(result);
	    
	    return result;
	    
	};
	
	//Call Get Rest Service
	this.callPostService =  function(service, functionality, parameters, webk) {
		
		if(webk == undefined){
			webk = webLink;
		}
		
		var result;
		//var vAuthentication = marisaSecurity.getAuthentication();
		//parameters["login"] = {};
		
		var requestService = {};
		requestService.cpfUsuario		= webLink.getVar("cpfUsuario");
		requestService.loginUsuario		= webLink.getVar("loginUsuario");
		requestService.ipUsuario		= webLink.getVar("ipUsuario");
		requestService.modulo			= webLink.getVar("modulo");
		requestService.rotina			= webLink.getVar("rotina");
		requestService.service			= service;
		requestService.functionality	= functionality;
		requestService.filial			= webLink.getVar("filial");
		requestService.cpfCliente		= webLink.getVar("cpfCliente");
				
		requestService.requestObjects	= parameters;
		
		logging(JSON.stringify(requestService));
		
		// Check if already "/"
		service = service.indexOf("/") == 0 ? service.substr(1, service.length) : service;
		
		//Call GetObject Rest Service
	    $.ajax({
	        url: urlService+"/centralservicosccm/rest/" + service + "/" + functionality,
	    	type: "POST",
	    	async: false,
	    	//dataType: "json",
	    	//contentType: "application/json",
	    	data: {requestService: JSON.stringify(requestService)},	    	
	    	//data: {requestService: requestService},
	    	success: function(data) {    		
	    		//$("#objectJson_save").val(data);
	    		result = data;
	    	},
			error: function (msg, url, line) {
	        	//alert('error trapped in error: function(msg, url, line)');
				try{
					result = JSON.parse(msg.responseText);
				}catch (e) {
					result = "ERROR: msg = " + msg + ", url = " + url + ", line = " + line;
				}	        }
	    });
	    
	    return result;
	    
	};
	
	//Call Get Rest Service
	this.callPutService =  function(service, functionality, parameters) {
		var result;
		//var vAuthentication = marisaSecurity.getAuthentication();
		//parameters["login"] = {};
		
		var requestService = {};
		requestService.cpfUsuario		= webLink.getVar("cpfUsuario");
		requestService.loginUsuario		= webLink.getVar("loginUsuario");
		requestService.ipUsuario		= webLink.getVar("ipUsuario");
		requestService.modulo			= webLink.getVar("modulo");
		requestService.rotina			= webLink.getVar("rotina");
		requestService.service			= service;
		requestService.functionality	= functionality;
		requestService.filial			= webLink.getVar("filial");
		requestService.cpfCliente		= webLink.getVar("cpfCliente");
				
		requestService.requestObjects	= parameters;
		
		logging(JSON.stringify(requestService));
		
		// Check if already "/"
		service = service.indexOf("/") == 0 ? service.substr(1, service.length) : service;
		
		//Call GetObject Rest Service
	    $.ajax({
	        url: urlService+"/centralservicosccm/rest/" + service + "/" + functionality,
	    	type: "PUT",
	    	async: true,
	    	//dataType: "json",
	    	contentType: "application/json",
	    	data: {requestService: JSON.stringify(requestService)},	    	
	    	beforeSend: function(){
//	    			logging(s);
	    	},
	    	success: function(data) {    		
	    		//$("#objectJson_save").val(data);
	    		logging(data);
	    		result = data;
	    		//result = data;
//	    		if(showDialog){waitingDialog.hide();}
	    		//result = JSON.parse(data);
	    	},
			error: function (msg, url, line) {
	        	//alert('error trapped in error: function(msg, url, line)');
				result = "ERROR: msg = " + msg + ", url = " + url + ", line = " + line;
	        }
	    });
	    
	    return result;
	    
	};
}

var centralServicosCCMServicesRS = new CentralServicosCCMServicesRS();