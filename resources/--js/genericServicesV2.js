require("../../resources/js/Util.js");
require("../../resources/js/frontConfig.js");
console.log('LOAD GENERICSERVICESV2!!!!')
/**
 * Features of manipulating services
 */

function require(script) {
	$.ajax({
		url : script,
		dataType : "script",
		async : false, // <-- This is the key
		success : function() {
			// all good...
		},
		error : function() {
			throw new Error("Could not load script " + script);
		}
	});
}

/*
	Criação de uma nova hash pra cada servico
*/
	function criacaoHashUnica(tipoHash){
		var dateTime = +new Date();
		var timestamp = Math.floor((Math.random() * dateTime) / 1000);
		var hashPro = "";
		var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
		for (var i = 0; i < 6; i++){
			hashPro += possible.charAt(Math.floor(Math.random() * possible.length));
		}
		return tipoHash + timestamp + hashPro;
	}

function GenericServiceV2(pPath){
	
	var path = pPath; // psfsecurity
	
	
	//Call Get Rest Service
	this.callGetService =  function(service, functionality, parameters) {
		var result;
		//Call GetObject Rest Service
		
		// Check if already "/"
		service = service.indexOf("/") == 0 ? service.substr(1, service.length) : service;
		
	    $.ajax({
	        url: urlService+"/" + path + "/rest/" + service + "/" + functionality,
	    	type: "GET",
	    	async: false,
	    	data: parameters,
	    	success: function(data) {    		
	    		result = JSON.parse(data);	    
	    	},
			error: function (msg, url, line) {
				result = {"ERROR": msg, "url": url, "line": line};
	        }
	    });
	    
	    return result;
	    
	};
	
	//Call Get Rest Service
	this.callPostService =  function(service, functionality, idProcesso, parameters) {
		var result;
		
		parameters.cpfUsuario		= webLink.getVar("cpfUsuario");
		parameters.loginUsuario		= webLink.getVar("loginUsuario");
		parameters.ipUsuario		= webLink.getVar("ipUsuario");
		parameters.modulo			= webLink.getVar("modulo");
		parameters.rotina			= webLink.getVar("rotina");

		parameters.idProcesso       = idProcesso;
		parameters.idServico        = criacaoHashUnica(functionality);

		parameters.service			= service;
		parameters.functionality	= functionality;
		parameters.filial			= webLink.getVar("filial");
    
		
		
		// Check if already "/"
		service = service.indexOf("/") == 0 ? service.substr(1, service.length) : service;
		parametersPostNv = JSON.stringify(parameters);
		//Call GetObject Rest Service
	    $.ajax({
	        url: urlService+"/" + path + "/rest/" + service + "/" + functionality,
	    	type: "POST",
	    	async: false,
	    	dataType: "json",
	    	contentType: "application/json",
	    	data: parametersPostNv,
	    	     	    	
	    	success: function(data) { 
	    		try{
	    			result = JSON.parse(data);
	    		}catch (e) {
	    			result = data;
				}
	    	},
			error: function (msg, url, line) {
				try{
					result = JSON.parse(msg.responseText);
				}catch (e) {
					result = "ERROR: msg = " + msg + ", url = " + url + ", line = " + line;
				}	        
			}
	    });
	    
	    return result;
	    
	};
	
	//Call Get Rest Service
	this.callPutService =  function(service, functionality,idProcesso, parameters) {
		var result;
		
		parameters.cpfUsuario		= webLink.getVar("cpfUsuario");
		parameters.loginUsuario		= webLink.getVar("loginUsuario");
		parameters.ipUsuario		= webLink.getVar("ipUsuario");
		parameters.modulo			= webLink.getVar("modulo");
		parameters.rotina			= webLink.getVar("rotina");
		parameters.idProcesso       = idProcesso;
		parameters.idServico        = criacaoHashUnica(functionality);
		parameters.service			= service;
		parameters.functionality	= functionality;
		parameters.filial			= webLink.getVar("filial");
		
		// Check if already "/"
		service = service.indexOf("/") == 0 ? service.substr(1, service.length) : service;
		
		//Call GetObject Rest Service
	    $.ajax({
	        url: urlService+"/" + path + "/rest/" + service + "/" + functionality,
	    	type: "PUT",
	    	async: false,
	    	data: parameters,
	    	success: function(data) {    		
	    		result = JSON.parse(data);
	    	},
			error: function (msg, url, line) {
				result = "ERROR: msg = " + msg + ", url = " + url + ", line = " + line;
	        }
	    });
	    
	    return result;
	    
	};
}

