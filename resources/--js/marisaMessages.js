require("../../resources/js/Util.js");
//require("../../resources/js/loadWebLink.js");

function require(script) {
	$.ajax({
		url : script,
		dataType : "script",
		async : false, // <-- This is the key
		success : function() {
			// all good...
		},
		error : function() {
			throw new Error("Could not load script " + script);
		}
	});
}

function isFunction(x) {
  return Object.prototype.toString.call(x) == '[object Function]';
}
    
function MarisaMessage(){
	var resp = false;
	this.sucessDialog =  function(title, content) {
		
//		$("#modalMessage #modalContent").css("modal-sucess");
		$("#modalMessage #messageTitle").text(title);
		$("#modalMessage #dialog-message").text(content);
		$("#modalMessage").modal("show");
		
	};
	
	this.warningDialog =  function(title, content) {
//		$("#modalMessage #modalContent").css("panel-warning");
		$("#modalMessage #messageTitle").text(title);
		$("#modalMessage #dialog-message").text(content);
		$("#modalMessage").modal("show");
	};
	
	this.questionDialog =  function(title, content, labelBtnYes, labelBtnNo, yesFn, noFn, type) {
//		this.questionDialog =  function(title, content, labelBtnYes, labelBtnNo, yesFn, noFn, type = marisaMessageType_INFO) {

		if(type == undefined){
			type = marisaMessageType_INFO;
		}
		logging("TYPE: "+type);
		switch(type) {
	    	case marisaMessageType_INFO:
				$('#modalQuestion #messageHeader').css("background-color","rgba(33, 129, 212, 0.45)");
				$("#modalQuestion #dialog-icon").removeAttr('class');
				$('#modalQuestion #dialog-icon').addClass('fa fa-question-circle col-lg-1');
				$('#modalQuestion #dialog-icon').css("color","rgba(33, 129, 212, 0.95)");
				break;
		    case marisaMessageType_SUCCESS:
	    		$('#modalQuestion #messageHeader').css("background-color","rgba(93, 181, 93, 0.39)");
	    		$("#modalQuestion #dialog-icon").removeAttr('class');
	    		$('#modalQuestion #dialog-icon').addClass('fa fa-check-circle col-lg-1');
	    		$('#modalQuestion #dialog-icon').css("color","rgba(93, 181, 93, 0.89)");
		        break;
		    case marisaMessageType_WARNING:
	    		$('#modalQuestion #messageHeader').css("background-color","rgba(240, 173, 78, 0.5)");
	    		$("#modalQuestion #dialog-icon").removeAttr('class');
	    		$('#modalQuestion #dialog-icon').addClass('fa fa-exclamation-circle col-lg-1');
	    		$('#modalQuestion #dialog-icon').css("color","rgb(240, 173, 78)");
		        break;
		    case marisaMessageType_ERROR:
	    		$('#modalQuestion #messageHeader').css("background-color","rgba(217, 83, 79, 0.5)");
	    		$("#modalQuestion #dialog-icon").removeAttr('class');
	    		$('#modalQuestion #dialog-icon').addClass('fa fa-times-circle col-lg-1');
	    		$('#modalQuestion #dialog-icon').css("color","rgb(217, 83, 79)");
		        break;
		    case marisaMessageType_DEFAULT:
	    		$('#modalQuestion #messageHeader').css("background-color","rgba(212, 212, 212, 0.95)");
	    		$("#modalQuestion #dialog-icon").removeAttr('class');
	    		$('#modalQuestion #dialog-icon').addClass('fa fa-envelope-o col-lg-1');
	    		$('#modalQuestion #dialog-icon').css("color","rgba(112, 112, 112, 0.95)");
		        break;
	    	default:
				$('#modalQuestion #messageHeader').css("background-color","rgba(33, 129, 212, 0.45)");
				$("#modalQuestion #dialog-icon").removeAttr('class');
				$('#modalQuestion #dialog-icon').addClass('fa fa-question-circle col-lg-1');
				$('#modalQuestion #dialog-icon').css("color","rgba(33, 129, 212, 0.95)");
				break;
	    		
		}

		$("#modalQuestion #messageTitle").text(title);
		$("#modalQuestion #dialog-message").html(content);

		$("#modalQuestion #btnYesOk").text(labelBtnYes);
		$("#modalQuestion #btnNoCancel").text(labelBtnNo);
		$("#modalQuestion #btnNoCancel").removeAttr('class');
		$("#modalQuestion #btnNoCancel").addClass('btn btn-default');
		
		$("#modalQuestion").modal("show");
		
		$("#modalQuestion #btnYesOk").off('click').click(function () {
			logging("Call Callback yesFn");
			try{
		        yesFn();	        
    		}catch (e) {
    		}
	    });
		
		$("#modalQuestion #btnNoCancel").off('click').click(function () {
			logging("Call Callback noFn");
			try{
    			noFn();
    		}catch (e) {
    		}
	    });
		
	};

	this.showDialog =  function(type, title, content) {
		$("#modalMessage #btnYesOk").text('Fechar');
		$("#modalMessage #btnNoCancel").text('Cancelar');
		$("#modalMessage #btnNoCancel").removeAttr('class');
		$("#modalMessage #btnNoCancel").addClass('btn btn-default hide');
		
		switch(type) {
		    case marisaMessageType_INFO:
	    		$('#modalMessage #messageHeader').css("background-color","rgba(91, 192, 222, 0.5)");
	    		$("#modalMessage #dialog-icon").removeAttr('class');
	    		$('#modalMessage #dialog-icon').addClass('fa fa-info-circle col-lg-1');
	    		$('#modalMessage #dialog-icon').css("color","rgb(91, 192, 222)");
		        break;
		    case marisaMessageType_SUCCESS:
	    		$('#modalMessage #messageHeader').css("background-color","rgba(93, 181, 93, 0.39)");
	    		$("#modalMessage #dialog-icon").removeAttr('class');
	    		$('#modalMessage #dialog-icon').addClass('fa fa-check-circle col-lg-1');
	    		$('#modalMessage #dialog-icon').css("color","rgba(93, 181, 93, 0.89)");
		        break;
		    case marisaMessageType_WARNING:
	    		$('#modalMessage #messageHeader').css("background-color","rgba(240, 173, 78, 0.5)");
	    		$("#modalMessage #dialog-icon").removeAttr('class');
	    		$('#modalMessage #dialog-icon').addClass('fa fa-exclamation-circle col-lg-1');
	    		$('#modalMessage #dialog-icon').css("color","rgb(240, 173, 78)");
		        break;
		    case marisaMessageType_ERROR:
	    		$('#modalMessage #messageHeader').css("background-color","rgba(217, 83, 79, 0.5)");
	    		$("#modalMessage #dialog-icon").removeAttr('class');
	    		$('#modalMessage #dialog-icon').addClass('fa fa-times-circle col-lg-1');
	    		$('#modalMessage #dialog-icon').css("color","rgb(217, 83, 79)");
		        break;
		    case marisaMessageType_DEFAULT:
	    		$('#modalMessage #messageHeader').css("background-color","rgba(212, 212, 212, 0.95)");
	    		$("#modalMessage #dialog-icon").removeAttr('class');
	    		$('#modalMessage #dialog-icon').addClass('fa fa-envelope-o col-lg-1');
	    		$('#modalMessage #dialog-icon').css("color","rgba(112, 112, 112, 0.95)");
		        break;
		    case marisaMessageType_EMAIL:
	    		$('#modalMessage #messageHeader').css("background-color","rgba(91, 192, 222, 0.5)");
	    		$("#modalMessage #dialog-icon").removeAttr('class');
	    		$('#modalMessage #dialog-icon').addClass('fa fa-info-circle col-lg-1');
	    		$('#modalMessage #dialog-icon').css("color","rgb(91, 192, 222)");
		        break;
	    	default:
	    		$('#modalMessage #messageHeader').css("background-color","rgba(212, 212, 212, 0.45)");
	    		$("#modalMessage #dialog-icon").removeAttr('class');
	    		$('#modalMessage #dialog-icon').addClass('fa fa-envelope-o col-lg-1');
	    		$('#modalMessage #dialog-icon').css("color","rgba(212, 212, 212, 0.95)");
	    		
		}
		

		$("#modalMessage #messageTitle").text(title);
		$("#modalMessage #dialog-message").html(content);
		$("#modalMessage").modal("show");
	};
}

var marisaMessage = new MarisaMessage();
var marisaMessageType_DEFAULT = 0;
var marisaMessageType_INFO = 1;
var marisaMessageType_SUCCESS = 2;
var marisaMessageType_WARNING = 3;
var marisaMessageType_ERROR = 4;
var marisaMessageType_EMAIL = 5;

